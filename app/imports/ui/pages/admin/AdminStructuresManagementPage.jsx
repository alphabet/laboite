import React, { useEffect, useState } from 'react';
import i18n from 'meteor/universe:i18n';
import { _ } from 'meteor/underscore';
import { Roles } from 'meteor/alanning:roles';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import CardActions from '@mui/material/CardActions';
import Card from '@mui/material/Card';
import Fade from '@mui/material/Fade';
import Container from '@mui/material/Container';
import Modal from '@mui/material/Modal';
import { makeStyles } from 'tss-react/mui';
import TextField from '@mui/material/TextField';
import IconButton from '@mui/material/IconButton';
import ClearIcon from '@mui/icons-material/Clear';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import AddBox from '@mui/icons-material/AddBox';
import Typography from '@mui/material/Typography';
import Pagination from '@mui/material/Pagination';
import Button from '@mui/material/Button';
import SearchIcon from '@mui/icons-material/Search';

import CustomDialog from '../../components/system/CustomDialog';
import Spinner from '../../components/system/Spinner';
import { useObjectState } from '../../utils/hooks';

import { useAppContext } from '../../contexts/context';
import AdminStructureMailModal from '../../components/admin/AdminStructureMailModal';
import AdminStructureManagementButtons from '../../components/admin/AdminStructureManagementButtons';

export const useModalStyles = makeStyles()(() => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  actions: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  structureDisplay: {
    justifyContent: 'space-between',
    display: 'flex',
    width: '50vw',
  },
  structurePagination: {
    padding: 20,
    justifyContent: 'center',
    display: 'flex',
    width: '50vw',
  },
  treeBackButton: {
    paddingLeft: '2%',
    paddingTop: '2%',
    alignItems: 'center',
    width: '100%',
  },
}));

const AdminStructureManagementPage = ({ match: { path } }) => {
  const [{ userId, structure: currentUserStructure }] = useAppContext();

  const isAdminStructureMode = path.startsWith('/admin/substructures');

  const [parentIds, setParentIds] = useState([]);

  /** For controlled tree view api */
  const [expandedIds, setExpandedIds] = useState([]);

  /** Main window */
  const [searchText, setSearchText] = useState('');
  const [structures, setStructures] = useState([]);
  const [searchTree, setSearchTree] = useState([]);
  const [searchPage, setSearchPage] = useState(1);
  const [page, setPage] = useState(1);
  const [searchMode, setSearchMode] = useState(false);
  const [treeMode, setTreeMode] = useState(false);

  const [loading, setLoading] = useState(false);
  const [treeDisplay, setTreeDisplay] = useState({});
  const [authorizedStructures, setAuthorizedStructures] = useState([]);

  /** Modal utilities  */
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isMailModalOpen, setIsMailModalOpen] = useState(false);

  const openModal = () => setIsModalOpen(true);
  const closeModal = () => setIsModalOpen(false);
  const openMailModal = () => setIsMailModalOpen(true);
  const closeMailModal = () => setIsMailModalOpen(false);

  const [isEditMode, setIsEditMode] = useState(false);

  const setEditMode = () => setIsEditMode(true);
  const setCreateMode = () => setIsEditMode(false);
  const [createInCurrentMode, setCreateInCurrentMode] = useState(false);

  const initialStructure = { name: '', id: '', _id: '', childrenIds: [], ancestorsIds: [], parentId: null };

  const [selectedStructure, setSelectedStructure] = useObjectState(initialStructure);
  const [currentStructure, setCurrentStructure] = useState(null);

  const { classes: globalClasses } = useModalStyles();

  const initTree = (res) => {
    setTreeMode(true);
    setSearchMode(false);
    setSearchTree(res);
    setSearchPage(Math.ceil(res.length / 20));
    setLoading(false);
    setPage(1);
  };

  const resetSearch = (topLevel = false, ignoreTreeMethod = false) => {
    if (topLevel) {
      setCurrentStructure(null);
      setCreateInCurrentMode(false);
      setSelectedStructure(null);
      setTreeMode(false);
    }
    setSearchMode(false);
    setSearchTree([]);
    setSearchPage(Math.ceil(structures.length / 20));
    setPage(1);
    if (isAdminStructureMode && !ignoreTreeMethod) {
      Meteor.call(
        'structures.getTreeOfStructure',
        { structureId: currentUserStructure._id, parentToChild: true },
        (err, res) => {
          if (res) {
            initTree(res.tree);
            setTreeDisplay({
              _id: currentUserStructure._id,
              name: currentUserStructure.name,
              parentId: currentUserStructure.parentId,
            });
            setLoading(false);
          }
        },
      );
    }
  };

  const resetTextSearch = () => {
    resetSearch(true, false);
    setSearchText('');
  };

  const searchStructure = (structureId) => {
    resetSearch(false, true);
    setLoading(true);
    Meteor.call('structures.searchStructureById', { structureId }, (err, res) => {
      if (res) {
        setSearchMode(true);
        setTreeMode(false);
        setSearchTree(res);
        setSearchPage(Math.ceil(res.length / 20));
        setLoading(false);
        setPage(1);
      }
    });
  };

  const getChildsOfStructure = (struc) => {
    setCurrentStructure(struc);
    setTreeDisplay({ _id: struc._id, name: struc.name, parentId: struc.parentId });

    resetSearch(false, true);
    setLoading(true);
    Meteor.call('structures.getTreeOfStructure', { structureId: struc._id, parentToChild: true }, (err, res) => {
      if (res) {
        initTree(res.tree);
      }
    });
  };

  const canNotNavigateToStructure = (structure) => {
    const res =
      !Roles.userIsInRole(userId, 'admin') &&
      !Roles.userIsInRole(userId, 'adminStructure', structure) &&
      !authorizedStructures.includes(structure);
    return res;
  };

  const getParentsOfStructure = (strucId) => {
    setLoading(true);
    Meteor.call('structures.getTreeOfStructure', { structureId: strucId, parentToChild: false }, (err, res) => {
      if (res) {
        if (res.parent) {
          if (!canNotNavigateToStructure(res.parent._id)) {
            resetSearch(false, true);
            initTree(res.tree);
            setCurrentStructure(res.parent);
            setTreeDisplay({ _id: res.parent._id, name: res.parent.name, parentId: res.parent.parentId });
          }
          setLoading(false);
        } else {
          resetSearch(true, true);
          setLoading(false);
        }
      }
    });
  };

  const refresh = (struc, deleteStatus = false) => {
    if (struc.parentId) {
      if (deleteStatus) {
        const tree = searchTree.filter((str) => str.structure._id !== struc._id);
        setSearchTree(tree);
        return;
      }
      Meteor.call('structures.getTreeOfStructure', { structureId: struc._id, parentToChild: false }, (err, res) => {
        if (res) {
          if (res.parent) {
            initTree(res.tree);
            setTreeDisplay({ _id: res.parent._id, name: res.parent.name, parentId: res.parent.parentId });
          } else {
            resetSearch(true);
            setLoading(false);
          }
        }
      });
    } else if (isAdminStructureMode) {
      Meteor.call(
        'structures.getTreeOfStructure',
        { structureId: currentUserStructure._id, parentToChild: true },
        (err, res) => {
          if (res) {
            if (res.parent) {
              initTree(res.tree);
              setTreeDisplay({ _id: res.parent._id, name: res.parent.name, parentId: res.parent.parentId });
            } else {
              resetSearch();
              setLoading(false);
            }
          }
        },
      );
    } else {
      Meteor.call('structures.getTopLevelStructures', (err, res) => {
        if (res) {
          setStructures(res);
          setSearchText('');
          setTreeDisplay({});
          setSearchPage(Math.ceil(res.length / 20));
          setPage(1);
        }
      });
    }
  };

  const onCreate = ({ name, parentId, updateParentIdsList }) => {
    Meteor.call('structures.createStructure', { name, parentId: parentId || null }, (error, res) => {
      if (error) {
        msg.error(error.reason || error.details[0].message);
      }
      if (res) {
        msg.success(i18n.__('api.methods.operationSuccessMsg'));
        updateParentIdsList({ ids: [parentId] });
        refresh(res);
      }
    });
  };

  const onEdit = ({ structureId, name }) => {
    Meteor.call('structures.updateStructure', { structureId, name }, (error, res) => {
      if (error) {
        msg.error(error.reason || error.details[0].message);
      } else {
        msg.success(i18n.__('api.methods.operationSuccessMsg'));
        refresh(res);
      }
    });
  };

  const onDelete = ({ structure }) => {
    Meteor.call('structures.removeStructure', { structureId: structure._id }, (error) => {
      if (error) {
        msg.error(error.reason || error.details[0].message);
      } else {
        msg.success(i18n.__('api.methods.operationSuccessMsg'));
        refresh(structure, true);
      }
    });
  };

  const updateParentIdsList = ({ ids }) => {
    const uniqueList = [...new Set([...parentIds, ...ids])];
    if (_.isEqual(parentIds.sort(), uniqueList.sort())) return;
    if (!isEditMode) setExpandedIds([...expandedIds, ...ids]);
    setParentIds(uniqueList);
  };

  /**
   * SelectedStructure is a context
   * It is the parentStructure when you are in create mode
   * It is the want-to-update one when you are in edit mode
   */

  const onSubmit = (e) => {
    e.preventDefault();
    const { _id } = selectedStructure;
    const { value: structureName } = e.target.structureName;

    if (isEditMode) {
      onEdit({ structureId: _id, name: structureName.trim() });
    } else if (createInCurrentMode) {
      onCreate({
        updateParentIdsList,
        name: structureName.trim(),
        parentId: currentStructure ? currentStructure._id : null,
      });
    } else {
      onCreate({
        updateParentIdsList,
        name: structureName.trim(),
        parentId: _id || null,
      });
    }

    setSelectedStructure(initialStructure);
    closeModal();
  };

  const onClickAddBtn = (nodes) => {
    openModal();
    setCreateMode();
    setCreateInCurrentMode(false);
    setSelectedStructure(nodes);
  };

  const onClickAddInCurrentBtn = () => {
    openModal();
    setCreateMode();
    setCreateInCurrentMode(true);
    setSelectedStructure(null);
  };

  const onClickEditBtn = (nodes) => {
    openModal();
    setEditMode();
    setSelectedStructure(nodes);
  };

  const [isOpenDeleteConfirm, setIsOpenDeleteConfirm] = useState(false);

  const onClickDeleteBtn = (nodes) => {
    setSelectedStructure(nodes);
    setIsOpenDeleteConfirm(true);
  };

  const onDeleteConfirm = () => {
    onDelete({
      structure: selectedStructure,
    });
    setIsOpenDeleteConfirm(false);
    setSelectedStructure(initialStructure);
  };

  const onClickMailBtn = (nodes) => {
    setSelectedStructure(nodes);
    openMailModal();
  };

  useEffect(() => {
    if (isAdminStructureMode) {
      Meteor.call(
        'structures.getTreeOfStructure',
        { structureId: currentUserStructure._id, parentToChild: true },
        (err, res) => {
          if (res) {
            setCurrentStructure(currentUserStructure);
            initTree(res.tree);
            const tab = [];
            res.tree.forEach((struc) => {
              tab.push(struc.structure._id);
              if (struc.structure.childrenIds && struc.structure.childrenIds.length > 0) {
                struc.structure.childrenIds.forEach((subStruc) => tab.push(subStruc));
              }
            });

            setAuthorizedStructures(tab);
            setTreeDisplay({
              _id: currentUserStructure._id,
              name: currentUserStructure.name,
              parentId: currentUserStructure.parentId,
            });
            setLoading(false);
          }
        },
      );
    } else {
      Meteor.call('structures.getTopLevelStructures', (err, res) => {
        if (res) {
          setStructures(res);
          setSearchPage(Math.ceil(res.length / 20));
          setPage(1);
        }
      });
    }
  }, []);

  const triggerSearch = () => {
    if (searchText.length >= 3) {
      resetSearch();
      setLoading(true);
      Meteor.call(
        'structures.searchStructure',
        { searchText, structureAdminMode: isAdminStructureMode, userStructure: currentUserStructure._id },
        (err, res) => {
          if (res) {
            setSearchMode(true);
            setTreeMode(false);
            setSearchTree(res);
            setSearchPage(Math.ceil(res.length / 20));
            setLoading(false);
            setPage(1);
          }
        },
      );
    } else {
      msg.error('');
    }
  };

  const handlePage = (event, value) => {
    setPage(value);
    window.scrollTo(0, 0);
  };

  return (
    <Fade in timeout={{ enter: 200 }}>
      <Container style={{ overflowX: 'auto' }}>
        <Modal className={globalClasses.modal} open={isModalOpen} onClose={closeModal} closeAfterTransition>
          <Fade in={isModalOpen}>
            <Card>
              <CardHeader
                title={i18n.__(
                  `components.AdminStructureTreeItem.actions.${isEditMode ? 'editStructure' : 'addStructure'}`,
                )}
                action={
                  <IconButton
                    title={i18n.__('pages.AdminStructuresManagementPage.modal.close')}
                    onClick={closeModal}
                    size="large"
                  >
                    <ClearIcon />
                  </IconButton>
                }
              />
              <form onSubmit={onSubmit}>
                <CardContent>
                  <TextField
                    label={i18n.__('pages.AdminStructuresManagementPage.columnName')}
                    defaultValue={isEditMode ? selectedStructure.name : ''}
                    name="structureName"
                    fullWidth
                    onChange={(e) => {
                      setSelectedStructure({ name: e.target.value });
                    }}
                    autoFocus
                  />
                </CardContent>
                <CardActions className={globalClasses.actions}>
                  <Button onClick={closeModal}>
                    <Typography>{i18n.__('pages.AdminStructuresManagementPage.modal.cancel')}</Typography>
                  </Button>

                  <Button type="submit" variant="contained" color="primary">
                    <Typography>{i18n.__('pages.AdminStructuresManagementPage.modal.submit')}</Typography>
                  </Button>
                </CardActions>
              </form>
            </Card>
          </Fade>
        </Modal>
        {isMailModalOpen && (
          <AdminStructureMailModal
            open={isMailModalOpen}
            onClose={closeMailModal}
            setIsModalMail={setIsMailModalOpen}
            choosenStructureMail={selectedStructure}
          />
        )}
        {isOpenDeleteConfirm && (
          <CustomDialog
            nativeProps={{ maxWidth: 'xs' }}
            isOpen={isOpenDeleteConfirm}
            title={selectedStructure.name}
            content={i18n.__('components.AdminStructureTreeItem.actions.deleteStructureConfirm')}
            onCancel={() => setIsOpenDeleteConfirm(false)}
            onValidate={onDeleteConfirm}
          />
        )}
        <Card>
          <Box display="flex" justifyContent="space-between">
            <Box>
              <CardHeader
                title={i18n.__(
                  `pages.AdminStructuresManagementPage${isAdminStructureMode ? '.adminStructureMode' : ''}.title`,
                )}
              />
              <Typography color="textSecondary" style={{ paddingLeft: 16 }}>
                {i18n.__('pages.AdminStructuresManagementPage.helpText')}
              </Typography>
            </Box>
          </Box>
          <CardContent>
            {currentUserStructure && (
              <Box>
                <Typography>
                  {`${i18n.__('components.AdminStructureTreeView.isPartOf')}: `}
                  <span>{currentUserStructure.name}</span>
                </Typography>
              </Box>
            )}
            <Box display="flex" alignItems="center">
              <Box>
                <Typography variant="h6">
                  {i18n.__('components.AdminStructureTreeItem.actions.addStructure')}
                </Typography>
              </Box>
              <Box>
                <IconButton
                  id="create-structure-btn"
                  onClick={() => onClickAddInCurrentBtn()}
                  title={i18n.__('components.AdminStructureTreeItem.actions.addStructure')}
                  size="large"
                >
                  <AddBox />
                </IconButton>
              </Box>
            </Box>
            {loading && <Spinner />}
          </CardContent>
          {/* Search Bar */}
          <Box sx={{ paddingLeft: 2, width: '50vw' }}>
            <TextField
              fullWidth
              variant="outlined"
              label={i18n.__('pages.AdminStructuresManagementPage.searchBar')}
              value={searchText}
              onChange={(e) => setSearchText(e.target.value)}
              onKeyDown={(e) => e.key === 'Enter' && triggerSearch(e)}
              InputProps={{
                type: 'text',
                endAdornment: (
                  <>
                    <IconButton
                      title={i18n.__('components.AdminStructureSearchBar.filterByName')}
                      aria-label={i18n.__('components.AdminStructureSearchBar.filterByName')}
                      onClick={(e) => triggerSearch(e)}
                      position="start"
                    >
                      <SearchIcon />
                    </IconButton>

                    <IconButton
                      title={i18n.__('components.AdminStructureSearchBar.clear')}
                      aria-label={i18n.__('components.AdminStructureSearchBar.clear')}
                      size="small"
                      onClick={(e) => {
                        resetTextSearch(e);
                      }}
                    >
                      <ClearIcon fontSize="small" />
                    </IconButton>
                  </>
                ),
              }}
            />
          </Box>
          {/* Parent structure display */}
          {!searchMode && !treeMode ? (
            <div>
              {structures && searchPage > 1 && (
                <div className={globalClasses.structurePagination}>
                  <Pagination count={searchPage} page={page} onChange={handlePage} />
                </div>
              )}
              {structures
                ? structures.slice(page * 20 - 20, page * 20).map((struc) => (
                    <div key={struc._id} style={{ padding: 10 }}>
                      <div className={globalClasses.structureDisplay}>
                        <Button onClick={() => getChildsOfStructure(struc)}>{struc.name}</Button>
                        <AdminStructureManagementButtons
                          structure={struc}
                          onClickMailBtn={onClickMailBtn}
                          onClickAddBtn={onClickAddBtn}
                          onClickEditBtn={onClickEditBtn}
                          onClickDeleteBtn={onClickDeleteBtn}
                        />
                      </div>
                    </div>
                  ))
                : null}
              {structures && (
                <div className={globalClasses.structurePagination}>
                  <Pagination count={searchPage} page={page} onChange={handlePage} />
                </div>
              )}
            </div>
          ) : null}
          {/* Search result display */}
          {searchMode ? (
            <div>
              {searchTree && searchTree.length > 0 && searchPage > 1 && (
                <div className={globalClasses.structurePagination}>
                  <Pagination count={searchPage} page={page} onChange={handlePage} />
                </div>
              )}
              {searchTree && searchTree.length > 0 ? (
                searchTree.slice(page * 20 - 20, page * 20).map((struc) => (
                  <div key={struc._id} style={{ padding: 10 }}>
                    <div className={globalClasses.structureDisplay}>
                      <Button onClick={() => getChildsOfStructure(struc)}>{struc.name}</Button>
                      <AdminStructureManagementButtons
                        structure={struc}
                        onClickMailBtn={onClickMailBtn}
                        onClickAddBtn={onClickAddBtn}
                        onClickEditBtn={onClickEditBtn}
                        onClickDeleteBtn={onClickDeleteBtn}
                      />
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                      {struc.structurePath && struc.structurePath.length > 0
                        ? struc.structurePath.map((pathStruc) => (
                            <div key={pathStruc.structureId} style={{ paddingLeft: '1vw' }}>
                              <Button
                                onClick={() => searchStructure(pathStruc.structureId)}
                                disabled={canNotNavigateToStructure(pathStruc.structureId)}
                                sx={{ textTransform: 'lowercase' }}
                              >
                                <i>{pathStruc.structureName}</i>
                              </Button>
                            </div>
                          ))
                        : null}
                    </div>
                  </div>
                ))
              ) : (
                <p style={{ padding: 10 }}>{i18n.__(`components.AdminStructureTreeItem.noStructureFound`)}</p>
              )}
              {searchTree && searchTree.length > 0 && (
                <div className={globalClasses.structurePagination}>
                  <Pagination count={searchPage} page={page} onChange={handlePage} />
                </div>
              )}
            </div>
          ) : null}
          {/* Tree view display */}
          {treeMode ? (
            <div>
              <div className={globalClasses.treeBackButton}>
                {i18n.__(`components.AdminStructureTreeItem.currentPosition`)} {treeDisplay.name}
                <br />
                {treeDisplay.parentId && !canNotNavigateToStructure(treeDisplay.parentId) ? (
                  <>
                    {i18n.__(`components.AdminStructureTreeItem.position`)}{' '}
                    <Button
                      sx={{ margin: 1 }}
                      variant="outlined"
                      onClick={() => getParentsOfStructure(treeDisplay._id)}
                    >
                      {i18n.__(`components.AdminStructureTreeItem.toParent`)}
                    </Button>
                  </>
                ) : null}
                {treeDisplay.parentId && !isAdminStructureMode ? (
                  <>{i18n.__(`components.AdminStructureTreeItem.or`)}</>
                ) : null}
                {!isAdminStructureMode ? (
                  <>
                    {i18n.__(`components.AdminStructureTreeItem.backTo`)}{' '}
                    <Button sx={{ marging: 1 }} variant="outlined" onClick={() => resetSearch(true)}>
                      {i18n.__(`components.AdminStructureTreeItem.globalView`)}
                    </Button>
                  </>
                ) : null}
              </div>
              {searchTree && searchTree.length > 0 && searchPage > 1 && (
                <div className={globalClasses.structurePagination}>
                  <Pagination count={searchPage} page={page} onChange={handlePage} />
                </div>
              )}
              {searchTree && searchTree.length > 0 ? (
                searchTree.slice(page * 20 - 20, page * 20).map((struc) => (
                  <div key={struc._id} style={{ padding: 10 }}>
                    <div className={globalClasses.structureDisplay}>
                      <Button onClick={() => getChildsOfStructure(struc.structure)}>{struc.structure.name}</Button>
                      <AdminStructureManagementButtons
                        structure={struc.structure}
                        onClickMailBtn={onClickMailBtn}
                        onClickAddBtn={onClickAddBtn}
                        onClickEditBtn={onClickEditBtn}
                        onClickDeleteBtn={onClickDeleteBtn}
                      />
                    </div>
                    <div style={{ display: 'flex', flexDirection: 'row' }}>
                      {struc.structure.structurePath && struc.structure.structurePath.length > 0
                        ? struc.structure.structurePath.map((pathStruc) => (
                            <div key={pathStruc.structureId} style={{ paddingLeft: '1vw' }}>
                              <Button
                                onClick={() => searchStructure(pathStruc.structureId)}
                                disabled={canNotNavigateToStructure(pathStruc.structureId)}
                                sx={{ textTransform: 'lowercase' }}
                              >
                                <i>{pathStruc.structureName}</i>
                              </Button>
                            </div>
                          ))
                        : null}
                    </div>
                  </div>
                ))
              ) : (
                <p style={{ padding: 10 }}>{i18n.__(`components.AdminStructureTreeItem.noChildenStructure`)}</p>
              )}
              {searchTree && searchTree.length > 0 && (
                <div className={globalClasses.structurePagination}>
                  <Pagination count={searchPage} page={page} onChange={handlePage} />
                </div>
              )}
            </div>
          ) : null}
        </Card>
      </Container>
    </Fade>
  );
};

AdminStructureManagementPage.propTypes = {
  match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired,
};

export default AdminStructureManagementPage;
