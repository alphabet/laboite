import React, { useState } from 'react';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import i18n from 'meteor/universe:i18n';
import { withTracker } from 'meteor/react-meteor-data';
import PersonAddIcon from '@mui/icons-material/PersonAdd';
import CommentIcon from '@mui/icons-material/Comment';
import AdminUserValidationTable from '../../components/admin/AdminUserValidationTable';
import UserMotivationDialog from '../../components/users/UserMotivationDialog';

function AdminUserValidationPage({ usersrequest, loading }) {
  const columnsFields = ['username', 'lastName', 'firstName', 'emails', 'structure', 'createdAt'];
  const [motivation, setMotivation] = useState('');
  const [dialogOpen, setDialogOpen] = useState(false);

  return (
    <>
      <AdminUserValidationTable
        title={`${i18n.__('pages.AdminUserValidationPage.title')} (${usersrequest.length})`}
        users={usersrequest}
        loading={loading}
        columnsFields={columnsFields}
        actions={[
          {
            icon: CommentIcon,
            tooltip: i18n.__('pages.AdminUserValidationPage.showMotivation'),
            onClick: (event, rowData) => {
              setMotivation(rowData.motivation || i18n.__('pages.AdminUserValidationPage.noMotivation'));
              setDialogOpen(true);
            },
          },
          {
            icon: PersonAddIcon,
            tooltip: i18n.__('components.AdminUserValidationTable.actions_tooltip'),
            onClick: (event, rowData) => {
              Meteor.call('users.setActive', { userId: rowData._id }, (err) => {
                if (err) {
                  msg.error(err.reason);
                }
              });
            },
          },
        ]}
        editable={{
          onRowDelete: (oldData) =>
            new Promise((resolve, reject) => {
              Meteor.call('users.removeUser', { userId: oldData._id }, (error, res) => {
                if (error) {
                  msg.error(error.reason);
                  reject(error);
                } else {
                  msg.success(i18n.__('pages.AdminUsersPage.successDeleteUser'));
                  resolve(res);
                }
              });
            }),
        }}
      />
      <UserMotivationDialog open={dialogOpen} motivation={motivation} onClose={() => setDialogOpen(false)} />
    </>
  );
}

AdminUserValidationPage.propTypes = {
  usersrequest: PropTypes.arrayOf(PropTypes.object).isRequired,
  loading: PropTypes.bool.isRequired,
};

export default withTracker(() => {
  const usersrequestHandle = Meteor.subscribe('users.request');
  const structuresHandle = Meteor.subscribe('structures.all');
  const loading = !usersrequestHandle.ready() || !structuresHandle.ready();
  const usersrequest = Meteor.users.find({ isActive: { $ne: true } }).fetch();
  return {
    usersrequest,
    loading,
  };
})(AdminUserValidationPage);
