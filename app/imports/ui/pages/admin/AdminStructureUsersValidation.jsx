import React, { useState } from 'react';
import i18n from 'meteor/universe:i18n';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';
import DoneIcon from '@mui/icons-material/Done';
import CommentIcon from '@mui/icons-material/Comment';
import DeleteIcon from '@mui/icons-material/DeleteOutline';

import AdminUserValidationTable from '../../components/admin/AdminUserValidationTable';
import UserMotivationDialog from '../../components/users/UserMotivationDialog';

const AdminStructureUsersValidation = ({ loading, users }) => {
  const accept = (user) => {
    const data = user.awaitingStructure ? { targetUserId: user._id } : { userId: user._id };
    const methodName = user.awaitingStructure ? 'users.acceptAwaitingStructure' : 'users.setActive';
    Meteor.call(methodName, { ...data }, (err) => {
      if (err) {
        msg.error(err.reason || err.message);
      } else msg.success(i18n.__('api.methods.operationSuccessMsg'));
    });
  };

  const refuse = (user) => {
    const data = user.awaitingStructure ? { targetUserId: user._id } : { userId: user._id };
    // if user is not yet active and has no awaiting structure, remove user from database
    const methodName = user.awaitingStructure ? 'users.refuseAwaitingStructure' : 'users.removeUser';
    Meteor.call(methodName, { ...data }, (err) => {
      if (err) {
        msg.error(err.reason || err.message);
      } else msg.success(i18n.__('api.methods.operationSuccessMsg'));
    });
  };

  const columnsFields = ['username', 'lastName', 'firstName', 'emails', 'awaitingStructure', 'isActive'];
  const [motivation, setMotivation] = useState('');
  const [dialogOpen, setDialogOpen] = useState(false);

  return (
    <>
      <AdminUserValidationTable
        title={i18n.__('pages.AdminStructureUsersValidationPage.title')}
        users={users}
        loading={loading}
        columnsFields={columnsFields}
        actions={[
          {
            icon: CommentIcon,
            tooltip: i18n.__('pages.AdminStructureUsersValidationPage.actions.showMotivation'),
            onClick: (event, rowData) => {
              setMotivation(rowData.motivation || i18n.__('pages.AdminUserValidationPage.noMotivation'));
              setDialogOpen(true);
            },
          },
          {
            icon: DoneIcon,
            tooltip: i18n.__('pages.AdminStructureUsersValidationPage.actions.validate'),
            onClick: (event, rowData) => {
              accept(rowData);
            },
          },
          {
            icon: DeleteIcon,
            tooltip: i18n.__('pages.AdminStructureUsersValidationPage.actions.refuse'),
            onClick: (event, rowData) => {
              refuse(rowData);
            },
          },
        ]}
      />
      <UserMotivationDialog open={dialogOpen} motivation={motivation} onClose={() => setDialogOpen(false)} />
    </>
  );
};

AdminStructureUsersValidation.propTypes = {
  users: PropTypes.arrayOf(PropTypes.any).isRequired,
  loading: PropTypes.bool.isRequired,
};

export default withTracker(() => {
  const subUsers = Meteor.subscribe('users.awaitingForStructure');
  const users = Meteor.users.find({ $or: [{ awaitingStructure: { $ne: null } }, { isActive: { $ne: true } }] }).fetch();
  const loading = !subUsers.ready();
  return { loading, users };
})(AdminStructureUsersValidation);
