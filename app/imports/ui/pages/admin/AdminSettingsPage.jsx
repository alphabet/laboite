import React, { useEffect, useState, useRef } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import i18n from 'meteor/universe:i18n';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Fade from '@mui/material/Fade';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import OutlinedInput from '@mui/material/OutlinedInput';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import Grid from '@mui/material/Grid';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import { TextareaAutosize } from '@mui/base/TextareaAutosize';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import Alert from '@mui/material/Alert';

import PropTypes from 'prop-types';
import { makeStyles } from 'tss-react/mui';
import { TuiViewer } from '../../utils/tui';

import SearchField from '../../components/system/SearchField';
import Spinner from '../../components/system/Spinner';
import AppSettings from '../../../api/appsettings/appsettings';
import LegalComponent from '../../components/admin/LegalComponent';
import TabbedForms from '../../components/system/TabbedForms';

import { useAppContext } from '../../contexts/context';
import {
  switchMaintenanceStatus,
  updateTextMaintenance,
  setUserStructureValidationMandatoryStatus,
} from '../../../api/appsettings/methods';
import InfoEditionComponent from '../../components/admin/InfoEditionComponent';
import AdminStructureValidationMandatory from '../../components/admin/AdminStructureValidationMandatory';
import { dataMap } from '../../../api/contextsettings/defaultValue.contextSettings';

const useStyles = makeStyles()((theme) => ({
  root: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(5),
  },
  container: {
    flexGrow: 1,
    display: 'flex',
  },
  containerForm: {
    padding: 25,
    display: 'block',
    width: '100%',
  },
  title: {
    marginBottom: theme.spacing(2),
    marginRight: theme.spacing(4),
  },
  cellodd: {
    paddingLeft: '2vh',
    paddingRight: '2vh',
  },
  celleven: {
    backgroundColor: theme.palette.primary.light,
    paddingLeft: '2vh',
    paddingRight: '2vh',
  },
  buttonAdmin: {
    color: theme.palette.text.primary,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textTransform: 'none',
  },
  typographyLiStyle: {
    paddingRight: 5,
  },
  spanLiStyle: {
    placeContent: 'center',
  },
  butttonDivStyle: {
    display: 'flex',
    justifyContent: 'space-evenly',
    marginTop: '5%',
  },
  maintenanceDivButton: {
    display: 'flex',
    justifyContent: 'space-evenly',
  },
  list: {
    marginTop: '2%',
    width: '100%',
  },
  listItemStyle: {
    minHeight: '5vh',
  },
}));

const AdminSettingsPage = ({ appsettings = {} }) => {
  const [msgMaintenance, setMsgMaintenance] = useState(appsettings.textMaintenance);
  const { classes } = useStyles();
  const [userStructureValidationMandatory, setUserStructureValidationMandatory] = useState(
    appsettings.userStructureValidationMandatory,
  );
  const [loading, setLoading] = useState(true);
  const [{ isMobile }] = useAppContext();
  const [open, setOpen] = useState(false);
  const [migrationVersion, setMigrationVersion] = useState('');
  // table of settings
  const [settings, setSettings] = useState([]);
  // keep in variable the last modified value if the user want to cancel the input
  const [currentElement, setCurrentElement] = useState();
  // keep in variable all default settings before next save
  const [defaultSettings, setDefaultSettings] = useState([]);
  const [openModalSetting, setOpenModalSetting] = useState(false);
  // saved button is disabled while settings has not been modified
  const [settingsSaved, setSettingsSaved] = useState(false);
  const [search, setSearch] = useState('');
  const [filteredSettings, setFilteredSettings] = useState([]);
  const [restarting, setRestarting] = useState(false);
  // for type password input
  const [showCurrentPassword, setShowCurrentPassword] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  const toastRef = useRef(null);

  const tabs = [
    {
      key: 'introduction',
      title: i18n.__('pages.AdminSettingsPage.introduction'),
      Element: InfoEditionComponent,
      ElementProps: { data: appsettings.introduction || [] },
    },
    {
      key: 'legal',
      title: i18n.__('pages.AdminSettingsPage.legal'),
      Element: LegalComponent,
      ElementProps: { legal: appsettings.legal || [] },
    },
    {
      key: 'personalData',
      title: i18n.__('pages.AdminSettingsPage.personalData'),
      Element: LegalComponent,
      ElementProps: { personalData: appsettings.personalData || [] },
    },
    {
      key: 'accessibility',
      title: i18n.__('pages.AdminSettingsPage.accessibility'),
      Element: LegalComponent,
      ElementProps: { accessibility: appsettings.accessibility || [] },
    },
    {
      key: 'gcu',
      title: i18n.__('pages.AdminSettingsPage.gcu'),
      Element: LegalComponent,
      ElementProps: { gcu: appsettings.gcu || [] },
    },
    {
      key: 'personalSpace',
      title: i18n.__('pages.AdminSettingsPage.personalSpace'),
      Element: LegalComponent,
      ElementProps: { personalSpace: appsettings.personalSpace },
      hideExternal: false,
    },
  ];

  useEffect(() => {
    Meteor.call('migration.getVersion', {}, (error, result) => {
      if (error) console.log(error);
      setMigrationVersion(result);
    });
    Meteor.call('contextSetting.findSettings', (error, result) => {
      if (error) console.log(error);
      setDefaultSettings(result);
      setSettings(result);
      setFilteredSettings(result);
    });
  }, []);

  useEffect(() => {
    setFilteredSettings(settings.filter((el) => el.key.toLowerCase().includes(search.toLowerCase())));
  }, [search, settings]);

  useEffect(() => {
    if (appsettings.textMaintenance !== msgMaintenance) setMsgMaintenance(appsettings.textMaintenance);
    if (appsettings.userStructureValidationMandatory !== userStructureValidationMandatory)
      setUserStructureValidationMandatory(appsettings.userStructureValidationMandatory);
  }, [appsettings]);

  const switchMaintenance = (unlockMigration = false) => {
    setLoading(true);
    setOpen(false);
    switchMaintenanceStatus.call({ unlockMigration }, (error) => {
      setLoading(false);
      if (error) {
        msg.error(i18n.__('pages.AdminSettingsPage.errorMongo'));
      }
    });
  };

  const onSetUserStructureValidationMandatoryStatus = () => {
    setLoading(true);
    setUserStructureValidationMandatoryStatus.call(
      { isValidationMandatory: userStructureValidationMandatory },
      (error) => {
        setLoading(false);
        if (error) {
          msg.error(error.message);
        } else {
          msg.success(i18n.__('api.methods.operationSuccessMsg'));
        }
      },
    );
  };

  const onCheckMaintenance = () => {
    if (appsettings.maintenance && appsettings.textMaintenance === 'api.appsettings.migrationLockedText') {
      setOpen(true);
    } else {
      switchMaintenance();
    }
  };

  const onButtonMaintenanceClick = () => {
    <Spinner full />;
    setLoading(true);
    updateTextMaintenance.call({ text: msgMaintenance }, (error) => {
      setLoading(false);
      if (error) {
        msg.error(error.message);
      } else {
        msg.success(i18n.__('api.methods.operationSuccessMsg'));
      }
    });
  };

  const onUpdateField = (event) => {
    const { value } = event.target;
    setMsgMaintenance(value);
  };

  const handleCloseDialog = () => {
    setOpen(false);
  };

  const handleCloseModal = () => {
    setOpenModalSetting(false);
  };

  const buttonIsActive = !!(
    msgMaintenance === null ||
    msgMaintenance === undefined ||
    msgMaintenance === '' ||
    msgMaintenance === appsettings.textMaintenance
  );

  const onUpdateSettings = (element) => {
    const index = settings.findIndex((x) => x.key === element.key);
    const settingsCopy = [...settings];
    // Replace current element by new one
    settingsCopy.splice(index, 1, element);
    setSettings(settingsCopy);
  };

  // formate array into string for default value in Modal
  const formateArrayToString = (element) => {
    if (element === undefined) {
      return '';
    }
    if (element.type === 'textArea') {
      let newString = '';
      element.value.forEach((el) => {
        newString += `${el}\n`;
      });
      return newString.slice(0, -1);
    }
    return element.value;
  };

  const formateStringToArray = (value) => {
    return value.split('\n');
  };

  const customToggle = () => {
    const value = currentElement.value[0];
    return (
      <>
        <Button
          className={classes.buttonAdmin}
          variant="outlined"
          margin="normal"
          onClick={() => {
            setCurrentElement({ ...currentElement, value: [!value] });
          }}
        >
          {value
            ? i18n.__(`pages.AdminSettingsPage.settingIsEnabled`)
            : i18n.__(`pages.AdminSettingsPage.settingIsDisabled`)}
        </Button>
        <br />
      </>
    );
  };

  const customInputText = () => {
    const element = currentElement;
    return (
      <TextField
        value={element.value}
        name="inputText"
        label={i18n.__(`pages.AdminSettingsPage.inputSetting`)}
        variant="outlined"
        fullWidth
        margin="normal"
        onChange={(e) => {
          setCurrentElement({ ...element, value: e.target.value });
        }}
      />
    );
  };

  const customInputNumber = () => {
    const element = currentElement;
    return (
      <TextField
        value={element.value}
        type="number"
        name="inputNumber"
        label={i18n.__(`pages.AdminSettingsPage.inputSetting`)}
        variant="outlined"
        fullWidth
        margin="normal"
        onChange={(e) => {
          setCurrentElement({ ...element, value: e.target.value });
        }}
      />
    );
  };

  const customInputTextArea = () => {
    const element = currentElement;
    return (
      <TextareaAutosize
        value={formateArrayToString(element)}
        name="textArea"
        label={i18n.__(`pages.AdminSettingsPage.inputSetting`)}
        variant="outlined"
        margin="normal"
        style={{ width: '100%' }}
        onChange={(e) => {
          setCurrentElement({ ...element, value: formateStringToArray(e.target.value) });
        }}
      />
    );
  };

  const customInputPassword = () => {
    const element = currentElement;
    return (
      <OutlinedInput
        value={element.value}
        type={showPassword ? 'text' : 'password'}
        endAdornment={
          <InputAdornment position="end">
            <IconButton
              aria-label="toggle password visibility"
              onClick={() => setShowPassword(!showPassword)}
              edge="end"
            >
              {showPassword ? <VisibilityOff /> : <Visibility />}
            </IconButton>
          </InputAdornment>
        }
        onChange={(e) => {
          setCurrentElement({ ...element, value: formateStringToArray(e.target.value) });
        }}
      />
    );
  };

  const buttonType = {
    boolean: customToggle,
    string: customInputText,
    number: customInputNumber,
    textArea: customInputTextArea,
    password: customInputPassword,
  };

  const setField = (type) => {
    return buttonType[type]();
  };

  // This modal display all informations from one setting and allow to change his value
  const settingModal = (element) => {
    const { key, type } = element;
    const defaultValue = dataMap[key] !== undefined ? dataMap[key].value : '';
    const formatedDefaultValue = formateArrayToString(dataMap[key]);
    const description =
      // If the key has no value or if the key do not exist, it display a default message
      i18n.__(`pages.AdminSettingsPage.modalDescription.${key}`) === 'none' ||
      i18n.__(`pages.AdminSettingsPage.modalDescription.${key}`) === `pages.AdminSettingsPage.modalDescription.${key}`
        ? i18n.__(`pages.AdminSettingsPage.modalDescription.noDescriptionYet`)
        : i18n.__(`pages.AdminSettingsPage.modalDescription.${key}`);

    const formatedValue =
      formateArrayToString(defaultSettings.find((x) => x.key === element.key)).toString() ||
      i18n.__('pages.AdminSettingsPage.valueIsNull');

    return (
      <Dialog
        fullWidth
        open={openModalSetting}
        onClose={handleCloseModal}
        aria-labelledby="alert-dialog-setting-title"
        aria-describedby="alert-dialog-setting-description"
      >
        <IconButton
          aria-label="close"
          onClick={() => setOpenModalSetting(false)}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
        <DialogContent>
          <Typography className={classes.title} variant={isMobile ? 'h6' : 'h4'}>
            {i18n.__('pages.AdminSettingsPage.setting')}
          </Typography>
          <div id="alert-dialog-setting-description">
            <Typography className={classes.typographyLiStyle} variant="h6">
              {i18n.__('pages.AdminSettingsPage.title')}:
            </Typography>
            <span className={classes.spanLiStyle}>{key}</span>
            <Typography className={classes.typographyLiStyle} variant="h6">
              {i18n.__('pages.AdminSettingsPage.description')}:
            </Typography>
            <span className={classes.spanLiStyle}>
              <TuiViewer ref={toastRef} initialValue={description} />
            </span>
            <Typography className={classes.typographyLiStyle} variant="h6">
              {i18n.__('pages.AdminSettingsPage.defaultValue')}:
            </Typography>
            <span className={classes.spanLiStyle}>
              {formatedDefaultValue.toString() || i18n.__('pages.AdminSettingsPage.valueIsNull')}
            </span>
            <Typography className={classes.typographyLiStyle} variant="h6">
              {i18n.__('pages.AdminSettingsPage.actualValue')}:
            </Typography>
            <span className={classes.spanLiStyle}>
              {type === 'password' ? (
                <OutlinedInput
                  value={formatedValue}
                  type={showCurrentPassword ? 'text' : 'password'}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={() => setShowCurrentPassword(!showCurrentPassword)}
                        edge="end"
                      >
                        {showCurrentPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              ) : (
                formatedValue
              )}
            </span>
            <br />
            <br />
            <Divider />
            <br />
            {type === 'textArea' && (
              <>
                <Alert severity="warning">
                  <Typography>{i18n.__('pages.AdminSettingsPage.infoTextArea')}</Typography>
                </Alert>
                <br />
              </>
            )}
            <Typography className={classes.typographyLiStyle} variant="h6">
              {i18n.__('pages.AdminSettingsPage.newValue')}:
            </Typography>
            <span className={classes.spanLiStyle}>{setField(type)}</span>
            <div className={classes.butttonDivStyle}>
              <Button
                variant="contained"
                color="success"
                onClick={() => {
                  onUpdateSettings(currentElement);
                  setOpenModalSetting(false);
                  setShowPassword(false);
                }}
              >
                {i18n.__('components.CustomDialog.validate')}
              </Button>
              <Button
                variant="contained"
                color="error"
                onClick={() => {
                  setOpenModalSetting(false);
                  setShowPassword(false);
                }}
              >
                {i18n.__('components.CustomDialog.cancel')}
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={() => {
                  setCurrentElement({ ...element, value: defaultValue });
                }}
              >
                {i18n.__('components.CustomDialog.default')}
              </Button>
            </div>
          </div>
        </DialogContent>
      </Dialog>
    );
  };

  const updateSearch = (e) => {
    setSearch(e.target.value);
  };
  const onResetSearch = () => {
    setSearch('');
  };

  return loading && !appsettings ? (
    <Spinner full />
  ) : (
    <Fade in>
      <Container>
        <Typography className={classes.title} variant={isMobile ? 'h6' : 'h4'}>
          {i18n.__('components.AdminMenu.menuAdminAppSettings')}
        </Typography>
        <TabbedForms tabs={tabs} globalTitle={i18n.__('pages.AdminSettingsPage.edition')} />
        <Paper className={classes.root}>
          <Grid container spacing={4}>
            <Grid item md={12}>
              <Typography variant={isMobile ? 'h6' : 'h4'}>{i18n.__('pages.AdminSettingsPage.maintenance')}</Typography>
              <Typography>
                {i18n.__('pages.AdminSettingsPage.dbVersion')} : {migrationVersion}
              </Typography>
            </Grid>
            <Grid item md={12} className={classes.container}>
              <FormControlLabel
                control={
                  <Checkbox checked={appsettings.maintenance || false} onChange={onCheckMaintenance} name="external" />
                }
                label={i18n.__(`pages.AdminSettingsPage.toggleMaintenance`)}
              />
            </Grid>
            <Grid item md={12} className={classes.container}>
              <FormControlLabel
                className={classes.containerForm}
                control={
                  <div style={{ marginTop: '-20px' }}>
                    <TextField
                      onChange={onUpdateField}
                      value={msgMaintenance}
                      name="link"
                      label={i18n.__(`pages.AdminSettingsPage.textMaintenance`)}
                      variant="outlined"
                      fullWidth
                      margin="normal"
                    />
                    <Button
                      size="medium"
                      disabled={buttonIsActive}
                      variant="contained"
                      color="primary"
                      onClick={onButtonMaintenanceClick}
                    >
                      {i18n.__(`pages.AdminSettingsPage.buttonTextMaintenance`)}
                    </Button>
                  </div>
                }
              />
            </Grid>
          </Grid>
        </Paper>
        <Paper className={classes.root}>
          <AdminStructureValidationMandatory
            userStructureValidationMandatory={userStructureValidationMandatory}
            isUserStructureValidationMandatoryButtonActive={
              userStructureValidationMandatory !== appsettings.userStructureValidationMandatory
            }
            onCheckStructureAdminValidationMandatory={setUserStructureValidationMandatory}
            onSetUserStructureValidationMandatoryStatus={onSetUserStructureValidationMandatoryStatus}
          />
        </Paper>
        {restarting ? <Spinner full message={i18n.__('pages.AdminSettingsPage.restarting')} /> : null}
        <Paper className={classes.root}>
          <Typography variant={isMobile ? 'h6' : 'h4'}>{i18n.__('pages.AdminSettingsPage.contextsettings')}</Typography>
          <Typography>{i18n.__('pages.AdminSettingsPage.selectParameterModify')}</Typography>
          <List className={classes.list} disablePadding>
            <SearchField
              updateSearch={updateSearch}
              search={search}
              resetSearch={onResetSearch}
              label={i18n.__('pages.AdminSettingsPage.searchSetting')}
            />
            {filteredSettings
              .sort((a, b) => {
                const nameA = a.key.toLowerCase();
                const nameB = b.key.toLowerCase();
                if (nameA < nameB) {
                  return -1;
                }
                if (nameA > nameB) {
                  return 1;
                }
                return 0;
              })
              .map((el, i) => (
                <ListItem
                  key={el.key}
                  className={`${i % 2 ? classes.cellodd : classes.celleven} ${classes.listItemStyle}`}
                >
                  <ListItemText>{el.key}</ListItemText>
                  <ListItemSecondaryAction sx={{ width: '40%' }}>
                    <Button
                      className={classes.buttonAdmin}
                      fullWidth
                      variant="outlined"
                      margin="normal"
                      sx={{ textTransform: 'none' }}
                      onClick={() => {
                        setOpenModalSetting(true);
                        setCurrentElement(el);
                      }}
                    >
                      {el.type === 'textArea'
                        ? el.value.join(' ') || i18n.__('pages.AdminSettingsPage.valueIsNull')
                        : el.type === 'password' && el.value[0]
                        ? '***********'
                        : el.value.toString() || i18n.__('pages.AdminSettingsPage.valueIsNull')}
                    </Button>
                  </ListItemSecondaryAction>
                </ListItem>
              ))}
          </List>
          <div className={classes.butttonDivStyle}>
            <Button
              size="medium"
              disabled={defaultSettings === settings}
              variant="contained"
              color="primary"
              onClick={() => {
                Meteor.call('contextSetting.updateSettings', { settings_updated: settings }, (error) => {
                  if (error) {
                    msg.error(error.message);
                  } else {
                    msg.success(i18n.__('api.methods.operationSuccessMsg'));
                    setSettingsSaved(true);
                  }
                });
              }}
            >
              {i18n.__(`pages.AdminSettingsPage.buttonSettingsSaved`)}
            </Button>
            <Button
              size="medium"
              disabled={!settingsSaved}
              variant="contained"
              color="primary"
              onClick={() => {
                setRestarting(true);
                Meteor.call('contextSetting.stopMeteor', (error) => {
                  setRestarting(false);
                  if (error) {
                    msg.error(error.message);
                  } else {
                    msg.success(i18n.__('pages.AdminSettingsPage.restartedServices'));
                  }
                });
              }}
            >
              {i18n.__(`pages.AdminSettingsPage.buttonSettingsRestart`)}
            </Button>
          </div>
        </Paper>
        {openModalSetting ? settingModal(currentElement) : ''}
        <Dialog
          open={open}
          onClose={handleCloseDialog}
          aria-labelledby="alert-dialog-migration-title"
          aria-describedby="alert-dialog-migration-description"
        >
          <DialogContent>
            <DialogTitle id="alert-dialog-migration-title">
              {i18n.__('pages.AdminSettingsPage.unlockMigration.title')}
            </DialogTitle>
            <DialogContentText id="alert-dialog-migration-description">
              {i18n.__('pages.AdminSettingsPage.unlockMigration.mainText')}
              <br />
              {i18n.__('pages.AdminSettingsPage.unlockMigration.checkLogs')}
              <br />
              <br />
              {i18n.__('pages.AdminSettingsPage.unlockMigration.ask')}
            </DialogContentText>
          </DialogContent>
          <DialogActions className={classes.maintenanceDivButton}>
            <Button variant="contained" onClick={() => switchMaintenance(true)} color="primary" autoFocus>
              {i18n.__('pages.AdminSettingsPage.unlockMigration.confirm')}
            </Button>
            <Button variant="contained" onClick={() => switchMaintenance()} color="secondary">
              {i18n.__('pages.AdminSettingsPage.unlockMigration.cancel')}
            </Button>
          </DialogActions>
        </Dialog>
      </Container>
    </Fade>
  );
};

export default withTracker(() => {
  const appsettings = AppSettings.findOne();
  return {
    appsettings,
  };
})(AdminSettingsPage);

AdminSettingsPage.propTypes = {
  appsettings: PropTypes.objectOf(PropTypes.any),
};
