import React, { useState, useEffect } from 'react';
import { Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';
import i18n from 'meteor/universe:i18n';
import MaterialTable from '@material-table/core';
import DeleteIcon from '@mui/icons-material/Delete';
import SendIcon from '@mui/icons-material/Send';
import Fade from '@mui/material/Fade';
import Container from '@mui/material/Container';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import CardHeader from '@mui/material/CardHeader';
import IconButton from '@mui/material/IconButton';
import CardActions from '@mui/material/CardActions';
import Card from '@mui/material/Card';
import ClearIcon from '@mui/icons-material/Clear';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import Modal from '@mui/material//Modal';
import PropTypes from 'prop-types';
import StructureSelectAutoComplete, { structureSort } from '../../components/structures/StructureSelectAutoComplete';
import setMaterialTableLocalization from '../../components/initMaterialTableLocalization';
import { useModalStyles } from './AdminStructuresManagementPage';
import Invitations from '../../../api/invitations/invitations';
import RegEx from '../../../api/regExp';

const options = {
  pageSize: 10,
  pageSizeOptions: [10, 20, 50, 100],
  paginationType: 'stepped',
  actionsColumnIndex: 6,
  addRowPosition: 'first',
  emptyRowsWhenPaging: false,
};

const InvitationForm = ({ updateCurrentInvitation, invitationDataInfos, isValidMail }) => {
  const [email, setEmail] = useState(invitationDataInfos.email);
  const [language, setLanguage] = useState(invitationDataInfos.language);
  const onUpdateField = (event) => {
    switch (event.target.name) {
      case 'email':
        setEmail(event.target.value);
        updateCurrentInvitation({
          invitationId: invitationDataInfos._id || null,
          structureId: invitationDataInfos.structureId || null,
          email: event.target.value,
          language,
        });
        break;
      case 'language':
        setLanguage(event.target.value);
        updateCurrentInvitation({
          invitationId: invitationDataInfos._id || null,
          structureId: invitationDataInfos.structureId || null,
          email,
          language: event.target.value,
        });
        break;
      default:
        break;
    }
  };
  return (
    <Card>
      <CardContent>
        <TextField
          onChange={onUpdateField}
          fullWidth
          name="email"
          value={email}
          label={i18n.__('api.invitations.labels.email')}
          placeholder={i18n.__('pages.AdminInvitationsPage.modal.emails')}
          required
          error={!isValidMail() && email}
        />
      </CardContent>
      <CardContent>
        <FormControl variant="outlined" fullWidth>
          <InputLabel id="language-label" htmlFor="language">
            {i18n.__('api.invitations.labels.language')}
          </InputLabel>
          <Select
            labelId="language-label"
            id="language"
            name="language"
            value={language}
            label={i18n.__('api.invitations.labels.language')}
            onChange={onUpdateField}
          >
            <MenuItem value="fr">{i18n.__('pages.AdminInvitationsPage.emailLanguages.fr')}</MenuItem>
            <MenuItem value="en">{i18n.__('pages.AdminInvitationsPage.emailLanguages.en')}</MenuItem>
          </Select>
        </FormControl>
      </CardContent>
    </Card>
  );
};

InvitationForm.propTypes = {
  updateCurrentInvitation: PropTypes.func.isRequired,
  invitationDataInfos: PropTypes.object.isRequired,
  isValidMail: PropTypes.func.isRequired,
};

const AdminInvitationsPage = () => {
  const { classes: modalClasses } = useModalStyles();
  const data = useTracker(() => {
    Meteor.subscribe('invitations.admin');
    const query = {};

    const searchResult = Invitations.find(query, {
      sort: { email: 1 },
      limit: 10000,
    }).fetch();
    return searchResult;
  });

  const [structuresLoaded, setStructuresLoaded] = useState(false);
  const [structures, setStructures] = useState([]);
  useEffect(() => {
    Meteor.call('structures.getAdminStructures', (err, res) => {
      if (err) msg.error(err.reason || err.message);
      else {
        setStructures(res.sort(structureSort));
        setStructuresLoaded(true);
      }
    });
  }, []);

  const getStructure = (structureId) => {
    return structures.find((struc) => struc._id === structureId);
  };

  const [currentInvitation, setCurrentInvitation] = useState({
    _id: null,
    email: '',
    structureId: null,
    language: 'fr',
  });

  const columns = [
    {
      field: 'email',
      title: i18n.__('api.invitations.labels.email'),
      customSort: (a, b) => {
        return a.email.toLowerCase() >= b.email.toLowerCase();
      },
    },
    {
      field: 'structureId',
      title: i18n.__('pages.AdminInvitationsPage.structureName'),
      render: (rowData) => {
        return (
          <span>
            {rowData.structureId
              ? getStructure(rowData.structureId)?.name
              : i18n.__('pages.AdminInvitationsPage.noStructure')}
          </span>
        );
      },
      customSort: (a, b) => {
        const aName = getStructure(a.structureId)?.name.toLowerCase();
        const bName = getStructure(b.structureId)?.name.toLowerCase();
        return aName >= bName;
      },
    },
    { field: 'language', title: i18n.__('api.invitations.labels.language') },
    {
      field: 'invitedAt',
      title: i18n.__('api.invitations.labels.invitedAt'),
      type: 'datetime',
    },
  ];

  const [modalOpen, setModalOpen] = useState(false);
  const [isDeleteMailExtensionAction, setIsDeleteMailExtensionAction] = useState(false);
  const openModal = ({ invitationId, email, structureId, language, isDeleteAction = false }) => {
    setIsDeleteMailExtensionAction(isDeleteAction);
    if (invitationId !== undefined) {
      setCurrentInvitation({
        _id: invitationId,
        email,
        structureId,
        language,
      });
    }

    setModalOpen(true);
  };

  const resetAsamState = () =>
    setCurrentInvitation({
      _id: null,
      email: '',
      structureId: null,
      language: 'fr',
    });
  const updateCurrentAsam = ({ invitationId, structureId, email, language }) => {
    setCurrentInvitation({
      _id: invitationId || null,
      email,
      structureId: structureId || null,
      language,
    });
  };
  const closeModal = () => {
    setModalOpen(false);
    setIsDeleteMailExtensionAction(false);
    resetAsamState();
  };

  const [structureSearchText, setStructureSearchText] = useState('');
  const [isSubmitting, setIsSubmitting] = useState(false);

  const mailIsValid = () => {
    const regex = new RegExp(RegEx.Email, 'g');
    return currentInvitation.email.match(regex);
  };

  const isValidData = () => {
    if (isDeleteMailExtensionAction) return false;
    if (!currentInvitation.email || !currentInvitation.structureId || !currentInvitation.language) return true;
    return false;
  };
  const onSubmit = (e) => {
    e.preventDefault();
    if (!isDeleteMailExtensionAction) {
      setIsSubmitting(true);
      Meteor.call(
        'invitations.createInvitation',
        {
          email: currentInvitation.email,
          language: currentInvitation.language,
          structureId: currentInvitation.structureId,
        },
        (err) => {
          setIsSubmitting(false);
          if (err) {
            if (err.error === 'validation-error') {
              msg.error(err.details[0].message);
            } else {
              msg.error(err.reason || err.message);
            }
          } else {
            closeModal();
            msg.success(i18n.__('pages.AdminInvitationsPage.invitationSent'));
          }
        },
      );
    } else {
      Meteor.call('invitations.deleteInvitation', { invitationId: currentInvitation._id }, (err) => {
        if (err) msg.error(err.reason || err.message);
        else msg.success(i18n.__('api.methods.operationSuccessMsg'));
        closeModal();
      });
    }
  };

  return (
    <Fade in>
      <div>
        <Modal
          open={modalOpen}
          onClose={closeModal}
          className={modalClasses.modal}
          closeAfterTransition
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={modalOpen}>
            <Card>
              <CardHeader
                title={
                  !isDeleteMailExtensionAction
                    ? `${i18n.__('pages.AdminInvitationsPage.modal.addInvitation')}`
                    : `${i18n.__('pages.AdminInvitationsPage.modal.deleteInvitation')} "${currentInvitation.email}"`
                }
                action={
                  <IconButton title={i18n.__('pages.AdminInvitationsPage.modal.close')} onClick={closeModal}>
                    <ClearIcon />
                  </IconButton>
                }
              />
              <form onSubmit={onSubmit}>
                <CardContent style={{ display: !isDeleteMailExtensionAction ? 'block' : 'none' }}>
                  <StructureSelectAutoComplete
                    style={{ width: '100%' }}
                    flatData={structures}
                    loading={!structuresLoaded}
                    noOptionsText={i18n.__('pages.AdminInvitationsPage.modal.noOptions')}
                    onChange={(event, newValue) => {
                      setCurrentInvitation((asam) => ({ ...asam, structureId: newValue?._id }));
                    }}
                    searchText={structureSearchText}
                    onInputChange={(event, newInputValue) => {
                      setStructureSearchText(newInputValue);
                    }}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        onChange={({ target: { value } }) => setStructureSearchText(value)}
                        variant="outlined"
                        label={
                          currentInvitation.structureId
                            ? getStructure(currentInvitation.structureId)?.name
                            : i18n.__('pages.AdminInvitationsPage.modal.noOptions')
                        }
                        placeholder={i18n.__('pages.AdminInvitationsPage.modal.noOptions')}
                      />
                    )}
                  />
                </CardContent>
                <CardContent style={{ display: !isDeleteMailExtensionAction ? 'block' : 'none' }}>
                  <InvitationForm
                    updateCurrentInvitation={updateCurrentAsam}
                    invitationDataInfos={currentInvitation}
                    isValidMail={mailIsValid}
                  />
                </CardContent>
                <DialogContent style={{ display: !isDeleteMailExtensionAction ? 'none' : 'block' }}>
                  <DialogContentText id="alert-dialog-description">
                    {`${i18n.__('pages.AdminInvitationsPage.modal.confirmMsg')} "${currentInvitation.email}" ?`}
                  </DialogContentText>
                </DialogContent>
                <CardActions className={modalClasses.actions}>
                  <Button onClick={closeModal}>
                    <Typography>{i18n.__('pages.AdminInvitationsPage.modal.cancel')}</Typography>
                  </Button>

                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    disabled={!mailIsValid() || isValidData() || isSubmitting}
                  >
                    <Typography>{i18n.__('pages.AdminInvitationsPage.modal.submit')}</Typography>
                  </Button>
                </CardActions>
              </form>
            </Card>
          </Fade>
        </Modal>
        <Container>
          <MaterialTable
            title={i18n.__('pages.AdminInvitationsPage.title')}
            columns={columns}
            data={data.map((row) => ({ ...row, id: row._id }))}
            options={options}
            localization={setMaterialTableLocalization('pages.AdminInvitationsPage')}
            actions={[
              {
                icon: () => (
                  <Button color="primary" variant="contained" style={{ textTransform: 'none' }} size="small">
                    {i18n.__('pages.AdminInvitationsPage.actions.invitationButton')}
                  </Button>
                ),
                tooltip: i18n.__('pages.AdminInvitationsPage.actions.addInvitation'),
                isFreeAction: true,
                onClick: (event, rowData) => {
                  openModal({
                    invitationId: rowData._id,
                    email: rowData.email,
                    language: rowData.language,
                    structureId: rowData.structureId || null,
                  });
                },
              },
              {
                icon: SendIcon,
                tooltip: i18n.__('pages.AdminInvitationsPage.actions.resendInvitation'),
                onClick: (event, rowData) => {
                  Meteor.call('invitations.sendEmail', { invitationId: rowData._id }, (err) => {
                    if (err) msg.error(err.reason || err.message);
                    else msg.success(i18n.__('pages.AdminInvitationsPage.invitationSent'));
                  });
                },
              },
              {
                icon: DeleteIcon,
                tooltip: i18n.__('pages.AdminInvitationsPage.actions.deleteInvitation'),
                onClick: (event, rowData) => {
                  openModal({
                    invitationId: rowData._id,
                    email: rowData.email || '',
                    language: rowData.language || 'fr',
                    structureId: rowData.structureId || null,
                    isDeleteAction: true,
                  });
                },
              },
            ]}
          />
        </Container>
      </div>
    </Fade>
  );
};

export default AdminInvitationsPage;
