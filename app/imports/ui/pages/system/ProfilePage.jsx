import React, { useState, useEffect } from 'react';
import { Meteor } from 'meteor/meteor';
import i18n from 'meteor/universe:i18n';
import { makeStyles } from 'tss-react/mui';
// import { useTracker } from 'meteor/react-meteor-data';
import { useHistory } from 'react-router-dom';

import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import OutlinedInput from '@mui/material/OutlinedInput';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import InputLabel from '@mui/material/InputLabel';
import Divider from '@mui/material/Divider';
import Fade from '@mui/material/Fade';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';
import Grid from '@mui/material/Grid';
import Tooltip from '@mui/material/Tooltip';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import MailIcon from '@mui/icons-material/Mail';
import Input from '@mui/material/Input';
import { MuiTelInput } from 'mui-tel-input';
import Alert from '@mui/material/Alert';

import Spinner from '../../components/system/Spinner';
import { useAppContext } from '../../contexts/context';
import LanguageSwitcher from '../../components/system/LanguageSwitcher';
import debounce from '../../utils/debounce';
import { useObjectState } from '../../utils/hooks';
import { downloadBackupPublications, uploadBackupPublications } from '../../../api/articles/methods';
import AvatarPicker from '../../components/users/AvatarPicker';
import { getStructure, useStructure, useAwaitingStructure } from '../../../api/structures/hooks';
import { formatDate, formatDateWithoutTime, testMeteorSettingsUrl, getLocaleDate } from '../../utils/utilsFuncs';
import { nextInstance } from '../../../api/utils';
import Finder from '../../components/admin/Finder';

const useStyles = makeStyles()((theme) => ({
  root: {
    padding: theme.spacing(3),
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(5),
  },
  form: {
    marginTop: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
  },
  buttonGroup: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginTop: '10px',
  },
  keycloakMessage: {
    padding: theme.spacing(1),
  },
  inputFile: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    opacity: 0,
  },
  fileWrap: {
    position: 'relative',
  },
  buttonWrapper: {
    display: 'flex',
    justifyContent: 'center',
  },
  maxWidth: {
    maxWidth: '88vw',
  },
  labelLanguage: {
    fontSize: 16,
    margin: 10,
    marginBottom: 20,
  },
  keycloakLink: {
    textDecoration: 'underline',
    color: theme.palette.primary.main,
    '&:hover, &:focus': {
      color: theme.palette.secondary.main,
      outline: 'none',
    },
  },
  buttonTextColor: {
    color: 'white',
  },
  buttons: {
    textAlign: 'center',
  },
  buttonResetDelegate: {
    width: 'fit-content',
    backgroundColor: 'red',
    '&:hover': {
      color: 'red',
      backgroundColor: theme.palette.tertiary.main,
    },
  },
  buttonCancel: {
    width: 'fit-content',
    marginTop: '1rem',
    backgroundColor: 'red',
    '&:hover': {
      color: 'red',
      backgroundColor: theme.palette.tertiary.main,
    },
  },
  flexColumn: {
    display: 'flex',
    flexDirection: 'column',
  },
  paperContainer: {
    paddingLeft: '2rem',
    paddingRight: '2rem',
    paddingTop: '1rem',
    paddingBottom: '1rem',
    marginBottom: '2rem',
  },
  flexGapContainer: {
    display: 'flex',
    gap: theme.spacing(1),
  },
}));

const accountTypeLabels = {
  personnal: i18n.__('pages.ProfilePage.accountType.personnal'),
  functional: i18n.__('pages.ProfilePage.accountType.functional'),
};

const defaultState = {
  firstName: '',
  lastName: '',
  displayNameComplement: '',
  email: '',
  username: '',
  logoutType: '',
  avatar: '',
  advancedPersonalPage: false,
  betaServices: false,
  nclocator: '',
  motivation: '',
  createdAt: '',
  updatedAt: '',
  phoneNumber: '',
  location: '',
  jobTitle: '',
  accountType: '',
  profileStatus: 1,
  departure: '',
  delegate: '',
};

const logoutTypeLabels = {
  ask: 'api.users.logoutTypes.ask',
  local: 'api.users.logoutTypes.local',
  global: 'api.users.logoutTypes.global',
};

const ProfilePage = () => {
  const history = useHistory();
  const [userData, setUserData] = useState(defaultState);
  const [userFromFinder, setUserFromFinder] = useState();
  const [submitOk, setSubmitOk] = useState(false);
  const [errors, setErrors] = useObjectState(defaultState);
  const [submitted, setSubmitted] = useState(false);
  const [structChecked, setStructChecked] = useState(false);
  const { classes } = useStyles();
  const { disabledFeatures = {}, jobInformation = {} } = Meteor.settings.public;
  const enableBlog = !disabledFeatures.blog;
  const isDefaultSpaceEnabled = !disabledFeatures?.menuAdminDefaultSpaces;
  const isApiTokenEnabled = !disabledFeatures?.getApiToken;
  const [selectedStructureId, setSelectedStructureId] = useState('');
  const [isAwaiting, setIsAwaiting] = useState(false);
  const [finderId] = useState(new Date().getTime());
  const [changeDelegate, setChangeDelegate] = useState(false);
  const [currentDelegate, setCurrentDelegate] = useState();

  const getProfileStatutsLabel = (status) => {
    switch (status) {
      case 1:
        return 'Actif';
      case 2:
        return 'Inactif';
      case 3:
        return 'A supprimer';
      default:
        return 'Actif';
    }
  };

  function resetSelectedStrucInfo() {
    setSelectedStructureId('');
    localStorage.removeItem('selectedStrucId');
    localStorage.removeItem('selectedStrucName');
  }

  useEffect(() => {
    if (userData.delegate) {
      Meteor.call('users.getOne', { userId: userData.delegate }, (err, res) => {
        if (res) {
          setCurrentDelegate(res);
        }
      });
    }
  }, [userData]);

  useEffect(() => {
    if (selectedStructureId === '') setSelectedStructureId(localStorage.getItem('selectedStrucId'));
    else resetSelectedStrucInfo();
  }, [history]);

  const userHaveNcLocator = userData?.username && userData?.nclocator;

  const { awaitingStructure, ready: isAwaitingStructureReady } = useAwaitingStructure();
  const [{ user, loadingUser, isMobile }, dispatch] = useAppContext();
  const userStructure = useStructure();
  const [selectedStructure, setSelectedStructure] = useState(userStructure || null);

  useEffect(() => {
    (async () => {
      if (user.structure && user.structure.length > 0) {
        const structure = await getStructure(user.structure);
        setSelectedStructure(structure);
      }
    })();
  }, [userStructure, user.structure]);

  useEffect(() => {
    // check if selectedStructure will require validation
    if (selectedStructureId && selectedStructureId !== user.structure) {
      Meteor.call('users.checkStructure', { structure: selectedStructureId }, (err, res) => {
        if (err) {
          msg.error(err.reason || err.message);
        } else {
          setIsAwaiting(!res);
        }
      });
    } else {
      setIsAwaiting(false);
    }
  }, [selectedStructureId]);

  const [tempImageLoaded, setTempImageLoaded] = useState(false);
  const { minioEndPoint } = Meteor.settings.public;

  const checkSubmitOk = () => {
    const errSum = Object.keys(errors).reduce((sum, name) => {
      if (['advancedPersonalPage', 'betaServices', 'motivation', 'profileStatus'].includes(name)) {
        // checkbox not concerned by errors
        return sum;
      }
      return sum + errors[name];
    }, '');
    if (errSum !== '') {
      return false;
    }
    return true;
  };

  const setData = (data, reset = false) => {
    setUserData({
      username: errors.username === '' || reset ? data.username : userData.username,
      logoutType: data.logoutType || 'ask',
      firstName: errors.firstName === '' || reset ? data.firstName || '' : userData.firstName,
      lastName: errors.lastName === '' || reset ? data.lastName || '' : userData.lastName,
      displayNameComplement:
        errors.displayNameComplement === '' || reset
          ? data.displayNameComplement || ''
          : userData.displayNameComplement,
      email: errors.email === '' || reset ? data.emails[0].address : userData.email,
      avatar: userData.avatar === '' || reset ? data.avatar : userData.avatar,
      advancedPersonalPage:
        userData.advancedPersonalPage === false || reset ? data.advancedPersonalPage : userData.advancedPersonalPage,
      betaServices: userData.betaServices === false || reset ? data.betaServices : userData.betaServices,
      nclocator: data.nclocator || userData.nclocator,
      motivation: data.motivation || userData.motivation,
      createdAt: data.createdAt || userData.createdAt,
      updatedAt: data.updatedAt || userData.updatedAt,
      phoneNumber: data.phoneNumber || userData.phoneNumber,
      location: data.location || userData.location,
      jobTitle: data.jobTitle || userData.jobTitle,
      accountType: data.accountType || userData.accountType,
      profileStatus: data.profileStatus || userData.profileStatus,
      departure: data.lifeCycleDates?.departure || userData.departure,
      delegate: data.delegate || userData.delegate,
    });
    if (reset === true) {
      resetSelectedStrucInfo();
      setErrors(defaultState);
    }
    setSubmitted(false);
  };

  useEffect(() => {
    const departureStateLocal = userData.departure ? new Date(userData.departure).getTime() : '';
    const departureBdd = user.lifeCycleDates?.departure ? new Date(user.lifeCycleDates.departure).getTime() : '';
    if (
      submitted &&
      userData.firstName === user.firstName &&
      userData.lastName === user.lastName &&
      userData.displayNameComplement === user.displayNameComplement &&
      userData.email === user.emails[0].address &&
      userData.username === user.username &&
      userData.logoutType === user.logoutType &&
      userData.avatar === user.avatar &&
      userData.advancedPersonalPage === user.advancedPersonalPage &&
      userData.betaServices === user.betaServices &&
      userData.nclocator === user.nclocator &&
      userData.motivation === user.motivation &&
      userData.phoneNumber === user.phoneNumber &&
      userData.location === user.location &&
      userData.jobTitle === user.jobTitle &&
      userData.accountType === user.accountType &&
      userData.delegate === user.delegate &&
      departureStateLocal === departureBdd
    ) {
      msg.success(i18n.__('pages.ProfilePage.updateSuccess'));
      setSubmitted(false);
    }
    if (user._id) {
      setData(user);
    }
  }, [user]);

  useEffect(() => {
    setSubmitOk(checkSubmitOk());
  }, [errors]);

  function validateName(name) {
    if (name.trim() !== '') {
      Meteor.call('users.checkUsername', { username: name.trim() }, (err, res) => {
        if (err) {
          msg.error(err.message);
        } else if (res === true) {
          setErrors({ username: '' });
        } else {
          setErrors({ username: i18n.__('pages.ProfilePage.usernameError') });
        }
      });
    }
  }
  const debouncedValidateName = debounce(validateName, 500);

  const resetForm = () => {
    if (tempImageLoaded && minioEndPoint) {
      // A temporary image has been loaded in minio with name "Avatar_TEMP"
      // => delete it
      Meteor.call('files.selectedRemove', {
        path: `users/${Meteor.userId()}`,
        toRemove: [`users/${Meteor.userId()}/Avatar_TEMP.png`],
      });
    }
    setTempImageLoaded(false);
    setData(user, true);
    msg.info('Modifications annulées');
  };

  const onCheckOption = (event) => {
    const { name, checked } = event.target;
    setUserData({ ...userData, [name]: checked });
  };

  const onUpdateField = (event) => {
    const { name, value } = event.target;
    setUserData({ ...userData, [name]: value });

    if (name === 'departure' && value.trim() === '') {
      setUserFromFinder(null);
    }

    if (value.trim() === '' && name !== 'departure') {
      setErrors({ [name]: i18n.__('pages.ProfilePage.valueRequired') });
    } else if (name === 'username') {
      setSubmitOk(false);
      // check for username validity
      debouncedValidateName(value);
    } else {
      setErrors({ [name]: '' });
    }
  };

  const onUpdatePhoneNumber = (value) => {
    setUserData({ ...userData, phoneNumber: value });
    if (value.trim() === '') {
      setErrors({ phoneNumber: i18n.__('pages.ProfilePage.valueRequired') });
    }
  };

  const onUpdateAccountType = (value) => {
    setUserData({ ...userData, accountType: value });
    if (value.trim() === '') {
      setErrors({ accountType: i18n.__('pages.ProfilePage.valueRequired') });
    }
  };

  const resetDelegate = () => {
    setUserFromFinder(null);
    setUserData({ ...userData, delegate: '' });
  };

  const cancelChangeDelegate = () => {
    setChangeDelegate(false);
  };

  const submitUpdateUser = () => {
    setSubmitted(true);
    setChangeDelegate(false);
    let modifications = false;
    const updateError = (errMsg) => {
      msg.error(errMsg);
      setSubmitted(false);
    };
    if (userData.displayNameComplement !== user.displayNameComplement) {
      modifications = true;
      Meteor.call(
        'users.setDisplayNameComplement',
        { displayNameComplement: userData.displayNameComplement },
        (error) => {
          if (error) {
            updateError(error.reason || error.message);
            setErrors({ displayNameComplement: error.reason || error.message });
          } else {
            setErrors({ displayNameComplement: '' });
          }
        },
      );
    }
    if (localStorage.getItem('selectedStrucId') && localStorage.getItem('selectedStrucId') !== user.structure) {
      modifications = true;
      Meteor.call('users.setStructure', { structure: localStorage.getItem('selectedStrucId') }, (error) => {
        if (error) {
          updateError(error.error === 'validation-error' ? error.details[0].message : error.message);
        } else {
          msg.success(i18n.__('api.methods.operationSuccessMsg'));
          resetSelectedStrucInfo();
        }
      });
    }
    if (userData.logoutType !== user.logoutType) {
      modifications = true;
      Meteor.call('users.setLogoutType', { logoutType: userData.logoutType }, (error) => {
        if (error) {
          updateError(error.error === 'validation-error' ? error.details[0].message : error.message);
        }
      });
    }
    if (userData.avatar !== user.avatar) {
      modifications = true;
      if (tempImageLoaded && minioEndPoint) {
        // A temporary image has been loaded in user minio with name "Avatar_TEMP"
        if (userData.avatar.includes('Avatar_TEMP')) {
          // This image will be the new user's avatar
          Meteor.call('files.rename', {
            path: `users/${Meteor.userId()}`,
            oldName: 'Avatar_TEMP.png',
            newName: 'Avatar.png',
          });
        } else {
          // the temporary image will not be used => delete it
          Meteor.call('files.selectedRemove', {
            path: `users/${Meteor.userId()}`,
            toRemove: [`users/${Meteor.userId()}/Avatar_TEMP.png`],
          });
        }
        setTempImageLoaded(false);
      }
      // replace 'Avatar_TEMP.png' even if the image comes from gallery then the name will be unchanged
      Meteor.call('users.setAvatar', { avatar: userData.avatar.replace('Avatar_TEMP.png', 'Avatar.png') }, (error) => {
        if (error) {
          updateError(error.error === 'validation-error' ? error.details[0].message : error.message);
        }
      });
    }
    if (userData.advancedPersonalPage !== user.advancedPersonalPage) {
      modifications = true;
      Meteor.call('users.toggleAdvancedPersonalPage', {}, (error) => {
        if (error) {
          updateError(error.error === 'validation-error' ? error.details[0].message : error.message);
        }
      });
    }
    if (userData.betaServices !== user.betaServices) {
      modifications = true;
      Meteor.call('users.toggleBetaServices', {}, (error) => {
        if (error) {
          updateError(error.error === 'validation-error' ? error.details[0].message : error.message);
        }
      });
    }
    if (userData.motivation !== user.motivation) {
      modifications = true;
      Meteor.call('users.setMotivation', { motivation: userData.motivation }, (error) => {
        if (error) {
          updateError(error.error === 'validation-error' ? error.details[0].message : error.message);
        }
      });
    }
    if (userData.phoneNumber !== user.phoneNumber) {
      modifications = true;
      Meteor.call('users.setPhoneNumber', { phoneNumber: userData.phoneNumber }, (error) => {
        if (error) {
          updateError(error.error === 'validation-error' ? error.details[0].message : error.message);
        }
      });
    }
    if (userData.location !== user.location) {
      modifications = true;
      Meteor.call('users.setLocation', { location: userData.location }, (error) => {
        if (error) {
          updateError(error.error === 'validation-error' ? error.details[0].message : error.message);
        }
      });
    }
    if (userData.jobTitle !== user.jobTitle) {
      modifications = true;
      Meteor.call('users.setJobTitle', { jobTitle: userData.jobTitle }, (error) => {
        if (error) {
          updateError(error.error === 'validation-error' ? error.details[0].message : error.message);
        }
      });
    }
    if (userData.accountType !== user.accountType) {
      modifications = true;
      Meteor.call('users.setAccountType', { accountType: userData.accountType }, (error) => {
        if (error) {
          updateError(error.error === 'validation-error' ? error.details[0].message : error.message);
        }
      });
    }
    if (userData.departure !== user.lifeCycleDates?.departure) {
      modifications = true;
      Meteor.call(
        'users.setDepartureDate',
        { departure: userData.departure !== '' ? new Date(userData.departure) : null },
        (error) => {
          if (error) {
            updateError(error.error === 'validation-error' ? error.details[0].message : error.message);
          }
        },
      );
    }
    if (userData.delegate !== userFromFinder?._id) {
      modifications = true;

      if (userFromFinder) setUserData({ ...userData, delegate: userFromFinder._id || '' });
      else setUserData({ ...userData, delegate: '' });

      Meteor.call('users.setDelegate', { delegate: userFromFinder ? userFromFinder._id : null }, (error) => {
        if (error) {
          updateError(error.error === 'validation-error' ? error.details[0].message : error.message);
        }
      });
    }
    if (modifications === true) {
      Meteor.call('users.setUpdatedAt', { updatedAt: new Date() }, (error) => {
        if (error) {
          updateError(error.error === 'validation-error' ? error.details[0].message : error.message);
        }
      });
    }
    if (modifications === false) {
      setSubmitted(false);
      msg.info(i18n.__('pages.ProfilePage.noModifications'));
    }
  };

  const useEmail = () => {
    setUserData({ ...userData, username: userData.email });
    setErrors({ username: '' });
  };
  const downloadBackup = () => {
    downloadBackupPublications.call((error, results) => {
      if (error) {
        msg.error(error.reason);
      } else {
        const file = `data:text/json;charset=utf-8,${encodeURIComponent(JSON.stringify(results))}`;

        const a = document.createElement('a');
        a.style.display = 'none';
        a.href = file;
        // the filename you want
        a.download = `backup_${new Date().getTime()}.json`;
        a.click();
      }
    });
  };
  const uploadData = ({ target: { files = [] } }) => {
    const reader = new FileReader();

    reader.onload = function uploadArticles(theFile) {
      const articles = JSON.parse(theFile.target.result);
      uploadBackupPublications.call({ articles, updateStructure: structChecked }, (error) => {
        if (error) {
          msg.error(error.reason);
        } else {
          msg.success(i18n.__('pages.ProfilePage.uploadSuccess'));
        }
      });
    };
    reader.readAsText(files[0]);
  };

  const accountURL = `${testMeteorSettingsUrl(Meteor.settings.public.keycloakUrl)}/realms/${
    Meteor.settings.public.keycloakRealm
  }/account`;

  const authorBlogPage = Meteor.settings.public.services.laboiteBlogURL
    ? `${testMeteorSettingsUrl(Meteor.settings.public.services.laboiteBlogURL)}/structures/${user.structure}`
    : `${Meteor.absoluteUrl()}public/`;

  const SendNewAvatarToMedia = (avImg) => {
    dispatch({
      type: 'uploads.add',
      data: {
        name: 'Avatar_TEMP',
        fileName: 'Avatar_TEMP',
        file: avImg,
        type: 'png',
        path: `users/${Meteor.userId()}`,
        storage: true,
        isUser: true,
        onFinish: (url) => {
          // Add time to url to avoid caching
          setUserData({ ...userData, avatar: `${url}?${new Date().getTime()}` });
          setTempImageLoaded(true);
        },
      },
    });
  };

  const onAssignAvatar = (avatarObj) => {
    // avatarObj = [{image: base64...}] or {url: http...}
    if (avatarObj?.[0]?.image) {
      SendNewAvatarToMedia(avatarObj[0].image);
    } else if (avatarObj.url !== user.avatar) {
      setUserData({ ...userData, avatar: avatarObj.url });
    }
  };

  const getAuthToken = () => {
    Meteor.call('users.getAuthToken', (error, result) => {
      if (!result) {
        msg.error(i18n.__('pages.ProfilePage.noAuthToken'));
      } else if (error) {
        msg.error(error.reason);
      } else {
        navigator.clipboard.writeText(result).then(msg.success(i18n.__('pages.ProfilePage.successCopyAuthToken')));
      }
    });
  };

  const resetAuthToken = () => {
    Meteor.call('users.resetAuthToken', (error, result) => {
      if (!result) {
        msg.error(i18n.__('pages.ProfilePage.noAuthToken'));
      } else if (error) {
        msg.error(error.reason);
      } else {
        navigator.clipboard
          .writeText(result)
          .then(msg.success(i18n.__('pages.ProfilePage.successResetAndCopyAuthToken')));
      }
    });
  };

  const resetAwaitingStructure = () => {
    Meteor.call('users.resetAwaitingStructure', (error, result) => {
      if (result) {
        msg.success(i18n.__('pages.ProfilePage.resetAwaitingStructureSuccess'));
        resetSelectedStrucInfo();
      } else {
        msg.error(error.reason);
      }
    });
  };

  const structName = selectedStructure?.name || '';
  const applyDefaultSpace = () => {
    if (user.advancedPersonalPage) {
      msg.error(i18n.__('pages.ProfilePage.ApplyDefaultSpaceWarning'));
    } else {
      Meteor.call('personalspaces.generateDefaultPersonalSpace', { userId: Meteor.userId() }, (error) => {
        if (error) {
          msg.error(error.details[0].message);
        } else {
          msg.success(i18n.__('api.methods.operationSuccessMsg'));
        }
      });
    }
  };

  return loadingUser || submitted ? (
    <Spinner />
  ) : (
    <Fade in>
      <Container>
        <form noValidate autoComplete="off">
          <Grid container className={classes.form}>
            <Paper className={classes.paperContainer}>
              {/* ID card block */}
              <div className={classes.flexGapContainer}>
                {user.displayName ? (
                  <Typography variant="h4">{user.displayName}</Typography>
                ) : (
                  <>
                    <Typography variant="h4">{user.firstName}</Typography>
                    <Typography variant="h4">{user.lastName}</Typography>
                  </>
                )}
              </div>

              <Paper className={classes.paperContainer}>
                <Grid container spacing={2} style={{ alignItems: 'center' }}>
                  <Grid item xs={isMobile ? 16 : 8}>
                    <div className={classes.flexGapContainer}>
                      <TextField
                        disabled
                        margin="normal"
                        autoComplete="fname"
                        id="firstName"
                        label={i18n.__('pages.ProfilePage.firstNameLabel')}
                        name="firstName"
                        error={errors.firstName !== ''}
                        helperText={errors.firstName}
                        onChange={onUpdateField}
                        fullWidth
                        type="text"
                        value={userData.firstName || ''}
                        variant="outlined"
                      />
                      <TextField
                        disabled
                        margin="normal"
                        id="lastName"
                        autoComplete="lname"
                        label={i18n.__('pages.ProfilePage.lastNameLabel')}
                        name="lastName"
                        error={errors.lastName !== ''}
                        helperText={errors.lastName}
                        onChange={onUpdateField}
                        fullWidth
                        type="text"
                        value={userData.lastName || ''}
                        variant="outlined"
                      />
                    </div>
                    {/* DisplayNameComplement if homonyms detected */}
                    {user.displayNameComplement ? (
                      <div>
                        <div className={classes.flexGapContainer}>
                          <FormControl variant="outlined" fullWidth margin="normal">
                            <InputLabel
                              error={errors.displayNameComplement !== ''}
                              htmlFor="displayNameComplement"
                              id="displayNameComplement-label"
                            >
                              {user.accountType === 'functional'
                                ? i18n.__('api.users.labels.displayName')
                                : i18n.__('api.users.labels.displayNameComplement')}
                            </InputLabel>
                            <OutlinedInput
                              id="displayNameComplement"
                              name="displayNameComplement"
                              value={userData.displayNameComplement}
                              error={errors.displayNameComplement !== ''}
                              onChange={onUpdateField}
                              label={
                                user.accountType === 'functional'
                                  ? i18n.__('api.users.labels.displayName')
                                  : i18n.__('api.users.labels.displayNameComplement')
                              }
                            />
                            <FormHelperText
                              id="displayNameComplement-helper-text"
                              error={errors.displayNameComplement !== ''}
                            >
                              {errors.displayNameComplement}
                            </FormHelperText>
                            <Alert severity="info" sx={{ alignItems: 'center' }}>
                              <Typography>
                                {user.accountType === 'functional'
                                  ? i18n.__('pages.ProfilePage.blockTitle.displayNameFunctionalSubtitle')
                                  : i18n.__('pages.ProfilePage.blockTitle.displayNameComplementSubtitle')}
                              </Typography>
                            </Alert>
                          </FormControl>
                        </div>
                      </div>
                    ) : (
                      ''
                    )}
                    <FormControl variant="outlined" fullWidth disabled margin="normal">
                      <InputLabel error={errors.username !== ''} htmlFor="username" id="username-label">
                        {i18n.__('api.users.labels.username')}
                      </InputLabel>
                      <OutlinedInput
                        id="username"
                        name="username"
                        value={userData.username}
                        error={errors.username !== ''}
                        onChange={onUpdateField}
                        label={i18n.__('api.users.labels.username')}
                        endAdornment={
                          <InputAdornment position="end">
                            <Tooltip
                              title={i18n.__('pages.ProfilePage.useEmail')}
                              aria-label={i18n.__('pages.ProfilePage.useEmail')}
                            >
                              <span>
                                <IconButton onClick={useEmail} disabled size="large">
                                  <MailIcon />
                                </IconButton>
                              </span>
                            </Tooltip>
                          </InputAdornment>
                        }
                      />
                      <FormHelperText id="username-helper-text" error={errors.username !== ''}>
                        {errors.username}
                      </FormHelperText>
                    </FormControl>

                    <TextField
                      disabled
                      margin="normal"
                      id="email"
                      label={i18n.__('pages.ProfilePage.emailLabel')}
                      name="email"
                      autoComplete="email"
                      error={errors.email !== ''}
                      helperText={errors.email}
                      fullWidth
                      onChange={onUpdateField}
                      type="text"
                      value={userData.email || ''}
                      variant="outlined"
                    />
                    <br />
                    <br />
                    <Grid spacing={1} container>
                      <Grid xs={6} item>
                        <TextField
                          type="datetime-local"
                          label={i18n.__('pages.ProfilePage.createdDate')}
                          value={formatDate(userData.createdAt)}
                          disabled
                          fullWidth
                        />
                      </Grid>
                      <Grid xs={6} item>
                        <TextField
                          label={i18n.__('pages.ProfilePage.updatedDate')}
                          type="datetime-local"
                          value={formatDate(userData.updatedAt)}
                          disabled
                          fullWidth
                        />
                      </Grid>
                    </Grid>
                    <br />
                    <Grid spacing={1} container>
                      <Grid xs={4} item>
                        <LanguageSwitcher relative />
                      </Grid>
                      <Grid xs={4} item>
                        <TextField
                          name="accountType"
                          value={userData.accountType}
                          variant="outlined"
                          label={i18n.__('api.users.labels.accountType')}
                          select
                          fullWidth
                        >
                          {Object.keys(accountTypeLabels).map((val) => (
                            <MenuItem key={val} value={val} onClick={() => onUpdateAccountType(val)}>
                              {accountTypeLabels[val]}
                            </MenuItem>
                          ))}
                        </TextField>
                      </Grid>
                      <Grid xs={4} item>
                        <TextField
                          name="profileStatus"
                          value={getProfileStatutsLabel(userData.profileStatus)}
                          variant="outlined"
                          disabled
                          label={i18n.__('api.users.labels.profileStatus')}
                          fullWidth
                        />
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid item xs={isMobile ? 12 : 4}>
                    <AvatarPicker
                      userAvatar={userData.avatar || ''}
                      userFirstName={userData.firstName || ''}
                      onAssignAvatar={onAssignAvatar}
                      userActive={user.isActive}
                    />
                  </Grid>
                </Grid>
              </Paper>
              {/* Departure block */}
              <Paper className={classes.paperContainer}>
                <Typography variant={isMobile ? 'h6' : 'h4'}>
                  {i18n.__('pages.ProfilePage.departureBlock.title')}
                </Typography>
                <br />

                <TextField
                  sx={{ width: '66%' }}
                  type="date"
                  label={i18n.__('pages.ProfilePage.departureBlock.expectedDate')}
                  id="departure"
                  name="departure"
                  value={formatDateWithoutTime(userData.departure)}
                  inputProps={{ min: getLocaleDate(new Date()) }}
                  onChange={onUpdateField}
                />

                {userData.departure && (
                  <div>
                    {userData.delegate && !changeDelegate ? (
                      <div
                        style={{ marginTop: '2vh', width: '100%', display: 'flex', justifyContent: 'space-between' }}
                      >
                        {i18n.__('pages.ProfilePage.departureBlock.delegateInfo')}:<br />
                        {currentDelegate?.firstName} {currentDelegate?.lastName} ({currentDelegate?.username})
                        <div style={{ width: '45%', display: 'flex', justifyContent: 'space-between' }}>
                          <Button variant="contained" onClick={() => setChangeDelegate(!changeDelegate)}>
                            {i18n.__('pages.ProfilePage.departureBlock.changeDelegate')}
                          </Button>
                          <Button
                            variant="contained"
                            className={classes.buttonResetDelegate}
                            onClick={() => resetDelegate()}
                          >
                            {i18n.__('pages.ProfilePage.departureBlock.resetDelegate')}
                          </Button>
                        </div>
                      </div>
                    ) : (
                      <div style={{ display: 'flex', justifyContent: 'space-between', width: '66%', marginTop: '2vh' }}>
                        <Finder
                          onSelected={setUserFromFinder}
                          key={finderId}
                          defaultValue={userFromFinder}
                          i18nCode="DelegateFinder"
                          method="users.findUsers"
                          page="profile"
                        />
                        {changeDelegate && (
                          <Button
                            variant="contained"
                            className={classes.buttonResetDelegate}
                            onClick={() => cancelChangeDelegate()}
                          >
                            {i18n.__('pages.ProfilePage.departureBlock.cancelChangeDelegate')}
                          </Button>
                        )}
                      </div>
                    )}
                  </div>
                )}

                <br />
              </Paper>
              {/* Keycloak + CGU block */}
              <Paper className={classes.paperContainer}>
                <Typography variant={isMobile ? 'h6' : 'h4'}>
                  {i18n.__('pages.ProfilePage.blockTitle.identity')}
                </Typography>
                <Typography>{i18n.__('pages.ProfilePage.keycloakProcedure')}</Typography>
                <br />
                <Button variant="contained" href={accountURL}>
                  {i18n.__('pages.ProfilePage.keycloakProcedureLink')}
                </Button>
              </Paper>
              {/* Complementary info block */}
              {jobInformation.enable ? (
                <Paper className={classes.paperContainer}>
                  <Typography variant={isMobile ? 'h6' : 'h4'}>
                    {i18n.__('pages.ProfilePage.blockTitle.wokInformation')}
                  </Typography>
                  <Typography>{i18n.__('pages.ProfilePage.blockTitle.wokInformationSubtitle')}</Typography>
                  <br />
                  <div className={classes.flexGapContainer}>
                    <MuiTelInput
                      fullWidth
                      forceCallingCode
                      onlyCountries={['FR']}
                      defaultCountry="FR"
                      getFlagElement={(isoCode, { countryName }) => {
                        return <img aria-label={countryName} height="30px" src="/images/i18n/fr.png" />;
                      }}
                      value={userData.phoneNumber}
                      label={i18n.__('pages.ProfilePage.phone')}
                      onChange={onUpdatePhoneNumber}
                    />
                    <FormControl variant="outlined" fullWidth>
                      <InputLabel htmlFor="location" id="location-label">
                        {i18n.__('pages.ProfilePage.location')}
                      </InputLabel>
                      <Select
                        labelId="location-label"
                        label={i18n.__('pages.ProfilePage.location')}
                        id="location"
                        name="location"
                        value={userData.location}
                        onChange={onUpdateField}
                      >
                        {jobInformation.location.map((el) => (
                          <MenuItem key={el} value={el}>
                            {el}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                    <FormControl variant="outlined" fullWidth>
                      <InputLabel htmlFor="jobTitle" id="jobTitle-label">
                        {i18n.__('pages.ProfilePage.jobTitle')}
                      </InputLabel>
                      <Select
                        labelId="jobTitle-label"
                        label={i18n.__('pages.ProfilePage.jobTitle')}
                        id="jobTitle"
                        name="jobTitle"
                        value={userData.jobTitle}
                        onChange={onUpdateField}
                      >
                        {jobInformation.jobTitle.map((el) => (
                          <MenuItem key={el} value={el}>
                            {el}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </div>
                </Paper>
              ) : (
                ''
              )}

              {/* Structure block */}
              <Paper className={classes.paperContainer}>
                <FormControl>
                  <Typography variant={isMobile ? 'h6' : 'h4'}>
                    {i18n.__('pages.ProfilePage.blockTitle.structure')}
                  </Typography>
                  <div className={classes.flexGapContainer}>
                    <div style={{ display: 'flex', flexDirection: 'column', minWidth: '50%' }}>
                      <div className={classes.flexGapContainer}>
                        <Typography sx={{ alignContent: 'center' }}>
                          {i18n.__('pages.ProfilePage.currentStructure')}
                        </Typography>
                        <Typography color="primary" variant="h6">
                          {selectedStructure && selectedStructure.name ? (
                            <b>{selectedStructure.name}</b>
                          ) : (
                            <b>{i18n.__('pages.ProfilePage.noCurrentStructure')}</b>
                          )}
                        </Typography>
                      </div>
                      {localStorage.getItem('selectedStrucId') && (
                        <Typography color="primary" variant="h6">
                          {i18n.__('pages.ProfilePage.structureUpdate', {
                            structName: localStorage.getItem('selectedStrucName'),
                          })}
                        </Typography>
                      )}
                      <br />
                      <Button
                        variant="contained"
                        className={classes.buttonTextColor}
                        onClick={() => history.push('/profilestructureselection')}
                        sx={{ width: 'fit-content' }}
                      >
                        {i18n.__('pages.ProfilePage.openStructureSelection')}
                      </Button>
                      <br />
                      {user.structure && isDefaultSpaceEnabled ? (
                        <>
                          <Typography variant={isMobile ? 'h4' : 'h6'}>
                            {i18n.__('pages.ProfilePage.ApplyDefaultSpace', { structName })}
                          </Typography>
                          <Typography>{i18n.__('pages.ProfilePage.ApplyDefaultSpaceMessage')}</Typography>
                          <br />
                          <Button
                            variant="contained"
                            onClick={applyDefaultSpace}
                            className={classes.buttonTextColor}
                            sx={{ width: 'fit-content' }}
                          >
                            {i18n.__('pages.ProfilePage.ApplyDefaultSpaceBtn', { structName })}
                          </Button>
                        </>
                      ) : null}
                    </div>
                    {(user.isActive === false || isAwaiting === true || user.awaitingStructure) && (
                      <>
                        <div style={{ display: 'flex' }}>
                          <Divider variant="middle" orientation="vertical" />
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', paddingLeft: '1%' }}>
                          {isAwaitingStructureReady &&
                            awaitingStructure !== undefined &&
                            (awaitingStructure._id === selectedStructureId || !selectedStructureId) && (
                              <>
                                <Typography sx={{ paddingTop: 2 }} variant={isMobile ? 'h4' : 'h6'}>
                                  {i18n.__('pages.ProfilePage.awaitingForStructure')}{' '}
                                  <b style={{ color: '#011CAA' }}>{awaitingStructure.name}</b>
                                </Typography>
                              </>
                            )}
                          <>
                            <TextField
                              margin="normal"
                              id="motivation"
                              multiline
                              rows={3}
                              label={i18n.__('pages.ProfilePage.motivationLabel')}
                              name="motivation"
                              onChange={onUpdateField}
                              fullWidth
                              type="text"
                              value={userData.motivation || ''}
                              variant="outlined"
                              helperText={
                                user.isActive
                                  ? i18n.__('pages.ProfilePage.motivationHelper')
                                  : i18n.__('pages.ProfilePage.motivationHelperForInactive')
                              }
                            />
                            {user.awaitingStructure && (
                              <div className={classes.buttons}>
                                <Button
                                  variant="contained"
                                  onClick={resetAwaitingStructure}
                                  className={classes.buttonCancel}
                                >
                                  {i18n.__('pages.ProfilePage.cancelAwaitingStructure')}
                                </Button>
                              </div>
                            )}
                          </>
                        </div>
                      </>
                    )}
                  </div>
                </FormControl>
              </Paper>

              {/* Nextcloud block */}
              {Meteor.settings.public.groupPlugins.nextcloud.enable && (
                <Paper className={classes.paperContainer}>
                  <Typography variant={isMobile ? 'h6' : 'h4'}>
                    {i18n.__('pages.ProfilePage.blockTitle.nextcloud')}
                  </Typography>
                  <TextField
                    disabled
                    margin="normal"
                    id="nclocator"
                    label={i18n.__('pages.ProfilePage.nclocator')}
                    name="nclocator"
                    autoComplete="nclocator"
                    fullWidth
                    type="text"
                    value={
                      userHaveNcLocator
                        ? `${userData.username}@${nextInstance(userData.nclocator)}`
                        : i18n.__('pages.ProfilePage.emptyNcLocator')
                    }
                    variant="outlined"
                  />
                </Paper>
              )}

              {/* Options */}
              <Paper className={classes.paperContainer}>
                <Typography variant={isMobile ? 'h6' : 'h4'}>
                  {i18n.__('pages.ProfilePage.blockTitle.options')}
                </Typography>
                <Typography>{i18n.__('pages.ProfilePage.blockTitle.optionsSubtitle')}</Typography>
                <div className={classes.flexGapContainer}>
                  <div className={classes.flexColumn}>
                    <FormControlLabel
                      control={
                        <Checkbox
                          id="advancedPersonalPage"
                          name="advancedPersonalPage"
                          checked={userData.advancedPersonalPage}
                          onChange={onCheckOption}
                          inputProps={{ 'aria-label': 'primary checkbox' }}
                        />
                      }
                      label={i18n.__('pages.ProfilePage.advancedPersonalPage')}
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          id="betaServices"
                          name="betaServices"
                          checked={userData.betaServices}
                          onChange={onCheckOption}
                          inputProps={{ 'aria-label': 'primary checkbox' }}
                        />
                      }
                      label={i18n.__('pages.ProfilePage.betaServices')}
                    />
                  </div>
                  <div style={{ display: 'flex', width: '60%', alignItems: 'center' }}>
                    <FormControl fullWidth sx={{ marginTop: '1%', marginLeft: '5%' }}>
                      <InputLabel htmlFor="logoutType" id="logoutType-label">
                        {i18n.__('pages.ProfilePage.logoutType')}
                      </InputLabel>
                      <Select
                        labelId="logoutType-label"
                        label={i18n.__('pages.ProfilePage.logoutType')}
                        id="logoutType"
                        name="logoutType"
                        value={userData.logoutType}
                        onChange={onUpdateField}
                      >
                        {Object.keys(logoutTypeLabels).map((val) => (
                          <MenuItem key={val} value={val}>
                            {i18n.__(logoutTypeLabels[val])}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </div>
                </div>
              </Paper>

              {/* Buttons */}
              <div className={classes.buttonGroup}>
                <Button variant="contained" onClick={resetForm} color="secondary" className={classes.buttonTextColor}>
                  {i18n.__('pages.ProfilePage.reset')}
                </Button>
                <Button
                  variant="contained"
                  disabled={!submitOk}
                  color="primary"
                  onClick={submitUpdateUser}
                  className={classes.buttonTextColor}
                >
                  {i18n.__('pages.ProfilePage.update')}
                </Button>
              </div>
            </Paper>
          </Grid>
        </form>
        {/* Blog */}
        {enableBlog && (
          <Paper className={classes.paperContainer}>
            <Typography variant={isMobile ? 'h6' : 'h4'} sx={{ width: '100%' }}>
              {i18n.__('pages.ProfilePage.backupTitle')}
            </Typography>
            <Grid container>
              {user.structure ? (
                <p>
                  <FormControlLabel
                    control={
                      <Checkbox
                        disabled={!user.structure}
                        checked={structChecked}
                        onChange={() => setStructChecked(!structChecked)}
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                      />
                    }
                    label={i18n.__('pages.ProfilePage.structureMessage')}
                  />
                  <Typography>
                    {i18n.__('pages.ProfilePage.structureArticleSubtitle')}
                    <a target="_blank" rel="noreferrer noopener" href={authorBlogPage} className={classes.keycloakLink}>
                      {i18n.__('pages.ProfilePage.backupTitle')}
                    </a>
                  </Typography>
                </p>
              ) : null}

              <div>
                <Typography variant="h6">{i18n.__('pages.ProfilePage.backupMessageTitle')}</Typography>
                {i18n.__('pages.ProfilePage.backupMessage')}
                <br />
                <br />
              </div>
              <Grid item xs={12} sm={6} md={6} className={classes.buttonWrapper}>
                <Button
                  variant="contained"
                  onClick={downloadBackup}
                  color="secondary"
                  className={classes.buttonTextColor}
                >
                  {i18n.__('pages.ProfilePage.downloadPublicationBackup')}
                </Button>
              </Grid>
              <Grid item xs={12} sm={6} md={6} className={classes.buttonWrapper}>
                <div className={classes.fileWrap}>
                  <label htmlFor="upload">
                    <Input className={classes.inputFile} type="file" id="upload" onChange={uploadData} />
                    <Button
                      variant="contained"
                      color="secondary"
                      component="span"
                      tabIndex={-1}
                      className={classes.buttonTextColor}
                    >
                      {i18n.__('pages.ProfilePage.UploadPublicationBackup')}
                    </Button>
                  </label>
                </div>
              </Grid>
            </Grid>
          </Paper>
        )}
        {/* Token API */}
        {isApiTokenEnabled && (
          <Paper className={classes.root}>
            <Typography variant={isMobile ? 'h6' : 'h4'}>{i18n.__('pages.ProfilePage.authTokenTitle')}</Typography>
            <p>{i18n.__('pages.ProfilePage.authTokenMessage')}</p>
            <p>
              <b>{i18n.__('pages.ProfilePage.resetTokenMessage')}</b>
            </p>

            <Grid container>
              <Grid item xs={12} sm={6} md={6} className={classes.buttonWrapper}>
                <Button variant="contained" onClick={getAuthToken} className={classes.buttonTextColor}>
                  {i18n.__('pages.ProfilePage.getAuthToken')}
                </Button>
              </Grid>
              <Grid item xs={12} sm={6} md={6} className={classes.buttonWrapper}>
                <Button variant="contained" onClick={resetAuthToken} className={classes.buttonTextColor}>
                  {i18n.__('pages.ProfilePage.resetAuthToken')}
                </Button>
              </Grid>
            </Grid>
          </Paper>
        )}
      </Container>
    </Fade>
  );
};

export default ProfilePage;
