import React, { createContext, useState } from 'react';
import { StyledEngineProvider, ThemeProvider } from '@mui/material/styles';
import PropTypes from 'prop-types';
import appTheme from '../../themes/themes';

// Inspired from https://sachinkanishka.medium.com/adding-dark-mode-for-react-typescript-app-with-mui-226fa0154571

const ThemeContext = createContext({
  switchColorMode: () => {},
});

const ThemeContextProvider = ({ children }) => {
  const [mode, setMode] = useState('light');

  const switchColorMode = () => {
    setMode((prevMode) => (prevMode === 'light' ? 'dark' : 'light'));
  };

  const theme = appTheme(mode);
  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <ThemeContext.Provider value={{ switchColorMode }}>{children}</ThemeContext.Provider>
      </ThemeProvider>
    </StyledEngineProvider>
  );
};

export { ThemeContext, ThemeContextProvider };

ThemeContextProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
