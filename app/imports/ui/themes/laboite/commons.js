function getCommons(mode) {
  const COMMONS = {
    signinBackground: 'url(images/bg-laboite.jpg)',
    logos: {
      SMALL_LOGO: mode === 'dark' ? '/images/logos/laboite/Logo-A-light.svg' : '/images/logos/laboite/Logo-A.svg',
      LONG_LOGO:
        mode === 'dark' ? '/images/logos/laboite/apps-logo-light.svg' : '/images/logos/laboite/apps-logo-sansfond.svg',
      SMALL_LOGO_MAINTENANCE: '/images/logos/laboite/Logo-A-maintenance.svg',
      LONG_LOGO_MAINTENANCE: '/images/logos/laboite/apps-logo-maintenance.svg',
    },
    shape: {
      headerHeight: 100,
      borderRadius: 8,
    },
    typography: {
      fontFamily: 'WorkSansRegular',
      h1: {
        fontFamily: 'WorkSansBold',
      },
      h2: {
        fontFamily: 'WorkSansBold',
      },
      h3: {
        fontFamily: 'WorkSansBold',
      },
      h4: {
        fontFamily: 'WorkSansBold',
      },
      h5: {
        fontFamily: 'WorkSansBold',
      },
      h6: {
        fontFamily: 'WorkSansBold',
      },
    },
  };
  return COMMONS;
}

export default getCommons;
