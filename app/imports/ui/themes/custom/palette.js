import { computeCustoms } from '../utils';
import getCommons from './commons';
import { components } from './customs';

function getCustomTheme(mode) {
  const palette = {
    mode,
    ...(mode === 'light'
      ? {
          primary: {
            main: '#011CAA',
            light: '#ECEEF8',
            dark: '#212F74',
          },
          secondary: {
            main: '#E48231',
            light: '#FFDBA5',
          },
          tertiary: {
            main: '#fff',
          },
          backgroundFocus: {
            main: '#ffe0b2',
          },
          text: {
            primary: '#040D3E',
            green: '#27a658',
          },
          background: {
            default: '#F9F9FD',
            inputs: '#F9F9FD',
          },
          info: {
            main: '#518fff',
            light: '#85acff',
            dark: '#9ebdff',
          },
        }
      : {
          primary: {
            main: '#9ebdff',
            light: '#5302AB',
            dark: '#454B55',
          },
          secondary: {
            main: '#E48231',
            light: '#FFDBA5',
            dark: '#E36130',
          },

          tertiary: {
            main: '#161616',
          },
          backgroundFocus: {
            main: '#313178',
          },
          text: {
            primary: '#ffffff',
            green: '#45b870',
          },
          background: {
            default: '#161616',
            inputs: '#454B55',
            paper: '#383838',
          },
          info: {
            main: '#518fff',
            light: '#85acff',
            dark: '#9ebdff',
          },
        }),
  };
  const COMMONS = getCommons(mode);
  const custom = {
    ...COMMONS,
    components: computeCustoms(palette, components),
    palette,
  };

  return custom;
}

export default getCustomTheme;
