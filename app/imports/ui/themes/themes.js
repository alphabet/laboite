import { useMemo } from 'react';
import { createTheme } from '@mui/material/styles';
import getRizomoTheme from './rizomo/palette';
import getLaboiteTheme from './laboite/palette';
import getEoleTheme from './eole/palette';
import getCustomTheme from './custom/palette';
import getMimTheme from './mim-libre/palette';
import getPsadTheme from './psad/palette';

function appTheme(mode) {
  const allThemes = {
    rizomo: getRizomoTheme(mode),
    laboite: getLaboiteTheme(mode),
    eole: getEoleTheme(mode),
    custom: getCustomTheme(mode),
    mim: getMimTheme(mode),
    psad: getPsadTheme(mode),
  };
  let selectedTheme = Meteor.settings.public.theme;
  // if theme in settings doesn't exist, set eole theme to default theme
  if (!Object.keys(allThemes).includes(selectedTheme)) selectedTheme = 'eole';
  const theme = useMemo(() => createTheme(allThemes[selectedTheme]), [mode]);
  return theme;
}

export default appTheme;
