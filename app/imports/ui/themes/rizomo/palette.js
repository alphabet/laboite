import { computeCustoms } from '../utils';
import COMMONS from './commons';
import { components } from './customs';

function getRizomoTheme(mode) {
  const palette = {
    mode,
    ...(mode === 'light'
      ? {
          primary: {
            main: '#000091',
            light: '#E5E5F4',
            dark: '#0909b9',
          },
          secondary: {
            main: '#c9191e',
            light: '#af161a',
            dark: '#9b1317',
          },
          tertiary: {
            main: '#fff',
          },
          backgroundFocus: {
            main: '#f5f5fe',
          },
          text: {
            primary: '#161616',
          },
          background: {
            default: '#f6f6f6',
            inputs: '#f0f0f0',
            paper: '#fff',
          },
          info: {
            main: '#0063cb',
            light: '#0055af',
            dark: '#004b99',
          },
          success: {
            main: '#18753c',
            light: '#2aac5c',
            dark: '#249851',
          },
          warning: {
            main: '#b34000',
            light: '#983600',
            dark: '#842f00',
          },
          error: {
            main: '#ce0500',
            light: '#b20400',
            dark: '#9c0400',
          },
          grey: {
            main: '#e0e0e0',
            light: '#bdbdbd',
            dark: '#9e9e9e',
          },
        }
      : {
          primary: {
            main: '#8585f6',
            light: '#5f5ff1',
            dark: '#7070f3',
          },
          secondary: {
            main: '#f95c5e',
            light: '#fa8d8e',
            dark: '#fba5a6',
          },
          tertiary: {
            main: '#161616',
          },
          backgroundFocus: {
            main: '#313178',
          },
          text: {
            primary: '#ffffff',
          },
          background: {
            default: '#2a2a2a',
            inputs: '#383838',
            paper: '#313178',
          },
          info: {
            main: '#518fff',
            light: '#85acff',
            dark: '#9ebdff',
          },
          success: {
            main: '#27a658',
            light: '#53c37d',
            dark: '#45b870',
          },
          warning: {
            main: '#fc5d00',
            light: '#fd7b44',
            dark: '#fe8b58',
          },
          error: {
            main: '#ff5655',
            light: '#ff8988',
            dark: '#ffa2a2',
          },
          grey: {
            main: '#e0e0e0',
            light: '#bdbdbd',
            dark: '#9e9e9e',
          },
        }),
  };

  const rizomo = {
    ...COMMONS,
    components: computeCustoms(palette, components),
    palette,
  };

  return rizomo;
}

export default getRizomoTheme;
