function getCommons(mode) {
  const COMMONS = {
    signinBackground: 'url(images/image-de-fond-login-pc.png)',
    logos: {
      SMALL_LOGO:
        mode === 'dark' ? '/images/logos/mim-libre/small-logo-dark.svg' : '/images/logos/mim-libre/logo-9.svg',
      LONG_LOGO: mode === 'dark' ? '/images/logos/mim-libre/logo-dark.svg' : '/images/logos/mim-libre/logo-6.svg',
      SMALL_LOGO_MAINTENANCE: '/images/logos/mim-libre/logo-9.svg',
      LONG_LOGO_MAINTENANCE: '/images/logos/mim-libre/logo-6.svg',
    },
    shape: {
      headerHeight: 100,
      borderRadius: 8,
    },
    typography: {
      fontFamily: 'WorkSansRegular',
      h1: {
        fontFamily: 'WorkSansBold',
      },
      h2: {
        fontFamily: 'WorkSansBold',
      },
      h3: {
        fontFamily: 'WorkSansBold',
      },
      h4: {
        fontFamily: 'WorkSansBold',
      },
      h5: {
        fontFamily: 'WorkSansBold',
      },
      h6: {
        fontFamily: 'WorkSansBold',
      },
    },
  };
  return COMMONS;
}

export default getCommons;
