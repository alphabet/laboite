/**
 * Adapated from https://github.com/nhn/tui.editor/issues/2798#issuecomment-1756114494
 *
 * React wrapper for tui.editor (https://github.com/nhn/tui.editor)
 * Based on https://github.com/nhn/tui.editor/blob/master/apps/react-editor/src/editor.tsx
 */

import React, { useEffect, useRef, forwardRef } from 'react';
import PropTypes from 'prop-types';
import ToastuiEditor from '@toast-ui/editor';
import ToastuiViewer from '@toast-ui/editor/dist/toastui-editor-viewer';

function getBindingEventNames(props) {
  return Object.keys(props)
    .filter((key) => /^on[A-Z][a-zA-Z]+/.test(key))
    .filter((key) => props[key]);
}

function bindEventHandlers(editor, props) {
  getBindingEventNames(props).forEach((key) => {
    const eventName = key[2].toLowerCase() + key.slice(3);

    editor.off(eventName);
    editor.on(eventName, props[key]);
  });
}

function getInitEvents(props) {
  return getBindingEventNames(props).reduce((acc, key) => {
    const eventName = key[2].toLowerCase() + key.slice(3);

    acc[eventName] = props[key];

    return acc;
  }, {});
}

export const TuiEditor = forwardRef(function TuiEditor(props, ref) {
  const divRef = useRef(null);
  const toastRef = ref;
  useEffect(() => {
    if (divRef.current) {
      toastRef.current = new ToastuiEditor({
        ...props,
        el: divRef.current,
        usageStatistics: false,
        events: getInitEvents(props),
      });
    }
  }, []);

  useEffect(() => {
    if (props.height) {
      toastRef.current?.setHeight(props.height);
    }

    if (props.previewStyle) {
      toastRef.current?.changePreviewStyle(props.previewStyle);
    }

    if (toastRef.current) {
      bindEventHandlers(toastRef.current, props);
    }
  }, [props]);
  return <div ref={divRef} />;
});

TuiEditor.propTypes = {
  height: PropTypes.string.isRequired,
  previewStyle: PropTypes.string.isRequired,
};

export const TuiViewer = forwardRef(function TuiViewer(props, ref) {
  const divRef = useRef(null);
  const toastRef = ref;
  useEffect(() => {
    if (divRef.current) {
      toastRef.current = new ToastuiViewer({
        ...props,
        el: divRef.current,
        usageStatistics: false,
        events: getInitEvents(props),
      });
    }
  }, []);

  useEffect(() => {
    if (toastRef.current) {
      bindEventHandlers(toastRef.current, props);
    }
  }, [props]);
  return <div ref={divRef} />;
});
