import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';

/**
 *
 * @param {string} email
 * @returns
 */
export const validateEmail = (email) => {
  return email.match(
    // eslint-disable-next-line max-len, no-useless-escape
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  );
};

export const isUrlExternal = (url) => {
  if (url.search(Meteor.absoluteUrl()) === -1 && url.search('http') !== -1) {
    return true;
  }
  return false;
};

// Test if the url from Meteor settings end by /
// if no, add it
// withSlash is a boolean to decide if you want a slash at the end of the url
export const testMeteorSettingsUrl = (settingsUrl, withSlash = false) => {
  const url = settingsUrl;
  if (withSlash) {
    if (settingsUrl.charAt(settingsUrl.length - 1) !== '/') {
      return `${url}/`;
    }
    return url;
  }
  if (settingsUrl.length > 0 && settingsUrl.charAt(settingsUrl.length - 1) === '/') {
    // delete last slash
    return url.replace(/.$/, '');
  }
  return url;
};

export const getGroupName = (group) => {
  if (group.type !== 15) return group.name;

  return `${group.name.slice(group.name.indexOf('_') + 1, group.name.length)}`;
};

export const compareBussinessRegrouping = (firstBussRegr, secBussRegr) => {
  // Use toUpperCase() to ignore character casing
  const firstBussRegrName = firstBussRegr.name.toUpperCase();
  const secBussRegrName = secBussRegr.name.toUpperCase();

  let comparison = 0;
  if (firstBussRegrName > secBussRegrName) {
    comparison = 1;
  } else if (firstBussRegrName < secBussRegrName) {
    comparison = -1;
  }
  return comparison;
};

export const getUserRoleInGroup = (userId, groupId) => {
  const role = Roles.userIsInRole(userId, 'admin', groupId)
    ? 'admin'
    : Roles.userIsInRole(userId, 'animator', groupId)
    ? 'animator'
    : 'member';

  return role;
};

function pad(number) {
  if (number < 10) {
    return `0${number}`;
  }
  return number;
}

export const formatDate = (dateString) => {
  const date = new Date(dateString);
  const finalDate = `${date.getFullYear()}-${pad(date.getMonth() + 1)}-${pad(date.getDate())}T${pad(
    date.getHours(),
  )}:${pad(date.getMinutes())}`;
  return finalDate;
};

export const formatDateWithoutTime = (dateString) => {
  const date = new Date(dateString);
  const finalDate = `${date.getFullYear()}-${pad(date.getMonth() + 1)}-${pad(date.getDate())}`;
  return finalDate;
};

export const getLocaleDate = (date) => {
  if (date) {
    const newDate = new Date(date);
    return newDate.toISOString().split('T')[0];
  }
  return ' ';
};

export const openInNewTab = (item) => {
  if (typeof item === 'string') {
    window.open(item, '_blank').focus();
  } else {
    window.open(item.path, '_blank').focus();
  }
};
