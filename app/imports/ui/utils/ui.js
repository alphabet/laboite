export const GRID_VIEW_MODE = { compact: 'compact', detail: 'detail' };

// Inline styles for switch button;
// To concatenate multiple styles, use ...classe.attribute in style tag attribute
export const switchStyle = {
  global: {
    borderRadius: 20,
    alignSelf: 'center',
    padding: 10,
    height: 10,
  },
  container: {
    display: 'flex',
    justifyContent: 'space-evenly',
  },
  mainContainer: { display: 'flex', alignItems: 'center' },
  green: { backgroundColor: 'lightgreen' },
  red: { backgroundColor: 'red' },
};
