import React from 'react';
import i18n from 'meteor/universe:i18n';
import PropTypes from 'prop-types';

import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';

import ContactMailIcon from '@mui/icons-material/ContactMail';
import AddBox from '@mui/icons-material/AddBox';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/DeleteOutline';

const AdminStructureManagementButtons = ({
  structure,
  onClickMailBtn,
  onClickAddBtn,
  onClickEditBtn,
  onClickDeleteBtn,
}) => {
  const hasChildren = (struc) => {
    return struc && struc.childrenIds && struc.childrenIds.length > 0;
  };

  return (
    <div>
      <Tooltip title={i18n.__('components.AdminStructureTreeItem.actions.sendEmailToAdmins')}>
        <span>
          <IconButton onClick={() => onClickMailBtn(structure)} size="large">
            <ContactMailIcon />
          </IconButton>
        </span>
      </Tooltip>

      <Tooltip title={i18n.__('components.AdminStructureTreeItem.actions.addStructure')}>
        <span>
          <IconButton onClick={() => onClickAddBtn(structure)} size="large">
            <AddBox />
          </IconButton>
        </span>
      </Tooltip>

      <Tooltip title={i18n.__('components.AdminStructureTreeItem.actions.editStructure')}>
        <span>
          <IconButton onClick={() => onClickEditBtn(structure)} size="large">
            <EditIcon />
          </IconButton>
        </span>
      </Tooltip>

      <Tooltip
        title={i18n.__(
          `components.AdminStructureTreeItem.actions.deleteStructure${
            hasChildren(structure) ? 'ImpossibleHasChildren' : ''
          }`,
        )}
      >
        <span>
          <IconButton
            disabled={hasChildren(structure)}
            role="button"
            onClick={() => onClickDeleteBtn(structure)}
            size="large"
          >
            <DeleteIcon />
          </IconButton>
        </span>
      </Tooltip>
    </div>
  );
};

AdminStructureManagementButtons.propTypes = {
  structure: PropTypes.object.isRequired,
  onClickMailBtn: PropTypes.func.isRequired,
  onClickAddBtn: PropTypes.func.isRequired,
  onClickEditBtn: PropTypes.func.isRequired,
  onClickDeleteBtn: PropTypes.func.isRequired,
};

export default AdminStructureManagementButtons;
