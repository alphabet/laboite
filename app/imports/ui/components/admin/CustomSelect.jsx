import React from 'react';
import PropTypes from 'prop-types';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';

export default function CustomSelect({ value = '', error, onChange, options }) {
  const selectedValue = options.some((op) => op.value === value) ? value : options.length > 0 ? options[0].value : '';
  return (
    <Select labelId="structure-label" name="structureSelect" value={selectedValue} error={error} onChange={onChange}>
      {options.map((op) => (
        <MenuItem key={op.value} value={op.value}>
          {op.label}
        </MenuItem>
      ))}
    </Select>
  );
}

CustomSelect.propTypes = {
  value: PropTypes.string,
  error: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
};
