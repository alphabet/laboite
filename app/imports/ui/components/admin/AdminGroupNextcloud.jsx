import React from 'react';
import i18n from 'meteor/universe:i18n';
import { makeStyles } from 'tss-react/mui';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardActions from '@mui/material/CardActions';
import IconButton from '@mui/material/IconButton';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import ClearIcon from '@mui/icons-material/Clear';
import PropTypes from 'prop-types';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { useAppContext } from '../../contexts/context';
import COMMON_STYLES from '../../themes/styles';

const useStyles = makeStyles()((theme, isMobile) => ({
  root: COMMON_STYLES.root,
  actions: COMMON_STYLES.actions,
  paper: COMMON_STYLES.paper(isMobile),
  buttonGroup: {
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: theme.spacing(5),
  },
  text: {
    marginTop: theme.spacing(2),
  },
  alert: COMMON_STYLES.alert,
}));

const AdminGroupNextcloud = ({ disableNextcloud, open, onClose }) => {
  const [{ isMobile }] = useAppContext();
  const { classes } = useStyles(isMobile);
  return (
    <Modal open={open} onClose={onClose}>
      <div className={classes.paper}>
        <Card className={classes.root}>
          <CardHeader
            title={i18n.__('components.AdminGroupNextcloud.subtitle')}
            action={
              <IconButton onClick={onClose} size="large">
                <ClearIcon />
              </IconButton>
            }
          />
          <CardContent>
            <Typography className={classes.text}>{i18n.__('components.AdminGroupNextcloud.mainText')}</Typography>
            <Typography className={classes.text}>{i18n.__('components.AdminGroupNextcloud.backupText')}</Typography>
          </CardContent>
          <CardActions className={classes.actions}>
            <div className={classes.buttonGroup}>
              <Button style={{ marginRight: 10 }} onClick={onClose}>
                {i18n.__('components.AdminGroupNextcloud.cancel')}
              </Button>
              <Button onClick={disableNextcloud} variant="contained" style={{ backgroundColor: 'red', color: 'white' }}>
                {i18n.__('components.AdminGroupNextcloud.validate')}
              </Button>
            </div>
          </CardActions>
        </Card>
      </div>
    </Modal>
  );
};

AdminGroupNextcloud.propTypes = {
  disableNextcloud: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default AdminGroupNextcloud;
