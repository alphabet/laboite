import React from 'react';
import AutoComplete from '@mui/material/Autocomplete';
import PropTypes from 'prop-types';
import { normalizeString } from '../../utils/MaterialTable';

// filter options ignores accentuated characters
const filterOptions = (options, state) =>
  options.filter((option) => {
    const searchText = [option.name, ...option.structurePath.map((struc) => struc.structureName)].join('');
    return normalizeString(searchText).includes(normalizeString(state.inputValue));
  });

export const renderStructure = (props, option) => {
  let parent = '';
  const { parentId, name, _id, structurePath } = option;
  if (parentId) {
    parent = structurePath.map((struc) => struc.structureName).join('/');
  }
  return (
    <div {...props} style={{ display: 'flex', flexDirection: 'column', alignItems: 'start' }} key={_id}>
      <div>{name}</div>
      {!!parentId && <div style={{ fontSize: 10, color: 'grey', fontStyle: 'italic' }}>{parent}</div>}
    </div>
  );
};

export const structureSort = (struc1, struc2) => {
  // sort structures alphabetically by structure path, then name
  const struc1Path = struc1.structurePath
    .map((path) => path.structureName)
    .join('/')
    .toLowerCase();
  const struc2Path = struc2.structurePath
    .map((path) => path.structureName)
    .join('/')
    .toLowerCase();
  if (struc1Path === struc2Path) {
    if (struc1.name === struc2.name) return 0;
    if (struc1.name > struc2.name) return 1;
    return -1;
  }
  if (struc1Path > struc2Path) return 1;
  return -1;
};

const StructureSelectAutoComplete = ({
  flatData,
  loading,
  onChange,
  searchText,
  noOptionsText,
  onInputChange,
  disabled = false,
  renderInput,
  ...rest
}) => {
  return (
    <AutoComplete
      options={flatData}
      noOptionsText={noOptionsText}
      filterOptions={filterOptions}
      loading={loading}
      getOptionLabel={(option) => option.name}
      renderOption={(props, option) => {
        let parent = '';
        if (option.parentId) {
          parent = option.structurePath.map((struc) => struc.structureName).join('/');
        }
        return (
          <div
            key={option.id}
            {...props}
            style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}
          >
            <div>{option.name}</div>
            {!!option.parentId && <div style={{ fontSize: 10, color: 'grey', fontStyle: 'italic' }}>{parent}</div>}
          </div>
        );
      }}
      onChange={onChange}
      inputValue={searchText}
      onInputChange={onInputChange}
      disabled={disabled}
      // getOptionSelected={(opt, val) => opt._id === val._id}
      style={{ width: 500 }}
      renderInput={renderInput}
      {...rest}
    />
  );
};

StructureSelectAutoComplete.propTypes = {
  flatData: PropTypes.arrayOf(PropTypes.any).isRequired,
  loading: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  onInputChange: PropTypes.func.isRequired,
  searchText: PropTypes.string.isRequired,
  renderInput: PropTypes.func.isRequired,
  noOptionsText: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
};

export default StructureSelectAutoComplete;
