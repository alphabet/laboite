import React from 'react';
import i18n from 'meteor/universe:i18n';
import { makeStyles } from 'tss-react/mui';
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';
import InputAdornment from '@mui/material/InputAdornment';

import PropTypes from 'prop-types';
import { useAppContext } from '../../contexts/context';

const LanguageSwitcher = ({ topbar = false, relative = false }) => {
  const allLanguages = i18n.getLanguages();
  const [{ language }, dispatch] = useAppContext();
  const T = i18n.createComponent('languages');
  const switchLanguage = (lan) => {
    document.documentElement.setAttribute('lang', lan);
    dispatch({ type: 'language', data: { language: lan } });
  };

  const useStyles = makeStyles()(() => ({
    switcher: {
      width: topbar || relative ? '100%' : 'auto',
    },
    flag: {
      height: 15,
    },
  }));
  const { classes } = useStyles();
  const flag = (
    <img
      alt={`flag for ${i18n.getLanguageNativeName(language)}`}
      className={classes.flag}
      src={`/images/i18n/${language}.png`}
    />
  );
  return (
    <>
      <TextField
        InputProps={{
          startAdornment: <InputAdornment position="start">{flag}</InputAdornment>,
        }}
        defaultValue={language}
        variant="outlined"
        label="Choix de la langue"
        select
        className={classes.switcher}
      >
        {allLanguages.map((lan) => (
          <MenuItem key={lan} value={lan} onClick={() => switchLanguage(lan)}>
            <T>{lan}</T>
          </MenuItem>
        ))}
      </TextField>
    </>
  );
};

export default LanguageSwitcher;

LanguageSwitcher.propTypes = {
  topbar: PropTypes.bool,
  relative: PropTypes.bool,
};
