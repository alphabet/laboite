import React from 'react';
import i18n from 'meteor/universe:i18n';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';

const Transition = React.forwardRef((props, ref) => <Slide direction="up" ref={ref} {...props} />);

function UserMotivationDialog({ open, motivation = '', onClose }) {
  return (
    <Dialog
      open={open}
      onClose={onClose}
      TransitionComponent={Transition}
      keepMounted
      aria-labelledby="motivation-dialog-slide-title"
      aria-describedby="motivation-dialog-slide-description"
    >
      <DialogTitle id="motivation-dialog-slide-title">
        {i18n.__('components.UserMotivationDialog.dialogTitle')}
      </DialogTitle>
      <DialogContent>
        <DialogContentText sx={{ whiteSpace: 'pre-wrap' }} id="motivation-dialog-slide-description">
          {motivation}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">
          {i18n.__('components.UserMotivationDialog.buttonClose')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

UserMotivationDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  motivation: PropTypes.string.isRequired,
};

export default UserMotivationDialog;
