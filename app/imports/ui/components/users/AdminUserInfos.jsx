import React from 'react';
import Card from '@mui/material/Card';
import i18n from 'meteor/universe:i18n';
import moment from 'moment';
import { makeStyles } from 'tss-react/mui';
import CardActions from '@mui/material/CardActions';
import CardHeader from '@mui/material/CardHeader';
import ClearIcon from '@mui/icons-material/Clear';
import IconButton from '@mui/material/IconButton';
import Modal from '@mui/material/Modal';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { useAppContext } from '../../contexts/context';
import COMMON_STYLES from '../../themes/styles';
import { nextInstance } from '../../../api/utils';

const useStyles = makeStyles()((theme, isMobile) => ({
  root: COMMON_STYLES.root,
  actions: COMMON_STYLES.actions,
  paper: COMMON_STYLES.paper(isMobile, '50%'),
}));

const AdminUserInfos = ({ data, open, onClose }) => {
  const [{ isMobile }] = useAppContext();
  const { classes } = useStyles(isMobile);
  return (
    <Modal open={open} onClose={onClose}>
      <div className={classes.paper}>
        <Card className={classes.root}>
          <CardHeader
            title={`${i18n.__('components.AdminUserInfos.subtitle')} ${data.firstName} ${data.lastName}`}
            action={
              <IconButton onClick={onClose} size="large">
                <ClearIcon />
              </IconButton>
            }
          />
          <CardContent>
            <Typography>
              <b>{i18n.__('components.AdminUserInfos.username')}</b> {data.username}
            </Typography>
            {data.emails?.length > 0 && (
              <Typography>
                <b>{i18n.__('components.AdminUserInfos.email')}</b> {data.emails[0].address}
              </Typography>
            )}
            <Typography>
              <b>{i18n.__('components.AdminUserInfos.createdAt')}</b> {moment(data.createdAt).format('LLL')}
            </Typography>
            <Typography>
              <b>{i18n.__('components.AdminUserInfos.activation')} </b>
              {data.activatedAt
                ? i18n.__('components.AdminUserInfos.validatedAt', {
                    At: moment(data.activatedAt).format('LLL'),
                    ByUsername: data.activatedBy.username,
                    ByEmail: data.activatedBy.email,
                  })
                : data.isActive === true
                ? i18n.__('components.AdminUserInfos.active')
                : i18n.__('components.AdminUserInfos.inactive')}
            </Typography>
            {data.lastLogin && (
              <Typography>
                <b>{i18n.__('components.AdminUserInfos.lastLogin')}</b> {moment(data.lastLogin).format('LLL')}
              </Typography>
            )}
            {data.nclocator && (
              <Typography>
                <b>{i18n.__('components.AdminUserInfos.nclocator')}</b> {data.username}@{nextInstance(data.nclocator)}
              </Typography>
            )}
            <Typography>
              <b>{i18n.__('components.AdminUserInfos.groupsCreated')}</b> {data.groupCount}/{data.groupQuota}
            </Typography>
            {data.phoneNumber && (
              <Typography>
                <b>{i18n.__('pages.ProfilePage.phone')}:</b> {data.phoneNumber}
              </Typography>
            )}
            {data.location && (
              <Typography>
                <b>{i18n.__('pages.ProfilePage.location')}:</b> {data.location}
              </Typography>
            )}
            {data.jobTitle && (
              <Typography>
                <b>{i18n.__('pages.ProfilePage.jobTitle')}:</b> {data.jobTitle}
              </Typography>
            )}
            {data.profileStatus && (
              <Typography>
                <b>{i18n.__('api.users.labels.profileStatus')}:</b> {data.profileStatus}
              </Typography>
            )}
            {data.accountType && (
              <Typography>
                <b>{i18n.__('api.users.labels.accountType')}:</b> {data.accountType}
              </Typography>
            )}
            {data.lifeCycleDates?.departure && (
              <Typography>
                <b>{i18n.__('api.users.labels.lifeCycleDates.departure')}:</b>{' '}
                {data.lifeCycleDates.departure.toLocaleDateString()}
              </Typography>
            )}
            {data.lifeCycleDates?.firstMail && (
              <Typography>
                <b>{i18n.__('api.users.labels.lifeCycleDates.firstMail')}:</b>{' '}
                {data.lifeCycleDates.firstMail.toLocaleDateString()}
              </Typography>
            )}
            {data.lifeCycleDates?.desactivate && (
              <Typography>
                <b>{i18n.__('api.users.labels.lifeCycleDates.desactivate')}:</b>{' '}
                {data.lifeCycleDates.desactivate.toLocaleDateString()}
              </Typography>
            )}
            {data.lifeCycleDates?.deletion && (
              <Typography>
                <b>{i18n.__('api.users.labels.lifeCycleDates.deletion')}:</b>{' '}
                {data.lifeCycleDates.deletion.toLocaleDateString()}
              </Typography>
            )}
          </CardContent>
          <CardActions className={classes.actions}>
            <Button onClick={onClose}>{i18n.__('components.AdminUserInfos.close')}</Button>
          </CardActions>
        </Card>
      </div>
    </Modal>
  );
};

AdminUserInfos.propTypes = {
  data: PropTypes.objectOf(PropTypes.any).isRequired,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default AdminUserInfos;
