import { Meteor } from 'meteor/meteor';
import { createSettings } from '../../../api/contextsettings/server/methods';

const newSettings = createSettings();
Object.keys(newSettings).forEach((el) => {
  if (!Meteor.settings[el]) {
    Meteor.settings[el] = {};
  }
  Object.assign(Meteor.settings[el], newSettings[el]);
});
