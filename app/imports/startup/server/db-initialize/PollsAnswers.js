import PollsAnswers from '../../../api/polls_answers/polls_answers';
import logServer, { levels, scopes } from '../../../api/logging';

if (Meteor.isDevelopment) {
  const fakePollAnswers = [
    {
      _id: 'mqk978XJScsBAFM47',
      email: 'yo@yo.com',
      pollId: 'T8ttjMCLsQGW8ES2g',
      choices: [
        {
          date: new Date('2024-03-30T23:00:00.000Z'),
          slots: ['20:15'],
          present: false,
        },
        {
          date: new Date('2024-04-14T22:00:00.000Z'),
          slots: [],
          present: false,
        },
      ],
      confirmed: false,
      createdAt: new Date('2024-03-21T09:56:31.018Z'),
      meetingSlot: [],
      name: 'yo 31 à 20h15',
      updatedAt: new Date('2024-03-21T09:56:31.018Z'),
      userId: null,
    },
    {
      _id: 'TuyF4qurMqui9fEuA',
      email: 'yo2@yo.com',
      pollId: 'T8ttjMCLsQGW8ES2g',
      choices: [
        {
          date: new Date('2024-03-30T23:00:00.000Z'),
          slots: [],
          present: false,
        },
        {
          date: new Date('2024-04-14T22:00:00.000Z'),
          slots: ['09:00', '10:45'],
          present: false,
        },
      ],
      confirmed: false,
      createdAt: new Date('2024-03-21T09:57:20.715Z'),
      meetingSlot: [],
      name: 'yo les 2 15',
      updatedAt: new Date('2024-03-21T09:57:20.716Z'),
      userId: null,
    },
    {
      _id: 'BXiehK5ATFm66FrCY',
      email: 'yo3@yo.com',
      pollId: 'T8ttjMCLsQGW8ES2g',
      choices: [
        {
          date: new Date('2024-03-30T23:00:00.000Z'),
          slots: ['15:30'],
          present: false,
        },
        {
          date: new Date('2024-04-14T22:00:00.000Z'),
          slots: ['09:00'],
          present: false,
        },
      ],
      confirmed: false,
      createdAt: new Date('2024-03-21T10:02:04.839Z'),
      meetingSlot: [],
      name: 'yo 31 à 15h30 et 15 à 9h',
      updatedAt: new Date('2024-03-21T10:02:04.839Z'),
      userId: null,
    },
    {
      _id: '5WTrBzkEZEiTbjFmR',
      email: 'yo@yo.com',
      pollId: 'ZceLcYkCXREBMyjXt',
      choices: [
        {
          date: new Date('2024-04-01T22:00:00.000Z'),
          slots: [],
          present: true,
        },
        {
          date: new Date('2024-04-16T22:00:00.000Z'),
          slots: [],
          present: false,
        },
        {
          date: new Date('2024-04-18T22:00:00.000Z'),
          slots: [],
          present: false,
        },
      ],
      confirmed: false,
      createdAt: new Date('2024-03-21T10:11:20.816Z'),
      meetingSlot: [],
      name: 'yo le 2',
      updatedAt: new Date('2024-03-21T10:11:20.816Z'),
      userId: null,
    },
    {
      _id: 'DPhgEWPAihcSfr54C',
      email: 'yo2@yo.com',
      pollId: 'ZceLcYkCXREBMyjXt',
      choices: [
        {
          date: new Date('2024-04-01T22:00:00.000Z'),
          slots: [],
          present: false,
        },
        {
          date: new Date('2024-04-16T22:00:00.000Z'),
          slots: [],
          present: true,
        },
        {
          date: new Date('2024-04-18T22:00:00.000Z'),
          slots: [],
          present: true,
        },
      ],
      confirmed: false,
      createdAt: new Date('2024-03-21T10:11:50.889Z'),
      meetingSlot: [],
      name: 'yo les 17 et 19',
      updatedAt: new Date('2024-03-21T10:11:50.889Z'),
      userId: null,
    },
  ];

  /** When running app for first time, pass a settings file to set up default groups. */
  logServer('step 1 - DROP POLLS_ANSWERS');
  PollsAnswers.remove({});
  logServer(`STARTUP - POLLS_ANSWERS - CREATE - Creating the default polls_answers.`, levels.INFO, scopes.SYSTEM, {});
  fakePollAnswers.forEach((pa) => {
    try {
      PollsAnswers.insert({
        _id: pa._id,
        email: pa.email,
        pollId: pa.pollId,
        choices: pa.choices,
        confirmed: pa.confirmed,
        createdAt: pa.createdAt,
        meetingSlot: pa.meetingSlot,
        name: pa.name,
        updateAt: pa.updateAt,
        userId: pa.userId,
      });
    } catch (error) {
      logServer(
        `STARTUP - POLLS - ERROR - Error creating poll_answers: ${error.reason || error.message || error}`,
        levels.INFO,
        scopes.SYSTEM,
        {},
      );
    }
  });
}
