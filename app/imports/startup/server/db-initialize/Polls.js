import Polls from '../../../api/polls/polls';
import logServer, { levels, scopes } from '../../../api/logging';

if (Meteor.isDevelopment) {
  const sondages = [
    {
      _id: 'T8ttjMCLsQGW8ES2g',
      dates: [
        {
          date: new Date('2024-03-30T23:00:00.000Z'),
          slots: ['15:30', '20:15'],
        },
        {
          date: new Date('2024-04-14T22:00:00.000Z'),
          slots: ['09:00', '10:45'],
        },
      ],
      title: 'Nouveau sondage',
      description: 'yooo',
      groups: [],
      meetingSlots: [],
      duration: '00:30',
      public: true,
      hideParticipantsList: false,
      allDay: false,
      type: 'POLL',
      active: true,
      completed: false,
      userId: '4Wyr9onZwcHhe4kvJ',
      createdAt: new Date('2024-03-21T09:51:55.582Z'),
      updatedAt: new Date('2024-03-21T09:51:55.582Z'),
    },
    {
      _id: 'ZceLcYkCXREBMyjXt',
      dates: [
        { date: new Date('2024-04-01T22:00:00.000Z'), slots: [] },
        { date: new Date('2024-04-16T22:00:00.000Z'), slots: [] },
        { date: new Date('2024-04-18T22:00:00.000Z'), slots: [] },
      ],
      title: 'Nouveau sondage allDay',
      description: 'yoo',
      groups: [],
      meetingSlots: [],
      duration: '00:30',
      public: true,
      hideParticipantsList: false,
      allDay: true,
      type: 'POLL',
      active: true,
      completed: false,
      userId: '4Wyr9onZwcHhe4kvJ',
      createdAt: new Date('2024-03-21T10:10:42.342Z'),
      updatedAt: new Date('2024-03-21T10:10:45.613Z'),
    },
  ];
  /** When running app for first time, pass a settings file to set up default groups. */
  logServer('step 1 - DROP POLLS');
  Polls.remove({});
  logServer(`STARTUP - POLLS - CREATE - Creating the default polls.`, levels.INFO, scopes.SYSTEM, {});
  sondages.forEach((el) => {
    try {
      Polls.insert({
        _id: el._id,
        dates: el.dates,
        title: el.title,
        groups: el.groups,
        meetingSlots: el.meetingSlots,
        duration: el.duration,
        public: el.public,
        hideParticipantsList: el.hideParticipantsList,
        allDay: el.allDay,
        type: el.type,
        active: el.active,
        completed: el.completed,
        userId: el.userId,
        createdAt: el.createdAt,
        updatedAt: el.updatedAt,
      });
    } catch (error) {
      logServer(
        `STARTUP - POLLS - ERROR - Error creating poll: ${error.reason || error.message || error}`,
        levels.INFO,
        scopes.SYSTEM,
        {},
      );
    }
  });
}
