import { Meteor } from 'meteor/meteor';
import { data } from '../../../api/contextsettings/defaultValue.contextSettings';
import ContextSettings from '../../../api/contextsettings/contextsettings';
import logServer, { levels, scopes } from '../../../api/logging';
import { get } from '../../../api/utils';

/** When running app for first time, pass a settings file to set up default settings. */
if (ContextSettings.find().count() === 0) {
  logServer(`STARTUP - SETTINGS - CREATE - Creating the default settings.`, levels.INFO, scopes.SYSTEM, {});
  data.forEach((el) => {
    // get value from current meteor settings
    let currentValue = get(Meteor.settings, el.key);
    // if not found, check in public settings
    if (currentValue === undefined) currentValue = get(Meteor.settings, `public.${el.key}`);
    // if not found, check in private settings
    if (currentValue === undefined) currentValue = get(Meteor.settings, `private.${el.key}`);
    // use default value if setting is not set
    if (currentValue === undefined) currentValue = el.value;
    // convert value to array if needed
    if (!Array.isArray(currentValue)) currentValue = [currentValue];
    try {
      ContextSettings.insert({
        key: el.key,
        type: el.type,
        value: currentValue,
      });
    } catch (error) {
      logServer(
        `STARTUP - SETTINGS - ERROR - Error creating settings: ${error.reason || error.message || error}`,
        levels.INFO,
        scopes.SYSTEM,
        {},
      );
    }
  });
} else if (ContextSettings.find().count() > 0) {
  data.forEach((el) => {
    if (ContextSettings.find({ key: el.key }).fetch().length === 0) {
      try {
        ContextSettings.insert({
          key: el.key,
          type: el.type,
          value: el.value,
        });
      } catch (error) {
        logServer(
          `STARTUP - SETTINGS - ERROR - Error inserting new setting(s): ${error.reason || error.message || error}`,
          levels.INFO,
          scopes.SYSTEM,
          {},
        );
      }
    } else if (ContextSettings.find({ key: el.key }).fetch()[0].type !== el.type) {
      try {
        ContextSettings.update({ key: el.key }, { $set: { type: el.type } });
      } catch (error) {
        logServer(
          `STARTUP - SETTINGS - ERROR - Error updating setting type: ${error.reason || error.message || error}`,
          levels.INFO,
          scopes.SYSTEM,
          {},
        );
      }
    }
  });
} else {
  logServer(`STARTUP - SETTINGS - Infos already initialized:`, levels.INFO, scopes.SYSTEM, {});
}
