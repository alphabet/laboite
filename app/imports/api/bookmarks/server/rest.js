import { Meteor } from 'meteor/meteor';
import logServer, { levels, scopes } from '../../logging';
import Groups from '../../groups/groups';
import { _createBookmarkUrl } from '../methods';
import Bookmarks from '../bookmarks';

function CreateBookmarkFromAPI(bookmark) {
  const user = Meteor.users.findOne({ username: bookmark.author });
  const group = Groups.findOne({ name: bookmark.group });
  if (user && group) {
    try {
      _createBookmarkUrl(bookmark.url, bookmark.name, bookmark.tag, group._id, user._id);
      return 200;
    } catch (error) {
      error.statusCode = 409;
      throw error;
    }
  }
  if (!user) {
    logServer(`API - BOOKMARKS - POST - User not found`, levels.ERROR, scopes.SYSTEM, {
      user: bookmark.author,
    });
    const error = new Meteor.Error('api.CreateBookmarkFromAPI.userNotFound', `User not found (${bookmark.author})`);
    error.statusCode = 404;
    throw error;
  }
  if (!group) {
    logServer(`API - BOOKMARKS - POST - Group not found`, levels.ERROR, scopes.SYSTEM, {
      group: bookmark.group,
    });
    const error = new Meteor.Error('api.CreateBookmarkFromAPI.groupNotFound', `Group not found (${bookmark.group})`);
    error.statusCode = 404;
    throw error;
  }
}

export async function createOneBookmark(req, content) {
  // sample use:
  // curl -X POST -H "X-API-KEY: createuser-password" \
  //      -H "Content-Type: application/json" \
  //      -d '{"url":"http:\/\/url.com", "name":"bookmark1", "tag":"tag", "group": "groupe1", "author":"username1"}'
  //      http://localhost:3000/api/bookmarks/create

  // eslint-disable-next-line spaced-comment
  // curl -X POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"url":"https://portail.eole3.dev", "name":"bookmark1", "tag":"tag", "group": "groupe1", "author":"admin4"}' http://localhost:3000/api/bookmarks/create

  if (content) {
    return CreateBookmarkFromAPI(content);
  }
  const error = new Meteor.Error('api.createOneBookmark.emptyContent', 'Empty content');
  error.statusCode = 409;
  throw error;
}

function generateDataForBookmark(bookmark) {
  const gr = Groups.findOne({ _id: bookmark.groupId });
  if (gr) {
    const author = Meteor.users.findOne({ _id: bookmark.author });
    if (author) {
      const obj = {
        name: bookmark.name,
        tag: bookmark.tag,
        group: gr.name,
        author: author.username,
        url: bookmark.url,
      };
      return obj;
    }
    return null;
  }
  return null;
}

// sample use:
// curl -X  POST -H "X-API-KEY: createuser-password" \
//      -H "Content-Type: application/json" \
//      -d '{"group": "groupName", "url":"http:\/\/test.com" }' \
//      http://localhost:3000/api/bookmarks
// curl -X  POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"group": "groupName", "url":"http:\/\/test.com" }' http://localhost:3000/api/bookmarks
export async function getBookmarks(req, content) {
  if ('group' in content && 'url' in content) {
    const group = Groups.findOne({ name: content.group });
    if (group) {
      const bookmark = Bookmarks.findOne({ $and: [{ groupId: group._id }, { url: content.url }] });
      if (bookmark) {
        const obj = generateDataForBookmark(bookmark);
        return obj;
      }
      const error = new Meteor.Error('api.getBookmarks.notFound', 'Bookmark not found');
      error.statusCode = 404;
      throw error;
    }
    const error = new Meteor.Error('api.getBookmarks.groupNotFound', 'Group not found');
    error.statusCode = 404;
    throw error;
  }
  const error = new Meteor.Error('api.getBookmarks.emptyContent', 'Empty or invalid content');
  error.statusCode = 409;
  throw error;
}

// sample use:
// curl -H "X-API-KEY: createuser-password" http://localhost:3000/api/bookmarks | jq

export async function getAllBookmarks() {
  const bookmarks = Bookmarks.find().fetch();
  const res = [];
  if (bookmarks) {
    bookmarks.forEach((bookmark) => {
      if (bookmark.groupId) {
        const obj = generateDataForBookmark(bookmark);
        if (obj) {
          res.push(obj);
        }
      }
    });
  }
  return res;
}

export async function deleteBookmark(req, content) {
  // sample use:
  // curl -X DELETE -H "X-API-KEY: createuser-password" \
  //      -H "Content-Type: application/json" \
  //      -d '{"group": "groupName", "url":"http:\/\/url.com"}',
  //      http://localhost:3000/api/bookmarks/delete

  // eslint-disable-next-line spaced-comment
  // curl -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"group": "groupName", "url":"http:\/\/url.com"}' http://localhost:3000/api/bookmarks/delete

  if ('group' in content && 'url' in content) {
    const group = Groups.findOne({ name: content.group });
    if (group) {
      const bookmark = Bookmarks.findOne({ $and: [{ groupId: group._id }, { url: content.url }] });
      if (bookmark) {
        Bookmarks.remove({ _id: bookmark._id });
        return 200;
      }
      logServer(`API - BOOKMARKS - DELETE - Bookmark not found`, levels.ERROR, scopes.SYSTEM, {
        group: content.group,
      });
      const error = new Meteor.Error('api.deleteBookmarks.noFound', 'Bookmark not found');
      error.statusCode = 404;
      throw error;
    }
    const error = new Meteor.Error('api.deleteBookmarks.groupNotFound', 'Group not found');
    error.statusCode = 404;
    throw error;
  }
  const error = new Meteor.Error('api.deleteBookmarks.emptyContent', 'Empty content');
  error.statusCode = 409;
  throw error;
}
