import { Meteor } from 'meteor/meteor';
import i18n from 'meteor/universe:i18n';
import { v4 as uuidv4 } from 'uuid';
import logServer, { levels, scopes } from '../logging';

let apiToken = null;

function _getToken() {
  if (!apiToken) apiToken = uuidv4();
  return apiToken;
}

export function checkApiToken(funcName, data, token) {
  if (!token || token !== _getToken()) {
    logServer(
      `USERS - METHODS - METEOR ERROR - ${funcName} - ${i18n.__('api.users.notPermitted')}`,
      levels.ERROR,
      scopes.SYSTEM,
      data,
    );
    throw new Meteor.Error(`api.hooks.${funcName}.notPermitted`, i18n.__('api.users.notPermitted'));
  }
}

export function getToken() {
  return _getToken();
}
