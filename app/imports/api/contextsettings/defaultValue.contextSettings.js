/* eslint-disable max-len */
export const data = [
  {
    key: 'public.jobInformation.enable',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.jobInformation.urlCGU',
    type: 'string',
    value: [''],
  },
  {
    key: 'public.jobInformation.location',
    type: 'textArea',
    value: [
      'CIO',
      'Mission locale',
      'DSDEN',
      'Rectorat',
      'Conseil régional',
      'Conseil départemental',
      'Ministère Education Nationale',
      'Ministère Agriculture',
      'Ministère Travail',
      'UNML/ARML',
      'Autre collectivité territoriale',
      'Etablissement scolaire Education nationale',
      'Etablissement scolaire Enseignement agricole',
      'CFA',
      'EPIDE',
      'E2C',
      'AFPA',
      'Autre ministère ou organisation',
    ],
  },
  {
    key: 'public.jobInformation.jobTitle',
    type: 'textArea',
    value: [
      'Chargé de mission',
      'Chercheur',
      'Commissaire lutte contre pauvreté',
      'Conseiller mission locale',
      'Coordonnateur academique MLDS',
      'Coordonnateur départemental MLDS',
      'Coordonnateur MIJEC',
      'Coordonnateur MLDS de bassin/district',
      'Coordonnateur région academique MLDS',
      'CPE',
      'Directeur de CIO, co-responsables de PSAD',
      'Directeur de mission locale, co-responsable de PSAD',
      'DRAAF/DRIAAF',
      'DRAIO',
      'DRAIO adjoint/CSAIO/CSAIO adjoint',
      'DREETS',
      'Enseignant/Formateur',
      'IA-DASEN/IA-DAASEN',
      'IA-IPR',
      'IEN ET-EG',
      'IEN-IO',
      'Inspecteur général',
      'Partenaire',
      'Personnel administratif CIO',
      'Personnel administratif DSDEN',
      'Personnel administratif RECTORAT',
      'Personnel/prestataire Conseil Régional',
      'Pilote national ministère Agriculture',
      'Pilote national ministère Education nationale',
      'Pilote national ministère Travail',
      'Pilote national UNML',
      'Pilote régional ARML',
      'Président Conseil Départemental ou son représentant',
      'Président Conseil Régional ou son représentant',
      'Psy-EN',
      "Recteur d'académie",
      'Recteur région academique',
      'Référent obligation de formation en mission locale',
      'Autre fonction',
    ],
  },
  {
    key: 'public.appName',
    type: 'string',
    value: ['LaBoîte'],
  },
  {
    key: 'public.appDescription',
    type: 'string',
    value: [''],
  },
  {
    key: 'public.offlinePage',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.enableBBB',
    type: 'boolean',
    value: [true],
  },
  {
    key: 'public.BBBUrl',
    type: 'string',
    value: [''],
  },
  {
    key: 'public.imageFilesTypes',
    type: 'textArea',
    value: ['svg', 'png', 'jpg', 'gif', 'jpeg'],
  },
  {
    key: 'public.audioFilesTypes',
    type: 'textArea',
    value: ['wav', 'mp3', 'ogg'],
  },
  {
    key: 'public.videoFilesTypes',
    type: 'textArea',
    value: ['mp4', 'webm', 'avi', 'wmv'],
  },
  {
    key: 'public.textFilesTypes',
    type: 'textArea',
    value: ['pdf', 'odt', 'txt', 'docx'],
  },
  {
    key: 'public.otherFilesTypes',
    type: 'textArea',
    value: ['csv'],
  },
  {
    key: 'public.minioFileSize',
    type: 'number',
    value: [500000],
  },
  {
    key: 'public.minioStorageFilesSize',
    type: 'number',
    value: [3000000],
  },
  {
    key: 'public.maxMinioDiskPerUser',
    type: 'number',
    value: [10000000],
  },
  {
    key: 'public.theme',
    type: 'string',
    value: ['eole'],
  },
  {
    key: 'public.hideGroupPlugins',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.keycloackPopupStyle',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.keycloackPopupStyleIframe',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.loginWithTokenUrls',
    type: 'string',
    value: ['http://localhost'],
  },
  {
    key: 'public.disabledFeatures.blog',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.disabledFeatures.groups',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.disabledFeatures.notificationsTab',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.disabledFeatures.introductionTab',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.disabledFeatures.menuAdminDefaultSpaces',
    type: 'boolean',
    value: [true],
  },
  {
    key: 'public.disabledFeatures.mediaStorageMenu',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.disabledFeatures.bookmarksFromClipboard',
    type: 'boolean',
    value: [true],
  },
  {
    key: 'public.disabledFeatures.analytics',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.disabledFeatures.usersExport',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.disabledFeatures.structure',
    type: 'boolean',
    value: [false],
    responsability: 'admin',
  },
  {
    key: 'public.onBoarding.enabled',
    type: 'boolean',
    value: [true],
  },
  {
    key: 'public.onBoarding.appDesignation',
    type: 'string',
    value: ['Laboîte'],
  },
  {
    key: 'public.ui.defaultGridViewMode',
    type: 'string',
    value: ['compact'],
  },
  {
    key: 'public.ui.isBusinessRegroupingMode',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.ui.feedbackLink',
    type: 'string',
    value: [''],
  },
  {
    key: 'public.matomo.siteId',
    type: 'string',
    value: [null],
  },
  {
    key: 'public.matomo.urlBase',
    type: 'string',
    value: [null],
  },
  {
    key: 'public.NotificationsExpireDays.setRole',
    type: 'number',
    value: [null],
  },
  {
    key: 'public.NotificationsExpireDays.unsetRole',
    type: 'number',
    value: [null],
  },
  {
    key: 'public.NotificationsExpireDays.request',
    type: 'number',
    value: [null],
  },
  {
    key: 'public.NotificationsExpireDays.group',
    type: 'number',
    value: [null],
  },
  {
    key: 'public.NotificationsExpireDays.default',
    type: 'number',
    value: [null],
  },
  {
    key: 'public.groupPlugins.rocketChat.enable',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.groupPlugins.rocketChat.URL',
    type: 'string',
    value: [''],
  },
  {
    key: 'public.groupPlugins.rocketChat.groupURL',
    type: 'string',
    value: [''],
  },
  {
    key: 'public.groupPlugins.rocketChat.enableChangeName',
    type: 'boolean',
    value: [true],
  },
  {
    key: 'public.groupPlugins.nextcloud.enable',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.groupPlugins.nextcloud.URL',
    type: 'string',
    value: ['https://nuage03.pp.appseducation.org'],
  },
  {
    key: 'public.groupPlugins.nextcloud.groupURL',
    type: 'string',
    value: ['[NCLOCATOR]/apps/files/?dir=/[NCSHARENAME]'],
  },
  {
    key: 'public.groupPlugins.nextcloud.enableChangeName',
    type: 'boolean',
    value: [true],
  },
  {
    key: 'public.services.agendaUrl',
    type: 'string',
    value: ['http://localhost:3030'],
  },
  {
    key: 'public.services.sondagesUrl',
    type: 'string',
    value: ['http://localhost:3010'],
  },
  {
    key: 'public.services.mezigUrl',
    type: 'string',
    value: ['http://localhost:3020'],
  },
  {
    key: 'public.services.questionnaireURL',
    type: 'string',
    value: ['http://localhost:3060'],
  },
  {
    key: 'public.services.laboiteBlogURL',
    type: 'string',
    value: ['http://localhost:4000'],
  },
  {
    key: 'public.services.rendezVousUrl',
    type: 'string',
    value: ['http://localhost:3010'],
  },
  {
    key: 'public.services.questionnaire.dataDeletionDelay',
    type: 'number',
    value: 90,
  },
  {
    key: 'public.services.questionnaire.defaultFormExpirationDelay',
    type: 'number',
    value: 60,
  },
  {
    key: 'nextcloud.nextcloudUser',
    type: 'string',
    value: [''],
  },
  {
    key: 'nextcloud.nextcloudPassword',
    type: 'password',
    value: [''],
  },
  {
    key: 'nextcloud.circlesUser',
    type: 'string',
    value: [''],
  },
  {
    key: 'nextcloud.circlesPassword',
    type: 'password',
    value: [''],
  },
  {
    key: 'nextcloud.nextcloudApiKeys',
    type: 'textArea',
    value: [''],
  },
  {
    key: 'nextcloud.sessionTokenKey',
    type: 'password',
    value: [''],
  },
  {
    key: 'nextcloud.sessionTokenAppName',
    type: 'string',
    value: [''],
  },
  {
    key: 'rocketChat.rocketChatUser',
    type: 'string',
    value: [''],
  },
  {
    key: 'rocketChat.rocketChatPassword',
    type: 'password',
    value: [''],
  },
  {
    key: 'private.fillWithFakeData',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'private.apiKeys',
    type: 'textArea',
    value: [''],
  },
  {
    key: 'private.BBBSecret',
    type: 'string',
    value: [''],
    responsability: 'admin',
  },
  {
    key: 'private.whiteDomains',
    type: 'textArea',
    value: ['^ac-[a-z-]_\\.fr', '^[a-z-]_\\.gouv.fr'],
  },
  {
    key: 'private.cspFrameAncestors',
    type: 'textArea',
    value: ["'self'"],
  },
  {
    key: 'private.createUserTokenApiKeys',
    type: 'textArea',
    value: [''],
    responsability: 'admin',
  },
  {
    key: 'private.createUserApiKeys',
    type: 'textArea',
    value: [''],
    responsability: 'admin',
  },
  {
    key: 'smtp.url',
    type: 'password',
    value: ['smtps://USERNAME:PASSWORD@HOST:PORT'],
  },
  {
    key: 'smtp.fromEmail',
    type: 'string',
    value: [''],
  },
  {
    key: 'smtp.toEmail',
    type: 'string',
    value: [''],
  },
  {
    key: 'public.analyticsExpirationInDays',
    type: 'number',
    value: [24],
  },
  {
    key: 'public.disabledFeatures.franceTransfert',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'franceTransfert.apiKey',
    type: 'password',
    value: [''],
  },
  {
    key: 'franceTransfert.endpoint',
    type: 'string',
    value: [''],
  },
  {
    key: 'countly.consent',
    type: 'boolean',
    value: [true],
  },
  {
    key: 'countly.appKey',
    type: 'password',
    value: [''],
  },
  {
    key: 'countly.url',
    type: 'string',
    value: ['www.countly.url.fr'],
  },
  {
    key: 'private.loginExpirationInDays',
    type: 'number',
    value: [1],
  },
  {
    key: 'private.checkKeyCloakWhiteListDomain',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.forceRedirectToPersonalSpace',
    type: 'boolean',
    value: [true],
  },
  {
    key: 'public.defaultGroupQuota',
    type: 'number',
    value: [10],
    responsability: 'admin',
  },
  {
    key: 'public.ui.defaultGroupsViewMode',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.disabledFeatures.getApiToken',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'public.skipSignInPage',
    type: 'boolean',
    value: [false],
  },
  {
    key: 'private.usernameMaxSize',
    type: 'number',
    value: [64],
  },
  {
    key: 'private.usernameAllowedChars',
    type: 'string',
    value: ['[a-z0-9-_]'],
  },
  {
    key: 'private.nextcloudDisplayNameLength',
    type: 'number',
    value: [64],
  },
  {
    key: 'public.maskedAdminList',
    type: 'textArea',
    value: ['admin', 'admapi'],
  },
  {
    key: 'public.mails.firstInactiveAccountWarning',
    type: 'textArea',
    value: [
      `Bonjour {firstname} {lastname}

Vous avez un compte sur la plateforme apps.education.fr. 
Vous recevez ce mail, car vous ne vous êtes pas connecté depuis plus d'une année aux différents services et n'avez pas répondu à notre premier mail.

Pouvez-vous nous signaler que vous êtes toujours utilisateur des services Apps Education en cliquant sur le lien suivant {magiclink}
Passé un délai de 3 mois, vous recevrez un autre rappel avant la suppression de votre compte et de toutes vous publications

Cordialement,

Le service Apps Education
{mailto}`,
    ],
  },
  {
    key: 'public.mails.secondInactiveAccountWarning',
    type: 'textArea',
    value: [
      `Bonjour {firstname} {lastname}

Vous avez un compte sur la plateforme apps.education.fr. 
Vous recevez ce mail, car vous ne vous êtes pas connecté depuis plus d'une année aux différents services et n'avez pas répondu à notre premier mail.

Pouvez-vous nous signaler que vous êtes toujours utilisateur des services Apps Education en cliquant sur le lien suivant {magiclink}
Passé un délai de 3 mois, vous recevrez un autre rappel avant la suppression de votre compte et de toutes vous publications.

Cordialement,

Le service Apps Education
{mailto}`,
    ],
  },
  {
    key: 'public.mails.accountDisabled',
    type: 'textArea',
    value: [
      `Bonjour {firstname} {lastname}

Vous aviez un compte sur la plateforme apps.education.fr. 
Vous recevez ce mail pour vous signaler que votre compte a été désactivé suite aux 2 précédents mails sans réponse.

Cordialement,

Le service Apps Education 
{mailto}
`,
    ],
  },
  {
    key: 'public.mails.departureConfirmed',
    type: 'textArea',
    value: [
      `Bonjour {firstname} {lastname}

Vous aviez un compte sur la plateforme apps.education.fr.

Vous recevez ce mail pour vous signaler que votre compte est désactivé. 
Vos données seront être supprimées dans 30 jours ou transférées à {firstnameDelegatee} {lastnameDelegatee} conformément à votre demande.

Cordialement,

Le service Apps Education 
{mailto}
`,
    ],
  },
  {
    key: 'public.mails.departureConfirmedWithoutDelegatee',
    type: 'textArea',
    value: [
      `Bonjour {firstname} {lastname}

Vous aviez un compte sur la plateforme apps.education.fr.

Vous recevez ce mail pour vous signaler que votre compte est désactivé. 
Vos données seront être supprimées dans 30 jours conformément à votre demande.

Cordialement,

Le service Apps Education 
{mailto}
`,
    ],
  },
];

export const dataMap = {};
data.forEach((element) => {
  dataMap[element.key] = element;
});
