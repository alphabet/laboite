import i18n from 'meteor/universe:i18n';
import logServer, { levels, scopes } from '../../logging';

export async function shutdownNode() {
  // sample use:
  // curl -X GET -H "X-API-KEY: 849b7648-14b8-4154-9ef2-8d1dc4c2b7e9" \
  //      http://localhost:3000/api/shutdown
  logServer(`SYSTEM - SETTINGS - ${i18n.__('api.contextSettings.exitProcess')}`, levels.INFO, scopes.SYSTEM);
  setTimeout(() => {
    process.exit(1);
  }, 2000);
  return 'OK';
}

export async function livenessProbe() {
  // simple endpoint returning "OK" when API is initialized
  // sample use:
  // curl -X GET -H "X-API-KEY: 849b7648-14b8-4154-9ef2-8d1dc4c2b7e9" \
  //      http://localhost:3000/api/liveness
  return 'OK';
}
