import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import i18n from 'meteor/universe:i18n';
import ContextSettings from '../contextsettings';
import { notifyUpdate } from '../../discovery';
import { isActive, set } from '../../utils';
import logServer, { levels, scopes } from '../../logging';
import { dataMap } from '../defaultValue.contextSettings';

export const findSettings = Meteor.methods({
  'contextSetting.findSettings': () => {
    const authorized = isActive(Meteor.userId()) && Roles.userIsInRole(Meteor.userId(), 'admin');
    if (!authorized) {
      logServer(
        `CONTEXTSETTINGS - METHOD - METEOR ERROR - findSettings - ${i18n.__('api.users.adminNeeded')}`,
        levels.ERROR,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.contextSettings.findSettings.notPermitted', i18n.__('api.users.adminNeeded'));
    }
    try {
      return ContextSettings.find().fetch();
    } catch (error) {
      throw new Meteor.Error(error, error);
    }
  },
});

export const updateSettings = Meteor.methods({
  'contextSetting.updateSettings': ({ settings_updated: obj }) => {
    const authorized = isActive(Meteor.userId()) && Roles.userIsInRole(Meteor.userId(), 'admin');
    if (!authorized) {
      logServer(
        `CONTEXTSETTINGS - METHOD - METEOR ERROR - updateSettings - ${i18n.__('api.users.adminNeeded')}`,
        levels.ERROR,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.contextSettings.updateSettings.notPermitted', i18n.__('api.users.adminNeeded'));
    }
    try {
      obj.forEach((param) => {
        ContextSettings.update({ key: param.key }, { $set: { value: param.value } });
      });
    } catch (error) {
      throw new Meteor.Error(error, error);
    }
  },
});

export const stopMeteor = Meteor.methods({
  'contextSetting.stopMeteor': async () => {
    const authorized = isActive(Meteor.userId()) && Roles.userIsInRole(Meteor.userId(), 'admin');
    if (!authorized) {
      logServer(
        `CONTEXTSETTINGS - METHOD - METEOR ERROR - stopMeteor - ${i18n.__('api.users.adminNeeded')}`,
        levels.ERROR,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.contextSettings.stopMeteor.notPermitted', i18n.__('api.users.adminNeeded'));
    }
    const result = await notifyUpdate();
    return result;
  },
});

export const createSettings = () => {
  const dataBase = ContextSettings.find().fetch() || [];
  const checkIfTypeTextArea = (value, type) => {
    if (type !== 'textArea') {
      return value.value[0];
    }
    return value.value;
  };
  const getSettings = (keyLabel) => {
    const defaultValue = dataMap[keyLabel];
    const { type } = dataMap[keyLabel];
    // get data from data-base
    const dataBaseObject = dataBase.filter((el) => el.key === keyLabel);
    // data-base is empty
    if (dataBaseObject.length === 0) {
      return checkIfTypeTextArea(defaultValue, type);
    }
    return checkIfTypeTextArea(dataBaseObject[0], type);
  };
  try {
    const newSettings = {};
    Object.keys(dataMap).forEach((element) => {
      set(newSettings, element, getSettings(element));
    });
    return newSettings;
  } catch (error) {
    throw new Meteor.Error(error, error);
  }
};
