import { Meteor } from 'meteor/meteor';
import { WebApp } from 'meteor/webapp';
import bodyParser from 'body-parser';
import cors from 'cors';
import Rest from 'connect-rest';

import addNotification from './notifications/server/rest';
import getStats from './stats/server/rest';
import { getNcToken, getUserToken } from './nextcloud/server/rest';
import createUser, { notificationKeycloak, setUserStructure } from './users/server/rest';
import {
  addGroupAdmin,
  addGroupAnimator,
  addGroupMember,
  createOneGroup,
  getAllGroups,
  deleteGoup,
  getGroups,
  removeGroupAdmin,
  removeGroupAnimator,
  removeGroupMember,
} from './groups/server/rest';
import ftUploadProxy from './francetransfert/server/rest';
import createUserToken from './users/server/restToken';
import initWidgetApi from './widgetApi';
import { createOneStructure, getAllStructures, deleteStructure, getStructures } from './structures/server/rest';
import {
  createOneStructureRuleDispatch,
  getAllStructureRuleDispatchs,
  getStructureRuleDispatchs,
  deleteStructureRuleDispatch,
} from './asamextensions/server/rest';
import { createOneBookmark, getAllBookmarks, deleteBookmark, getBookmarks } from './bookmarks/server/rest';
import {
  createOneUserBookmark,
  getAllUserBookmarks,
  getUserBookmarks,
  deleteUserBookmark,
} from './userBookmarks/server/rest';
import createService, { getAllServices, getServices } from './services/server/rest';
import { shutdownNode, livenessProbe } from './contextsettings/server/rest';

export default function initRestApi() {
  const unless = (path, middleware) => (req, res, next) => {
    if (path === req.path) {
      return next();
    }
    return middleware(req, res, next);
  };

  // We don't want to parse data for francetransfert proxy
  WebApp.connectHandlers.use(unless('/api/francetransfert/upload', bodyParser.urlencoded({ extended: false })));
  WebApp.connectHandlers.use(unless('/api/francetransfert/upload', bodyParser.json()));
  WebApp.connectHandlers.use('*', cors());
  // // gzip/deflate outgoing responses
  // var compression = require('compression');
  // app.use(compression());

  // Initialize France Transfert proxy
  WebApp.connectHandlers.use('/api/francetransfert/upload', Meteor.bindEnvironment(ftUploadProxy));

  // Initialize REST library
  const options = {
    context: '/api',
    // logger: { file: 'mochaTest.log', level: 'debug' },
    apiKeys: Meteor.settings.private.apiKeys,
    // discover: { path: 'discover', secure: true },
    // proto: { path: 'proto', secure: true }
  };
  const rest = Rest.create(options);

  // adds connect-rest middleware to connect
  WebApp.connectHandlers.use(rest.processRequest());

  // rest.get('/notifications/?userid', Meteor.bindEnvironment(getNotifications));
  rest.post({ path: '/notifications', version: '>=1.0.0' }, Meteor.bindEnvironment(addNotification));
  rest.get({ path: '/stats', version: '>=1.0.0' }, Meteor.bindEnvironment(getStats));

  const ncApiKeys = Meteor.settings.nextcloud.nextcloudApiKeys;
  // specific endpoint and api key for nextcloud token retrieval
  // Only active if at least one API key is defined in config (nextcloud.nextcloudApiKeys)
  if (ncApiKeys && ncApiKeys.length > 0) {
    const restNc = Rest.create({ ...options, apiKeys: ncApiKeys });
    WebApp.connectHandlers.use(restNc.processRequest());
    restNc.post({ path: '/nctoken', version: '>=1.0.0' }, Meteor.bindEnvironment(getNcToken));
  }
  // other endpoint for user token retrieval (using user's laboite authToken)
  const restUnprotected = Rest.create({ ...options });
  WebApp.connectHandlers.use(restUnprotected.processRequest());
  restUnprotected.post(
    { path: '/usertoken', unprotected: true, version: '>=1.0.0' },
    Meteor.bindEnvironment(getUserToken),
  );

  const cuApiKeys = Meteor.settings.private.createUserApiKeys;
  // specific endpoint and api key for user creation
  // Only active if at least one API key is defined in config (private.createUserApiKeys)

  if (cuApiKeys && cuApiKeys.length > 0) {
    const restCu = Rest.create({ ...options, apiKeys: cuApiKeys });
    WebApp.connectHandlers.use(restCu.processRequest());

    // User
    restCu.post({ path: '/createuser', version: '>=1.0.0' }, Meteor.bindEnvironment(createUser));
    restCu.post({ path: '/notificationkeycloak', version: '>=1.0.0' }, Meteor.bindEnvironment(notificationKeycloak));
    restCu.post({ path: '/users/setstructure', version: '>=1.0.0' }, Meteor.bindEnvironment(setUserStructure));

    // Groups
    restCu.post({ path: '/groups/create', version: '>=1.0.0' }, Meteor.bindEnvironment(createOneGroup));
    restCu.post({ path: '/groups', version: '>=1.0.0' }, Meteor.bindEnvironment(getGroups));
    restCu.get({ path: '/groups', version: '>=1.0.0' }, Meteor.bindEnvironment(getAllGroups));
    restCu.del({ path: '/groups/delete', version: '>=1.0.0' }, Meteor.bindEnvironment(deleteGoup));
    restCu.post({ path: '/groups/addmember', version: '>=1.0.0' }, Meteor.bindEnvironment(addGroupMember));
    restCu.post({ path: '/groups/addadmin', version: '>=1.0.0' }, Meteor.bindEnvironment(addGroupAdmin));
    restCu.post({ path: '/groups/addanimator', version: '>=1.0.0' }, Meteor.bindEnvironment(addGroupAnimator));
    restCu.post({ path: '/groups/removemember', version: '>=1.0.0' }, Meteor.bindEnvironment(removeGroupMember));
    restCu.post({ path: '/groups/removeadmin', version: '>=1.0.0' }, Meteor.bindEnvironment(removeGroupAdmin));
    restCu.post({ path: '/groups/removeanimator', version: '>=1.0.0' }, Meteor.bindEnvironment(removeGroupAnimator));

    // Structure rule dispatch (ASAM)
    restCu.post(
      { path: '/structureruledispatchs/create', version: '>=1.0.0' },
      Meteor.bindEnvironment(createOneStructureRuleDispatch),
    );
    restCu.post(
      { path: '/structureruledispatchs', version: '>=1.0.0' },
      Meteor.bindEnvironment(getStructureRuleDispatchs),
    );
    restCu.get(
      { path: '/structureruledispatchs', version: '>=1.0.0' },
      Meteor.bindEnvironment(getAllStructureRuleDispatchs),
    );
    restCu.del(
      { path: '/structureruledispatchs/delete', version: '>=1.0.0' },
      Meteor.bindEnvironment(deleteStructureRuleDispatch),
    );
    // Bookmark
    restCu.post({ path: '/bookmarks/create', version: '>=1.0.0' }, Meteor.bindEnvironment(createOneBookmark));
    restCu.post({ path: '/bookmarks', version: '>=1.0.0' }, Meteor.bindEnvironment(getBookmarks));
    restCu.get({ path: '/bookmarks', version: '>=1.0.0' }, Meteor.bindEnvironment(getAllBookmarks));
    restCu.del({ path: '/bookmarks/delete', version: '>=1.0.0' }, Meteor.bindEnvironment(deleteBookmark));
    // User Bookmarks
    restCu.post({ path: '/userbookmarks/create', version: '>=1.0.0' }, Meteor.bindEnvironment(createOneUserBookmark));
    restCu.post({ path: '/userbookmarks', version: '>=1.0.0' }, Meteor.bindEnvironment(getUserBookmarks));
    restCu.get({ path: '/userbookmarks', version: '>=1.0.0' }, Meteor.bindEnvironment(getAllUserBookmarks));
    restCu.del({ path: '/userbookmarks/delete', version: '>=1.0.0' }, Meteor.bindEnvironment(deleteUserBookmark));

    // Structure
    restCu.post({ path: '/structures/create', version: '>=1.0.0' }, Meteor.bindEnvironment(createOneStructure));
    restCu.post({ path: '/structures', version: '>=1.0.0' }, Meteor.bindEnvironment(getStructures));
    restCu.get({ path: '/structures', version: '>=1.0.0' }, Meteor.bindEnvironment(getAllStructures));
    restCu.del({ path: '/structures/delete', version: '>=1.0.0' }, Meteor.bindEnvironment(deleteStructure));

    // Services
    restCu.post({ path: '/services/create', version: '>=1.0.0' }, Meteor.bindEnvironment(createService));
    restCu.post({ path: '/services', version: '>=1.0.0' }, Meteor.bindEnvironment(getServices));
    restCu.get({ path: '/services', version: '>=1.0.0' }, Meteor.bindEnvironment(getAllServices));

    restCu.get({ path: '/shutdown', version: '>=1.0.0' }, Meteor.bindEnvironment(shutdownNode));
    restCu.get({ path: '/liveness', version: '>=1.0.0' }, Meteor.bindEnvironment(livenessProbe));
  }

  const cutApiKeys = Meteor.settings.private.createUserTokenApiKeys;
  if (cutApiKeys && cutApiKeys.length > 0) {
    const restCut = Rest.create({ ...options, apiKeys: cutApiKeys });
    WebApp.connectHandlers.use(restCut.processRequest());
    restCut.post({ path: '/createusertoken', version: '>=1.0.0' }, Meteor.bindEnvironment(createUserToken));
  }

  initWidgetApi();
}
