import { Meteor } from 'meteor/meteor';
import fetch from 'node-fetch';
import i18n from 'meteor/universe:i18n';
import dns from 'dns';
import * as ip from 'neoip';
import logServer, { levels, scopes } from './logging';

const resolveName = 'laboite-headless';

async function callShutdownApi(address, apikey) {
  let respShutdown = 'NOK';
  try {
    const response = await fetch(`http://${address}:3000/api/shutdown`, {
      headers: {
        'X-API-KEY': apikey,
      },
    });
    // statusText should be "OK" if node stopped
    if (response.ok) respShutdown = response.statusText;
  } catch (error) {
    logServer(
      `SYSTEM - SETTINGS - callShutdownApi - ${i18n.__('api.contextSettings.errorCallShutdownApi', { address })}`,
      levels.ERROR,
      scopes.SYSTEM,
      { error },
    );
  }
  return respShutdown;
}

async function callLivenessProbe(address, apikey) {
  let respLiveness = 'NOK';
  try {
    const response = await fetch(`http://${address}:3000/api/liveness`, {
      headers: {
        'X-API-KEY': apikey,
      },
    });
    // statusText should be "OK" if node is alive
    if (response.ok) respLiveness = response.statusText;
  } catch (err) {
    logServer(
      `SYSTEM - SETTINGS - callLivenessProbe - ${i18n.__('api.contextSettings.nodeNotAliveYet', { address })}`,
      levels.INFO,
      scopes.SYSTEM,
    );
  }
  return respLiveness;
}

function getApiKeys() {
  const apiKeys = Meteor.settings.private.createUserApiKeys;
  if (apiKeys.length < 1) {
    logServer(
      `SYSTEM - SETTINGS - shutdownNodes - ${i18n.__('api.contextSettings.apiKeyNotFound')}`,
      levels.ERROR,
      scopes.SYSTEM,
    );
    throw new Meteor.Error('api.contextSetting.shutdownNodes', i18n.__('api.contextSettings.apiKeyNotFound'));
  }
  return apiKeys;
}

async function shutdownNodes(addresses, apiKeys) {
  // make API call on each other node to shutdown processes
  const allPromises = addresses.map((addr) => callShutdownApi(addr, apiKeys[0]));
  const shutdownResults = await Promise.all(allPromises);
  // if at least one node returns NOK, consider that shutdown failed
  if (shutdownResults.includes('NOK')) return false;
  return true;
}

async function getOtherNodes(dnsName) {
  // returns all nodes for a dns name
  const selfAddress = ip.address();
  let addresses = [];
  try {
    addresses = await dns.promises.lookup(dnsName, { family: 4, all: true });
  } catch (error) {
    logServer(
      `SYSTEM - SETTINGS - getOtherNodes - ${i18n.__('api.contextSettings.lookupError')}`,
      levels.ERROR,
      scopes.SYSTEM,
      { error },
    );
  }
  return addresses.map((addrInfo) => addrInfo.address).filter((addr) => addr !== selfAddress);
}

const sleep = (delay) => new Promise((resolve) => setTimeout(resolve, delay));

async function checkNodes(adresses, apiKeys, iteration = 0) {
  logServer(
    `SYSTEM - SETTINGS - checkNodes - ${i18n.__('api.contextSettings.waitBeforeCheck')}`,
    levels.INFO,
    scopes.SYSTEM,
  );
  // wait for 15 seconds and check if nodes are live
  await sleep(15000);
  // call liveness probe on each node
  const allPromises = adresses.map((addr) => callLivenessProbe(addr, apiKeys[0]));
  const livenessResults = await Promise.all(allPromises);
  // return true if all nodes are responding
  if (!livenessResults.includes('NOK')) return true;
  if (iteration >= 40) {
    // if we've been waiting for more than 10 minutes, consider that nodes failed to restart
    logServer(
      `SYSTEM - SETTINGS - checkNodes - ${i18n.__('api.contextSettings.maxTimeElapsed')}`,
      levels.ERROR,
      scopes.SYSTEM,
    );
    return false;
  }
  // otherwise, wait and check liveness again
  return checkNodes(adresses, apiKeys, iteration + 1);
}

export async function notifyUpdate() {
  // launch DNS lookup and restart matching nodes
  const apiKeys = getApiKeys();
  const initialAddresses = await getOtherNodes(resolveName);
  const allStopped = await shutdownNodes(initialAddresses, apiKeys);
  if (!allStopped) {
    // some nodes have not been stopped
    throw new Meteor.Error('api.contextSetting.shutdownNodes', i18n.__('api.contextSettings.nodesNotStopped'));
  }
  // determine when other nodes have finished restarting
  const nodeRestarted = await checkNodes(initialAddresses, apiKeys);
  if (!nodeRestarted) {
    // some nodes have not been restarted in the defined delay
    throw new Meteor.Error('api.contextSetting.checkNodes', i18n.__('api.contextSettings.nodesNotRestarted'));
  }
  logServer(
    `SYSTEM - SETTINGS - notifyUpdate - ${i18n.__('api.contextSettings.allNodesRestarted')}`,
    levels.INFO,
    scopes.SYSTEM,
  );
  // exit process in 10 seconds after a new node is ready
  setTimeout(() => {
    logServer(`SYSTEM - SETTINGS - ${i18n.__('api.contextSettings.exitProcess')}`, levels.INFO, scopes.SYSTEM);
    process.exit(1);
  }, 10000);
  return true;
}
