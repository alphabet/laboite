import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import i18n from 'meteor/universe:i18n';
import { checkUsername, findStructureByEmail } from '../users';
import logServer, { levels, scopes } from '../../logging';
import NextcloudClient from '../../appclients/nextcloud';
import Structures from '../../structures/structures';
import Groups from '../../groups/groups';
import { AddUserToGroupOfStructure, checkGroupsPermissions, RemoveUserFromGroupsOfOldStructure } from './methods';
import { GetStructure } from '../../structures/utils';
import { checkDisplayName } from '../AccountUserHook';

const nextcloudPlugin = Meteor.settings.public.groupPlugins.nextcloud;
let nextClient = null;
if (nextcloudPlugin && nextcloudPlugin.enable) nextClient = new NextcloudClient();

function createUserFromAPI(content, showError) {
  if (!checkUsername(content.username)) {
    if (showError) {
      logServer(`USERS - REST - ERROR - createUser - username is invalid`, levels.WARN, scopes.USER, {
        username: content.username,
      });
      const usernameError = new Meteor.Error(
        'restapi.users.createuser.invalidUsername',
        `username does not respect allowed size or characters: ${content.username}`,
      );
      usernameError.statusCode = 409;
      throw usernameError;
    } else {
      return null;
    }
  }
  const emailUser = Accounts.findUserByEmail(content.email);
  if (emailUser) {
    if (showError) {
      logServer(`USERS - REST - ERROR - createUser - user already exists with this email`, levels.WARN, scopes.USER, {
        emailUser,
      });
      const emailError = new Meteor.Error(
        'restapi.users.createuser.emailExists',
        `user already exists with this email: ${content.email}`,
      );
      emailError.statusCode = 409;
      throw emailError;
    } else {
      return null;
    }
  }
  const user = Meteor.users.findOne({ username: content.username });
  if (!user) {
    // create user account if not existing
    const userData = {
      username: content.username,
      createdAt: new Date(),
      emails: [{ address: content.email, verified: true }],
      primaryEmail: content.email,
      firstName: content.firstname,
      lastName: content.lastname,
      profile: {},
    };

    if ('structure' in content) {
      const structure = GetStructure(content.structure.split('/'), null, 0);
      if (structure) {
        userData.structure = structure._id;
      } else {
        logServer(`API - USERS - CREATEUSER - Structure not found`, levels.ERROR, scopes.SYSTEM, {
          structurePath: content.structure,
        });
        const error = new Meteor.Error('restapi.users.createuser.structureNotFound', 'Structure not found');
        error.statusCode = 404;
        throw error;
      }
    } else {
      const structure = findStructureByEmail(content.email);
      if (structure) userData.structure = structure._id;
    }

    try {
      const { emails, ...u } = checkDisplayName(userData);
      const userId = Accounts.createUser({ ...u, email: emails[0].address });
      Meteor.users.update(userId, { $set: { emails, isActive: true } });

      if (userData.structure) {
        const newUser = Meteor.users.findOne({ _id: userId });
        const structure = Structures.findOne({ _id: userData.structure });
        try {
          AddUserToGroupOfStructure(newUser, structure);
          checkGroupsPermissions(newUser._id);
        } catch (error) {
          error.statusCode = 409;
          throw error;
        }
      }
      // create user on nextcloud server
      if (nextClient) {
        return nextClient
          .addUser(userId)
          .then((resp) => {
            if (resp === true) return { response: 'user created' };
            throw new Meteor.Error(
              'restapi.users.createuser.nextcloudError',
              `Error encountered while creating user ${content.username} on nextcloud server`,
            );
          })
          .catch(() => {
            throw new Meteor.Error(
              'restapi.users.createuser.nextcloudError',
              `Error encountered while creating user ${content.username} on nextcloud server`,
            );
          });
      }
      return { response: 'user created' };
    } catch (err) {
      logServer(
        `USERS - REST - ERROR - createUser - ${i18n.__('api.users.insertError', { user: content.username })}`,
        levels.ERROR,
        scopes.USER,
        {
          userData,
          error: err.message,
        },
      );
      throw new Meteor.Error(
        'restapi.users.createuser.insertError',
        `Error encountered while creating user ${content.username}`,
      );
    }
  }
  if (showError) {
    logServer(`USERS - REST - ERROR - createUser - username already exists`, levels.WARN, scopes.USER, {
      user: content.username,
    });
    const usernameError = new Meteor.Error('restapi.users.createuser.alreadyExists', 'username already exists');
    usernameError.statusCode = 409;
    throw usernameError;
  }
  return 200;
}

export default async function createUser(req, content) {
  // sample use:
  // curl -X POST -H "X-API-KEY: createuser-password" \
  //      -H "Content-Type: application/json" \
  //      -d '{"username":"utilisateur1", "firstname":"", "lastname":"", "email":"", "structure": "api1/api2" }' \
  //      http://localhost:3000/api/createuser
  if ('username' in content && 'firstname' in content && 'lastname' in content && 'email' in content) {
    // check username validity
    return createUserFromAPI(content, true);
  }
  logServer(`USERS - REST - ERROR - createUser - Missing request parameters`, levels.ERROR, scopes.USER, {
    content,
  });
  const parametersError = new Meteor.Error('restapi.users.createuser.dataMissing', 'Missing request parameters');
  parametersError.statusCode = 400;
  throw parametersError;
}

export async function setUserStructure(req, content) {
  // sample use:
  // curl -X POST -H "X-API-KEY: createuser-password" \
  //      -H "Content-Type: application/json" \
  //      -d '{"username":"utilisateur1", "path": "api1/api2/api3" }' \
  //      http://localhost:3000/api/users/setstructure
  // curl -X POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"username":"utilisateur1", "path": "api1/api2/api3" }' http://localhost:3000/api/users/setstructure
  if ('username' in content && 'path' in content) {
    const user = Meteor.users.findOne({ username: content.username });
    if (user) {
      if (user.structure) {
        const currentStructure = Structures.findOne({ _id: user.structure });
        if (currentStructure) {
          const currentGroup = Groups.findOne({ _id: currentStructure.groupId });
          if (currentGroup) {
            RemoveUserFromGroupsOfOldStructure(user);
          }
        }
      }

      const structure = GetStructure(content.path.split('/'), null, 0);
      if (structure) {
        Meteor.users.update({ _id: user._id }, { $set: { structure: structure._id } });
        try {
          AddUserToGroupOfStructure(user, structure);
          checkGroupsPermissions(user._id);
          return 200;
        } catch (error) {
          error.statusCode = 409;
          throw error;
        }
      }
      logServer(`API - USERS - SETSTRUCTURE - Structure not found`, levels.ERROR, scopes.SYSTEM);
      const error = new Meteor.Error('api.users.structureNotFound', 'Structure not found');
      error.statusCode = 404;
      throw error;
    }
    logServer(`API - USERS - SETSTRUCTURE - User not found`, levels.ERROR, scopes.SYSTEM);
    const error = new Meteor.Error('api.users.notFound', 'User not found');
    error.statusCode = 404;
    throw error;
  }
  logServer(`API - USERS - SETSTRUCTURE - Invalid or empty content`, levels.ERROR, scopes.SYSTEM);
  const error = new Meteor.Error('api.users.invalidContent', 'Invalid or empty content');
  error.statusCode = 400;
  throw error;
}

export async function notificationKeycloak(req, content) {
  // sample use:
  // curl -X POST -H "X-API-KEY: createuser-password" \
  //      -H "Content-Type: application/json" \
  //      -d '{"username":"utilisateur1", "firstname":"", "lastname":"", "email":"", "structure": "api1/api2", "lastlogin": "date" }' \
  //      http://localhost:3000/api/notificationkeycloak
  // curl -X POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"username":"utilisateur1", "firstname":"", "lastname":"", "email":"", "structure": "api1/api2", "lastlogin": "date" }' http://localhost:3000/api/notificationkeycloack
  if (
    'username' in content &&
    'firstname' in content &&
    'lastname' in content &&
    'email' in content &&
    'lastlogin' in content
  ) {
    // Create user
    createUserFromAPI(content, false);

    try {
      Meteor.users.update(
        { username: content.username },
        {
          $set: {
            lastLogin: new Date(content.lastlogin * 1000),
            profileStatus: 1,
            lifeCycleDates: {
              firstMail: null,
              desactivate: null,
              departure: null,
              deletion: null,
            },
          },
        },
      );
      return 200;
    } catch (error) {
      logServer(
        `USERS - REST - ERROR - notificationKeycloak - Error resetting lifecycle data`,
        levels.ERROR,
        scopes.USER,
        {
          username: content.username,
        },
      );
      error.statusCode = 409;
      throw error;
    }
  }
  logServer(`USERS - REST - ERROR - notificationKeycloak - Missing request parameters`, levels.ERROR, scopes.USER, {
    content,
  });
  const parametersError = new Meteor.Error('restapi.users.createuser.dataMissing', 'Missing request parameters');
  parametersError.statusCode = 400;
  throw parametersError;
}
