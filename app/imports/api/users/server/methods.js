import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import i18n from 'meteor/universe:i18n';
import { _ } from 'meteor/underscore';
import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { Roles } from 'meteor/alanning:roles';

import { isActive, getLabel, checkPaginationParams, validateString } from '../../utils';
import { checkApiToken, getToken } from '../../server/utils';
import Groups from '../../groups/groups';
// initialize Meteor.users customizations
import AppRoles, { findStructureByEmail } from '../users';

import { favGroup, unfavGroup } from '../../groups/methods';
import PersonalSpaces from '../../personalspaces/personalspaces';
import { createRoleNotification, createRequestNotification } from '../../notifications/server/notifsutils';
import logServer, { levels, scopes } from '../../logging';

import { getRandomNCloudURL } from '../../nextcloud/methods';
import Structures from '../../structures/structures';
import Nextcloud from '../../nextcloud/nextcloud';
import { queryUsersAdmin, queryUsersByStructure } from './utils';
import EventsAgenda from '../../eventsAgenda/eventsAgenda';
import {
  hasAdminRightOnStructure,
  hasRightToAcceptAwaitingStructure,
  hasRightToSetStructureDirectly,
  userStructures,
} from '../../structures/utils';
import RegEx from '../../regExp';
import Notifications from '../../notifications/notifications';
import { addExpiration } from '../../notifications/methods';
import ContextSettings from '../../contextsettings/contextsettings';

if (Meteor.settings.private) {
  const { whiteDomains } = Meteor.settings.private;
  if (!!whiteDomains && whiteDomains.length > 0) {
    logServer(
      `USERS - METHODS - INFO - ${i18n.__('api.users.logWhiteDomains', { domains: JSON.stringify(whiteDomains) })}`,
      levels.INFO,
      scopes.SYSTEM,
      {},
    );
  }
}

const validateSchema = {
  userId: { type: String, regEx: RegEx.Id, label: getLabel('api.users.labels.id') },
  groupId: { type: String, regEx: RegEx.Id, label: getLabel('api.groups.labels.id') },
  username: { type: String, min: 1, label: getLabel('api.users.labels.username') },
};
// users.findUsers: Returns users using pagination
//   filter: string to search for in username/firstname/lastname/emails (case insensitive search)
//   page: number of the page requested
//   pageSize: number of entries per page
//   sortColumn/sortOrder: sort entries on a specific field with given order (1/-1)
//   exclude: specify a groupId and role (users in this role for this group will be excluded)
export const findUsers = new ValidatedMethod({
  name: 'users.findUsers',
  validate: new SimpleSchema({
    page: {
      type: SimpleSchema.Integer,
      min: 1,
      defaultValue: 1,
      optional: true,
      label: getLabel('api.methods.labels.page'),
    },
    pageSize: {
      type: SimpleSchema.Integer,
      min: 1,
      defaultValue: 10,
      optional: true,
      label: getLabel('api.methods.labels.pageSize'),
    },
    filter: {
      type: String,
      defaultValue: '',
      optional: true,
      label: getLabel('api.methods.labels.filter'),
    },
    sortColumn: {
      type: String,
      allowedValues: ['_id', ...Meteor.users.schema.objectKeys()],
      defaultValue: 'username',
      optional: true,
      label: getLabel('api.methods.labels.sortColumn'),
    },
    sortOrder: {
      type: SimpleSchema.Integer,
      allowedValues: [1, -1],
      defaultValue: 1,
      optional: true,
      label: getLabel('api.methods.labels.sortOrder'),
    },
    exclude: {
      type: Object,
      optional: true,
    },
    'exclude.groupId': {
      type: String,
      regEx: RegEx.Id,
      label: getLabel('api.methods.labels.excludeGroupId'),
    },
    'exclude.role': {
      type: String,
      allowedValues: AppRoles,
      label: getLabel('api.methods.labels.excludeRole'),
    },
  }).validator({ clean: true }),
  run({ page, pageSize, filter, sortColumn, sortOrder, exclude }) {
    const isAdmin = Roles.userIsInRole(this.userId, 'admin');
    // calculate number of entries to skip
    const skip = (page - 1) * pageSize;
    let query = {};
    if (filter && filter.length > 0) {
      const emails = {
        $elemMatch: {
          address: { $regex: `.*${filter}.*`, $options: 'i' },
        },
      };
      query.$or = [
        { emails },
        {
          username: { $regex: `.*${filter}.*`, $options: 'i' },
        },
        {
          lastName: { $regex: `.*${filter}.*`, $options: 'i' },
        },
        {
          firstName: { $regex: `.*${filter}.*`, $options: 'i' },
        },
        {
          displayName: { $regex: `.*${filter}.*`, $options: 'i' },
        },
      ];
    }
    if (exclude) {
      const usersField = `${exclude.role}s`;
      const group = Groups.findOne(exclude.groupId);
      if (group && group[usersField].length > 0) {
        if (Object.keys(query).length > 0) {
          query = { $and: [{ _id: { $nin: group[usersField] } }, query] };
        } else {
          query = { _id: { $nin: group[usersField] } };
        }
      }
    }
    // skip admin list based on new public.maskedAdminList contextSetting
    const ignoredAdmins = ContextSettings.findOne({ key: 'public.maskedAdminList' })?.value || [];
    query = { $and: [{ username: { $nin: ignoredAdmins } }, query] };

    const sort = {};
    sort[sortColumn] = sortOrder;
    let data;
    let totalCount;
    try {
      totalCount = Meteor.users.find(query).count();
      data = Meteor.users
        .find(query, {
          fields: isAdmin ? Meteor.users.adminFields : Meteor.users.publicFields,
          limit: pageSize,
          skip,
          sort,
        })
        .fetch();
    } catch {
      totalCount = 0;
      data = [];
    }
    return { data, page, totalCount };
  },
});

export const removeUser = new ValidatedMethod({
  name: 'users.removeUser',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
  }).validator(),

  run({ userId }) {
    // check if current user has global admin rights or self removal
    const authorized = isActive(this.userId) && (Roles.userIsInRole(this.userId, 'admin') || userId === this.userId);
    if (!authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - removeUseR - ${i18n.__('api.users.notPermitted')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.removeUser.notPermitted', i18n.__('api.users.notPermitted'));
    }

    // check user existence
    const user = Meteor.users.findOne({ _id: userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - removeUseR - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.removeUser.unknownUser', i18n.__('api.users.unknownUser'));
    }

    // check if user to remove is not in admin list
    const ignoredAdmins = ContextSettings.findOne({ key: 'public.maskedAdminList' })?.value || [];
    if (ignoredAdmins.includes(user.username)) {
      logServer(
        `USERS - METHODS - METEOR ERROR - removeUseR - ${i18n.__('api.users.adminCantBeRemoved')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.removeUser.adminCantBeRemoved', i18n.__('api.users.adminCantBeRemoved'));
    }

    // check if user to remove is not in adminEmails list
    const declaredAdmins = Meteor.settings?.keycloak?.adminEmails || [];
    if (declaredAdmins.includes(user.emails[0].address)) {
      logServer(
        `USERS - METHODS - METEOR ERROR - removeUseR - ${i18n.__('api.users.adminCantBeRemoved')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.removeUser.adminCantBeRemoved', i18n.__('api.users.adminCantBeRemoved'));
    }

    // delete role assignements and remove from groups
    const groups = Roles.getScopesForUser(userId);
    groups.forEach((groupId) => {
      logServer(
        `USERS - METHODS - UPDATE - removeUser (group update) - groupId: ${groupId}`,
        levels.VERBOSE,
        scopes.SYSTEM,
        { userId },
      );
      Groups.update(
        { _id: groupId },
        {
          $pull: {
            admins: userId,
            members: userId,
            animators: userId,
            candidates: userId,
          },
        },
      );
    });
    logServer(`USERS - METHODS - REMOVE - removeUser (meteor role)`, levels.INFO, scopes.SYSTEM, { userId });
    Meteor.roleAssignment.remove({ 'user._id': userId });
    logServer(`USERS - METHODS - REMOVE - removeUser (personal space)`, levels.INFO, scopes.SYSTEM, { userId });
    PersonalSpaces.remove({ userId });

    const element = Nextcloud.findOne({ url: user.nclocator });

    if (element !== undefined) {
      element.count -= 1;
      Nextcloud.update({ url: user.nclocator }, { $set: { count: element.count } });
    }
    logServer(`USERS - METHODS - REMOVE - removeUser (meteor)`, levels.INFO, scopes.SYSTEM, { userId });
    Meteor.users.remove({ _id: userId });
  },
});

export function _unsetAnimatorOf(user, group) {
  // check group and user existence

  if (group.animators.indexOf(user._id) === -1) {
    logServer(
      `USERS - METHODS - METEOR ERROR - unsetAnimatorOf - ${i18n.__('api.users.unknownUser')}`,
      levels.ERROR,
      scopes.SYSTEM,
      { userId: user._id, groupId: group._id },
    );
    throw new Meteor.Error('api.users.unsetAnimatorOf.unknownUser', i18n.__('api.users.unknownUser'));
  }
  // remove role from user collection
  Roles.removeUsersFromRoles(user._id, 'animator', group._id);
  // update info in group collection
  if (group.animators.indexOf(user._id) !== -1) {
    logServer(`USERS - METHODS - UPDATE - unsetAnimatorOf (groups)`, levels.VERBOSE, scopes.SYSTEM, {
      userId: user._id,
      groupId: group._id,
    });
    Groups.update(group._id, { $pull: { animators: user._id } });
  }
  // if user has no longer roles, remove group from personalspace
  if (Roles.getRolesForUser(user._id, { scope: group._id, onlyScoped: true }).length === 0) {
    unfavGroup._execute({ userId: user._id }, { groupId: group._id });
  }
  if (group.type !== 15)
    Meteor.call('users.animatorOfUnset', { userId: user._id, groupId: group._id, token: getToken() });
  // Notify user
  if (this.userId !== user._id) createRoleNotification(this.userId, user._id, group._id, 'animator', false);
}

export function _unsetMemberOf(user, group) {
  if (group.members.indexOf(user._id) === -1) {
    logServer(
      `USERS - METHODS - METEOR ERROR - unsetMemberOf - ${i18n.__('api.users.unknownUser')}`,
      levels.ERROR,
      scopes.SYSTEM,
      { userId: user._id, groupId: group._id },
    );
    throw new Meteor.Error('api.users.unsetMemberOf.unknownUser', i18n.__('api.users.unknownUser'));
  }

  // add role to user collection
  Roles.removeUsersFromRoles(user._id, 'member', group._id);
  // update info in group collection
  if (group.members.indexOf(user._id) !== -1) {
    logServer(`USERS - METHODS - UPDATE - unsetMemberOf`, levels.VERBOSE, scopes.SYSTEM, {
      userId: user._id,
      groupId: group._id,
    });
    Groups.update(group._id, {
      $pull: { members: user._id },
    });
  }
  // if user has no longer roles, remove group from personalspace
  if (Roles.getRolesForUser(user._id, { scope: group._id, onlyScoped: true }).length === 0) {
    unfavGroup._execute({ userId: user._id }, { groupId: group._id });
  }
  // notify plugins
  if (group.type !== 15)
    Meteor.call('users.memberOfUnset', { userId: user._id, groupId: group._id, token: getToken() });
  // Notify user
  if (this.userId !== user._id) createRoleNotification(this.userId, user._id, group._id, 'member', false);
}

export const unsetMemberOf = new ValidatedMethod({
  name: 'users.unsetMemberOf',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
    groupId: validateSchema.groupId,
  }).validator(),

  run({ userId, groupId }) {
    // check if current user has sufficient rights on group (or self remove)
    const authorized = userId === this.userId || Roles.userIsInRole(this.userId, ['admin', 'animator'], groupId);
    if (!isActive(this.userId) || !authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetMemberOf - ${i18n.__('api.users.notPermitted')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.unsetMemberOf.notPermitted', i18n.__('api.users.notPermitted'));
    }
    // check group and user existence
    const group = Groups.findOne({ _id: groupId });
    if (group === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetMemberOf - ${i18n.__('api.groups.unknownGroup')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.unsetMemberOf.unknownGroup', i18n.__('api.groups.unknownGroup'));
    } else if (group.type === 20) {
      // restricted group, only admins and animators can leave the group
      const authorizedRestricted = Roles.userIsInRole(this.userId, ['admin', 'animator'], group._id);
      if (!authorizedRestricted) {
        logServer(
          `USERS - METHODS - METEOR ERROR - unsetMemberOf - ${i18n.__('api.users.notPermitted')}`,
          levels.ERROR,
          scopes.SYSTEM,
          { userId, groupId: group._id },
        );
        throw new Meteor.Error('api.users.unsetMemberOf.notPermitted', i18n.__('api.users.notPermitted'));
      }
    }
    const user = Meteor.users.findOne({ _id: userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetMemberOf (groups) - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.setAdminOf.unknownUser', i18n.__('api.users.unknownUser'));
    }
    _unsetMemberOf(user, group);
    // also remove animator role if present
    if (group.animators.indexOf(user._id) !== -1) _unsetAnimatorOf(user, group);
  },
});

export function _unsetAdminOf(user, group) {
  if (group.admins.indexOf(user._id) === -1) {
    logServer(
      `USERS - METHODS - METEOR ERROR - unsetAdminOf - ${i18n.__('api.users.unknownUser')}`,
      levels.ERROR,
      scopes.SYSTEM,
      { userId: user._id, groupId: group._id },
    );
    throw new Meteor.Error('api.users.unsetAdminOf.unknownUser', i18n.__('api.users.unknownUser'));
  }
  // remove role from user collection
  Roles.removeUsersFromRoles(user._id, 'admin', group._id);
  // update info in group collection
  if (group.admins.indexOf(user._id) !== -1) {
    logServer(`USERS - METHODS - UPDATE - unsetAdminOf`, levels.VERBOSE, scopes.SYSTEM, {
      userId: user._id,
      groupId: group._id,
    });
    Groups.update(group._id, { $pull: { admins: user._id } });
  }
  // if user has no longer roles, remove group from personalspace
  if (Roles.getRolesForUser(user._id, { scope: group._id, onlyScoped: true }).length === 0) {
    unfavGroup._execute({ userId: user._id }, { groupId: group._id });
  }
  // notify plugins
  if (group.type !== 15) Meteor.call('users.adminOfUnset', { userId: user._id, groupId: group._id, token: getToken() });
  // Notify user
  if (this.userId !== user._id) createRoleNotification(this.userId, user._id, group._id, 'admin', false);
}

export const unsetAdminOf = new ValidatedMethod({
  name: 'users.unsetAdminOf',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
    groupId: validateSchema.groupId,
  }).validator(),

  run({ userId, groupId }) {
    // check if current user has admin rights on group (or global admin)
    const authorized = isActive(this.userId) && Roles.userIsInRole(this.userId, 'admin', groupId);
    if (!authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetAdminOf - ${i18n.__('api.groups.adminGroupNeeded')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.unsetAdminOf.notPermitted', i18n.__('api.groups.adminGroupNeeded'));
    }
    // check group and user existence
    const group = Groups.findOne({ _id: groupId });
    if (group === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetAdminOf - ${i18n.__('api.groups.unknownGroup')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.unsetAdminOf.unknownGroup', i18n.__('api.groups.unknownGroup'));
    }
    const user = Meteor.users.findOne({ _id: userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetAdminOf (groups) - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.setAdminOf.unknownUser', i18n.__('api.users.unknownUser'));
    }
    _unsetAdminOf(user, group);
  },
});

export function RemoveAllRolesFromGroup(user, group) {
  const roles = ['admin', 'animator', 'member', 'candidate'];
  const rolesOfUser = roles.filter((role) => group[`${role}s`].includes(user._id));

  Roles.removeUsersFromRoles(user._id, roles, group._id);

  if (rolesOfUser.length > 0) {
    logServer(`USERS - METHODS - UPDATE - RemoveAllRolesFromGroup - groupId: ${group._id}`, levels.INFO, scopes.SYSTEM);
    Groups.update(group._id, {
      $pull: rolesOfUser.reduce((mod, role) => ({ ...mod, [`${role}s`]: user._id }), {}),
    });
  }
}

export function RemoveUserFromGroupsOfOldStructure(user) {
  if (user.structure) {
    const oldStructure = Structures.findOne({ _id: user.structure });
    if (oldStructure) {
      const ancestors = Structures.find({ _id: { $in: oldStructure.ancestorsIds } }).fetch();
      if (oldStructure.groupId) {
        const group = Groups.findOne({ _id: oldStructure.groupId });
        if (group) {
          RemoveAllRolesFromGroup(user, group);
          unfavGroup._execute({ userId: user._id }, { groupId: oldStructure.groupId });
        }
      }
      if (ancestors) {
        ancestors.forEach((st) => {
          if (st.groupId) {
            const gr = Groups.findOne({ _id: st.groupId });
            if (gr) {
              RemoveAllRolesFromGroup(user, gr);
              unfavGroup._execute({ userId: user._id }, { groupId: st.groupId });
            }
          }
        });
      }
    }
  }
}

export const removeUserFromStructure = new ValidatedMethod({
  name: 'users.removeUserFromStructure',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
  }).validator(),

  run({ userId }) {
    const user = Meteor.users.findOne({ _id: userId });
    // check if current user has structure admin rights or self removal
    const authorized =
      isActive(this.userId) &&
      (hasAdminRightOnStructure({ userId: this.userId, structureId: user.structure }) || userId === this.userId);
    if (!authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - removeUserFromStructure - ${i18n.__('api.users.notPermitted')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.removeUserFromStructure.notPermitted', i18n.__('api.users.notPermitted'));
    }
    // check user existence
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - removeUserFromStructure - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.removeUserFromStructure.unknownUser', i18n.__('api.users.unknownUser'));
    }
    if (Roles.userIsInRole(userId, 'adminStructure', user.structure)) {
      Roles.removeUsersFromRoles(userId, 'adminStructure', user.structure);
    }
    RemoveUserFromGroupsOfOldStructure(user);
    logServer(`USERS - METHODS - UPDATE - removeUserFromStructure (meteor)`, levels.VERBOSE, scopes.SYSTEM, { userId });
    Meteor.users.update({ _id: userId }, { $set: { structure: null } });
  },
});

export const checkUsername = new ValidatedMethod({
  name: 'users.checkUsername',
  validate: new SimpleSchema({
    username: validateSchema.username,
  }).validator(),

  run({ username }) {
    // check that user is logged in
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - checkUsername - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { username },
      );
      throw new Meteor.Error('api.users.checkUsername.notLoggedIn', i18n.__('api.users.mustBeLoggedIn'));
    }
    // return false if another user as an email or username matching username
    let user = Accounts.findUserByUsername(username, { fields: { _id: 1 } });
    if (user && user._id !== this.userId) return false;
    user = Accounts.findUserByEmail(username, { fields: { _id: 1 } });
    if (user && user._id !== this.userId) return false;
    return true;
  },
});

export function _setMemberOf(user, group) {
  const userId = user._id;
  const groupId = group._id;
  // add role to user collection
  Roles.addUsersToRoles(userId, 'member', groupId);
  // remove candidate Role if present
  if (Roles.userIsInRole(userId, 'candidate', groupId)) {
    Roles.removeUsersFromRoles(userId, 'candidate', groupId);
  }
  // store info in group collection
  if (group.members.indexOf(userId) === -1) {
    logServer(`USERS - METHODS - UPDATE - setMemberOf (groups)`, levels.VERBOSE, scopes.SYSTEM, {
      userId,
      groupId,
    });
    Groups.update(groupId, {
      $push: { members: userId },
      $pull: { candidates: userId },
    });
  }
  // update user personalSpace
  favGroup._execute({ userId }, { groupId });

  const insertUser = { email: user.emails[0].address, _id: userId, groupId, status: 1 };

  logServer(
    `USERS - METHODS - UPDATE MANY - setMemberOf (event) - groupId: ${groupId} / participants: ${JSON.stringify(
      insertUser,
    )}`,
    levels.INFO,
    scopes.SYSTEM,
  );
  // update Events
  EventsAgenda.rawCollection().updateMany(
    { groups: { $elemMatch: { _id: groupId } } },
    { $push: { participants: insertUser } },
  );

  // notify plugins
  if (group.type !== 15) Meteor.call('users.memberOfSet', { userId, groupId, token: getToken() });
  // Notify user
  if (this.userId !== userId) createRoleNotification(this.userId, userId, groupId, 'member', true);
}

export function _setAdminOf(user, group) {
  // add role to user collection
  Roles.addUsersToRoles(user._id, 'admin', group._id);
  // store info in group collection
  if (group.admins.indexOf(user._id) === -1) {
    logServer(`USERS - METHODS - UPDATE - setAdminOf (groups)`, levels.VERBOSE, scopes.SYSTEM, {
      userId: user._id,
      groupId: group._id,
    });
    Groups.update({ _id: group._id }, { $push: { admins: user._id } });
  }
  // notify plugins
  if (group.type !== 15) Meteor.call('users.adminOfSet', { userId: user._id, groupId: group._id, token: getToken() });
  // Notify user
  if (this.userId !== user._id) createRoleNotification(this.userId, user._id, group._id, 'admin', true);
}

export const setAdminOf = new ValidatedMethod({
  name: 'users.setAdminOf',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
    groupId: validateSchema.groupId,
  }).validator(),

  run({ userId, groupId }) {
    // check if current user has admin rights on group (or global admin)
    const authorized = isActive(this.userId) && Roles.userIsInRole(this.userId, 'admin', groupId);
    if (!authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setAdminOf (groups) - ${i18n.__('api.groups.adminGroupNeeded')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.setAdminOf.notPermitted', i18n.__('api.groups.adminGroupNeeded'));
    }
    // check group and user existence
    const group = Groups.findOne({ _id: groupId });
    if (group === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setAdminOf (groups) - ${i18n.__('api.groups.unknownGroup')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.setAdminOf.unknownGroup', i18n.__('api.groups.unknownGroup'));
    }
    const user = Meteor.users.findOne({ _id: userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setAdminOf (groups) - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.setAdminOf.unknownUser', i18n.__('api.users.unknownUser'));
    }
    _setAdminOf(user, group);
    if (group.members.indexOf(user._id) === -1) {
      // set user as group member if not restricted by limiting structures
      let canBeMember = true;
      if (Array.isArray(group.structureIds) && group.structureIds.length >= 1) {
        const userStructs = userStructures(user);
        if (!group.structureIds.some((structureId) => userStructs.includes(structureId))) {
          canBeMember = false;
        }
      }
      if (canBeMember) _setMemberOf(user, group);
    }
  },
});

export function _setAnimatorOf(user, group) {
  // check if group is limited to specific structures
  if (group.structureIds) {
    const userStructs = userStructures(user);
    if (!group.structureIds.some((structureId) => userStructs.includes(structureId))) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setAnimatorOf - ${i18n.__('api.users.notInStructure')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId: user._id, groupId: group._id },
      );
      throw new Meteor.Error('api.users.setAnimatorOf.wrongStructure', i18n.__('api.users.notInStructure'));
    }
  }
  // add role to user collection
  Roles.addUsersToRoles(user._id, 'animator', group._id);
  // store info in group collection
  if (group.animators.indexOf(user._id) === -1) {
    logServer(`USERS - METHODS - UPDATE - setAnimatorOf (groups)`, levels.VERBOSE, scopes.SYSTEM, {
      userId: user._id,
      groupId: group._id,
    });
    Groups.update({ _id: group._id }, { $push: { animators: user._id } });
  }
  // update user personalSpace
  favGroup._execute({ userId: user._id }, { groupId: group._id });
  // notify plugins
  if (group.type !== 15)
    Meteor.call('users.animatorOfSet', { userId: user._id, groupId: group._id, token: getToken() });
  // Notify user
  if (this.userId !== user._id) createRoleNotification(this.userId, user._id, group._id, 'animator', true);
}

export const setAnimatorOf = new ValidatedMethod({
  name: 'users.setAnimatorOf',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
    groupId: validateSchema.groupId,
  }).validator(),

  run({ userId, groupId }) {
    // check if current user has admin rights on group (or global admin)
    const authorized = isActive(this.userId) && Roles.userIsInRole(this.userId, 'admin', groupId);
    if (!authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setAnimatorOf - ${i18n.__('api.groups.adminGroupNeeded')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.setAnimatorOf.notPermitted', i18n.__('api.groups.adminGroupNeeded'));
    }
    // check group and user existence
    const group = Groups.findOne({ _id: groupId });
    if (group === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setAnimatorOf - ${i18n.__('api.groups.unknownGroup')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.setAnimatorOf.unknownGroup', i18n.__('api.groups.unknownGroup'));
    }
    const user = Meteor.users.findOne({ _id: userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setAnimatorOf - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.setAnimatorOf.unknownUser', i18n.__('api.users.unknownUser'));
    }
    _setAnimatorOf(user, group);
    // also set user as group member if it's not already the case
    if (group.members.indexOf(user._id) === -1) _setMemberOf(user, group);
  },
});

export const unsetAnimatorOf = new ValidatedMethod({
  name: 'users.unsetAnimatorOf',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
    groupId: validateSchema.groupId,
  }).validator(),

  run({ userId, groupId }) {
    // check if current user has admin rights on group (or global admin) or self removal
    const authorized = userId === this.userId || Roles.userIsInRole(this.userId, 'admin', groupId);
    if (!isActive(this.userId) || !authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetAnimatorOf - ${i18n.__('api.groups.adminGroupNeeded')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.unsetAnimatorOf.notPermitted', i18n.__('api.groups.adminGroupNeeded'));
    }
    const group = Groups.findOne({ _id: groupId });
    if (group === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetAnimatorOf - ${i18n.__('api.groups.unknownGroup')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.unsetAnimatorOf.unknownGroup', i18n.__('api.groups.unknownGroup'));
    } else if (group.type === 20) {
      // restricted groupe, only admins and animators are authorized to leave the group
      const authorizedRestricted =
        Roles.userIsInRole(this.userId, ['admin'], group._id) ||
        (userId === this.userId && Roles.userIsInRole(this.userId, ['admin', 'animator'], group._id));
      if (!authorizedRestricted) {
        logServer(
          `USERS - METHODS - METEOR ERROR - unsetAnimatorOf - ${i18n.__('api.users.notPermitted')}`,
          levels.ERROR,
          scopes.SYSTEM,
          { userId, groupId: group._id },
        );
        throw new Meteor.Error('api.users.unsetAnimatorOf.notPermitted', i18n.__('api.users.notPermitted'));
      }
    }

    const user = Meteor.users.findOne({ _id: userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetAnimatorOf (groups) - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.setAdminOf.unknownUser', i18n.__('api.users.unknownUser'));
    }
    _unsetAnimatorOf(user, group);
  },
});

export const setMemberOf = new ValidatedMethod({
  name: 'users.setMemberOf',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
    groupId: validateSchema.groupId,
  }).validator(),

  run({ userId, groupId }) {
    // check group and user existence
    const group = Groups.findOne({ _id: groupId });
    if (group === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setMemberOf - ${i18n.__('api.groups.unknownGroup')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.setMemberOf.unknownGroup', i18n.__('api.groups.unknownGroup'));
    }
    const user = Meteor.users.findOne({ _id: userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setMemberOf - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.setMemberOf.unknownUser', i18n.__('api.users.unknownUser'));
    }
    // check if current user has sufficient rights on group
    let authorized = false;
    // check if group is limited to specific structures
    if (group.structureIds) {
      const userStructs = userStructures(user);
      if (!group.structureIds.some((structureId) => userStructs.includes(structureId))) {
        logServer(
          `USERS - METHODS - METEOR ERROR - setMemberOf - ${i18n.__('api.users.notInStructure')}`,
          levels.ERROR,
          scopes.SYSTEM,
          { userId, groupId },
        );
        throw new Meteor.Error('api.users.setMemberOf.wrongStructure', i18n.__('api.users.notInStructure'));
      }
    }
    if (group.type === 0) {
      // open group, users cand set themselve as member
      authorized = userId === this.userId || Roles.userIsInRole(this.userId, ['admin', 'animator'], groupId);
    } else if (group.type === 15) {
      // structure group, users can be set as member for groups matching their structure and parent structures
      authorized = Roles.userIsInRole(this.userId, ['admin', 'animator'], groupId);
      if (!authorized) {
        if (user.structure) {
          const userStruct = Structures.findOne(user.structure);
          if (userStruct.groupId === groupId) {
            authorized = true;
          } else {
            const parentStructsGroups = Structures.find({ _id: { $in: userStruct.ancestorsIds } })
              .fetch()
              .map((struct) => struct.groupId);
            authorized = parentStructsGroups.includes(groupId);
          }
        }
      }
    } else {
      authorized = Roles.userIsInRole(this.userId, ['admin', 'animator'], groupId);
    }
    if (!isActive(this.userId) || !authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setMemberOf - ${i18n.__('api.users.notPermitted')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.setMemberOf.notPermitted', i18n.__('api.users.notPermitted'));
    }
    _setMemberOf(user, group);
  },
});

export function _setCandidateOf(user, group) {
  // check if group is limited to specific structures
  if (group.structureIds) {
    const userStructs = userStructures(user);
    if (!group.structureIds.some((structureId) => userStructs.includes(structureId))) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setCandidateOf - ${i18n.__('api.users.notInStructure')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId: user._id, groupId: group._id },
      );
      throw new Meteor.Error('api.users.setCandidateOf.wrongStructure', i18n.__('api.users.notInStructure'));
    }
  }
  // add role to user collection
  Roles.addUsersToRoles(user._id, 'candidate', group._id);
  // store info in group collection
  if (group.candidates.indexOf(user._id) === -1) {
    logServer(`USERS - METHODS - UPDATE - setCandidateOf (groups)`, levels.VERBOSE, scopes.SYSTEM, {
      userId: user._id,
      groupId: group._id,
    });
    Groups.update(
      { _id: group._id },
      {
        $push: { candidates: user._id },
      },
    );
  }
  // update user personalSpace
  favGroup._execute({ userId: user._id }, { groupId: group._id });
  // Notify user
  if (this.userId !== user._id) createRoleNotification(this.userId, user._id, group._id, 'candidate', true);
  // Notify admins
  if (this.userId !== user._id) createRequestNotification(this.userId, user._id, group._id);
}

export const setCandidateOf = new ValidatedMethod({
  name: 'users.setCandidateOf',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
    groupId: validateSchema.groupId,
  }).validator(),

  run({ userId, groupId }) {
    // allow to set candidate for self or as admin/animator
    const authorized = userId === this.userId || Roles.userIsInRole(this.userId, ['admin', 'animator'], groupId);
    if (!isActive(this.userId) || !authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setCandidateOf - ${i18n.__('api.users.notPermitted')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.setCandidateOf.notPermitted', i18n.__('api.users.notPermitted'));
    }
    // check group and user existence
    const group = Groups.findOne({ _id: groupId });
    if (group === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setCandidateOf - ${i18n.__('api.groups.unknownGroup')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.setCandidateOf.unknownGroup', i18n.__('api.groups.unknownGroup'));
    }
    // only manage candidates on moderated groups
    if (group.type !== 5) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setCandidateOf - ${i18n.__('api.groups.moderatedGroupOnly')}`,
        levels.WARN,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.setCandidateOf.moderatedGroupOnly', i18n.__('api.groups.moderatedGroupOnly'));
    }
    const user = Meteor.users.findOne({ _id: userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setCandidateOf - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.setCandidateOf.unknownUser', i18n.__('api.users.unknownUser'));
    }
    _setCandidateOf(user, group);
  },
});

export const unsetCandidateOf = new ValidatedMethod({
  name: 'users.unsetCandidateOf',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
    groupId: validateSchema.groupId,
  }).validator(),

  run({ userId, groupId }) {
    // allow to unset candidate for self or as admin/animator
    const authorized = userId === this.userId || Roles.userIsInRole(this.userId, ['admin', 'animator'], groupId);
    if (!isActive(this.userId) || !authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetCandidateOf - ${i18n.__('api.users.notPermitted')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.unsetCandidateOf.notPermitted', i18n.__('api.users.notPermitted'));
    }
    // check group and user existence
    const group = Groups.findOne({ _id: groupId });
    if (group === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetCandidateOf - ${i18n.__('api.groups.unknownGroup')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.unsetCandidateOf.unknownGroup', i18n.__('api.groups.unknownGroup'));
    }
    if (group.candidates.indexOf(userId) === -1) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetCandidateOf - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId, groupId },
      );
      throw new Meteor.Error('api.users.unsetCandidateOf.unknownUser', i18n.__('api.users.unknownUser'));
    }
    // remove role from user collection
    Roles.removeUsersFromRoles(userId, 'candidate', groupId);
    // remove info from group collection
    if (group.candidates.indexOf(userId) !== -1) {
      logServer(`USERS - METHODS - UPDATE - unsetCandidateOf (groups)`, levels.VERBOSE, scopes.SYSTEM, {
        userId,
        groupId,
      });
      Groups.update(groupId, {
        $pull: { candidates: userId },
      });
    }
    // if user has no longer roles, remove group from personalspace
    if (Roles.getRolesForUser(userId, { scope: groupId, onlyScoped: true }).length === 0) {
      unfavGroup._execute({ userId }, { groupId });
    }
    // Notify user
    if (this.userId !== userId) createRoleNotification(this.userId, userId, groupId, 'candidate', false);
  },
});

export function AddUserToGroupOfStructure(user, structure) {
  if (structure.groupId) {
    const group = Groups.findOne(structure.groupId);
    _setMemberOf(user, group);
  }

  const ancestors = Structures.find({ _id: { $in: structure.ancestorsIds } }).fetch();
  if (ancestors) {
    ancestors.forEach((st) => {
      if (st.groupId) {
        const gr = Groups.findOne({ _id: st.groupId });
        if (gr) {
          _setMemberOf(user, gr);
        }
      }
    });
  }
}

// checks all subscribed groups to detect if user is still allowed to access them after structure change
export function checkGroupsPermissions(userId) {
  const user = Meteor.users.findOne(userId);
  // find all groups user is subscribed to, only for groups with structure limitations
  const groups = Groups.find({ _id: { $in: user.favGroups }, structureIds: { $ne: null } });
  const userStructs = userStructures(user);
  const unsubFuncs = {
    candidates: unsetCandidateOf,
    member: unsetMemberOf,
    animator: unsetAnimatorOf,
    admin: unsetAdminOf,
  };
  groups.forEach((group) => {
    if (!group.structureIds.some((structId) => userStructs.includes(structId))) {
      // user has no longer access to group, fetch his roles on this group
      const groupRoles = Roles.getRolesForUser(user._id, { scope: group._id, onlyScoped: true });
      groupRoles.forEach((role) => {
        // unsubscribe user for this role
        try {
          unsubFuncs[role]._execute({ userId }, { userId: user._id, groupId: group._id });
        } catch (err) {
          logServer(
            `USERS - METHODS - ERROR - checkGroupsPermissions - error when unsubscribing user`,
            levels.ERROR,
            scopes.SYSTEM,
            {
              errorMEssage: err.message,
            },
          );
        }
      });
    }
  });
}

const storeActivationInfos = (userId, activatorId) => {
  const activator = Meteor.users.findOne(activatorId);
  logServer(`USERS - METHODS - UPDATE - storeActivationInfos`, levels.INFO, scopes.SYSTEM, { userId, activatorId });
  Meteor.users.update(
    { _id: userId },
    {
      $set: {
        activatedAt: new Date(),
        activatedBy: { userId: activatorId, username: activator.username, email: activator.emails[0].address },
      },
    },
  );
};

export const acceptAwaitingStructure = new ValidatedMethod({
  name: 'users.acceptAwaitingStructure',
  validate: new SimpleSchema({
    targetUserId: validateSchema.userId,
  }).validator(),
  run({ targetUserId }) {
    // check that user is logged in
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - acceptAwaitingStructure - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { targetUserId },
      );
      throw new Meteor.Error('api.users.acceptAwaitingStructure.notLoggedIn', i18n.__('api.users.mustBeLoggedIn'));
    }

    const targetUser = Meteor.users.findOne({ _id: targetUserId });
    const { awaitingStructure } = targetUser;
    const structure = Structures.findOne({ _id: awaitingStructure });

    if (structure === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - acceptAwaitingStructure - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { targetUserId },
      );
      throw new Meteor.Error(
        'api.users.acceptAwaitingStructure.unknownStructure',
        i18n.__('api.structures.unknownStructure'),
      );
    }
    // only direct admin of structure can validate awaiting structure application
    const authorized = hasRightToAcceptAwaitingStructure({
      userId: this.userId,
      awaitingStructureId: awaitingStructure,
    });

    if (!authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - acceptAwaitingStructure - ${i18n.__('api.users.notPermitted')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { targetUserId },
      );
      throw new Meteor.Error('api.users.acceptAwaitingStructure.notPermitted', i18n.__('api.users.notPermitted'));
    }

    RemoveUserFromGroupsOfOldStructure(targetUser);
    logServer(
      `USERS - METHODS - UPDATE - acceptAwaitingStructure (user meteor) - userId: ${targetUserId} 
      / awaitingStructure: ${awaitingStructure}`,
      levels.VERBOSE,
      scopes.SYSTEM,
    );
    if (targetUser.isActive === false) {
      // if user was previously not active, store activation infos in database
      storeActivationInfos(targetUserId, this.userId);
    }
    Meteor.users.update(
      { _id: targetUserId },
      {
        $set: {
          structure: awaitingStructure,
          awaitingStructure: null,
          isActive: true,
          isRequest: false,
        },
      },
    );

    AddUserToGroupOfStructure(targetUser, structure);
    checkGroupsPermissions(targetUserId);
  },
});

export const refuseAwaitingStructure = new ValidatedMethod({
  name: 'users.refuseAwaitingStructure',
  validate: new SimpleSchema({
    targetUserId: validateSchema.userId,
  }).validator(),
  run({ targetUserId }) {
    // check that user is logged in
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - refuseAwaitingStructure - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { targetUserId },
      );
      throw new Meteor.Error('api.users.refuseAwaitingStructure.notLoggedIn', i18n.__('api.users.mustBeLoggedIn'));
    }

    const targetUser = Meteor.users.findOne({ _id: targetUserId });
    const { awaitingStructure } = targetUser;
    const structure = Structures.findOne({ _id: awaitingStructure });

    if (structure === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - refuseAwaitingStructure - ${i18n.__('api.structures.unknownStructure')}`,
        levels.WARN,
        scopes.SYSTEM,
        { targetUserId },
      );
      throw new Meteor.Error(
        'api.users.refuseAwaitingStructure.unknownStructure',
        i18n.__('api.structures.unknownStructure'),
      );
    }
    // only direct admin of structure can refuse awaiting structure application
    const authorized = hasRightToAcceptAwaitingStructure({
      userId: this.userId,
      awaitingStructureId: awaitingStructure,
    });

    if (!authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - refuseAwaitingStructure - ${i18n.__('api.users.notPermitted')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { targetUserId },
      );
      throw new Meteor.Error('api.users.refuseAwaitingStructure.notPermitted', i18n.__('api.users.notPermitted'));
    }

    logServer(
      `USERS - METHODS - UPDATE - refuseAwaitingStructure (user meteor) - userId: ${targetUserId} 
      / awaitingStructure: ${awaitingStructure}`,
      levels.VERBOSE,
      scopes.SYSTEM,
    );

    Meteor.users.update(
      { _id: targetUserId },
      {
        $set: {
          awaitingStructure: null,
        },
      },
    );
    // notify target user of refusal
    const notifData = addExpiration({
      userId: targetUserId,
      title: i18n.__('api.users.titleRefuseAwaitingStructure'),
      content: i18n.__('api.users.contentRefuseAwaitingStructure', { structure: structure.name }),
      type: 'message',
    });
    Notifications.insert(notifData);
  },
});

export const checkStructure = new ValidatedMethod({
  name: 'users.checkStructure',
  validate: new SimpleSchema({
    structure: {
      type: String,
      regEx: RegEx.Id,
      label: getLabel('api.users.labels.structure'),
    },
  }).validator(),

  run({ structure }) {
    // check that user is logged in
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - checkStructure - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { structure },
      );
      throw new Meteor.Error('api.users.checkStructure.notLoggedIn', i18n.__('api.users.mustBeLoggedIn'));
    }

    // check if structure exists
    const structureToFind = Structures.findOne({ _id: structure });
    if (!structureToFind) {
      logServer(
        `USERS - METHODS - METEOR ERROR - checkStructure - ${i18n.__('SimpleSchema.notAllowed', structure)}`,
        levels.ERROR,
        scopes.SYSTEM,
        { structure },
      );
      throw new Meteor.Error(`${structure} is not an allowed value`, i18n.__('SimpleSchema.notAllowed', structure));
    }

    const user = Meteor.users.findOne({ _id: this.userId });
    const userEmail = user.emails ? user.emails[0]?.address : null;
    const isAppAdmin = Roles.userIsInRole(user._id, 'admin');
    if (isAppAdmin) return true;
    const isAuthorized =
      findStructureByEmail(userEmail, true)?._id === structure || hasRightToSetStructureDirectly(user._id, structure);
    return isAuthorized;
  },
});

export function _setStructure(structure, user, isAuthorized) {
  // check if user has structure admin role and remove it only if new structure and old structure are different
  if (user.structure !== structure._id) {
    if (Roles.userIsInRole(user._id, 'adminStructure', user.structure)) {
      Roles.removeUsersFromRoles(user._id, 'adminStructure', user.structure);
    }

    if (isAuthorized) {
      // check group and user existence for old structure
      const oldStruct = Structures.findOne({ _id: user.structure });
      if (oldStruct) {
        if (oldStruct.groupId) {
          const group = Groups.findOne({ _id: oldStruct.groupId });
          if (group !== undefined && group.members.indexOf(user._id) !== -1) {
            RemoveUserFromGroupsOfOldStructure(user);
          }
        }
      }
    }
  }

  logServer(
    `USERS - METHODS - UPDATE - setStructure (user meteor) - userId: ${user._id} 
      / new structure: ${structure._id} / current structure: ${user.structure}`,
    levels.INFO,
    scopes.SYSTEM,
  );
  Meteor.users.update(
    { _id: user._id },
    {
      $set: {
        structure: isAuthorized ? structure._id : user.structure,
        awaitingStructure: isAuthorized ? null : structure._id,
      },
    },
  );

  if (isAuthorized) {
    AddUserToGroupOfStructure(user, structure);
    checkGroupsPermissions(user._id);
  }
}

export const setStructure = new ValidatedMethod({
  name: 'users.setStructure',
  validate: new SimpleSchema({
    structure: {
      type: String,
      regEx: RegEx.Id,
      /**
       * @deprecated
       * Since we're now using dynamic structures, we do not need to check if it's allowed in the schema
       */
      // allowedValues: getStructureIds,
      label: getLabel('api.users.labels.structure'),
    },
  }).validator(),

  run({ structure }) {
    // check that user is logged in
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setStructure - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { structure },
      );
      throw new Meteor.Error('api.users.setStructure.notLoggedIn', i18n.__('api.users.mustBeLoggedIn'));
    }

    // check if structure exists
    const structureToFind = Structures.findOne({ _id: structure });
    if (!structureToFind) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setStructure - ${i18n.__('SimpleSchema.notAllowed', structure)}`,
        levels.ERROR,
        scopes.SYSTEM,
        { structure },
      );
      throw new Meteor.Error(`${structure} is not an allowed value`, i18n.__('SimpleSchema.notAllowed', structure));
    }

    const user = Meteor.users.findOne({ _id: this.userId });
    // allow user to change structure directly if structure has no mandatory validation or if
    // user's email correspond to structure (ASAM rules or invitation)
    const userEmail = user.emails ? user.emails[0]?.address : null;
    const isAuthorizedToSetStructureDirectly =
      findStructureByEmail(userEmail)?._id === structure || hasRightToSetStructureDirectly(user._id, structure);
    const isAppAdmin = Roles.userIsInRole(user._id, 'admin');

    _setStructure(structureToFind, user, isAuthorizedToSetStructureDirectly || isAppAdmin);
  },
});

export const setNcloudUrlAll = new ValidatedMethod({
  name: 'users.setNCloudAll',
  validate: new SimpleSchema().validator(),

  run() {
    // check that user is logged in
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setNcloudUrlAll - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.setNcloudUrlAll.notLoggedIn', i18n.__('api.users.mustBeLoggedIn'));
    }

    const authorized = isActive(this.userId) && Roles.userIsInRole(this.userId, 'admin');
    if (!authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setNcloudUrlAll - ${i18n.__('api.users.adminNeeded')}`,
        levels.ERROR,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.setNcloudUrlAll.notPermitted', i18n.__('api.users.adminNeeded'));
    }

    const users = Meteor.users.find({ $or: [{ nclocator: { $exists: false } }, { nclocator: '' }] }).fetch();

    let cpt = 0;

    for (let i = 0; i < users.length; i += 1) {
      users[i].nclocator = getRandomNCloudURL();
      Meteor.users.update({ _id: users[i]._id }, { $set: { nclocator: users[i].nclocator } });
      cpt += 1;
    }

    return cpt;
  },
});

export const setAdmin = new ValidatedMethod({
  name: 'users.setAdmin',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
  }).validator(),

  run({ userId }) {
    // check if current user has global admin rights
    const authorized = isActive(this.userId) && Roles.userIsInRole(this.userId, 'admin');
    if (!authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setAdmin - ${i18n.__('api.users.adminNeeded')}`,
        levels.ERROR,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.setAdmin.notPermitted', i18n.__('api.users.adminNeeded'));
    }
    // check user existence
    const user = Meteor.users.findOne({ _id: userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setAdmin - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.setAdmin.unknownUser', i18n.__('api.users.unknownUser'));
    }
    // add role to user collection
    Roles.addUsersToRoles(userId, 'admin');
  },
});

export const setAdminStructure = new ValidatedMethod({
  name: 'users.setAdminStructure',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
  }).validator(),

  run({ userId }) {
    // check user existence
    const user = Meteor.users.findOne({ _id: userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setAdminStructure - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.setAdminStructure.unknownUser', i18n.__('api.users.unknownUser'));
    }
    // check if current user has global or structure-scoped admin rights
    const authorized =
      isActive(this.userId) &&
      (Roles.userIsInRole(this.userId, 'admin') ||
        hasAdminRightOnStructure({ userId: this.userId, structureId: user.structure }));
    if (!authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setAdminStructure - ${i18n.__('api.users.adminNeeded')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.setAdminStructure.notPermitted', i18n.__('api.users.adminNeeded'));
    }
    // add role to user collection
    Roles.addUsersToRoles(userId, 'adminStructure', user.structure);

    // Add admin role of structure group
    const structure = Structures.findOne({ _id: user.structure });
    if (structure) {
      if (structure.groupId) {
        const group = Groups.findOne({ _id: structure.groupId });
        if (group) {
          setAdminOf._execute({ userId: this.userId }, { userId, groupId: group._id });
        }
      }
    }
  },
});

export const setActive = new ValidatedMethod({
  name: 'users.setActive',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
  }).validator(),

  run({ userId }) {
    // check if current user has global admin rights or is adminStructure of target user's structure
    const userStruct = Meteor.users.findOne({ _id: userId })?.structure;
    const isStructAdmin = userStruct && hasAdminRightOnStructure({ userId: this.userId, structureId: userStruct });
    const authorized = isActive(this.userId) && (Roles.userIsInRole(this.userId, 'admin') || isStructAdmin);
    if (!authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setActive - ${i18n.__('api.users.adminNeeded')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.setActive.notPermitted', i18n.__('api.users.adminNeeded'));
    }
    // check user existence
    const user = Meteor.users.findOne({ _id: userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setActive - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.setActive.unknownUser', i18n.__('api.users.unknownUser'));
    }
    logServer(`USERS - METHODS - UPDATE - setActive (user meteor)`, levels.INFO, scopes.SYSTEM, { userId });
    if (user.isActive === false) {
      // user has been activated, store activation info in database
      storeActivationInfos(userId, this.userId);
    }
    Meteor.users.update(userId, { $set: { isActive: true, isRequest: false } });
  },
});

export const unsetActive = new ValidatedMethod({
  name: 'users.unsetActive',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
  }).validator(),

  run({ userId }) {
    // check if current user has global admin rights
    const authorized = isActive(this.userId) && Roles.userIsInRole(this.userId, 'admin');
    if (!authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetActive - ${i18n.__('api.users.adminNeeded')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.unsetActive.notPermitted', i18n.__('api.users.adminNeeded'));
    }
    // check user existence
    const user = Meteor.users.findOne({ _id: userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetActive - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.unsetActive.unknownUser', i18n.__('api.users.unknownUser'));
    }
    logServer(`USERS - METHODS - UPDATE - unsetActive (user meteor)`, levels.INFO, scopes.SYSTEM, { userId });
    Meteor.users.update(userId, { $set: { isActive: false } });
  },
});

export const unsetAdmin = new ValidatedMethod({
  name: 'users.unsetAdmin',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
  }).validator(),

  run({ userId }) {
    // check if current user has global admin rights
    const authorized = isActive(this.userId) && Roles.userIsInRole(this.userId, 'admin');
    if (!authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetAdmin - ${i18n.__('api.users.adminNeeded')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.unsetAdmin.notPermitted', i18n.__('api.users.adminNeeded'));
    }
    // check that user is not the only existing admin
    const admins = Roles.getUsersInRole('admin').fetch();
    if (admins.length === 1) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetAdmin - ${i18n.__('api.users.lastAdminError')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.unsetAdmin.lastAdmin', i18n.__('api.users.lastAdminError'));
    }
    // check user existence
    const user = Meteor.users.findOne({ _id: userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetAdmin - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.setAdmin.unknownUser', i18n.__('api.users.unknownUser'));
    }
    // remove role from user collection
    Roles.removeUsersFromRoles(userId, 'admin');
  },
});

export const unsetAdminStructure = new ValidatedMethod({
  name: 'users.unsetAdminStructure',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
  }).validator(),

  run({ userId }) {
    // check user existence
    const user = Meteor.users.findOne({ _id: userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetAdminStructure - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.unsetAdminStructure.unknownUser', i18n.__('api.users.unknownUser'));
    }
    // check if current user has global admin rights
    const isStructureAdmin =
      user.structure && hasAdminRightOnStructure({ userId: this.userId, structureId: user.structure });
    const authorized = isActive(this.userId) && (Roles.userIsInRole(this.userId, 'admin') || isStructureAdmin);
    if (!authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - unsetAdminStructure - ${i18n.__('api.users.adminNeeded')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.unsetAdminStructure.notPermitted', i18n.__('api.users.adminNeeded'));
    }

    // remove role from user collection
    Roles.removeUsersFromRoles(userId, 'adminStructure', user.structure);

    // remove admin role of structure group
    const structure = Structures.findOne({ _id: user.structure });
    if (structure) {
      if (structure.groupId) {
        const group = Groups.findOne({ _id: structure.groupId });
        if (group) {
          if (Roles.userIsInRole(this.userId, 'admin', group._id)) {
            unsetAdminOf._execute({ userId: this.userId }, { userId, groupId: group._id });
          }
        }
      }
    }
  },
});

export const setLanguage = new ValidatedMethod({
  name: 'users.setLanguage',
  validate: new SimpleSchema({
    language: { type: String, label: getLabel('api.users.labels.language') },
  }).validator(),

  run({ language }) {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setLanguage - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { language },
      );
      throw new Meteor.Error('api.users.setLanguage.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    validateString(language, true);
    logServer(
      `USERS - METHODS - UPDATE - setLanguage (user meteor) - userId: ${this.userId}`,
      levels.VERBOSE,
      scopes.SYSTEM,
      { language },
    );
    Meteor.users.update(this.userId, {
      $set: { language },
    });
  },
});

export const setMotivation = new ValidatedMethod({
  name: 'users.setMotivation',
  validate: new SimpleSchema({
    motivation: { type: String, label: getLabel('api.users.labels.motivation') },
  }).validator(),

  run({ motivation }) {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setMotivation - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { motivation },
      );
      throw new Meteor.Error('api.users.setMotivation.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    validateString(motivation);
    logServer(
      `USERS - METHODS - UPDATE - setMotivation (user meteor) - userId: ${this.userId}`,
      levels.VERBOSE,
      scopes.SYSTEM,
      { motivation },
    );
    Meteor.users.update(this.userId, {
      $set: { motivation },
    });
  },
});

export const setDisplayNameComplement = new ValidatedMethod({
  name: 'users.setDisplayNameComplement',
  validate: new SimpleSchema({
    displayNameComplement: { type: String, label: getLabel('api.users.labels.displayNameComplement') },
  }).validator(),

  run({ displayNameComplement }) {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setDisplayNameComplement - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { displayNameComplement },
      );
      throw new Meteor.Error('api.users.setDisplayNameComplement.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    validateString(displayNameComplement);
    logServer(
      `USERS - METHODS - UPDATE - setDisplayNameComplement - userId: ${this.userId}`,
      levels.VERBOSE,
      scopes.SYSTEM,
      { displayNameComplement },
    );
    const currentUser = Meteor.users.findOne(this.userId);
    if (displayNameComplement !== currentUser.username) {
      // for safety, displayNameComplement can not be set to another user's username
      const otherUser = Meteor.users.findOne({ username: displayNameComplement });
      if (otherUser)
        throw new Meteor.Error(
          'api.users.setDisplayNameComplement.reservedComplement',
          i18n.__('api.users.reservedComplement'),
        );
    }
    // check that there is no conflict with another user displayName
    const checkData = {
      _id: { $ne: currentUser._id },
      firstName: currentUser.firstName,
      lastName: currentUser.lastName,
    };
    if (displayNameComplement) checkData.displayNameComplement = displayNameComplement;
    const homonymUser = Meteor.users.findOne(checkData);
    if (homonymUser)
      throw new Meteor.Error(
        'api.users.setDisplayNameComplement.alreadyExists',
        i18n.__('api.users.displayNameAlreadyExists'),
      );
    Meteor.users.update(this.userId, {
      // also set firstName/lastName to force displayName recalculation
      $set: {
        _id: this.userId,
        displayNameComplement,
        firstName: currentUser.firstName,
        lastName: currentUser.lastName,
        accountType: currentUser.accountType,
      },
    });
  },
});

export const setLastGlobalInfoRead = new ValidatedMethod({
  name: 'users.setLastGlobalInfoRead',
  validate: new SimpleSchema({
    lastGlobalInfoReadDate: { type: Date },
  }).validator(),

  run({ lastGlobalInfoReadDate }) {
    if (!this.userId) {
      throw new Meteor.Error('api.users.setLanguage.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }

    Meteor.users.update(this.userId, {
      $set: { lastGlobalInfoReadDate },
    });
  },
});

export const setLogoutType = new ValidatedMethod({
  name: 'users.setLogoutType',
  validate: new SimpleSchema({
    logoutType: { type: String, label: getLabel('api.users.labels.logoutType') },
  }).validator(),

  run({ logoutType }) {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setLogoutType - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.VERBOSE,
        scopes.SYSTEM,
        { logoutType },
      );
      throw new Meteor.Error('api.users.setLogoutType.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    validateString(logoutType, true);
    logServer(
      `USERS - METHODS - UPDATE - setLogoutType (user meteor) - userId: ${this.userId}`,
      levels.VERBOSE,
      scopes.SYSTEM,
      { logoutType },
    );
    Meteor.users.update(this.userId, {
      $set: { logoutType },
    });
  },
});

export const setAvatar = new ValidatedMethod({
  name: 'users.setAvatar',
  validate: new SimpleSchema({
    avatar: {
      type: String,
      label: getLabel('api.users.labels.avatar'),
    },
  }).validator(),

  run({ avatar }) {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setAvatar - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { avatar },
      );
      throw new Meteor.Error('api.users.setAvatar.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    validateString(avatar);
    logServer(
      `USERS - METHODS - UPDATE - setAvatar (user meteor) - userId: ${this.userId}`,
      levels.VERBOSE,
      scopes.USER,
      { avatar },
    );
    Meteor.users.update(this.userId, {
      $set: { avatar },
    });
  },
});

// method to associate existing account with a Keycloak Id
export const setKeycloakId = new ValidatedMethod({
  name: 'users.setKeycloakId',
  validate: new SimpleSchema({
    email: {
      type: String,
      regEx: RegEx.Email,
      label: getLabel('api.users.labels.emailAddress'),
    },
    keycloakId: { type: String, label: getLabel('api.methods.labels.keycloakId') },
  }).validator(),

  run({ email, keycloakId }) {
    const authorized = isActive(this.userId) && Roles.userIsInRole(this.userId, 'admin');
    if (!authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setKeycloakId - ${i18n.__('api.users.adminNeeded')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { email, keycloakId },
      );
      throw new Meteor.Error('api.users.setKeycloakId.notPermitted', i18n.__('api.users.adminNeeded'));
    }
    const user = Accounts.findUserByEmail(email);
    if (user) {
      logServer(
        `USERS - METHODS - UPDATE - setKeycloakId (user meteor) - userId: ${user._id} / keycloakId: ${keycloakId}`,
        levels.VERBOSE,
        scopes.USER,
      );
      Meteor.users.update({ _id: user._id }, { $set: { services: { keycloak: { id: keycloakId } } } });
      return user._id;
    }
    logServer(
      `USERS - METHODS - METEOR ERROR - setKeycloakId - ${i18n.__('api.users.unknownUser')}`,
      levels.ERROR,
      scopes.SYSTEM,
      { email, keycloakId },
    );
    throw new Meteor.Error('api.users.setKeycloakId.unknownUser', i18n.__('api.users.unknownUser'));
  },
});

export const findUser = new ValidatedMethod({
  name: 'users.findUser',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
  }).validator(),
  run({ userId }) {
    return Meteor.users.findOne({ _id: userId }, { fields: { firstName: 1, lastName: 1, _id: 1 } });
  },
});

// BEGIN HOOK FUNCTIONS: these functions are used to trigger hooks for external applications plugins

export const userUpdated = new ValidatedMethod({
  name: 'users.userUpdated',
  validate: new SimpleSchema({
    userId: validateSchema.userId,
    token: String,
    data: {
      type: Object,
      optional: true,
      blackbox: true,
    },
  }).validator(),

  run({ userId, token, data }) {
    // this function is used to provide hooks when user data is updated
    // (currently when logging in with keycloak)
    checkApiToken('userUpdated', { userId, ...data }, token);
    return [userId, data];
  },
});

export const displayNameSet = new ValidatedMethod({
  name: 'users.displayNameSet',
  validate: null,
  run(data) {
    checkApiToken('displayNameSet', data, data.token);
  },
});

export const memberOfSet = new ValidatedMethod({
  name: 'users.memberOfSet',
  validate: null,
  run(data) {
    checkApiToken('memberOfSet', data, data.token);
  },
});

export const animatorOfSet = new ValidatedMethod({
  name: 'users.animatorOfSet',
  validate: null,
  run(data) {
    checkApiToken('animatorOfSet', data, data.token);
  },
});

export const adminOfSet = new ValidatedMethod({
  name: 'users.adminOfSet',
  validate: null,
  run(data) {
    checkApiToken('adminOfSet', data, data.token);
  },
});

export const memberOfUnset = new ValidatedMethod({
  name: 'users.memberOfUnset',
  validate: null,
  run(data) {
    checkApiToken('memberOfUnset', data, data.token);
  },
});

export const animatorOfUnset = new ValidatedMethod({
  name: 'users.animatorOfUnset',
  validate: null,
  run(data) {
    checkApiToken('animatorOfUnset', data, data.token);
  },
});

export const adminOfUnset = new ValidatedMethod({
  name: 'users.adminOfUnset',
  validate: null,
  run(data) {
    checkApiToken('adminOfUnset', data, data.token);
  },
});

// END HOOK FUNCTIONS

export const setQuota = new ValidatedMethod({
  name: 'users.setQuota',
  validate: new SimpleSchema({
    quota: { type: Number },
    userId: validateSchema.userId,
  }).validator(),

  run({ quota, userId }) {
    // this function is used to provide hooks when user data is updated
    // (currently when logging in with keycloak)
    if (!Meteor.isServer) {
      // this should be run by server side code only
      logServer(
        `USERS - METHODS - METEOR ERROR - setQuota - ${i18n.__('api.users.notPermitted')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { quota, userId },
      );
      throw new Meteor.Error('api.users.userUpdated.notPermitted', i18n.__('api.users.notPermitted'));
    }
    logServer(`USERS - METHODS - UPDATE - setQuota (user meteor)`, levels.VERBOSE, scopes.ADMIN, { quota, userId });
    Meteor.users.update(
      { _id: userId },
      {
        $set: {
          groupQuota: quota,
        },
      },
    );
  },
});

export const toggleAdvancedPersonalPage = new ValidatedMethod({
  name: 'users.toggleAdvancedPersonalPage',
  validate: null,

  run() {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - toggleAdvancedPersonalPage - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.toggleAdvancedPersonalPage.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    // check user existence
    const user = Meteor.users.findOne({ _id: this.userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - toggleAdvancedPersonalPage - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.toggleAdvancedPersonalPage.unknownUser', i18n.__('api.users.unknownUser'));
    }
    const newValue = !(user.advancedPersonalPage || false);
    logServer(
      `USERS - METHODS - UPDATE - toggleAdvancedPersonalPage (user meteor) - userId: ${this.userId} 
      / advancedPersonalPage: ${newValue}`,
      levels.INFO,
      scopes.USER,
    );
    Meteor.users.update(this.userId, { $set: { advancedPersonalPage: newValue } });
  },
});

export const toggleBetaServices = new ValidatedMethod({
  name: 'users.toggleBetaServices',
  validate: null,

  run() {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - toggleBetaServices - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.toggleBetaServices.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    // check user existence
    const user = Meteor.users.findOne({ _id: this.userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - toggleBetaServices - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.toggleBetaServices.unknownUser', i18n.__('api.users.unknownUser'));
    }
    const newValue = !(user.betaServices || false);
    logServer(
      `USERS - METHODS - UPDATE - toggleBetaServices (user meteor) - userId: ${this.userId} 
      / betaServices: ${newValue}`,
      levels.INFO,
      scopes.USER,
    );
    Meteor.users.update(this.userId, { $set: { betaServices: newValue } });
  },
});

export const getAuthToken = new ValidatedMethod({
  name: 'users.getAuthToken',
  validate: null,
  run() {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - getAuthToken - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.getAuthToken.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    // check user existence
    const user = Meteor.users.findOne({ _id: this.userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - getAuthToken - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.getAuthToken.unknownUser', i18n.__('api.users.unknownUser'));
    }
    return user.authToken;
  },
});

export const getIdToken = new ValidatedMethod({
  name: 'users.getIdToken',
  validate: null,
  run() {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - getIdToken - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.getIdToken.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    // check user existence
    const user = Meteor.users.findOne({ _id: this.userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - getIdToken - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.getIdToken.unknownUser', i18n.__('api.users.unknownUser'));
    }
    return user.services?.keycloak?.idToken || null;
  },
});

export const resetAuthToken = new ValidatedMethod({
  name: 'users.resetAuthToken',
  validate: null,
  run() {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - resetAuthToken - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.resetAuthToken.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    // check user existence
    const user = Meteor.users.findOne({ _id: this.userId });
    if (user === undefined) {
      logServer(
        `USERS - METHODS - METEOR ERROR - resetAuthToken - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.resetAuthToken.unknownUser', i18n.__('api.users.unknownUser'));
    }
    const newToken = Random.secret(150);
    logServer(
      `USERS - METHODS - UPDATE - resetAuthToken (user meteor) - userId: ${user._id} 
      / authToken: ${newToken}`,
      levels.INFO,
      scopes.USER,
    );
    Meteor.users.update({ _id: user._id }, { $set: { authToken: newToken } });
    return newToken;
  },
});

export const hasUserOnRequest = new ValidatedMethod({
  name: 'users.hasUserOnRequest',
  validate: null,
  run() {
    return !!Meteor.users.findOne({ isActive: { $ne: true } });
  },
});

export const hasUserOnAwaitingStructure = new ValidatedMethod({
  name: 'users.hasUserOnAwaitingStructure',
  validate: new SimpleSchema({
    structureId: { type: String, regEx: RegEx.Id },
  }).validator({ clean: true }),

  run({ structureId }) {
    return !!Meteor.users.findOne({ awaitingStructure: structureId });
  },
});

// This is a temporary method used to fix badly created users in database
// intended to be called by an admin user from a browser dev console :
//  Meteor.call('users.fixUsers', (err,res) => console.log(res))
//
export const fixUsers = new ValidatedMethod({
  name: 'users.fixUsers',
  validate: null,
  run() {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - fixUsers - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.fixUsers.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    // check if current user has global admin rights
    const authorized = isActive(this.userId) && Roles.userIsInRole(this.userId, 'admin');
    if (!authorized) {
      logServer(
        `USERS - METHODS - METEOR ERROR - fixUsers - ${i18n.__('api.users.adminNeeded')}`,
        levels.ERROR,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.fixUsers.notPermitted', i18n.__('api.users.adminNeeded'));
    }
    const badUsers = Meteor.users.find({ username: null }).fetch();
    let fixedCount = 0;
    badUsers.forEach((user) => {
      // try to get missing fields from keycloak data
      if (user.services.keycloak) {
        const updateInfos = {
          primaryEmail: user.services.keycloak.email,
        };
        Accounts.addEmail(user._id, user.services.keycloak.email, true);
        if (user.services.keycloak.given_name) {
          updateInfos.firstName = user.services.keycloak.given_name;
        }
        if (user.services.keycloak.family_name) {
          updateInfos.lastName = user.services.keycloak.family_name;
        }
        if (user.services.keycloak.preferred_username) {
          // use preferred_username as username if defined
          // (should be set as mandatory in keycloak)
          updateInfos.username = user.services.keycloak.preferred_username;
        }
        if (!user.nclocator) updateInfos.nclocator = getRandomNCloudURL();
        logServer(
          `USERS - METHODS - UPDATE - fixUsers (user meteor) - userId: ${user._id} 
      / updateInfos: ${updateInfos.primaryEmail}`,
          levels.INFO,
          scopes.USER,
        );
        Meteor.users.update({ _id: user._id }, { $set: updateInfos });
        fixedCount += 1;
      } else {
        logServer(
          `USERS - METHODS - ERROR - fixUsers - could not fix user '${user._id}', no keycloak data available`,
          levels.ERROR,
          scopes.SYSTEM,
        );
      }
    });
    return fixedCount;
  },
});

export const getUsersAdmin = new ValidatedMethod({
  name: 'users.admins',
  validate: null,
  run({ page = 1, itemPerPage = 10, search = '', ...rest }) {
    if (!isActive(this.userId) || !Roles.userIsInRole(this.userId, 'admin')) {
      return this.ready();
    }

    new SimpleSchema({
      userType: {
        type: String,
        allowedValues: ['adminStructure', 'admin', 'all'],
      },
    })
      .extend(checkPaginationParams)
      .validate({ page, itemPerPage, search, userType: rest.userType });

    const query = queryUsersAdmin({ search, userType: rest.userType });
    const sortQuery = rest.sortQuery !== undefined ? rest.sortQuery : { lastName: 1, firstName: 1 };
    const restCopy = { ...rest };
    delete restCopy.sortQuery;
    delete restCopy.userType;

    const users = Meteor.users
      .find(query, {
        fields: Meteor.users.adminFields,
        skip: itemPerPage * (page - 1),
        limit: itemPerPage,
        sort: sortQuery,
        ...restCopy,
      })
      .fetch();
    const total = Meteor.users.find(query).count();

    const structuresById = {};

    const data = users.map((user) => {
      if (!structuresById[user.structure]) {
        structuresById[user.structure] = Structures.findOne({ _id: user.structure }, { fields: { name: 1 } });
      }

      return { structureName: structuresById[user.structure]?.name || '', ...user };
    });

    return { data, pageSize: itemPerPage, page, total };
  },
});

export const getAdminsFromStructure = new ValidatedMethod({
  name: 'users.getAdminsFromStructure',
  validate: new SimpleSchema({
    structureId: {
      type: String,
    },
    subStructure: {
      type: Boolean,
    },
  }).validator(),

  run({ structureId, subStructure }) {
    const struc = Structures.findOne({ _id: structureId });
    if (!struc) {
      logServer(
        `USERS - METHODS - ERROR - getAdminsFromStructure - could not find structure '${structureId}'.`,
        levels.ERROR,
        scopes.SYSTEM,
      );
      throw new Meteor.Error(
        'api.users.getAdminsFromStructure.notFound',
        i18n.__('api.users.getAdminsFromStructure.structureNotFound'),
      );
    }

    let allStruc = [];

    if (subStructure) {
      const childs = Structures.find({ ancestorsIds: structureId })
        .fetch()
        .map((child) => child._id);
      allStruc = [...childs, structureId];
    } else {
      allStruc = [structureId];
    }

    const ret = Meteor.roleAssignment.find({
      'role._id': 'adminStructure',
      scope: { $in: allStruc },
    });

    const tabUsers = ret.fetch().map((user) => user.user._id);
    const users = Meteor.users.find({ _id: { $in: tabUsers } }, { fields: Meteor.users.publicFields }).fetch();

    const structures = Structures.find({ _id: { $in: allStruc } }, { fields: Structures.publicFields }).fetch();

    return { users, structures };
  },
});

export const getUsersByStructure = new ValidatedMethod({
  name: 'users.byStructure',
  validate: null,
  run({ selectedStructureId = null, page = 1, itemPerPage = 10, search, ...rest }) {
    const currentUser = Meteor.users.findOne({ _id: this.userId });
    const usedStructure = selectedStructureId || currentUser.structure;
    const hasAdminRight = hasAdminRightOnStructure({ userId: this.userId, structureId: usedStructure });

    const isAuthorized = isActive(this.userId) && (Roles.userIsInRole(this.userId, 'admin') || hasAdminRight);

    if (!isAuthorized) {
      // TODO add a normalized 403 here
      throw new Meteor.Error(403, 'User is not authorized');
    }

    new SimpleSchema({
      userType: {
        type: String,
        allowedValues: ['adminStructure', 'admin', 'all'],
      },
    })
      .extend(checkPaginationParams)
      .validate({ page, itemPerPage, search, userType: rest.userType });

    const query = queryUsersByStructure({ search, userType: rest.userType }, usedStructure);
    const sortQuery = rest.sortQuery !== undefined ? rest.sortQuery : { lastName: 1, firstName: 1 };
    const restCopy = { ...rest };
    delete restCopy.sortQuery;
    delete restCopy.userType;

    const users = Meteor.users
      .find(query, {
        fields: Meteor.users.adminFields,
        skip: itemPerPage * (page - 1),
        limit: itemPerPage,
        sort: sortQuery,
        ...restCopy,
      })
      .fetch();
    const total = Meteor.users.find(query).count();
    const structuresById = {};

    const data = users.map((user) => {
      if (!structuresById[user.structure]) {
        structuresById[user.structure] = Structures.findOne({ _id: user.structure }, { fields: { name: 1 } });
      }

      return { structureName: structuresById[user.structure]?.name || '', ...user };
    });

    return { data, pageSize: itemPerPage, page, total };
  },
});

export const setThemeMode = new ValidatedMethod({
  name: 'users.setThemeMode',
  validate: new SimpleSchema({
    mode: { type: String, label: getLabel('api.users.labels.themeMode') },
  }).validator(),

  run({ mode }) {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setThemeMode - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { mode },
      );
      throw new Meteor.Error('api.users.setThemeMode.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    validateString(mode);
    logServer(
      `USERS - METHODS - UPDATE - setThemeMode (user meteor) - userId: ${this.userId}`,
      levels.VERBOSE,
      scopes.USER,
      { mode },
    );
    Meteor.users.update(this.userId, {
      $set: { themeMode: mode },
    });
  },
});

export const setUpdatedAt = new ValidatedMethod({
  name: 'users.setUpdatedAt',
  validate: new SimpleSchema({
    updatedAt: {
      type: Date,
      label: getLabel('api.users.labels.updatedAt'),
    },
  }).validator(),

  run({ updatedAt }) {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setUpdatedAt - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { updatedAt },
      );
      throw new Meteor.Error('api.users.setUpdatedAt.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    logServer(
      `USERS - METHODS - UPDATE - setUpdatedAt (user meteor) - userId: ${this.userId}`,
      levels.VERBOSE,
      scopes.USER,
      { updatedAt },
    );
    Meteor.users.update(this.userId, {
      $set: { updatedAt },
    });
  },
});

export const setPhoneNumber = new ValidatedMethod({
  name: 'users.setPhoneNumber',
  validate: new SimpleSchema({
    phoneNumber: {
      type: String,
      label: getLabel('api.users.labels.phone'),
    },
  }).validator(),

  run({ phoneNumber }) {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setPhoneNumber - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { phoneNumber },
      );
      throw new Meteor.Error('api.users.setPhoneNumber.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    logServer(
      `USERS - METHODS - UPDATE - setPhoneNumber (user meteor) - userId: ${this.userId}`,
      levels.VERBOSE,
      scopes.USER,
      { phoneNumber },
    );
    Meteor.users.update(this.userId, {
      $set: { phoneNumber },
    });
  },
});

export const setLocation = new ValidatedMethod({
  name: 'users.setLocation',
  validate: new SimpleSchema({
    location: {
      type: String,
      label: getLabel('api.users.labels.location'),
    },
  }).validator(),

  run({ location }) {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setLocation - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { location },
      );
      throw new Meteor.Error('api.users.setLocation.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    logServer(
      `USERS - METHODS - UPDATE - setLocation (user meteor) - userId: ${this.userId}`,
      levels.VERBOSE,
      scopes.USER,
      { location },
    );
    Meteor.users.update(this.userId, {
      $set: { location },
    });
  },
});

export const setJobTitle = new ValidatedMethod({
  name: 'users.setJobTitle',
  validate: new SimpleSchema({
    jobTitle: {
      type: String,
      label: getLabel('api.users.labels.jobTitle'),
    },
  }).validator(),

  run({ jobTitle }) {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setJobTitle - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { jobTitle },
      );
      throw new Meteor.Error('api.users.setJobTitle.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    logServer(
      `USERS - METHODS - UPDATE - setJobTitle (user meteor) - userId: ${this.userId}`,
      levels.VERBOSE,
      scopes.USER,
      { jobTitle },
    );
    Meteor.users.update(this.userId, {
      $set: { jobTitle },
    });
  },
});

export const resetAwaitingStructure = new ValidatedMethod({
  name: 'users.resetAwaitingStructure',
  validate: null,
  run() {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - resetAwaitingStructure - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
      );
      throw new Meteor.Error('api.users.resetAwaitingStructure.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }

    return Meteor.users.update(this.userId, {
      $set: { awaitingStructure: null },
    });
  },
});

export const setAccountType = new ValidatedMethod({
  name: 'users.setAccountType',
  validate: new SimpleSchema({
    accountType: {
      type: String,
      label: getLabel('api.users.labels.accountType'),
    },
  }).validator(),

  run({ accountType }) {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setAccountType - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { accountType },
      );
      throw new Meteor.Error('api.users.setAccountType.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    logServer(
      `USERS - METHODS - UPDATE - setAccountType (user meteor) - userId: ${this.userId}`,
      levels.VERBOSE,
      scopes.USER,
      { accountType },
    );
    const currentUser = Meteor.users.findOne(this.userId);
    Meteor.users.update(this.userId, {
      $set: {
        displayNameComplement: currentUser.displayNameComplement,
        firstName: currentUser.firstName,
        lastName: currentUser.lastName,
        accountType,
      },
    });
  },
});

export const setDepartureDate = new ValidatedMethod({
  name: 'users.setDepartureDate',
  validate: new SimpleSchema({
    departure: {
      type: Date,
      optional: true,
      label: getLabel('api.users.labels.lifeCycleDates.departure'),
    },
  }).validator(),

  run({ departure }) {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setDepartureDate - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { departure },
      );
      throw new Meteor.Error('api.users.setDepartureDate.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    logServer(
      `USERS - METHODS - UPDATE - setDepartureDate (user meteor) - userId: ${this.userId}`,
      levels.VERBOSE,
      scopes.USER,
      { departure },
    );
    Meteor.users.update(this.userId, {
      $set: { lifeCycleDates: { departure } },
    });
  },
});

export const setDelegate = new ValidatedMethod({
  name: 'users.setDelegate',
  validate: new SimpleSchema({
    delegate: {
      type: String,
      optional: true,
      regEx: RegEx.Id,
      label: getLabel('api.users.labels.delegate'),
    },
  }).validator(),

  run({ delegate }) {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - setDelegate - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { delegate },
      );
      throw new Meteor.Error('api.users.setDelegate.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }
    logServer(
      `USERS - METHODS - UPDATE - setDelegate (user meteor) - userId: ${this.userId}`,
      levels.VERBOSE,
      scopes.USER,
      { delegate },
    );
    Meteor.users.update(this.userId, {
      $set: { delegate },
    });
  },
});

export const getOneUser = new ValidatedMethod({
  name: 'users.getOne',
  validate: new SimpleSchema({
    userId: {
      type: String,
      regEx: RegEx.Id,
      label: getLabel('api.users.labels.id'),
    },
  }).validator(),

  run({ userId }) {
    if (!this.userId) {
      logServer(
        `USERS - METHODS - METEOR ERROR - getOne - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { userId },
      );
      throw new Meteor.Error('api.users.getOne.notPermitted', i18n.__('api.users.mustBeLoggedIn'));
    }

    const newUser = Meteor.users.findOne({ _id: userId }, { fields: { firstName: 1, lastName: 1, username: 1 } });
    return newUser;
  },
});

// Get list of all method names on User
const LISTS_METHODS = _.pluck(
  [
    setLastGlobalInfoRead,
    setStructure,
    checkStructure,
    setActive,
    removeUser,
    removeUserFromStructure,
    setAdminOf,
    unsetAdminOf,
    setAdminStructure,
    unsetAdminStructure,
    setAnimatorOf,
    unsetAnimatorOf,
    setMemberOf,
    unsetMemberOf,
    setCandidateOf,
    unsetCandidateOf,
    findUsers,
    findUser,
    setLanguage,
    setMotivation,
    setDisplayNameComplement,
    setLogoutType,
    setKeycloakId,
    setAvatar,
    userUpdated,
    toggleAdvancedPersonalPage,
    toggleBetaServices,
    getAuthToken,
    getIdToken,
    fixUsers,
    hasUserOnRequest,
    hasUserOnAwaitingStructure,
    getUsersAdmin,
    getUsersByStructure,
    getAdminsFromStructure,
    adminOfSet,
    animatorOfSet,
    memberOfSet,
    displayNameSet,
    setThemeMode,
    setUpdatedAt,
    resetAwaitingStructure,
    setDepartureDate,
    setDelegate,
    getOneUser,
  ],
  'name',
);

if (Meteor.isServer) {
  // Only allow 5 list operations per connection per second
  DDPRateLimiter.addRule(
    {
      name(name) {
        return _.contains(LISTS_METHODS, name);
      },

      // Rate limit per connection ID
      connectionId() {
        return true;
      },
    },
    5,
    1000,
  );
}
