import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import checkDomain from '../domains';
import logServer, { levels, scopes } from '../logging';

import PersonalSpaces from '../personalspaces/personalspaces';
import Structures from '../structures/structures';
import Groups from '../groups/groups';
import { _setStructure, AddUserToGroupOfStructure } from './server/methods';
import { generateDefaultPersonalSpace } from '../personalspaces/methods';
import { getToken } from '../server/utils';
import { findStructureByEmail } from './users';

export function checkDisplayName(userData) {
  const updatedData = { ...userData };
  // checks that displayName is unique.
  const uniqueQuery = { firstName: userData.firstName, lastName: userData.lastName };
  // if updating an existing user, exclude him from search
  if ('_id' in userData) uniqueQuery._id = { $ne: userData._id };
  if (userData.displayNameComplement) uniqueQuery.displayNameComplement = userData.displayNameComplement;
  const otherUsers = Meteor.users.find(uniqueQuery).fetch();
  // If not unique, set username as default displayNameComplement (username is always unique)
  if (otherUsers.length >= 1) {
    // force complement to username if no complement yet or
    // if it conflicts with another user complement
    updatedData.displayNameComplement = userData.username;
    otherUsers.forEach((otherUser) => {
      // if other homonym users have no displayNameComplement, we also update them
      if (!otherUser.displayNameComplement) {
        Meteor.users.update(
          { _id: otherUser._id },
          {
            $set: {
              _id: otherUser._id,
              firstName: otherUser.firstName,
              lastName: otherUser.lastName,
              displayNameComplement: otherUser.username,
            },
          },
        );
      }
    });
  }
  return updatedData;
}

if (Meteor.isServer) {
  // server side login hook
  const isAlreadyInStruct = (structureId, currentStructId) => {
    const currentStruct = Structures.findOne({ _id: currentStructId });
    return currentStruct && (currentStruct.ancestorsIds.includes(structureId) || currentStruct._id === structureId);
  };
  Accounts.onLogin((details) => {
    const loginDate = new Date();
    if (details.type === 'keycloak') {
      // update user informations from existing data and keycloak service data
      const updateInfos = {
        ...details.user,
        lastLogin: loginDate,
        primaryEmail: details.user.services.keycloak.email,
      };
      delete updateInfos.services;
      delete updateInfos.profile;
      delete updateInfos.emails;
      const kcPreferredUsername =
        details.user.services.keycloak.preferred_username || details.user.services.keycloak.name;
      if (kcPreferredUsername && kcPreferredUsername !== details.user.username) {
        // use preferred_username as username if defined
        // (should be set as mandatory in keycloak)
        updateInfos.username = kcPreferredUsername;
        logServer(
          `USERS - METHOD - Accounts.onLogin - username changed for user ${details.user._id}`,
          levels.INFO,
          scopes.SYSTEM,
          { old_username: details.user.username, new_username: kcPreferredUsername },
        );
      }

      const kcGivenName = details.user.services.keycloak.given_name || '';
      const kcFamilyName = details.user.services.keycloak.family_name || kcPreferredUsername || '';

      // Dertermine functionnal accounts if account type is not defined in database
      const kcEmail = details.user.services.keycloak.email;
      const [localPart] = kcEmail.split('@');

      if (!updateInfos.accountType) {
        let accountTypeFonctional = false;

        // Switch case to determine if account is functionnal
        // Case 1: Both names are '-'
        if (kcGivenName === '-' && kcFamilyName === '-') accountTypeFonctional = true;
        // Matches pattern ce.d{7}[a-zA-Z]
        else if (new RegExp(/ce.\d{7}[a-zA-Z]/).test(localPart)) accountTypeFonctional = true;
        // Case 3: Matches pattern d{7}[a-zA-Z]
        else if (new RegExp(/\d{7}[a-zA-Z]/).test(localPart)) accountTypeFonctional = true;
        else accountTypeFonctional = false;

        updateInfos.accountType = accountTypeFonctional ? 'functional' : 'personnal';
      }

      updateInfos.firstName = kcGivenName;
      updateInfos.lastName = kcFamilyName;

      if (details.user.isActive === false) {
        // auto activate user based on email address
        if (
          checkDomain(details.user.services.keycloak.email) ||
          Meteor.settings.keycloak.adminEmails.indexOf(details.user.services.keycloak.email) !== -1
        ) {
          updateInfos.isActive = true;
          updateInfos.isRequest = false;
        } else {
          // user email not whitelisted, request activation by admin
          updateInfos.isRequest = true;
        }
      }

      const toNormalize = (name) => {
        const oldMaternelle = 'ECOLE MATERNELLE PUBLIQUE';
        const oldPrimaire = 'ECOLE PRIMAIRE PUBLIQUE';
        const oldElementaire = 'ECOLE ELEMENTAIRE PUBLIQUE';
        const newMaternelle = 'E.M.PU';
        const newPrimaire = 'E.P.PU';
        const newElementaire = 'E.E.PU';

        if (name.toUpperCase().includes(oldMaternelle)) return name.replace(oldMaternelle, newMaternelle);
        if (name.toUpperCase().includes(oldPrimaire)) return name.replace(oldPrimaire, newPrimaire);
        if (name.toUpperCase().includes(oldElementaire)) return name.replace(oldElementaire, newElementaire);
        return name;
      };

      // if account type is functionnal and one KC fields has changed then re-calcul display name complement
      if (
        updateInfos.accountType === 'functional' &&
        (details.user.displayNameComplement === undefined ||
          details.user.firstName !== kcGivenName ||
          details.user.lastName !== kcFamilyName ||
          details.user.primaryEmail !== kcEmail)
      ) {
        let tempDisplayName = '';

        // 1°) firstname = lastname (insensitive)
        if (kcGivenName.toUpperCase() === kcFamilyName.toUpperCase()) tempDisplayName = kcGivenName;

        // 2°) oidc.firstname = '-' et oidc.lastname = '-' et username = localpart email
        if (kcGivenName === '-' && kcFamilyName === '-' && updateInfos.username === localPart)
          tempDisplayName = updateInfos.username;

        // 3°) oidc.firstname = '-' et oidc.lastname = '-' et username != localpart email
        if (kcGivenName === '-' && kcFamilyName === '-' && updateInfos.username !== localPart)
          tempDisplayName = `${updateInfos.username} (${localPart})`;

        // 4°) firstname = '-' et lastname != '-' et (lastname != localpart email ou lastname != username)
        if (
          kcGivenName === '-' &&
          kcFamilyName !== '-' &&
          (kcFamilyName !== localPart || kcFamilyName !== updateInfos.username)
        )
          tempDisplayName = `${kcFamilyName} (${localPart})`;

        // 5°) firstname != '-' et lastname = '-' et (firstname != localpart email ou firstname != username)
        if (
          kcGivenName !== '-' &&
          kcFamilyName === '-' &&
          (kcGivenName !== localPart || kcGivenName !== updateInfos.username)
        )
          tempDisplayName = `${kcGivenName} (${localPart})`;

        if (tempDisplayName !== '') {
          tempDisplayName = toNormalize(tempDisplayName);
          updateInfos.displayNameComplement = tempDisplayName;
        }
      }

      // Manage primary email change
      if (details.user.primaryEmail !== details.user.services.keycloak.email) {
        try {
          Accounts.addEmail(details.user._id, details.user.services.keycloak.email, true);
          if (details.user.primaryEmail !== undefined) {
            Accounts.removeEmail(details.user._id, details.user.primaryEmail);
          }
        } catch (error) {
          logServer(
            `USERS - METHODS - ERROR - Accounts.onLogin - error changing email from \
${details.user.primaryEmail} to ${details.user.services.keycloak.email}`,
            levels.ERROR,
            scopes.SYSTEM,
            {
              username: details.user.username,
              email: details.user.services.keycloak.email,
              error,
              callerId: details.user._id,
            },
          );
          return;
        }
      }

      // manage structure changes
      const needStructureChange =
        !details.user.structure || details.user.primaryEmail !== details.user.services.keycloak.email;
      if (needStructureChange) {
        // check if structure can be determined based on user email
        const structure = findStructureByEmail(details.user.services.keycloak.email);
        if (structure) {
          // check if user structure or awaitingStructure is already a sub-structure of 'structure'
          // if that is the case, no changes are made. Otherwise, remove old structure/awaitingStructure and apply the new one.
          if (details.user.awaitingStructure) {
            // if awaiting structure is equal to detected structure or if it is not in detected structure,
            // cancel awaiting structure (as user will be switch to detected structure)
            if (
              structure._id === details.user.awaitingStructure ||
              !isAlreadyInStruct(structure._id, details.user.awaitingStructure)
            ) {
              // awaitingStructure is not in the target structure, cancel structure request
              updateInfos.awaitingStructure = null;
            }
          }
          if (!details.user.structure || !isAlreadyInStruct(structure._id, details.user.structure)) {
            // set user to detected structure, even if he still has an awaiting structure, and make him directly active
            _setStructure(structure, details.user, true);
            updateInfos.structure = structure._id;
            updateInfos.isActive = true;
          }
        }
      }

      // make sure that default values are set for this user and calculate displayNameComplement if needed
      // const cleanedInfo = Meteor.users.simpleSchema().clean(checkDisplayName(updateInfos));
      const cleanedInfo = checkDisplayName(updateInfos);
      logServer(
        `USERS - METHOD - UPDATE (meteor user infos) - Accounts.onLogin - user id: ${details.user._id}`,
        levels.INFO,
        scopes.SYSTEM,
      );
      // update user in database
      Meteor.users.update(details.user._id, { $set: cleanedInfo });

      // check if user is defined as admin in settings
      if (Meteor.settings.keycloak.adminEmails.indexOf(details.user.services.keycloak.email) !== -1) {
        if (!Roles.userIsInRole(details.user._id, 'admin')) {
          Roles.addUsersToRoles(details.user._id, 'admin');
          cleanedInfo.admin = true;
        }
      }
      // signal updates to plugins
      if (details.user.primaryEmail !== details.user.services.keycloak.email) {
        cleanedInfo.email = details.user.services.keycloak.email;
      }
      Meteor.call('users.userUpdated', { userId: details.user._id, data: cleanedInfo, token: getToken() }, (err) => {
        if (err) {
          logServer(`USERS - METHODS - ERROR - ACCOUNTSUSERHOOK`, levels.ERROR, scopes.SYSTEM, { err });
        }
      });

      if (Meteor.settings.public.disabledFeatures?.menuAdminDefaultSpaces === false) {
        // check if user has a personnal space generated from structure
        const pSpace = PersonalSpaces.findOne({ userId: details.user._id });
        if (cleanedInfo.structure && !pSpace) {
          generateDefaultPersonalSpace.call({ userId: details.user._id });
        }
      }

      // add user to group of structure if he / she is not a member of his structure's group
      const userStructure = Structures.findOne({ _id: cleanedInfo.structure });
      const userGroupOfStructure =
        userStructure && userStructure?.groupId ? Groups.findOne({ _id: userStructure?.groupId }) : null;

      if (userGroupOfStructure !== null && !userGroupOfStructure.members?.includes(details.user._id)) {
        try {
          const userData = Meteor.users.findOne(details.user._id);
          AddUserToGroupOfStructure(userData, userStructure);
        } catch (err) {
          logServer(`USERS - METHODS - ERROR - ACCOUNTSUSERHOOK`, levels.ERROR, scopes.SYSTEM, { err });
        }
      } else if (userStructure && userGroupOfStructure === null) {
        logServer(
          `USERS - METHODS - ERROR - ACCOUNTSUSERHOOK - There is no group attached to structure !!!: `,
          levels.ERROR,
          scopes.SYSTEM,
          { userGroupOfStructure },
        );
      }
    } else {
      logServer(
        `USERS - METHOD - UPDATE (meteor user) - Accounts.onLogin - user id: ${details.user._id} 
        / login date: ${loginDate}`,
        levels.INFO,
        scopes.SYSTEM,
      );
      Meteor.users.update({ _id: details.user._id }, { $set: { lastLogin: loginDate } });
    }
  });
}
