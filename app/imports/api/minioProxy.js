import { WebApp } from 'meteor/webapp';
import s3Client from './files/server/config';
import logServer, { levels, scopes } from './logging';

const { minioBucket } = Meteor.settings.public;

WebApp.connectHandlers.use(async function (req, res, next) {
  if (req._parsedUrl.pathname.startsWith('/objects/')) {
    const filepath = decodeURIComponent(req._parsedUrl.pathname.split('/objects/')[1]);
    let size = 0;
    try {
      // get content-type and other headers values from minio
      const stat = await s3Client.statObject(minioBucket, filepath);
      res.setHeader('Content-Type', stat.metaData['content-type']);
      res.setHeader('Content-Length', stat.size);
      res.setHeader('Last-Modified', stat.lastModified.toUTCString());
      res.setHeader('Etag', stat.etag);
      // fetch and send data from minio
      const dataStream1 = await s3Client.getObject(minioBucket, filepath);
      dataStream1.on('data', function (chunk) {
        size += chunk.length;
        res.write(chunk);
      });
      dataStream1.on('end', function () {
        logServer('API - MINIOPROXY - object sent', levels.VERBOSE, scopes.SYSTEM, { filepath, size });
        res.end();
      });
      dataStream1.on('error', function (error) {
        logServer('API - MINIOPROXY - ERROR - Error serving object', levels.ERROR, scopes.SYSTEM, {
          filepath,
          code: error.code,
        });
        res.writeHead(500, 'ERROR serving object');
        res.end();
      });
    } catch (error) {
      logServer('API - MINIOPROXY - ERROR - Error getting object', levels.ERROR, scopes.SYSTEM, {
        filepath,
        code: error.code,
      });
      res.writeHead(500, 'ERROR getting object');
      res.end();
    }
  } else next.apply();
});
