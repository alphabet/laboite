import { Meteor } from 'meteor/meteor';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { _ } from 'meteor/underscore';
import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { Roles } from 'meteor/alanning:roles';
import i18n from 'meteor/universe:i18n';
import sanitizeHtml from 'sanitize-html';
import slugify from 'slugify';
import { isActive, getLabel, validateString, sanitizeParameters } from '../../utils';
import Groups from '../groups';
import { favGroup, reservedGroupNames } from '../methods';
import logServer, { levels, scopes } from '../../logging';
import { userStructures } from '../../structures/utils';
import { checkApiToken, getToken } from '../../server/utils';
import { validateShareName } from '../utils';
import RegEx from '../../regExp';

export const addGroupMembersToGroup = new ValidatedMethod({
  name: 'groups.addGroupMembersToGroup',
  validate: new SimpleSchema({
    groupId: { type: String, regEx: RegEx.Id, label: getLabel('api.groups.labels.id') },
    otherGroupId: { type: String, regEx: RegEx.Id, label: getLabel('api.groups.labels.id') },
  }).validator(),

  run({ groupId, otherGroupId }) {
    // check group and user existence
    const group = Groups.findOne({ _id: groupId });
    const group2 = Groups.findOne({ _id: otherGroupId });
    if (group === undefined || group2 === undefined) {
      logServer(
        `GROUPS - METHODS - METEOR ERROR - addGroupMembersToGroup - ${i18n.__('api.groups.unknownGroup')}`,
        levels.ERROR,
        scopes.USER,
        { groupId, otherGroupId },
      );
      throw new Meteor.Error('api.groups.addGroupMemberToGroup.unknownGroup', i18n.__('api.groups.unknownGroup'));
    }
    // check if current user has admin rights on group (or global admin)
    const authorized =
      (isActive(this.userId) && Roles.userIsInRole(this.userId, 'admin', groupId)) ||
      (this.userId === group.owner && this.userId === group2.owner);
    if (!authorized) {
      logServer(
        `GROUPS - METHODS - METEOR ERROR - addGroupMembersToGroup - ${i18n.__('api.groups.adminGroupNeeded')}`,
        levels.ERROR,
        scopes.USER,
        { groupId, otherGroupId },
      );
      throw new Meteor.Error('api.groups.addGroupMemberToGroup.notPermitted', i18n.__('api.groups.adminGroupNeeded'));
    }

    const usersGroup = group2.members;

    const checkStructs = (user, groupData) => {
      // bypass check if group is not restricted
      if (!groupData.structureIds) return true;
      const userData = Meteor.users.findOne(user);
      if (userData) {
        const userStructs = userStructures(userData);
        if (groupData.structureIds.some((structureId) => userStructs.includes(structureId))) return true;
      }
      return false;
    };

    let nb = 0;
    usersGroup.forEach((user) => {
      // check if group is restricted to some structures
      if (checkStructs(user, group)) {
        // add role to user collection
        if (!Roles.userIsInRole(user, 'member', groupId)) {
          Roles.addUsersToRoles(user, 'member', groupId);
          // remove candidate Role if present
          if (Roles.userIsInRole(user, 'candidate', groupId)) {
            Roles.removeUsersFromRoles(user, 'candidate', groupId);
          }
          // store info in group collection
          if (group.members.indexOf(user) === -1) {
            Groups.update(groupId, {
              $push: { members: user },
              $pull: { candidates: user },
            });
          }
          // update user personalSpace
          favGroup._execute({ userId: usersGroup[nb] }, { groupId });
          nb += 1;
        }
      }
    });
    logServer(
      `GROUPS - METHODS - ADD - addGroupMembersToGroup - User ${this.userId} add members to group ${groupId} `,
      levels.VERBOSE,
      scopes.USER,
    );

    return nb;
  },
});

export const groupCreated = new ValidatedMethod({
  name: 'groups.groupCreated',
  validate: null,
  run(data) {
    // this function is used to provide hooks when a group is created
    checkApiToken('groupCreated', data, data.token);
    return data?.result;
  },
});

export function _createGroup({
  name,
  customSlug,
  type,
  content,
  description,
  avatar,
  plugins,
  userId,
  shareName,
  structureIds,
}) {
  if (reservedGroupNames.includes(name)) {
    logServer(
      `GROUPS - METHODS - METEOR ERROR - createGroup - ${i18n.__('api.groups.groupAlreadyExist')}`,
      levels.ERROR,
      scopes.SYSTEM,
      { name, type, content, description, avatar, plugins },
    );
    throw new Meteor.Error('api.groups.createGroup.notPermitted', i18n.__('api.groups.groupAlreadyExist'));
  }
  validateString(name);
  validateString(description);
  validateString(avatar);
  if (shareName) validateString(shareName);
  const sanitizedContent = sanitizeHtml(content, sanitizeParameters);
  validateString(sanitizedContent);
  try {
    const user = Meteor.users.findOne(userId);
    if (user.groupCount < user.groupQuota) {
      let finalCustomSlug = customSlug;
      if (!customSlug || customSlug === '') {
        finalCustomSlug = slugify(name, {
          replacement: '-', // replace spaces with replacement
          remove: null, // regex to remove characters
          lower: true, // result in lower case
        });
      }

      const existing = Groups.findOne({ customSlug: finalCustomSlug });
      if (existing) {
        logServer(
          `GROUPS - METHODS - METEOR ERROR - _createGroup - ${i18n.__('api.groups.groupSlugAlreadyExist')}`,
          levels.ERROR,
          scopes.SYSTEM,
          { name, type, content, description, avatar, plugins, userId },
        );
        throw new Meteor.Error('api.groups.createGroup.duplicateSlug', i18n.__('api.groups.groupSlugAlreadyExist'));
      }

      const groupData = {
        name,
        customSlug: finalCustomSlug,
        type,
        content: sanitizedContent,
        description,
        avatar,
        owner: userId,
        members: [],
        animators: [],
        admins: [userId],
        active: true,
        plugins,
        shareName,
      };
      let canBeMember = true;
      if (Array.isArray(structureIds) && structureIds.length > 0) {
        groupData.structureIds = structureIds;
        // check if admin is in one of the allowed structures,
        // if not, don't set him as member / animator
        const userStructs = userStructures(user);
        if (!structureIds.some((struct) => userStructs.includes(struct))) canBeMember = false;
      }
      if (canBeMember) {
        groupData.members.push(userId);
        groupData.animators.push(userId);
      }
      const groupId = Groups.insert(groupData);
      Roles.addUsersToRoles(userId, ['admin'], groupId);
      if (canBeMember) {
        Roles.addUsersToRoles(userId, ['animator', 'member'], groupId);
        favGroup._execute({ userId }, { groupId });
      }
      logServer(
        `GROUPS - METHODS - CREATE - createGroup - User ${userId} create group ${groupId}`,
        levels.VERBOSE,
        scopes.USER,
      );

      if (type !== 15) {
        user.groupCount += 1;
        Meteor.users.update(userId, {
          $set: { groupCount: user.groupCount },
        });
      }

      // move group temp avatar from user minio to group minio and update avatar link
      if (avatar !== '' && avatar.includes('groupAvatar.png')) {
        const LABOITE_HOST = new URL('/objects/', Meteor.absoluteUrl()).href;
        Meteor.call('files.move', {
          sourcePath: `users/${userId}`,
          destinationPath: `groups/${groupId}`,
          files: [`${LABOITE_HOST}users/${userId}/groupAvatar.png`],
        });

        const avatarLink = `${LABOITE_HOST}groups/${groupId}/groupAvatar.png?${new Date().getTime()}`;

        Groups.update({ _id: groupId }, { $set: { avatar: avatarLink } });
      }
      // notify plugins of group creation (except for automatic groups)
      if (Meteor.isServer && type !== 15) {
        Meteor.call('groups.groupCreated', {
          name,
          customSlug: finalCustomSlug,
          type,
          content,
          description,
          avatar,
          plugins,
          userId,
          shareName,
          structureIds,
          result: groupId,
          token: getToken(),
        });
      }
      return groupId;
    }
    logServer(
      `GROUPS - METHODS - METEOR ERROR - _createGroup - ${i18n.__('api.groups.toManyGroup')}`,
      levels.VERBOSE,
      scopes.SYSTEM,
    );
    throw new Meteor.Error('api.groups.createGroup.toManyGroup', i18n.__('api.groups.toManyGroup'));
  } catch (error) {
    if (error.code === 11000) {
      logServer(
        `GROUPS - METHODS - METEOR ERROR - _createGroup - ${i18n.__('api.groups.groupAlreadyExist')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { name, type, content, description, avatar, plugins, userId },
      );
      throw new Meteor.Error('api.groups.createGroup.duplicateName', i18n.__('api.groups.groupAlreadyExist'));
    } else {
      logServer(
        `GROUPS - METHODS - ERROR - createGroup - fail when user ${userId} create group`,
        levels.WARN,
        scopes.SYSTEM,
        {
          errorMessage: error.message,
        },
      );
      throw error;
    }
  }
}

export const createGroup = new ValidatedMethod({
  name: 'groups.createGroup',
  validate: new SimpleSchema({
    name: { type: String, min: 1, label: getLabel('api.groups.labels.name') },
    customSlug: { type: String, optional: true, label: getLabel('api.groups.labels.customSlug') },
    type: { type: SimpleSchema.Integer, min: 0, label: getLabel('api.groups.labels.type') },
    description: { type: String, label: getLabel('api.groups.labels.description') },
    content: { type: String, defaultValue: '', label: getLabel('api.groups.labels.content') },
    avatar: { type: String, defaultValue: '', label: getLabel('api.groups.labels.avatar') },
    plugins: { type: Object, optional: true, blackbox: true, label: getLabel('api.groups.labels.plugins') },
    shareName: {
      type: String,
      optional: true,
      label: getLabel('api.groups.labels.shareName'),
      custom: validateShareName,
    },
    structureIds: {
      type: Array,
      optional: true,
    },
    'structureIds.$': {
      type: String,
      regEx: RegEx.Id,
      label: getLabel('api.groups.labels.structureIds'),
    },
  }).validator({ clean: true }),

  run({ name, customSlug, type, content, description, avatar, plugins, shareName, structureIds }) {
    if (!isActive(this.userId)) {
      logServer(
        `GROUPS - METHODS - METEOR ERROR - createGroup - ${i18n.__('api.users.mustBeLoggedIn')}`,
        levels.WARN,
        scopes.SYSTEM,
        { name, type, content, description, avatar, plugins },
      );
      throw new Meteor.Error('api.groups.createGroup.notLoggedIn', i18n.__('api.users.mustBeLoggedIn'));
    }
    return _createGroup({
      name,
      customSlug,
      type,
      content,
      description,
      plugins,
      avatar,
      userId: this.userId,
      shareName,
      structureIds,
    });
  },
});

export const groupRemoved = new ValidatedMethod({
  name: 'groups.groupRemoved',
  validate: null,
  run(data) {
    // this function is used to provide hooks when a group is created
    checkApiToken('groupRemoved', data, data.token);
    return data?.result;
  },
});

// eslint-disable-next-line react/prop-types
export function _removeGroup({ groupId, userId }) {
  // check group existence
  const group = Groups.findOne({ _id: groupId });
  if (group === undefined) {
    logServer(
      `GROUPS - METHODS - METEOR ERROR - _removeGroup - ${i18n.__('api.groups.unknownGroup')}`,
      levels.ERROR,
      scopes.SYSTEM,
      { groupId, userId },
    );
    throw new Meteor.Error('api.groups.removeGroup.unknownGroup', i18n.__('api.groups.unknownGroup'));
  }
  // check if current user has admin rights on group (or global admin)
  // FIXME : allow only for owner or for all admins ?
  const isAdmin = isActive(userId) && Roles.userIsInRole(userId, 'admin', groupId);
  const authorized = isAdmin || userId === group.owner;
  if (!authorized) {
    logServer(
      `GROUPS - METHODS - METEOR ERROR - _removeGroup - ${i18n.__('api.groups.adminGroupNeeded')}`,
      levels.ERROR,
      scopes.SYSTEM,
      { groupId, userId },
    );
    throw new Meteor.Error('api.groups.removeGroup.notPermitted', i18n.__('api.groups.adminGroupNeeded'));
  }

  if (group.type !== 15) {
    // Update group quota for owner
    const owner = Meteor.users.findOne({ _id: group.owner });
    if (owner !== undefined) {
      owner.groupCount -= 1;
      if (owner.groupCount <= 0) {
        owner.groupCount = 0;
      }
      Meteor.users.update(group.owner, {
        $set: { groupCount: owner.groupCount },
      });
    }
  }

  // remove all roles set on this group
  Roles.removeScope(groupId);
  logServer(`GROUPS - METHODS - REMOVE - _removeGroup - groupId: ${groupId}`, levels.INFO, scopes.USER);
  Groups.remove(groupId);
  // remove from users favorite groups
  Meteor.users.update({ favGroups: { $all: [groupId] } }, { $pull: { favGroups: groupId } }, { multi: true });
  logServer(`GROUPS - METHODS - UPDATE - user ${userId} remove group ${groupId}`, levels.VERBOSE, scopes.USER);
  if (Meteor.isServer && group.type !== 15) {
    // trigger group plugins hooks
    Meteor.call('groups.groupRemoved', {
      groupId,
      result: group,
      token: getToken(),
    });
  }
  return group;
}

export const removeGroup = new ValidatedMethod({
  name: 'groups.removeGroup',
  validate: new SimpleSchema({
    groupId: { type: String, regEx: RegEx.Id, label: getLabel('api.groups.labels.id') },
  }).validator(),

  run({ groupId }) {
    return _removeGroup({ groupId, userId: this.userId });
  },
});

if (Meteor.isServer) {
  // Get list of all method names on User
  const LISTS_METHODS = _.pluck([addGroupMembersToGroup, groupCreated, createGroup, removeGroup], 'name');
  // Only allow 5 list operations per connection per second
  DDPRateLimiter.addRule(
    {
      name(name) {
        return _.contains(LISTS_METHODS, name);
      },

      // Rate limit per connection ID
      connectionId() {
        return true;
      },
    },
    5,
    1000,
  );
}
