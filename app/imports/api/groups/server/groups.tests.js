/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { Factory } from 'meteor/dburles:factory';
import { PublicationCollector } from 'meteor/johanbrook:publication-collector';
import { assert } from 'chai';
import { Random } from 'meteor/random';
import { faker } from '@faker-js/faker';
import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import '../../../startup/i18n/en.i18n.json';

// this file also includes tests on users/permissions
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';

import Groups from '../groups';
import PersonalSpaces from '../../personalspaces/personalspaces';
import { updateGroup, favGroup, unfavGroup, countMembersOfGroup, checkShareName } from '../methods';
import { createGroup, removeGroup } from './methods';
import './publications';
import './factories';
import '../../structures/server/factories';
import {
  setAdminOf,
  unsetAdminOf,
  setMemberOf,
  unsetMemberOf,
  setCandidateOf,
  unsetCandidateOf,
  setAnimatorOf,
  unsetAnimatorOf,
} from '../../users/server/methods';
import Structures from '../../structures/structures';

function pspaceHasGroup(user, id) {
  const pspace = PersonalSpaces.findOne({
    userId: user,
    unsorted: { $elemMatch: { type: 'group', element_id: id } },
  });
  const inFavs = Meteor.users.findOne(user).favGroups.includes(id);
  return pspace !== undefined && inFavs;
}

describe('groups', function () {
  describe('mutators', function () {
    it('builds correctly from factory', function () {
      const group = Factory.create('group', { owner: Random.id() });
      assert.typeOf(group, 'object');
      assert.equal(group.active, true);
    });
  });
  describe('publications', function () {
    let userId;
    let groupId;
    let structureId;
    let otherStructureId;
    let limitedGroup1Id;
    let limitedGroup2Id;
    beforeEach(function () {
      Groups.remove({});
      Structures.remove({});
      Meteor.roleAssignment.remove({});
      Meteor.roles.remove({});
      Meteor.users.remove({});
      Roles.createRole('admin');
      Roles.createRole('member');
      structureId = Factory.create('structure')._id;
      otherStructureId = Factory.create('structure')._id;
      const email = faker.internet.email();
      userId = Accounts.createUser({
        email,
        username: email,
        password: 'toto',
        structure: structureId,
        firstName: faker.person.firstName(),
        lastName: faker.person.lastName(),
        groupCount: 0,
        groupQuota: 10,
      });
      Meteor.users.update(userId, { $set: { isActive: true } });
      Groups.remove({});
      _.times(3, () => Factory.create('group', { name: `test${Random.id()}`, owner: Random.id() }));
      groupId = Factory.create('group', { owner: Random.id(), name: 'MonGroupe' })._id;
      // this group should be accessible to user 'userId'
      limitedGroup1Id = Factory.create('group', {
        owner: Random.id(),
        name: 'limitedGroup1',
        slug: 'limitedgroup1',
        structureIds: [structureId],
      })._id;
      // this group should not be accessible to user 'userId' (no access to limiting structure)
      limitedGroup2Id = Factory.create('group', {
        owner: Random.id(),
        name: 'limitedGroup2',
        slug: 'limitedgroup2',
        structureIds: [otherStructureId],
      })._id;
    });
    describe('groups.all', function () {
      it('sends all groups', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('groups.all', { page: 1, search: '', itemPerPage: 10 }, (collections) => {
          assert.equal(collections.groups.length, 5);
          // check that limitedGroup1 is returned, but not limitedGroup2
          const groupIds = collections.groups.map((group) => group._id);
          assert.equal(groupIds.includes(limitedGroup1Id), true);
          assert.equal(groupIds.includes(limitedGroup2Id), false);
          done();
        });
      });
      it('sends a specific page from all groups', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('groups.all', { page: 2, search: '', itemPerPage: 3 }, (collections) => {
          assert.equal(collections.groups.length, 2);
          done();
        });
      });
      it('sends all groups matching a filter', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('groups.all', { page: 1, search: 'test', itemPerPage: 10 }, (collections) => {
          assert.equal(collections.groups.length, 3);
          done();
        });
      });
    });
    describe('groups.single', function () {
      it('sends all public fields for a specific group', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('groups.single', { slug: 'mongroupe' }, (collections) => {
          assert.equal(collections.groups.length, 1);
          done();
        });
      });
      it('sends all public fields for an accessible structure limited group', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('groups.single', { slug: 'limitedgroup1' }, (collections) => {
          assert.equal(collections.groups.length, 1);
          done();
        });
      });
      it('sends nothing for a non accessible structure limited group', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('groups.single', { slug: 'limitedgroup2' }, (collections) => {
          assert.equal(collections.groups, undefined);
          done();
        });
      });
    });
    describe('groups.one', function () {
      it('sends all public fields for a specific group', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('groups.one', { slug: 'mongroupe' }, (collections) => {
          assert.equal(collections.groups.length, 1);
          done();
        });
      });
    });
    describe('groups.one.admin', function () {
      it('sends all admin fields for a specific group (to admin user only)', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('groups.one.admin', { _id: groupId }, (collections) => {
          assert.notProperty(collections, 'groups');
        });
        Roles.addUsersToRoles(userId, 'admin');
        collector.collect('groups.one.admin', { _id: groupId }, (collections) => {
          assert.equal(collections.groups.length, 1);
          assert.property(collections.groups[0], 'admins');
          done();
        });
      });
    });
    describe('groups.users', function () {
      it('sends all users with a given role on a group', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('groups.users', { groupId, role: 'member' }, (collections) => {
          assert.equal(collections.users, undefined);
        });
        setMemberOf._execute({ userId }, { userId, groupId });
        collector.collect('groups.users', { groupId, role: 'member' }, (collections) => {
          assert.equal(collections.users.length, 1);
          assert.equal(collections.users[0]._id, userId);
          done();
        });
      });
    });
    describe('groups.adminof', function () {
      it('sends all groups that user is admin/animator of', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('groups.adminof', (collections) => {
          assert.notProperty(collections, 'groups');
        });
        Roles.addUsersToRoles(userId, 'admin');
        // global admin : returns all existing groups
        collector.collect('groups.adminof', (collections) => {
          assert.equal(collections.groups.length, 6);
        });
        setAdminOf._execute({ userId }, { userId, groupId });
        Roles.removeUsersFromRoles(userId, 'admin');
        /**
         * Here, we need a brand new collector as the existing seems to keep track of documents that are NOT published
         * (you can check it by logging variables in the publications)
         */
        const newCollector = new PublicationCollector({ userId });
        // group admin only : returns only groups user is admin of
        newCollector.collect('groups.adminof', (collections) => {
          assert.equal(collections.groups.length, 1);
          assert.equal(collections.groups[0]._id, groupId);
          done();
        });
      });
    });
    describe('groups.one.admin', function () {
      it('sends all groups that user is admin/animator of', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('groups.adminof', (collections) => {
          assert.notProperty(collections, 'groups');
        });
        Roles.addUsersToRoles(userId, 'admin');
        // global admin : returns all existing groups
        collector.collect('groups.adminof', (collections) => {
          assert.equal(collections.groups.length, 6);
        });
        setAdminOf._execute({ userId }, { userId, groupId });
        Roles.removeUsersFromRoles(userId, 'admin');
        const newCollector = new PublicationCollector({ userId });

        // group admin only : returns only groups user is admin of
        newCollector.collect('groups.adminof', (collections) => {
          assert.equal(collections.groups.length, 1);
          assert.equal(collections.groups[0]._id, groupId);
          done();
        });
      });
    });
  });
  describe('methods', function () {
    let groupId;
    let group2Id;
    let group3Id;
    let group4Id;
    let group5Id;
    let group6Id;
    let group7Id;
    let group8Id;
    let moderatedGroupId;
    let closedGroupId;
    let restrictedGroupId;
    let userId;
    let adminId;
    let otherUserId;
    let structureId;
    let otherStructureId;
    beforeEach(function () {
      // Clear
      Groups.remove({});
      PersonalSpaces.remove({});
      Structures.remove({});
      Meteor.roleAssignment.remove({});
      Meteor.users.remove({});
      Meteor.roles.remove({});
      Roles.createRole('admin');
      Roles.createRole('animator');
      Roles.createRole('member');
      Roles.createRole('candidate');
      // Generate 'structures'
      structureId = Factory.create('structure')._id;
      otherStructureId = Factory.create('structure')._id;
      // Generate 'users'
      const email = faker.internet.email();
      userId = Accounts.createUser({
        email,
        username: email,
        password: 'toto',
        structure: structureId,
        firstName: faker.person.firstName(),
        lastName: faker.person.lastName(),
        groupCount: 0,
        groupQuota: 10,
      });
      const emailAdmin = faker.internet.email();
      adminId = Accounts.createUser({
        email: emailAdmin,
        username: emailAdmin,
        password: 'toto',
        structure: structureId,
        firstName: faker.person.firstName(),
        lastName: faker.person.lastName(),
        groupCount: 0,
        groupQuota: 10,
      });
      const emailOtherUser = faker.internet.email();
      otherUserId = Accounts.createUser({
        email: emailOtherUser,
        username: emailOtherUser,
        password: 'toto',
        structure: structureId,
        firstName: faker.person.firstName(),
        lastName: faker.person.lastName(),
        groupCount: 0,
        groupQuota: 10,
      });
      // set this user as global admin
      Roles.addUsersToRoles(adminId, 'admin');
      // set users as active
      Meteor.users.update({}, { $set: { isActive: true } }, { multi: true });
      // Create a group owned by userId
      groupId = Factory.create('group', { owner: userId, customSlug: 'groupId' })._id;
      setAdminOf._execute({ userId: adminId }, { userId, groupId });
      // Create a group owned by random user and set userId as admin
      group2Id = Factory.create('group', { owner: Random.id(), customSlug: 'group2Id' })._id;
      group3Id = Factory.create('group', { owner: Random.id(), customSlug: 'group3Id' })._id;
      group4Id = Factory.create('group', { name: 'group4', owner: userId, customSlug: 'group4Id' })._id;
      group5Id = Factory.create('group', {
        owner: Random.id(),
        structureIds: [structureId],
        customSlug: 'group5Id',
      })._id;
      group6Id = Factory.create('group', {
        owner: Random.id(),
        structureIds: [otherStructureId],
        customSlug: 'group6Id',
      })._id;
      setAdminOf._execute({ userId: adminId }, { userId, groupId: group4Id });
      // create moderated/closed groups
      moderatedGroupId = Factory.create('group', { type: 5, owner: Random.id(), customSlug: 'moderatedGroupId' })._id;
      closedGroupId = Factory.create('group', { type: 10, owner: Random.id(), customSlug: 'closedGroupId' })._id;
      restrictedGroupId = Factory.create('group', {
        type: 20,
        owner: otherUserId,
        customSlug: 'restrictedGroupId',
      })._id;
      setAdminOf._execute({ userId: adminId }, { userId: otherUserId, groupId: restrictedGroupId });
      group7Id = Factory.create('group', {
        type: 5,
        owner: Random.id(),
        structureIds: [structureId],
        customSlug: 'group7Id',
      })._id;
      group8Id = Factory.create('group', {
        type: 5,
        owner: Random.id(),
        structureIds: [otherStructureId],
        customSlug: 'group8Id',
      })._id;
      setAdminOf._execute({ userId: adminId }, { userId, groupId: group2Id });
    });
    describe('(un)setAdminOf', function () {
      it('global admin can set/unset a user as admin of a group', function () {
        setAdminOf._execute({ userId: adminId }, { userId, groupId: group3Id });
        let group = Groups.findOne(group3Id);
        assert.equal(Roles.userIsInRole(userId, 'admin', group3Id), true);
        assert.include(group.admins, userId, 'group admins list contains userId');
        unsetAdminOf._execute({ userId: adminId }, { userId, groupId: group3Id });
        group = Groups.findOne(group3Id);
        assert.equal(Roles.userIsInRole(userId, 'admin', group3Id), false);
        assert.notInclude(group.admins, userId, "group admins list shouldn't contain userId");
        // can setAdminOf even if target user is not in permitted structures
        setAdminOf._execute({ userId: adminId }, { userId, groupId: group5Id });
        setAdminOf._execute({ userId: adminId }, { userId, groupId: group6Id });
        assert.equal(Roles.userIsInRole(userId, 'admin', group5Id), true);
        assert.equal(Roles.userIsInRole(userId, 'admin', group6Id), true);
      });
      it('group admin can set/unset a user as admin of a group', function () {
        setAdminOf._execute({ userId }, { userId: otherUserId, groupId: group2Id });
        let group = Groups.findOne(group2Id);
        assert.equal(Roles.userIsInRole(otherUserId, 'admin', group2Id), true);
        assert.include(group.admins, otherUserId, 'group admins list contains otherUserId');
        unsetAdminOf._execute({ userId }, { userId: otherUserId, groupId: group2Id });
        group = Groups.findOne(group2Id);
        assert.equal(Roles.userIsInRole(otherUserId, 'admin', group2Id), false);
        assert.notInclude(group.admins, otherUserId, "group admins list shouldn't contain otherUserId");
      });
      it('only global or group admin can set/unset a user as admin of a group', function () {
        // Throws if non owner/admin user, or logged out user
        assert.throws(
          () => {
            setAdminOf._execute({ userId: otherUserId }, { userId, groupId });
          },
          Meteor.Error,
          /api.users.setAdminOf.notPermitted/,
        );
        assert.throws(
          () => {
            unsetAdminOf._execute({ userId: otherUserId }, { userId, groupId });
          },
          Meteor.Error,
          /api.users.unsetAdminOf.notPermitted/,
        );
      });
    });
    describe('(un)setAnimatorOf', function () {
      it('global admin can set/unset a user as animator of a group', function () {
        setAnimatorOf._execute({ userId: adminId }, { userId, groupId: group3Id });
        let group = Groups.findOne(group3Id);
        assert.equal(Roles.userIsInRole(userId, 'animator', group3Id), true);
        assert.include(group.animators, userId, 'group animators list contains userId');
        assert.include(group.members, userId, 'group members list contains userId');
        assert.equal(pspaceHasGroup(userId, group3Id), true, 'group is in personal space');
        unsetAnimatorOf._execute({ userId: adminId }, { userId, groupId: group3Id });
        group = Groups.findOne(group3Id);
        assert.equal(Roles.userIsInRole(userId, 'animator', group3Id), false);
        assert.equal(Roles.userIsInRole(userId, 'member', group3Id), true);
        assert.notInclude(group.animators, userId, "group animators list shouldn't contain userId");
        assert.include(group.members, userId, 'group members list contains userId');
        assert.equal(pspaceHasGroup(userId, group3Id), true, 'group is always in personal space');
        // can not setAnimatorOf if target user is not in permitted structures
        assert.throws(
          () => {
            setAnimatorOf._execute({ userId: adminId }, { userId, groupId: group6Id });
          },
          Meteor.Error,
          /api.users.setAnimatorOf.wrongStructure/,
        );
        // can setAnimatorOf if target user is in permitted structures
        setAnimatorOf._execute({ userId: adminId }, { userId, groupId: group5Id });
        assert.equal(Roles.userIsInRole(userId, 'animator', group5Id), true);
      });
      it('group admin can set/unset a user as animator of a group', function () {
        setAnimatorOf._execute({ userId }, { userId: otherUserId, groupId: group2Id });
        let group = Groups.findOne(group2Id);
        assert.equal(Roles.userIsInRole(otherUserId, 'member', group2Id), true);
        assert.equal(Roles.userIsInRole(otherUserId, 'animator', group2Id), true);
        assert.include(group.animators, otherUserId, 'group animators list contains otherUserId');
        assert.equal(pspaceHasGroup(otherUserId, group2Id), true, 'group is in personal space');
        unsetAnimatorOf._execute({ userId }, { userId: otherUserId, groupId: group2Id });
        group = Groups.findOne(group2Id);
        assert.equal(Roles.userIsInRole(otherUserId, 'animator', group2Id), false);
        assert.notInclude(group.animators, otherUserId, "group animators list shouldn't contain otherUserId");
        assert.include(group.members, otherUserId, 'otherUserId is always member of the group');
        assert.equal(pspaceHasGroup(otherUserId, group2Id), true, 'group is always in personal space');
      });
      it('only global or group admin can set/unset a user as animator of a group', function () {
        // Throws if non owner/admin user, or logged out user
        assert.throws(
          () => {
            setAnimatorOf._execute({ userId: otherUserId }, { userId, groupId });
          },
          Meteor.Error,
          /api.users.setAnimatorOf.notPermitted/,
        );
        assert.throws(
          () => {
            unsetAnimatorOf._execute({ userId: otherUserId }, { userId, groupId });
          },
          Meteor.Error,
          /api.users.unsetAnimatorOf.notPermitted/,
        );
      });
    });
    describe('(un)setMemberOf', function () {
      it('global admin can set/unset a user as member of a group', function () {
        setMemberOf._execute({ userId: adminId }, { userId, groupId: group3Id });
        let group = Groups.findOne(group3Id);
        assert.equal(Roles.userIsInRole(userId, 'member', group3Id), true);
        assert.include(group.members, userId, 'group members list contains userId');
        assert.equal(pspaceHasGroup(userId, group3Id), true, 'group is in personal space');
        unsetMemberOf._execute({ userId: adminId }, { userId, groupId: group3Id });
        group = Groups.findOne(group3Id);
        assert.equal(Roles.userIsInRole(userId, 'member', group3Id), false);
        assert.notInclude(group.members, userId, "group members list shouldn't contain userId");
        assert.equal(pspaceHasGroup(userId, group3Id), false, 'group is no longer in personal space');
        // can not setMemberOf if target user is not in permitted structures
        assert.throws(
          () => {
            setMemberOf._execute({ userId: adminId }, { userId, groupId: group6Id });
          },
          Meteor.Error,
          /api.users.setMemberOf.wrongStructure/,
        );
        // can setMemberOf if target user is in permitted structures
        setMemberOf._execute({ userId: adminId }, { userId, groupId: group5Id });
        assert.equal(Roles.userIsInRole(userId, 'member', group5Id), true);
      });
      it('group admin can set/unset a user as member of a group', function () {
        setMemberOf._execute({ userId }, { userId: otherUserId, groupId: group2Id });
        let group = Groups.findOne(group2Id);
        assert.equal(Roles.userIsInRole(otherUserId, 'member', group2Id), true);
        assert.include(group.members, otherUserId, 'group members list contains otherUserId');
        assert.equal(pspaceHasGroup(otherUserId, group2Id), true, 'group is in personal space');
        unsetMemberOf._execute({ userId }, { userId: otherUserId, groupId: group2Id });
        group = Groups.findOne(group2Id);
        assert.equal(Roles.userIsInRole(otherUserId, 'member', group2Id), false);
        assert.notInclude(group.members, otherUserId, "group members list shouldn't contain otherUserId");
        assert.equal(pspaceHasGroup(otherUserId, group2Id), false, 'group is no longer in personal space');
      });
      it('group animator can set/unset a user as member of a group', function () {
        setAnimatorOf._execute({ userId: adminId }, { userId: otherUserId, groupId: group2Id });
        setMemberOf._execute({ userId: otherUserId }, { userId, groupId: group2Id });
        let group = Groups.findOne(group2Id);
        assert.equal(Roles.userIsInRole(userId, 'member', group2Id), true);
        assert.include(group.members, userId, 'group members list contains userId');
        assert.equal(pspaceHasGroup(userId, group2Id), true, 'group is in personal space');
        unsetMemberOf._execute({ userId: otherUserId }, { userId, groupId: group2Id });
        group = Groups.findOne(group2Id);
        assert.equal(Roles.userIsInRole(userId, 'member', group2Id), false);
        assert.equal(Roles.userIsInRole(userId, 'animator', group2Id), false);
        assert.notInclude(group.members, userId, "group members list shouldn't contain userId");
        // as userId is also animator of group2, if he is deleted from member list he lost animator rights
        assert.notInclude(group.animators, userId, "group animators list shouldn't contain userId");
        // if we remove all permissions, group should no longer be in user personal space
        unsetAdminOf._execute({ userId: adminId }, { userId, groupId: group2Id });
        group = Groups.findOne(group2Id);
        assert.notInclude(group.admins, userId, "group admins list shouldn't contain userId");
        assert.equal(pspaceHasGroup(userId, group2Id), false, 'group is not in personal space');
      });
      it('normal user can set/unset himself as member only for open groups', function () {
        setMemberOf._execute({ userId }, { userId, groupId: group3Id });
        let group = Groups.findOne(group3Id);
        assert.equal(Roles.userIsInRole(userId, 'member', group3Id), true);
        assert.include(group.members, userId, 'group members list contains userId');
        assert.equal(pspaceHasGroup(userId, group3Id), true, 'group is in personal space');
        unsetMemberOf._execute({ userId }, { userId, groupId: group3Id });
        group = Groups.findOne(group3Id);
        assert.equal(Roles.userIsInRole(userId, 'member', group3Id), false);
        assert.notInclude(group.members, userId, "group members list shouldn't contain userId");
        assert.equal(pspaceHasGroup(userId, group3Id), false, 'group is no longer in personal space');
        assert.throws(
          () => {
            setMemberOf._execute({ userId }, { userId, groupId: moderatedGroupId });
          },
          Meteor.Error,
          /api.users.setMemberOf.notPermitted/,
        );
        assert.throws(
          () => {
            setMemberOf._execute({ userId }, { userId, groupId: closedGroupId });
          },
          Meteor.Error,
          /api.users.setMemberOf.notPermitted/,
        );
        // can not setMemberOf if user is not in permitted structures
        assert.throws(
          () => {
            setMemberOf._execute({ userId }, { userId, groupId: group6Id });
          },
          Meteor.Error,
          /api.users.setMemberOf.wrongStructure/,
        );
        // can setMemberOf if user is in permitted structures
        setMemberOf._execute({ userId }, { userId, groupId: group5Id });
        assert.equal(Roles.userIsInRole(userId, 'member', group5Id), true);
      });
      it('normal users can not set/unset another user as member of a group', function () {
        // Throws if non owner/admin user, or logged out user
        assert.throws(
          () => {
            setMemberOf._execute({ userId: otherUserId }, { userId, groupId });
          },
          Meteor.Error,
          /api.users.setMemberOf.notPermitted/,
        );
        assert.throws(
          () => {
            unsetMemberOf._execute({ userId: otherUserId }, { userId, groupId });
          },
          Meteor.Error,
          /api.users.unsetMemberOf.notPermitted/,
        );
        assert.throws(
          () => {
            setMemberOf._execute({ userId: otherUserId }, { userId, groupId: moderatedGroupId });
          },
          Meteor.Error,
          /api.users.setMemberOf.notPermitted/,
        );
        assert.throws(
          () => {
            unsetMemberOf._execute({ userId: otherUserId }, { userId, groupId: moderatedGroupId });
          },
          Meteor.Error,
          /api.users.unsetMemberOf.notPermitted/,
        );
        assert.throws(
          () => {
            setMemberOf._execute({ userId: otherUserId }, { userId, groupId: closedGroupId });
          },
          Meteor.Error,
          /api.users.setMemberOf.notPermitted/,
        );
        assert.throws(
          () => {
            unsetMemberOf._execute({ userId: otherUserId }, { userId, groupId: closedGroupId });
          },
          Meteor.Error,
          /api.users.unsetMemberOf.notPermitted/,
        );
      });
      it('group admin can set/unset a user as member of a restricted group', function () {
        setMemberOf._execute({ userId: otherUserId }, { userId, groupId: restrictedGroupId });
        let group = Groups.findOne(restrictedGroupId);
        assert.equal(Roles.userIsInRole(userId, 'member', restrictedGroupId), true);
        assert.include(group.members, userId, 'group members list contains userId');
        assert.equal(pspaceHasGroup(userId, restrictedGroupId), true, 'group is in personal space');
        unsetMemberOf._execute({ userId: otherUserId }, { userId, groupId: restrictedGroupId });
        group = Groups.findOne(restrictedGroupId);
        assert.equal(Roles.userIsInRole(userId, 'member', restrictedGroupId), false);
        assert.notInclude(group.members, userId, "group members list shouldn't contain otherUserId");
        assert.equal(pspaceHasGroup(userId, restrictedGroupId), false, 'group is no longer in personal space');
      });
      it('animator user can unset himself as animator of a restricted group', function () {
        setAnimatorOf._execute({ userId: otherUserId }, { userId, groupId: restrictedGroupId });
        assert.equal(Roles.userIsInRole(userId, 'animator', restrictedGroupId), true);
        unsetAnimatorOf._execute({ userId: otherUserId }, { userId, groupId: restrictedGroupId });
        assert.equal(Roles.userIsInRole(userId, 'animator', restrictedGroupId), false);
      });
      it('normal user can not unset himself as member of a restricted group', function () {
        setMemberOf._execute({ userId: otherUserId }, { userId, groupId: restrictedGroupId });
        assert.throws(
          () => {
            unsetMemberOf._execute({ userId }, { userId, groupId: restrictedGroupId });
          },
          Meteor.Error,
          /api.users.unsetMemberOf.notPermitted/,
        );
      });
    });
    describe('(un)setCandidateOf', function () {
      it('global admin can set/unset a user as candidate of a moderated group only', function () {
        setCandidateOf._execute({ userId: adminId }, { userId, groupId: moderatedGroupId });
        let group = Groups.findOne(moderatedGroupId);
        assert.equal(Roles.userIsInRole(userId, 'candidate', moderatedGroupId), true);
        assert.include(group.candidates, userId, 'group candidates list contains userId');
        assert.equal(pspaceHasGroup(userId, moderatedGroupId), true, 'group is in personal space');
        unsetCandidateOf._execute({ userId: adminId }, { userId, groupId: moderatedGroupId });
        group = Groups.findOne(moderatedGroupId);
        assert.equal(Roles.userIsInRole(userId, 'candidate', moderatedGroupId), false);
        assert.notInclude(group.candidates, userId, "group candidates list shouldn't contain userId");
        assert.equal(pspaceHasGroup(userId, moderatedGroupId), false, 'group is no longer in personal space');
        // the 2 below exceptions will be tested only for global admin (depends on group type, not user permissions)
        assert.throws(
          () => {
            setCandidateOf._execute({ userId: adminId }, { userId, groupId: closedGroupId });
          },
          Meteor.Error,
          /api.users.setCandidateOf.moderatedGroupOnly/,
        );
        assert.throws(
          () => {
            setCandidateOf._execute({ userId: adminId }, { userId, groupId: group2Id });
          },
          Meteor.Error,
          /api.users.setCandidateOf.moderatedGroupOnly/,
        );
        // can not setCandidateOf if target user is not in permitted structures
        assert.throws(
          () => {
            setCandidateOf._execute({ userId: adminId }, { userId, groupId: group8Id });
          },
          Meteor.Error,
          /api.users.setCandidateOf.wrongStructure/,
        );
        // can setCandidateOf if target user is in permitted structures
        setCandidateOf._execute({ userId: adminId }, { userId, groupId: group7Id });
        assert.equal(Roles.userIsInRole(userId, 'candidate', group7Id), true);
      });
      it('group admin can set/unset a user as candidate of a moderated group', function () {
        setAdminOf._execute({ userId: adminId }, { userId, groupId: moderatedGroupId });
        setCandidateOf._execute({ userId }, { userId: otherUserId, groupId: moderatedGroupId });
        let group = Groups.findOne(moderatedGroupId);
        assert.equal(Roles.userIsInRole(otherUserId, 'candidate', moderatedGroupId), true);
        assert.include(group.candidates, otherUserId, 'group candidates list contains userId');
        assert.equal(pspaceHasGroup(otherUserId, moderatedGroupId), true, 'group is in personal space');
        unsetCandidateOf._execute({ userId }, { userId: otherUserId, groupId: moderatedGroupId });
        group = Groups.findOne(moderatedGroupId);
        assert.equal(Roles.userIsInRole(otherUserId, 'candidate', moderatedGroupId), false);
        assert.notInclude(group.candidates, otherUserId, "group candidates list shouldn't contain userId");
        assert.equal(pspaceHasGroup(otherUserId, moderatedGroupId), false, 'group is no longer in personal space');
      });
      it('group animator can set/unset a user as candidate of a moderated group', function () {
        setAnimatorOf._execute({ userId: adminId }, { userId: otherUserId, groupId: moderatedGroupId });
        setCandidateOf._execute({ userId: otherUserId }, { userId, groupId: moderatedGroupId });
        let group = Groups.findOne(moderatedGroupId);
        assert.equal(Roles.userIsInRole(userId, 'candidate', moderatedGroupId), true);
        assert.include(group.candidates, userId, 'group candidates list contains userId');
        assert.equal(pspaceHasGroup(userId, moderatedGroupId), true, 'group is in personal space');
        unsetCandidateOf._execute({ userId: otherUserId }, { userId, groupId: moderatedGroupId });
        group = Groups.findOne(moderatedGroupId);
        assert.equal(Roles.userIsInRole(userId, 'candidate', moderatedGroupId), false);
        assert.notInclude(group.candidates, userId, "group candidates list shouldn't contain userId");
        assert.equal(pspaceHasGroup(userId, moderatedGroupId), false, 'group is no longer in personal space');
      });
      it('normal user can set/unset himself as candidate only for moderated groups', function () {
        setCandidateOf._execute({ userId }, { userId, groupId: moderatedGroupId });
        let group = Groups.findOne(moderatedGroupId);
        assert.equal(Roles.userIsInRole(userId, 'candidate', moderatedGroupId), true);
        assert.include(group.candidates, userId, 'group candidates list contains userId');
        assert.equal(pspaceHasGroup(userId, moderatedGroupId), true, 'group is in personal space');
        unsetCandidateOf._execute({ userId }, { userId, groupId: moderatedGroupId });
        group = Groups.findOne(moderatedGroupId);
        assert.equal(Roles.userIsInRole(userId, 'candidate', moderatedGroupId), false);
        assert.notInclude(group.candidates, userId, "group candidates list shouldn't contain userId");
        assert.equal(pspaceHasGroup(userId, moderatedGroupId), false, 'group is no longer in personal space');
      });
      it('normal users can not set/unset another user as candidate of a group', function () {
        assert.throws(
          () => {
            setCandidateOf._execute({ userId: otherUserId }, { userId, groupId: moderatedGroupId });
          },
          Meteor.Error,
          /api.users.setCandidateOf.notPermitted/,
        );
        assert.throws(
          () => {
            unsetCandidateOf._execute({ userId: otherUserId }, { userId, groupId: moderatedGroupId });
          },
          Meteor.Error,
          /api.users.unsetCandidateOf.notPermitted/,
        );
      });
    });
    describe('createGroup', function () {
      it('does create a group and set current user as owner', function () {
        createGroup._execute(
          { userId },
          {
            name: 'mongroupe',
            type: 0,
            description: 'une description',
            content: 'une note',
          },
        );
        const user = Meteor.users.findOne(userId);
        const group = Groups.findOne({ name: 'mongroupe' });
        assert.typeOf(group, 'object');
        assert.equal(group.active, true);
        assert.equal(group.owner, userId);
        assert.equal(Roles.userIsInRole(userId, 'admin', group._id), true);
        assert.equal(Roles.userIsInRole(userId, 'animator', group._id), true);
        assert.equal(user.groupCount, 1);
        assert.equal(pspaceHasGroup(userId, group._id), true, 'group is in personal space');
      });
      it('does fail to create a group if name already taken', function () {
        assert.throws(
          () => {
            createGroup._execute(
              { userId },
              {
                name: 'group4',
                type: 0,
                description: 'une description',
                content: 'une note',
              },
            );
          },
          Meteor.Error,
          /api.groups.createGroup.duplicateName/,
        );
      });
      it('does not set owner as member/animator if he does not have access to limiting structures', function () {
        createGroup._execute(
          { userId },
          {
            name: 'limitedGroupNoAccess',
            type: 0,
            description: 'une description',
            content: 'une note',
            structureIds: [otherStructureId],
          },
        );
        const groupNoAccess = Groups.findOne({ name: 'limitedGroupNoAccess' });
        assert.isArray(groupNoAccess.structureIds);
        assert.equal(groupNoAccess.structureIds[0], otherStructureId);
        assert.notInclude(groupNoAccess.members, userId);
        assert.notInclude(groupNoAccess.animators, userId);
        createGroup._execute(
          { userId },
          {
            name: 'limitedGroup',
            type: 0,
            description: 'une description',
            content: 'une note',
            structureIds: [structureId],
          },
        );
        const group = Groups.findOne({ name: 'limitedGroup' });
        assert.isArray(group.structureIds);
        assert.equal(group.structureIds[0], structureId);
        assert.include(group.members, userId);
        assert.include(group.animators, userId);
      });
      it('does not create a group when not logged in', function () {
        assert.throws(
          () => {
            createGroup._execute(
              {},
              {
                name: 'mongroupe',
                type: 0,
                description: 'une description',
                content: 'une note',
              },
            );
          },
          Meteor.Error,
          /api.groups.createGroup.notLoggedIn/,
        );
      });
    });
    describe('removeGroup', function () {
      it("does not delete a group you don't own or are admin of", function () {
        // Throws if non owner/admin user, or logged out user, tries to delete the group
        assert.throws(
          () => {
            removeGroup._execute({ userId }, { groupId: group3Id });
          },
          Meteor.Error,
          /api.groups.removeGroup.notPermitted/,
        );
        assert.throws(
          () => {
            removeGroup._execute({}, { groupId });
          },
          Meteor.Error,
          /api.groups.removeGroup.notPermitted/,
        );
      });
      it('does delete a group you own', function () {
        removeGroup._execute({ userId }, { groupId });
        assert.equal(Groups.findOne(groupId), undefined);
      });
      it('does delete a group you are admin of', function () {
        removeGroup._execute({ userId }, { groupId: group2Id });
        assert.equal(Groups.findOne(group2Id), undefined);
      });
      it('does delete any group when you are global admin', function () {
        removeGroup._execute({ userId: adminId }, { groupId: group3Id });
        assert.equal(Groups.findOne(group3Id), undefined);
      });
    });
    describe('updateGroup', function () {
      it("does not update a group you don't own or are admin of", function () {
        // Throws if non owner/admin user, or logged out user, tries to delete the group
        assert.throws(
          () => {
            updateGroup._execute({ userId }, { groupId: group3Id, data: { description: 'test' } });
          },
          Meteor.Error,
          /api.groups.updateGroup.notPermitted/,
        );
        assert.throws(
          () => {
            updateGroup._execute({}, { groupId, data: { description: 'test' } });
          },
          Meteor.Error,
          /api.groups.updateGroup.notPermitted/,
        );
      });
      it('does update a group you own', function () {
        updateGroup._execute({ userId }, { groupId, data: { description: 'test' } });
        assert.equal(Groups.findOne(groupId).description, 'test');
      });
      it('does update a group you are admin of', function () {
        updateGroup._execute({ userId }, { groupId: group2Id, data: { description: 'test' } });
        assert.equal(Groups.findOne(group2Id).description, 'test');
      });
      it('does update any group when you are global admin', function () {
        updateGroup._execute({ userId: adminId }, { groupId: group3Id, data: { description: 'test' } });
        assert.equal(Groups.findOne(group3Id).description, 'test');
      });
      it('does fail to update group if name already taken', function () {
        assert.throws(
          () => {
            updateGroup._execute({ userId }, { groupId, data: { name: 'group4' } });
          },
          Meteor.Error,
          /api.groups.updateGroup.duplicateName/,
        );
      });
      it('does unset strucutreIds when structureIds list is empty', function () {
        updateGroup._execute(
          { userId: adminId },
          { groupId: group3Id, data: { description: 'test', structureIds: [structureId] } },
        );
        assert.isArray(Groups.findOne(group3Id).structureIds);
        assert.equal(Groups.findOne(group3Id).structureIds.length, 1);
        updateGroup._execute(
          { userId: adminId },
          { groupId: group3Id, data: { description: 'test', structureIds: [] } },
        );
        assert.isUndefined(Groups.findOne(group3Id).structureIds);
      });
      it('does verify that all existing users are allowed when updating strucutreIds', function () {
        // limit to a structure that current admin is member of, should succeed
        updateGroup._execute(
          { userId: adminId },
          { groupId, data: { description: 'test', structureIds: [structureId] } },
        );
        assert.isArray(Groups.findOne(groupId).structureIds);
        assert.equal(Groups.findOne(groupId).structureIds.length, 1);
        // limit to a structure that current admin is not member of, should fail
        assert.throws(
          () => {
            updateGroup._execute(
              { userId: adminId },
              { groupId, data: { description: 'test', structureIds: [otherStructureId] } },
            );
          },
          Meteor.Error,
          /api.groups.checkGroupUsers.unauthorized/,
        );
        // limit to a structure that current admin is not member of when user is only admin of group, should succeed
        unsetMemberOf._execute({ userId: adminId }, { userId, groupId });
        updateGroup._execute(
          { userId: adminId },
          { groupId, data: { description: 'test', structureIds: [otherStructureId] } },
        );
        assert.isArray(Groups.findOne(groupId).structureIds);
        assert.equal(Groups.findOne(groupId).structureIds[0], otherStructureId);
      });
    });
    describe('(un)favGroup', function () {
      it('does (un)set a group as favorite', function () {
        favGroup._execute({ userId }, { groupId });
        let user = Meteor.users.findOne(userId);
        assert.include(user.favGroups, groupId, 'favorite groups list contains groupId');
        assert.equal(pspaceHasGroup(userId, groupId), true, 'group is in personal space');
        unfavGroup._execute({ userId }, { groupId });
        user = Meteor.users.findOne(userId);
        assert.notInclude(user.favGroups, groupId, 'favorite groups list does not contain groupId');
        assert.equal(pspaceHasGroup(userId, groupId), false, 'group is no longer in personal space');
      });
      it('does not set a group as favorite if not logged in', function () {
        assert.throws(
          () => {
            favGroup._execute({}, { groupId });
          },
          Meteor.Error,
          /api.groups.favGroup.notPermitted/,
        );
      });
      it('does not unset a group as favorite if not logged in', function () {
        assert.throws(
          () => {
            unfavGroup._execute({}, { groupId });
          },
          Meteor.Error,
          /api.groups.unfavGroup.notPermitted/,
        );
      });
    });
    describe('total count of group members', function () {
      it('does count all members of group', function () {
        const myGroupId = Factory.create('group', { owner: userId, name: 'toto' })._id;

        setAdminOf._execute({ userId: adminId }, { userId: adminId, groupId: myGroupId });
        setAdminOf._execute({ userId: adminId }, { userId: otherUserId, groupId: myGroupId });

        setAnimatorOf._execute({ userId: adminId }, { userId, groupId: myGroupId });

        setMemberOf._execute({ userId: adminId }, { userId: adminId, groupId: myGroupId });
        setMemberOf._execute({ userId: adminId }, { userId: otherUserId, groupId: myGroupId });
        setMemberOf._execute({ userId: adminId }, { userId, groupId: myGroupId });

        assert.equal(Groups.findOne(myGroupId).slug, 'toto');

        assert.equal(countMembersOfGroup._execute({}, { slug: 'toto' }), 3);
      });
      it('does return 0 if slug not found', function () {
        assert.equal(countMembersOfGroup._execute({}, { slug: 'SlugNotExist' }), 0);
      });
    });
    describe('check nextcloud share name', function () {
      it('does not return when not logged in', function () {
        assert.throws(
          () => {
            checkShareName._execute({}, { shareName: 'toto' });
          },
          Meteor.Error,
          /api.groups.checkShareName.notLoggedIn/,
        );
      });
      it('does return true if name is available', function () {
        Factory.create('group', { owner: userId, name: 'un groupe' });
        assert.equal(checkShareName._execute({ userId }, { shareName: 'toto' }), true);
        const myGroupId = Factory.create('group', { owner: userId, name: 'toto', shareName: 'toto' })._id;
        assert.equal(checkShareName._execute({ userId }, { shareName: 'toto', groupId: myGroupId }), true);
      });
      it('does return false if name already exists', function () {
        Factory.create('group', { owner: userId, name: 'toto', shareName: 'toto' });
        const myGroupId = Factory.create('group', { owner: userId, name: 'un groupe' })._id;
        assert.equal(checkShareName._execute({ userId }, { shareName: 'toto' }), false);
        assert.equal(checkShareName._execute({ userId }, { shareName: 'toto', groupId: myGroupId }), false);
      });
    });
  });
});
