import { Meteor } from 'meteor/meteor';
import logServer, { levels, scopes } from '../../logging';
import { _createGroup, _removeGroup } from './methods';
import {
  _setCandidateOf,
  _setMemberOf,
  _setAdminOf,
  _setAnimatorOf,
  _unsetAnimatorOf,
  _unsetAdminOf,
  _unsetMemberOf,
} from '../../users/server/methods';
import Groups from '../groups';

function CreateGroupFromAPI(group) {
  const owner = Meteor.users.findOne({ username: group.owner });
  if (owner) {
    try {
      const groupId = _createGroup({
        name: group.name,
        customSlug: group.customSlug,
        type: group.type,
        description: group.description,
        content: group.content,
        avatar: '',
        plugins: {},
        userId: owner._id,
      });

      const newGroup = Groups.findOne({ _id: groupId });
      if (newGroup) {
        if (group.members && group.members.length > 0) {
          group.members.forEach((username) => {
            const user = Meteor.users.findOne({ username });
            if (user) {
              _setMemberOf(user, newGroup);
            }
          });
        }

        if (group.candidates && group.candidates.length > 0) {
          group.candidates.forEach((username) => {
            const user = Meteor.users.findOne({ username });
            if (user) {
              _setCandidateOf(user, newGroup);
            }
          });
        }

        if (group.admins && group.admins.length > 0) {
          group.admins.forEach((username) => {
            const user = Meteor.users.findOne({ username });
            if (user) {
              _setAdminOf(user, newGroup);
            }
          });
        }

        if (group.animators && group.animators.length > 0) {
          group.animators.forEach((username) => {
            const user = Meteor.users.findOne({ username });
            if (user) {
              _setAnimatorOf(user, newGroup);
            }
          });
        }
        return 200;
      }
      logServer(`API - GROUPS - POST - Group not found, probably not created`, levels.ERROR, scopes.SYSTEM, {
        groupId,
      });
      const error = new Meteor.Error('api.CreateGroupFromAPI.groupNotFound', 'Group not found');
      error.statusCode = 404;
      throw error;
    } catch (error) {
      error.statusCode = 409;
      throw error;
    }
  }
  logServer(`API - GROUPS - POST - Owner not found`, levels.ERROR, scopes.SYSTEM, { user: group.owner });
  const error = new Meteor.Error('api.CreateGroupFromAPI.ownerNotFound', 'Owner not found');
  error.statusCode = 404;
  throw error;
}

export async function createOneGroup(req, content) {
  // sample use:
  // curl -X POST -H "X-API-KEY: createuser-password" \
  //      -H "Content-Type: application/json" \
  //      -d '{"name":"group1", "customSlug":"", "description":"", "content":"", "type":"", "owner":"", "admins": [], "animators": [], "members": [], "candidates": [] }' \
  //      http://localhost:3000/api/groups/create

  // eslint-disable-next-line spaced-comment
  //curl -X POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"groupTest3", "description":"test", "content":"test", "type":"5", "owner":"admin4"}' http://localhost:3000/api/groups/create

  if (content && 'name' in content && 'description' in content && 'owner' in content && 'type' in content) {
    return CreateGroupFromAPI(content);
  }
  const error = new Meteor.Error('api.createOneGroup.emptyContent', 'Empty or invalid content');
  error.statusCode = 400;
  throw error;
}

// sample use:
// curl -X POST -H "X-API-KEY: createuser-password" \
//      -H "Content-Type: application/json" \
//      -d '{"name":"groupname2, "member": "username1"}' \
//      http://localhost:3000/api/groups/addmember
export async function addGroupMember(req, content) {
  if ('name' in content && 'member' in content) {
    const group = Groups.findOne({ name: content.name });
    if (group) {
      const user = Meteor.users.findOne({ username: content.member });
      if (user) {
        try {
          _setMemberOf(user, group);
          return 200;
        } catch (error) {
          error.statusCode = 409;
          throw error;
        }
      }
      const error = new Meteor.Error('api.addGroupMember.userNotFound', 'User Not Found');
      error.statusCode = 404;
      throw error;
    }
    const error = new Meteor.Error('api.addGroupMember.groupNotFound', 'Group Not Found');
    error.statusCode = 404;
    throw error;
  }
  const error = new Meteor.Error('api.addGroupMember.invalidContent', 'Empty or invalid content');
  error.statusCode = 400;
  throw error;
}

// sample use:
// curl -X POST -H "X-API-KEY: createuser-password" \
//      -H "Content-Type: application/json" \
//      -d '{"name":"groupname, "admin": "username1"}' \
//      http://localhost:3000/api/groups/addadmin
export async function addGroupAdmin(req, content) {
  if ('name' in content && 'admin' in content) {
    const group = Groups.findOne({ name: content.name });
    if (group) {
      const user = Meteor.users.findOne({ username: content.admin });
      if (user) {
        try {
          _setAdminOf(user, group);
          return 200;
        } catch (error) {
          error.statusCode = 409;
          throw error;
        }
      }
      const error = new Meteor.Error('api.addGroupAdmin.userNotFound', 'User Not Found');
      error.statusCode = 404;
      throw error;
    }
    const error = new Meteor.Error('api.addGroupAdmin.groupNotFound', 'Group Not Found');
    error.statusCode = 404;
    throw error;
  }
  const error = new Meteor.Error('api.addGroupAdmin.invalidContent', 'Empty or invalid content');
  error.statusCode = 400;
  throw error;
}

// sample use:
// curl -X POST -H "X-API-KEY: createuser-password" \
//      -H "Content-Type: application/json" \
//      -d '{"name":"groupname, "animator": "username1"}
//      http://localhost:3000/api/groups/addanimator
export async function addGroupAnimator(req, content) {
  if ('name' in content && 'animator' in content) {
    const group = Groups.findOne({ name: content.name });
    if (group) {
      const user = Meteor.users.findOne({ username: content.animator });
      if (user) {
        try {
          _setAnimatorOf(user, group);
          return 200;
        } catch (error) {
          error.statusCode = 409;
          throw error;
        }
      }
      const error = new Meteor.Error('api.addGroupAnimator.userNotFound', 'User Not Found');
      error.statusCode = 404;
      throw error;
    }
    const error = new Meteor.Error('api.addGroupAnimator.groupNotFound', 'Group Not Found');
    error.statusCode = 404;
    throw error;
  }
  const error = new Meteor.Error('api.addGroupAnimator.invalidContent', 'Empty or invalid content');
  error.statusCode = 400;
  throw error;
}

// sample use:
// curl -X POST -H "X-API-KEY: createuser-password" \
//      -H "Content-Type: application/json" \
//      -d '{"name":"groupname, "member": "username1"}
//      http://localhost:3000/api/groups/removemember
export async function removeGroupMember(req, content) {
  if ('name' in content && 'member' in content) {
    const group = Groups.findOne({ name: content.name });
    if (group) {
      const user = Meteor.users.findOne({ username: content.member });
      if (user) {
        try {
          _unsetMemberOf(user, group);
          return 200;
        } catch (error) {
          error.statusCode = 409;
          throw error;
        }
      }
      const error = new Meteor.Error('api.removeGroupMember.userNotFound', 'User Not Found');
      error.statusCode = 404;
      throw error;
    }
    const error = new Meteor.Error('api.removeGroupMember.groupNotFound', 'Group Not Found');
    error.statusCode = 404;
    throw error;
  }
  const error = new Meteor.Error('api.removeGroupMember.invalidContent', 'Empty or invalid content');
  error.statusCode = 400;
  throw error;
}

// sample use:
// curl -X POST -H "X-API-KEY: createuser-password" \
//      -H "Content-Type: application/json" \
//      -d '{"name":"groupname, "admin": "username1"},
//      http://localhost:3000/api/groups/removeadmin
export async function removeGroupAdmin(req, content) {
  if ('name' in content && 'admin' in content) {
    const group = Groups.findOne({ name: content.name });
    if (group) {
      const user = Meteor.users.findOne({ username: content.admin });
      if (user) {
        try {
          _unsetAdminOf(user, group);
          return 200;
        } catch (error) {
          error.statusCode = 409;
          throw error;
        }
      }
      const error = new Meteor.Error('api.removeGroupAdmin.userNotFound', 'User Not Found');
      error.statusCode = 404;
      throw error;
    }
    const error = new Meteor.Error('api.removeGroupAdmin.groupNotFound', 'Group Not Found');
    error.statusCode = 404;
    throw error;
  }
  const error = new Meteor.Error('api.removeGroupAdmin.invalidContent', 'Empty or invalid content');
  error.statusCode = 400;
  throw error;
}

// sample use:
// curl -X POST -H "X-API-KEY: createuser-password" \
//      -H "Content-Type: application/json" \
//      -d '{"name":"groupname, "animator": "username1"}
//      http://localhost:3000/api/groups/removeanimator
export async function removeGroupAnimator(req, content) {
  if ('name' in content && 'animator' in content) {
    const group = Groups.findOne({ name: content.name });
    if (group) {
      const user = Meteor.users.findOne({ username: content.animator });
      if (user) {
        try {
          _unsetAnimatorOf(user, group);
          return 200;
        } catch (error) {
          error.statusCode = 409;
          throw error;
        }
      }
      const error = new Meteor.Error('api.removeGroupAnimator.userNotFound', 'User Not Found');
      error.statusCode = 404;
      throw error;
    }
    const error = new Meteor.Error('api.removeGroupAnimator.groupNotFound', 'Group Not Found');
    error.statusCode = 404;
    throw error;
  }
  const error = new Meteor.Error('api.removeGroupAnimator.invalidContent', 'Empty or invalid content');
  error.statusCode = 400;
  throw error;
}

function generateGroupData(group) {
  const owner = Meteor.users.findOne({ _id: group.owner });
  let members = [];
  let candidates = [];
  let animators = [];
  let admins = [];
  if (group.members && group.members.length > 0) {
    members = Meteor.users
      .find({ _id: { $in: group.members } })
      .fetch()
      .map((user) => user.username);
  }

  if (group.admins && group.admins.length > 0) {
    admins = Meteor.users
      .find({ _id: { $in: group.admins } })
      .fetch()
      .map((user) => user.username);
  }

  if (group.animators && group.animators.length > 0) {
    animators = Meteor.users
      .find({ _id: { $in: group.animators } })
      .fetch()
      .map((user) => user.username);
  }

  if (group.candidates && group.candidates.length > 0) {
    candidates = Meteor.users
      .find({ _id: { $in: group.candidates } })
      .fetch()
      .map((user) => user.username);
  }
  const obj = {
    name: group.name,
    description: group.description,
    content: group.content,
    type: group.type,
    owner: owner ? owner.username : null,
    admins,
    animators,
    members,
    candidates,
  };
  return obj;
}

export async function getAllGroups() {
  // sample use:
  // curl -H "X-API-KEY: createuser-password" http://localhost:3000/api/groups | jq

  const groups = Groups.find();

  const res = [];
  groups.forEach((group) => {
    const obj = generateGroupData(group);
    res.push(obj);
  });
  return res;
}

export async function getGroups(req, content) {
  // sample use:
  // curl -X  POST -H "X-API-KEY: createuser-password" \
  //      -H "Content-Type: application/json" \
  //      -d '{"name":"test" }' \
  //      http://localhost:3000/api/groups
  // curl -X  POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"test" }' http://localhost:3000/api/groups
  if ('name' in content) {
    const group = Groups.findOne({ name: content.name });
    if (group) {
      return generateGroupData(group);
    }
    return '';
  }
  const error = new Meteor.Error('api.getGroups.emptyContent', 'Empty content');
  error.statusCode = 404;
  throw error;
}

export async function deleteGoup(req, content) {
  // sample use:
  // curl -X DELETE -H "X-API-KEY: createuser-password" \
  //      -H "Content-Type: application/json" \
  //      -d '{"name":"group1"}' \
  //      http://localhost:3000/api/groups/delete

  // eslint-disable-next-line spaced-comment
  //curl -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"groupTest3"}' http://localhost:3000/api/groups/delete

  if (content) {
    const group = Groups.findOne({ name: content.name });
    if (group) {
      const admins = Meteor.users
        .find({
          emails: { $elemMatch: { address: { $in: Meteor.settings.keycloak.adminEmails } } },
        })
        .fetch();
      if (admins && admins.length > 0) {
        try {
          _removeGroup({ groupId: group._id, userId: admins[0]._id });
          return 200;
        } catch (error) {
          error.statusCode = 409;
          throw error;
        }
      }
      logServer(`API - GROUPS - DELETE - Admin not found, not created`, levels.ERROR, scopes.SYSTEM);
      const error = new Meteor.Error('api.deleteGroup.adminNotFound', 'Admin not found, not created');
      error.statusCode = 404;
      throw error;
    }
    logServer(`API - GROUPS - DELETE - Group not found`, levels.ERROR, scopes.SYSTEM);
    const error = new Meteor.Error('api.deleteGroup.notFound', 'Group not found');
    error.statusCode = 404;
    throw error;
  }
  const error = new Meteor.Error('api.deleteGoup.emptyContent', 'Empty content');
  error.statusCode = 404;
  throw error;
}
