import { Meteor } from 'meteor/meteor';
import logServer, { levels, scopes } from '../../logging';
import { _createStructure, _deleteStructure } from './methods';
import Structures from '../structures';
import { GetStructure } from '../utils';

function GetNextStructure(strucs, parent, admin, index) {
  if (strucs[index]) {
    try {
      const struc = Structures.findOne({ $and: [{ name: strucs[index] }, { parentId: parent._id }] });
      if (!struc) {
        const id = _createStructure(strucs[index], parent._id, admin._id);
        const newStruc = Structures.findOne({ _id: id });
        GetNextStructure(strucs, newStruc, admin, index + 1);
      } else {
        GetNextStructure(strucs, struc, admin, index + 1);
      }
    } catch (error) {
      error.statusCode = 409;
      throw error;
    }
  }
}

function CreateStructureFromAPI(pathStruc, admin) {
  const strucs = pathStruc.path.split('/');
  const struc = Structures.findOne({ name: strucs[0] });
  try {
    if (!struc) {
      const id = _createStructure(strucs[0], null, admin._id);
      const newStruc = Structures.findOne({ _id: id });
      GetNextStructure(strucs, newStruc, admin, 1);
    } else {
      GetNextStructure(strucs, struc, admin, 1);
    }
  } catch (error) {
    error.statusCode = 409;
    throw error;
  }
  return 200;
}

export async function createOneStructure(req, content) {
  // sample use:
  // curl -X POST -H "X-API-KEY: createuser-password" \
  //      -H "Content-Type: application/json" \
  //      -d '{"path": "struc1/struc2/struc3"}' \
  //      http://localhost:3000/api/structures/create

  // eslint-disable-next-line spaced-comment
  // curl -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path": "api1/api2/api3"}' http://localhost:3000/api/structures/create

  const admins = Meteor.users
    .find({
      emails: { $elemMatch: { address: { $in: Meteor.settings.keycloak.adminEmails } } },
    })
    .fetch();
  if (admins && admins.length > 0) {
    const admin = admins[0];
    if (admin) {
      if (content) {
        return CreateStructureFromAPI(content, admin);
      }
      logServer(`API - STRUCTURE - POST - Content empty or not valid`, levels.ERROR, scopes.SYSTEM);
      const error = new Meteor.Error('api.createOneStructure.emptyContent', 'Empty content');
      error.statusCode = 400;
      throw error;
    }
    logServer(`API - STRUCTURE - POST - Admin not found, not created`, levels.ERROR, scopes.SYSTEM);
    const error = new Meteor.Error('api.createOneStructure.adminNotFound', 'Admin not found, not created');
    error.statusCode = 404;
    throw error;
  }
  logServer(`API - STRUCTURE - POST - Admin not found, not created`, levels.ERROR, scopes.SYSTEM);
  const error = new Meteor.Error('api.createOneStructure.adminNotFound', 'Admin not found, not created');
  error.statusCode = 404;
  throw error;
}

function GeneratePath(structure, tree) {
  if (structure.parentId) {
    const parent = Structures.findOne({ _id: structure.parentId });
    if (parent) {
      tree.push(parent.name);
      GeneratePath(parent, tree);
    }
  }
}

function GetChildStructure(strucs, parent, index) {
  if (strucs[index]) {
    const struc = Structures.findOne({ $and: [{ name: strucs[index] }, { parentId: parent._id }] });
    if (!struc) {
      return undefined;
    }
    if (index < strucs.length - 1) return GetChildStructure(strucs, struc, index + 1);
    return struc;
  }
  return undefined;
}

// sample use:
// curl -X  POST -H "X-API-KEY: createuser-password" \
//      -H "Content-Type: application/json" \
//      -d '{"path":"api1/api2" }' \
//      http://localhost:3000/api/structures
// curl -X  POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path":"api1/api2" }' http://localhost:3000/api/structures

export async function getStructures(req, content) {
  if ('path' in content) {
    let strucs = [content.path];
    if (content.path.includes('/')) strucs = content.path.split('/');

    if (strucs && strucs.length > 0) {
      const parent = Structures.findOne({ name: strucs[0] });
      if (parent) {
        const struc = GetChildStructure(strucs, parent, 1);
        if (struc) {
          const obj = { path: content.path, structure: struc };
          return obj;
        }
        return { path: content.path, structure: parent };
      }
      logServer(`API - STRUCTURE - GET - No structure found`, levels.ERROR, scopes.SYSTEM);
      const error = new Meteor.Error('api.getStructures.noStructureFound', 'No structure found');
      error.statusCode = 404;
      throw error;
    } else {
      logServer(`API - STRUCTURE - GET - No structure found`, levels.ERROR, scopes.SYSTEM);
      const error = new Meteor.Error('api.getStructures.noStructureFound', 'No structure found');
      error.statusCode = 404;
      throw error;
    }
  }
  logServer(`API - STRUCTURE - GET - Content empty or not valid`, levels.ERROR, scopes.SYSTEM);
  const error = new Meteor.Error('api.getStructures.emptyContent', 'Empty content');
  error.statusCode = 400;
  throw error;
}

// sample use:
// curl -H "X-API-KEY: createuser-password" http://localhost:3000/api/structures | jq

export async function getAllStructures() {
  const res = [];
  const structures = Structures.find().fetch();
  if (structures) {
    structures.forEach((structure) => {
      const tree = [structure.name];
      if (structure.parentId) {
        const parent = Structures.findOne({ _id: structure.parentId });
        if (parent) {
          tree.push(parent.name);
          GeneratePath(parent, tree);
        }
      }

      const reversed = tree.reverse();
      const obj = { path: reversed.join('/'), structure };
      res.push(obj);
    });
  } else {
    logServer(`API - STRUCTURE - GET - No structure found`, levels.ERROR, scopes.SYSTEM);
    const error = new Meteor.Error('api.getAllStructures.noStructureFound', 'No structure found');
    error.statusCode = 404;
    throw error;
  }
  return res;
}

export async function deleteStructure(req, content) {
  // sample use:
  // curl -X DELETE -H "X-API-KEY: createuser-password" \
  //      -H "Content-Type: application/json" \
  //      -d '{"path": "struc1/struc2/struc3"}' \
  //      http://localhost:3000/api/structures/delete

  // eslint-disable-next-line spaced-comment
  // curl -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path": "api1/api2/api3"}' http://localhost:3000/api/structures/delete

  const admins = Meteor.users
    .find({
      emails: { $elemMatch: { address: { $in: Meteor.settings.keycloak.adminEmails } } },
    })
    .fetch();
  if (admins && admins.length > 0) {
    const admin = admins[0];
    if (admin) {
      if (content) {
        const structure = GetStructure(content.path.split('/'), null, 0);
        if (structure) {
          // Check if structure has children
          if (structure.childrenIds.length > 0) {
            logServer(`API - STRUCTURE - DELETE - This structure has children.}`, levels.ERROR, scopes.SYSTEM, {
              structureId: structure._id,
            });
            const error = new Meteor.Error('api.deleteStructure.hasChildren', 'Structure has children');
            error.statusCode = 409;
            throw error;
          }

          try {
            _deleteStructure(structure, admin._id);
            return 200;
          } catch (error) {
            error.statusCode = 409;
            throw error;
          }
        }
        logServer(`API - STRUCTURE - DELETE - Structure not found`, levels.ERROR, scopes.SYSTEM);
        const error = new Meteor.Error('api.deleteStructure.notFound', 'Structure not found');
        error.statusCode = 404;
        throw error;
      }
      logServer(`API - STRUCTURE - DELETE - Content empty or not valid`, levels.ERROR, scopes.SYSTEM);
      const error = new Meteor.Error('api.deleteStructure.emptyContent', 'Empty content');
      error.statusCode = 400;
      throw error;
    }
    logServer(`API - STRUCTURE - POST - Admin not found, not created`, levels.ERROR, scopes.SYSTEM);
    const error = new Meteor.Error('api.deleteStructure.adminNotFound', 'Admin not found, not created');
    error.statusCode = 404;
    throw error;
  }
  logServer(`API - STRUCTURE - POST - Admin not found, not created`, levels.ERROR, scopes.SYSTEM);
  const error = new Meteor.Error('api.deleteStructure.adminNotFound', 'Admin not found, not created');
  error.statusCode = 404;
  throw error;
}
