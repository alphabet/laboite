import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import SimpleSchema from 'simpl-schema';
import i18n from 'meteor/universe:i18n';
import { _ } from 'meteor/underscore';
import { Roles } from 'meteor/alanning:roles';
import { unauthorized } from '../../lib/errors';
import { getLabel, isActive, validateString } from '../../utils';
import Structures from '../structures';
import Groups from '../../groups/groups';
import { _createGroup, _removeGroup, removeGroup } from '../../groups/server/methods';
import { hasAdminRightOnStructure, isAStructureWithSameNameExistWithSameParent } from '../utils';
import { structureRemoveIconOrCoverImagesFromMinio } from '../methods';
import logServer, { levels, scopes } from '../../logging';
import RegEx from '../../regExp';
import Services from '../../services/services';
import Articles from '../../articles/articles';

export function generatePathOfStructure(structure, tree) {
  if (structure.parentId) {
    const parent = Structures.findOne({ _id: structure.parentId });
    if (parent) {
      const obj = { structureName: parent.name, structureId: parent._id };
      tree.push(obj);
      generatePathOfStructure(parent, tree);
    }
  }
}

export function getStructurePath(structureId) {
  const tree = [];
  const structure = Structures.findOne({ _id: structureId });
  if (structure) {
    generatePathOfStructure(structure, tree);
  }
  tree.reverse();
  return tree;
}

export function _createStructure(name, parentId, userId) {
  const structuresWithSameNameOnSameLevel = isAStructureWithSameNameExistWithSameParent({ name, parentId });

  if (structuresWithSameNameOnSameLevel) {
    logServer(
      `STRUCTURE - METHODS - METEOR ERROR - createStructure - ${i18n.__('api.structures.nameAlreadyTaken')}`,
      levels.WARN,
      scopes.SYSTEM,
      { name, parentId },
    );
    throw new Meteor.Error(
      'api.structures.createStructure.nameAlreadyTaken',
      i18n.__('api.structures.nameAlreadyTaken'),
    );
  }
  validateString(name);
  logServer(`STRUCTURE - METHODS - INSERT - createStructure`, levels.VERBOSE, scopes.SYSTEM, { name, parentId });

  const structureId = Structures.insert({
    name,
    parentId,
  });

  if (parentId) {
    logServer(
      `STRUCTURE - METHODS - UPDATE - createStructure - id: ${structureId} / ancestorsIds: ${parentId}`,
      levels.INFO,
      scopes.SYSTEM,
    );
    // Update ancestorsId with only direct parent
    Structures.update({ _id: structureId }, { $set: { ancestorsIds: [parentId] } });
    // generate full structurePath
    Structures.update({ _id: structureId }, { $set: { structurePath: getStructurePath(structureId) } });

    const directParentStructure = Structures.findOne({ _id: parentId });
    if (directParentStructure) {
      const { childrenIds: directParentStructureChildrenIds, ancestorsIds: directParentStructureAncestorIds } =
        directParentStructure;

      // Add structure to the parent childrenIds
      directParentStructureChildrenIds.push(structureId);
      logServer(
        `STRUCTURE - METHODS - UPDATE - createStructure - id: ${structureId}
        / childrenIds: ${directParentStructureChildrenIds}`,
        levels.INFO,
        scopes.SYSTEM,
      );

      Structures.update(
        { _id: parentId },
        {
          $set: {
            childrenIds: [...new Set(directParentStructureChildrenIds)],
          },
        },
      );

      logServer(
        `STRUCTURE - METHODS - UPDATE - createStructure - id: ${structureId}
        / ancestorsIds: ${parentId} / directParentStructureAncestorIds: ${directParentStructureAncestorIds}`,
        levels.INFO,
        scopes.SYSTEM,
      );
      // Update structure ancestors with parent structure's ancestors too
      Structures.update(
        { _id: structureId },
        { $set: { ancestorsIds: [parentId, ...directParentStructureAncestorIds] } },
      );
    }
  }

  const strucName = `${structureId}_${name}`;

  _createGroup({
    name: strucName,
    type: 15,
    description: 'groupe structure',
    content: '',
    avatar: '',
    plugins: {},
    userId,
  });

  const structure = Structures.findOne({ _id: structureId });

  if (structure) {
    const group = Groups.findOne({ name: strucName });
    if (group) {
      structure.groupId = group._id;
      logServer(
        `STRUCTURE - METHODS - UPDATE - createStructure - id: ${structureId} / groupId: ${group._id}`,
        levels.INFO,
        scopes.SYSTEM,
      );
      Structures.update({ _id: structureId }, { $set: { groupId: group._id } });
    }
  }

  return structureId;
}

export const createStructure = new ValidatedMethod({
  name: 'structures.createStructure',
  validate: Structures.schema.pick('name', 'parentId').validator(),
  run({ name, parentId }) {
    let isAdminOfStructure = false;

    if (parentId) {
      isAdminOfStructure = hasAdminRightOnStructure({ userId: this.userId, structureId: parentId });
    }

    const authorized = isActive(this.userId) && (Roles.userIsInRole(this.userId, 'admin') || isAdminOfStructure);

    if (!authorized) {
      throw new Meteor.Error('api.structures.createStructure.notPermitted', i18n.__('api.users.notPermitted'));
    }

    const id = _createStructure(name, parentId, this.userId);
    const newStruc = Structures.findOne({ _id: id });
    return newStruc;
  },
});

const removeGroupLimitations = (structureId, userId) => {
  // get all groups wih limitation on this structure
  const groups = Groups.find({ structureIds: structureId }).fetch();
  // for groups limited to this structure only, check if there are remaining users
  groups.forEach((group) => {
    if (group.structureIds.length === 1) {
      const numUsers = group.candidates.length + group.members.length + group.animators.length + group.admins.length;
      if (numUsers === 0) {
        // remove empty group
        removeGroup._execute({ userId }, { groupId: group._id });
      }
    }
  });
  // remove limitation on this structure on all groups concerned
  Groups.update({ structureIds: structureId }, { $pull: { structureIds: structureId } }, { multi: true });
};

export function _deleteStructure(structure, userId) {
  // Check if any user is attached to this structure
  const usersCursor = Meteor.users.find({ structure: structure._id });
  if (usersCursor.count() > 0) {
    logServer(
      `STRUCTURE - METHODS - METEOR ERROR - removeStructure - ${i18n.__('api.structures.hasUsers')}`,
      levels.ERROR,
      scopes.SYSTEM,
      { structureId: structure._id },
    );
    throw new Meteor.Error('api.structures.removeStructure.hasUsers', i18n.__('api.structures.hasUsers'));
  }

  logServer(`STRUCTURE - METHODS - REMOVE - removeStructure`, levels.INFO, scopes.SYSTEM, {
    structureId: structure._id,
  });
  // If there are any article attached to this structure, delete them
  Articles.rawCollection().deleteMany({ structure: structure._id });

  const { ancestorsIds } = structure;

  if (ancestorsIds.length > 0) {
    const ancestorsCursor = Structures.find({ _id: { $in: ancestorsIds } });
    if (ancestorsCursor.count() > 0) {
      ancestorsCursor.forEach((ancestor) => {
        logServer(
          `STRUCTURE - METHODS - UPDATE - removeStructure - ancestor id: ${ancestor._id}
               / structureid: ${structure._id}`,
          levels.INFO,
          scopes.SYSTEM,
        );
        Structures.update(
          { _id: ancestor._id },
          {
            $set: {
              childrenIds: [...new Set(_.without(ancestor.childrenIds, structure._id))],
            },
          },
        );
      });
    }
  }

  // remove structure automatic group
  const group = Groups.findOne({ _id: structure.groupId });
  if (group) {
    _removeGroup({ groupId: group._id, userId });
  }
  // If there are icon or cover images ==> delete them from minio
  structureRemoveIconOrCoverImagesFromMinio(structure, true, true);
  // If there are any group limited to this structure, remove limitation and delete unused groups
  removeGroupLimitations(structure._id, userId);

  logServer(`STRUCTURE - METHODS - REMOVE - removeStructure`, levels.VERBOSE, scopes.SYSTEM, {
    structureId: structure._id,
  });
  return Structures.remove({ _id: structure._id });
}

export const removeStructure = new ValidatedMethod({
  name: 'structures.removeStructure',
  validate: new SimpleSchema({
    structureId: { type: String, regEx: RegEx.Id, label: getLabel('api.structures.labels.id') },
  }).validator(),

  run({ structureId }) {
    // check structure existence
    const structure = Structures.findOne({ _id: structureId });
    if (structure === undefined) {
      logServer(
        `STRUCTURE - METHODS - METEOR ERROR - removeStructure - ${i18n.__('api.structures.unknownStructure')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { structureId },
      );
      throw new Meteor.Error(
        'api.structures.removeStructure.unknownStructure',
        i18n.__('api.structures.unknownStructure'),
      );
    }

    const isAdminOfStructure = hasAdminRightOnStructure({ userId: this.userId, structureId });
    const authorized = isActive(this.userId) && (Roles.userIsInRole(this.userId, 'admin') || isAdminOfStructure);

    if (!authorized) {
      logServer(
        `STRUCTURE - METHODS - METEOR ERROR - removeStructure - ${i18n.__('api.users.notPermitted')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { structureId },
      );
      throw new Meteor.Error('api.structures.removeStructure.notPermitted', i18n.__('api.users.notPermitted'));
    }

    // Check if structure has children
    if (structure.childrenIds.length > 0) {
      logServer(
        `STRUCTURE - METHODS - METEOR ERROR - removeStructure - ${i18n.__('api.structures.hasChildren')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { structureId },
      );
      throw new Meteor.Error('api.structures.removeStructure.hasChildren', i18n.__('api.structures.hasChildren'));
    }

    // Check if any service is attached to this structure
    const servicesCursor = Services.find({ structure: structureId });
    if (servicesCursor.count() > 0) {
      logServer(
        `STRUCTURE - METHODS - METEOR ERROR - removeStructure - ${i18n.__('api.structures.hasServices')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { structureId },
      );
      throw new Meteor.Error('api.structures.removeStructure.hasServices', i18n.__('api.structures.hasServices'));
    }

    return _deleteStructure(structure, this.userId);
  },
});

function checkImageUrl() {
  if (this.value === '-1') return undefined;
  try {
    // eslint-disable-next-line no-new
    new URL(this.value);
    return undefined;
  } catch {
    return SimpleSchema.ErrorTypes.VALUE_NOT_ALLOWED;
  }
}

const paramsSchema = new SimpleSchema({
  structureId: { type: String, regEx: RegEx.Id, label: getLabel('api.structures.labels.id') },
  iconUrlImage: {
    type: String,
    custom: checkImageUrl,
  },
  coverUrlImage: {
    type: String,
    custom: checkImageUrl,
  },
});

export const updateStructureIconOrCoverImage = new ValidatedMethod({
  name: 'structures.updateStructureIconOrCoverImage',
  validate: paramsSchema.validator(),
  run({ structureId, iconUrlImage, coverUrlImage }) {
    const structure = Structures.findOne({ _id: structureId });

    if (structure === undefined) {
      logServer(
        `STRUCTURES - METHOD - METEOR ERROR - updateStructureIconOrCoverImage - ${i18n.__(
          'api.structures.unknownStructure',
        )}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          structureId,
          iconUrlImage,
          coverUrlImage,
        },
      );
      throw new Meteor.Error(
        'api.structures.updateIconOrCoverImage.unknownStructure',
        i18n.__('api.structures.unknownStructure'),
      );
    }

    const authorized = isActive(this.userId) && hasAdminRightOnStructure({ userId: this.userId, structureId });

    if (!authorized) {
      logServer(
        `STRUCTURES - METHOD - METEOR ERROR - updateStructureIconOrCoverImage - ${i18n.__('api.users.notPermitted')}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          structureId,
          iconUrlImage,
          coverUrlImage,
        },
      );
      throw new Meteor.Error('api.structures.updateIconOrCoverImage.notPermitted', i18n.__('api.users.notPermitted'));
    }
    if (iconUrlImage) validateString(iconUrlImage);
    if (coverUrlImage) validateString(coverUrlImage);
    let res = -1;

    if (iconUrlImage !== '-1') res = Structures.update({ _id: structureId }, { $set: { iconUrlImage } });

    if (coverUrlImage !== '-1') res = Structures.update({ _id: structureId }, { $set: { coverUrlImage } });

    logServer(`STRUCTURES - METHOD - UPDATE - updateStructureIconOrCoverImage`, levels.INFO, scopes.SYSTEM, {
      structureId,
      iconUrlImage,
      coverUrlImage,
    });
    return res;
  },
});

export const deleteIconOrCoverImage = new ValidatedMethod({
  name: 'structures.deleteIconOrCoverImage',
  validate: paramsSchema.validator(),
  run({ structureId, iconUrlImage, coverUrlImage }) {
    const structure = Structures.findOne({ _id: structureId });

    if (structure === undefined) {
      logServer(
        `STRUCTURES - METHOD - METEOR ERROR - deleteIconOrCoverImage - ${i18n.__('api.structures.unknownStructure')}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          structureId,
          iconUrlImage,
          coverUrlImage,
        },
      );
      throw new Meteor.Error(
        'api.structures.updateIconOrCoverImage.unknownStructure',
        i18n.__('api.structures.unknownStructure'),
      );
    }

    const authorized = isActive(this.userId) && hasAdminRightOnStructure({ userId: this.userId, structureId });

    if (!authorized) {
      logServer(
        `STRUCTURES - METHOD - METEOR ERROR - deleteIconOrCoverImage - ${i18n.__('api.users.notPermitted')}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          structureId,
          iconUrlImage,
          coverUrlImage,
        },
      );
      throw new Meteor.Error('api.structures.deleteIconOrCoverImage.notPermitted', i18n.__('api.users.notPermitted'));
    }

    // If there are icon or cover images ==> delete them from minio
    structureRemoveIconOrCoverImagesFromMinio(structure, iconUrlImage !== '-1', coverUrlImage !== -1);

    let res = {};

    if (iconUrlImage !== '-1') res = Structures.update({ _id: structureId }, { $unset: { iconUrlImage: '' } });

    if (coverUrlImage !== '-1') res = Structures.update({ _id: structureId }, { $unset: { coverUrlImage: '' } });

    logServer(`STRUCTURES - METHOD - UPDATE - deleteIconOrCoverImage`, levels.INFO, scopes.SYSTEM, {
      structureId,
      iconUrlImage,
      coverUrlImage,
    });
    return res;
  },
});

// Recursive function to build the tree
const buildTree = (structure, allSubStructures) => {
  const subStructures = allSubStructures
    .filter((struct) => struct.parentId === structure._id)
    .map((child) => buildTree(child, allSubStructures));

  return {
    ...structure,
    subStructures,
  };
};

export const getStructureAndAllChilds = new ValidatedMethod({
  name: 'structures.getStructureAndAllChilds',
  validate: new SimpleSchema({
    allStructures: {
      type: Boolean,
      optional: true,
    },
  }).validator(),
  run({ allStructures }) {
    const user = Meteor.user({ structure: 1 });
    const isAdmin = Roles.userIsInRole(this.userId, 'admin');

    if (allStructures && !isAdmin) {
      throw unauthorized();
    }

    if (!(isAdmin || Roles.userIsInRole(this.userId, 'adminStructure', user.structure))) {
      throw unauthorized();
    }

    const $match = {};

    if (allStructures) {
      $match.parentId = null;
    } else {
      $match._id = user.structure;
    }

    const structureTree = Structures.aggregate([
      { $match },
      {
        $graphLookup: {
          from: 'structures',
          startWith: '$_id',
          connectFromField: '_id',
          connectToField: 'parentId',
          as: 'subStructures',
        },
      },
      {
        $project: {
          _id: 1,
          name: 1,
          subStructures: 1,
        },
      },
    ]);

    // Start building the tree from the initial structure
    const result = structureTree.map((struct) => buildTree(struct, struct.subStructures));

    return result;
  },
});

// Get list of all method names on Structures

if (Meteor.isServer) {
  const LISTS_METHODS = _.pluck(
    [
      createStructure,
      removeStructure,
      updateStructureIconOrCoverImage,
      deleteIconOrCoverImage,
      getStructureAndAllChilds,
    ],
    'name',
  );

  // Only allow 5 list operations per connection per second
  DDPRateLimiter.addRule(
    {
      name(name) {
        return _.contains(LISTS_METHODS, name);
      },

      // Rate limit per connection ID
      connectionId() {
        return true;
      },
    },
    5,
    1000,
  );
}
