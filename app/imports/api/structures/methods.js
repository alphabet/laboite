import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import SimpleSchema from 'simpl-schema';
import { Roles } from 'meteor/alanning:roles';
import i18n from 'meteor/universe:i18n';
import { _ } from 'meteor/underscore';
import { getLabel, isActive, validateString, accentInsensitive } from '../utils';
import Structures from './structures';
import { hasAdminRightOnStructure, isAStructureWithSameNameExistWithSameParent, getExternalService } from './utils';
import Groups from '../groups/groups';
import { removeFilesFolder } from '../files/server/methods';
import logServer, { levels, scopes } from '../logging';
import RegEx from '../regExp';

function updatePathOfStructure(structure, name, strucToEdit) {
  const structureOfPath = structure.childrenIds;
  if (structureOfPath && structureOfPath.length > 0) {
    const allStructures = Structures.find({ _id: { $in: structure.childrenIds } }).fetch();
    if (allStructures && allStructures.length > 0) {
      allStructures.map((struc) => {
        if (struc.structurePath) {
          const obj = struc.structurePath.find((strucOnPath) => strucOnPath.structureId === strucToEdit._id);
          if (obj) {
            obj.structureName = name;
            Structures.update({ _id: struc._id }, { $set: { structurePath: struc.structurePath } });
          }
        }
        updatePathOfStructure(struc, name, strucToEdit);
        return null;
      });
    }
  }
}

export const updateStructure = new ValidatedMethod({
  name: 'structures.updateStructure',
  validate: new SimpleSchema({
    structureId: { type: String, regEx: RegEx.Id, label: getLabel('api.structures.labels.id') },
    name: {
      type: String,
      min: 1,
      label: getLabel('api.structures.name'),
    },
  }).validator(),

  run({ structureId, name }) {
    // check structure existence
    const structure = Structures.findOne({ _id: structureId });
    if (structure === undefined) {
      logServer(
        `STRUCTURE - METHODS - METEOR ERROR - updateStructure - ${i18n.__('api.structures.unknownStructure')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { structureId, name },
      );
      throw new Meteor.Error(
        'api.structures.updateStructure.unknownStructure',
        i18n.__('api.structures.unknownStructure'),
      );
    }

    const isAdminOfStructure = hasAdminRightOnStructure({ userId: this.userId, structureId });
    const authorized = isActive(this.userId) && (Roles.userIsInRole(this.userId, 'admin') || isAdminOfStructure);

    if (!authorized) {
      logServer(
        `STRUCTURE - METHODS - METEOR ERROR - updateStructure - ${i18n.__('api.users.notPermitted')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { structureId, name },
      );
      throw new Meteor.Error('api.structures.updateStructure.notPermitted', i18n.__('api.users.notPermitted'));
    }

    const structuresWithSameNameOnSameLevel = isAStructureWithSameNameExistWithSameParent({
      name,
      parentId: structure.parentId,
      structureId: structure._id,
    });

    if (structuresWithSameNameOnSameLevel) {
      logServer(
        `STRUCTURE - METHODS - METEOR ERROR - updateStructure - ${i18n.__('api.structures.nameAlreadyTaken')}}`,
        levels.WARN,
        scopes.SYSTEM,
        { structureId, name },
      );
      throw new Meteor.Error('api.structures.updateStructure.notPermitted', i18n.__('api.structures.nameAlreadyTaken'));
    }
    validateString(name);
    const group = Groups.findOne({ _id: structure.groupId });
    if (group) {
      group.name = `${structure._id}_${name}`;
      Groups.update({ _id: group._id }, { $set: { name: `${structure._id}_${name}` } });
    }

    // update structurePath of children structures
    updatePathOfStructure(structure, name, structure);

    logServer(`STRUCTURE - METHODS - UPDATE - updateStructure`, levels.VERBOSE, scopes.SYSTEM, { structureId, name });
    Structures.update({ _id: structureId }, { $set: { name } });
    const struc = Structures.findOne({ _id: structureId });
    return struc;
  },
});

export const setUserStructureAdminValidationMandatoryStatus = new ValidatedMethod({
  name: 'structures.setUserStructureAdminValidationMandatoryStatus',
  validate: new SimpleSchema({
    structureId: { type: String, regEx: RegEx.Id, label: getLabel('api.structures.labels.id') },
    userStructureValidationMandatory: {
      type: Boolean,
      label: getLabel('api.appsettings.userStructureValidationMandatory'),
    },
  }).validator(),

  run({ structureId, userStructureValidationMandatory }) {
    // check structure existence
    const structure = Structures.findOne({ _id: structureId });
    if (structure === undefined) {
      logServer(
        `STRUCTURE - METHODS - METEOR ERROR - setUserStructureAdminValidationMandatoryStatus - ${i18n.__(
          'api.structures.unknownStructure',
        )}`,
        levels.ERROR,
        scopes.SYSTEM,
        { structureId, userStructureValidationMandatory },
      );
      throw new Meteor.Error(
        'api.structures.setUserStructureAdminValidationMandatoryStatus.unknownStructure',
        i18n.__('api.structures.unknownStructure'),
      );
    }

    const isAdminOfStructure = hasAdminRightOnStructure({ userId: this.userId, structureId });
    const authorized = isActive(this.userId) && (Roles.userIsInRole(this.userId, 'admin') || isAdminOfStructure);

    if (!authorized) {
      logServer(
        `STRUCTURE - METHODS - METEOR ERROR - setUserStructureAdminValidationMandatoryStatus - ${i18n.__(
          'api.users.notPermitted',
        )}`,
        levels.ERROR,
        scopes.SYSTEM,
        { structureId, userStructureValidationMandatory },
      );
      throw new Meteor.Error(
        'api.structures.setUserStructureAdminValidationMandatoryStatus.notPermitted',
        i18n.__('api.users.notPermitted'),
      );
    }

    logServer(
      `STRUCTURE - METHODS - UPDATE - setUserStructureAdminValidationMandatoryStatus`,
      levels.VERBOSE,
      scopes.SYSTEM,
      { structureId, userStructureValidationMandatory },
    );
    return Structures.update({ _id: structureId }, { $set: { userStructureValidationMandatory } });
  },
});

export const structureRemoveIconOrCoverImagesFromMinio = (structure, removeIconImg, removeCoverImg) => {
  if (Meteor.isServer && !Meteor.isTest && Meteor.settings.public.minioEndPoint) {
    if (structure.iconUrlImage && removeIconImg) {
      removeFilesFolder(`structures/${structure._id}/iconImg`);
    }

    if (structure.coverUrlImage && removeCoverImg) {
      removeFilesFolder(`structures/${structure._id}/coverImg`);
    }
  }
};

export const getAllChilds = new ValidatedMethod({
  name: 'structures.getAllChilds',
  validate: new SimpleSchema({
    structureId: { type: String, regEx: RegEx.Id, label: getLabel('api.structures.labels.id') },
  }).validator(),
  run({ structureId }) {
    const structure = Structures.findOne({ _id: structureId });

    if (structure === undefined) {
      logServer(
        `STRUCTURE - METHODS - METEOR ERROR - getAllChilds - ${i18n.__('api.structures.unknownStructure')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { structureId },
      );
      throw new Meteor.Error(
        'api.structures.getAllChilds.unknownStructure',
        i18n.__('api.structures.unknownStructure'),
      );
    }

    const authorized = isActive(this.userId);

    if (!authorized) {
      logServer(
        `STRUCTURE - METHODS - METEOR ERROR - getAllChilds - ${i18n.__('api.users.notPermitted')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { structureId },
      );
      throw new Meteor.Error('api.structures.getAllChilds.notPermitted', i18n.__('api.users.notPermitted'));
    }

    const childs = Structures.find({ ancestorsIds: structureId }).fetch();
    return childs;
  },
});

export const updateStructureContactEmail = new ValidatedMethod({
  name: 'structures.updateContactData',
  validate: new SimpleSchema({
    contactEmail: {
      type: String,
    },
    externalUrl: {
      type: String,
    },
    sendMailToParent: {
      type: Boolean,
    },
    sendMailToStructureAdmin: {
      type: Boolean,
    },
    structureId: { type: String, regEx: RegEx.Id, label: getLabel('api.structures.labels.id') },
  }).validator(),
  run({ structureId, contactEmail, externalUrl, sendMailToParent, sendMailToStructureAdmin }) {
    const structure = Structures.findOne({ _id: structureId });

    if (structure === undefined) {
      logServer(
        `STRUCTURE - METHODS - METEOR ERROR - updateStructureContactEmail - ${i18n.__(
          'api.structures.unknownStructure',
        )}`,
        levels.ERROR,
        scopes.SYSTEM,
        { structureId, contactEmail, externalUrl, sendMailToParent, sendMailToStructureAdmin },
      );
      throw new Meteor.Error(
        'api.structures.updateContactEmail.unknownStructure',
        i18n.__('api.structures.unknownStructure'),
      );
    }

    const authorized = isActive(this.userId) && hasAdminRightOnStructure({ userId: this.userId, structureId });

    if (!authorized) {
      logServer(
        `STRUCTURE - METHODS - METEOR ERROR - updateStructureContactEmail - ${i18n.__('api.users.notPermitted')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { structureId, contactEmail, externalUrl, sendMailToParent, sendMailToStructureAdmin },
      );
      throw new Meteor.Error('api.structures.updateContactEmail.notPermitted', i18n.__('api.users.notPermitted'));
    }
    if (contactEmail) {
      validateString(contactEmail);
    }
    if (externalUrl) {
      validateString(externalUrl);
    }
    logServer(`STRUCTURE - METHODS - UPDATE - updateStructureContactEmail`, levels.VERBOSE, scopes.SYSTEM, {
      structureId,
      contactEmail,
      externalUrl,
      sendMailToParent,
      sendMailToStructureAdmin,
    });

    return Structures.update(
      { _id: structureId },
      { $set: { contactEmail, externalUrl, sendMailToParent, sendMailToStructureAdmin } },
    );
  },
});

export const getStructures = new ValidatedMethod({
  name: 'structures.getStructures',
  validate: null,
  run() {
    return Structures.find().fetch();
  },
});

export const getAdminStructures = new ValidatedMethod({
  name: 'structures.getAdminStructures',
  validate: null,
  run() {
    if (isActive(this.userId)) {
      if (Roles.userIsInRole(this.userId, 'admin')) {
        return Structures.find().fetch();
      }
      const user = Meteor.users.findOne(this.userId);
      if (Roles.userIsInRole(this.userId, 'adminStructure', user.structure)) {
        return Structures.find({
          $or: [{ ancestorsIds: user.structure || '' }, { _id: user.structure || '' }],
        }).fetch();
      }
    }
    return [];
  },
});

export const getOneStructure = new ValidatedMethod({
  name: 'structures.getOneStructure',
  validate: new SimpleSchema({
    structureId: { type: String, regEx: RegEx.Id, label: getLabel('api.structures.labels.id') },
  }).validator(),
  run({ structureId }) {
    const structure = Structures.findOne({ _id: structureId });
    return structure;
  },
});

export const getContactURL = new ValidatedMethod({
  name: 'structures.getContactURL',
  validate: null,
  run() {
    if (!this.userId) {
      throw new Meteor.Error('api.structures.getContactURL.notPermitted', i18n.__('api.users.notPermitted'));
    }
    const user = Meteor.users.findOne(this.userId);
    const structure = Structures.findOne(user.structure);
    if (structure) return getExternalService(structure);
    return null;
  },
});

export const getTopLevelStructures = new ValidatedMethod({
  name: 'structures.getTopLevelStructures',
  validate: null,
  run() {
    return Structures.find({ $or: [{ parentId: null }, { parentId: '' }] }).fetch();
  },
});

function generateTreeParentToChild(structure, tree, level) {
  if (structure.childrenIds && structure.childrenIds.length > 0) {
    const structures = Structures.find({ _id: { $in: structure.childrenIds } }).fetch();
    if (structures) {
      structures.map((struc) => {
        const child = { structure: struc, level };
        tree.push(child);
        return tree;
      });
    }
  }
}

export const getTreeOfStructure = new ValidatedMethod({
  name: 'structures.getTreeOfStructure',
  validate: new SimpleSchema({
    structureId: { type: String, regEx: RegEx.Id, label: getLabel('api.structures.labels.id') },
    parentToChild: { type: Boolean, regEx: RegEx.Id, label: getLabel('api.structures.labels.bool') },
  }).validator(),
  run({ structureId, parentToChild }) {
    const tree = [];
    const structure = Structures.findOne({ _id: structureId });
    const parent = Structures.findOne({ _id: structure.parentId });

    if (parentToChild) generateTreeParentToChild(structure, tree, 1);
    else if (parent) {
      generateTreeParentToChild(parent, tree, 1);
    }

    return { tree, parent };
  },
});

export const searchStructure = new ValidatedMethod({
  name: 'structures.searchStructure',
  validate: new SimpleSchema({
    searchText: { type: String },
    structureAdminMode: { type: Boolean },
    userStructure: { type: String },
  }).validator(),
  run({ searchText, structureAdminMode, userStructure }) {
    const regex = new RegExp(accentInsensitive(searchText), 'i');
    let structures;

    if (structureAdminMode) {
      structures = Structures.find({
        $and: [{ name: { $regex: regex } }, { structurePath: { $elemMatch: { structureId: userStructure } } }],
      }).fetch();
    } else {
      structures = Structures.find({ name: { $regex: regex } }).fetch();
    }
    return structures;
  },
});

export const searchStructureById = new ValidatedMethod({
  name: 'structures.searchStructureById',
  validate: new SimpleSchema({
    structureId: { type: String },
  }).validator(),
  run({ structureId }) {
    const structure = Structures.find({ _id: structureId }).fetch();
    return structure;
  },
});

if (Meteor.isServer) {
  // Get list of all method names on Structures
  const LISTS_METHODS = _.pluck(
    [
      updateStructure,
      getAllChilds,
      updateStructureContactEmail,
      getStructures,
      getOneStructure,
      getContactURL,
      setUserStructureAdminValidationMandatoryStatus,
      getTopLevelStructures,
      getTreeOfStructure,
      searchStructure,
    ],
    'name',
  );

  // Only allow 5 list operations per connection per second
  DDPRateLimiter.addRule(
    {
      name(name) {
        return _.contains(LISTS_METHODS, name);
      },

      // Rate limit per connection ID
      connectionId() {
        return true;
      },
    },
    5,
    1000,
  );
}
