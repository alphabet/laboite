import logServer, { levels, scopes } from '../../logging';
import Structures from '../../structures/structures';
import { _createService } from '../methods';
import Services from '../services';

function GetChildStructure(strucs, parent, index) {
  if (strucs[index]) {
    const struc = Structures.findOne({ $and: [{ name: strucs[index] }, { parentId: parent._id }] });
    if (!struc) {
      return parent;
    }
    return GetChildStructure(strucs, struc, index + 1);
  }
}

export default async function createService(req, content) {
  // TODO

  // sample use:
  // curl -X POST -H "X-API-KEY: createuser-password" \
  //      -H "Content-Type: application/json" \
  //      -d '{"title": "test",
  //          "description": "test description",
  //          "url": "http://test.com",
  //          "logo": 1,
  //          "categories": "name",
  //          "businessReGrouping": 1,
  //          "usage": 1,
  //          "team": 1,
  //          "slug": 1,
  //          "state": 1,
  //          "offline": false,
  //          "isBeta": true,
  //          "structure": "structureName"'} \
  //      http://localhost:3000/api/services/create

  if (content) {
    if ('structure' in content) {
      const strucs = content.structure.split('/');
      if (strucs) {
        const parent = Structures.findOne({ name: strucs[0] });
        if (parent) {
          const struc = GetChildStructure(content.structure.split('/'));

          const data = content;
          data.structure = struc.name;
          const id = _createService(content);
          if (id) {
            return 200;
          }
          logServer(`API - SERVICES - POST - Content empty or not valid`, levels.ERROR, scopes.SYSTEM);
          throw new Meteor.Error('api.createStructures.emptyContent', 'Empty content');
        }
      }
    } else {
      const id = _createService(content);
      if (id) {
        return 200;
      }
      logServer(`API - SERVICES - POST - Content empty or not valid`, levels.ERROR, scopes.SYSTEM);
      throw new Meteor.Error('api.createStructures.emptyContent', 'Empty content');
    }
  }
  logServer(`API - SERVICES - POST - Content empty or not valid`, levels.ERROR, scopes.SYSTEM);
  throw new Meteor.Error('api.createStructures.emptyContent', 'Empty content');
}

function generateDataForService(service) {
  const obj = service;
  delete obj._id;

  const structure = Structures.findOne({ _id: service.structure });
  obj.structure = structure.name;

  return obj;
}

// sample use:
// curl -X  POST -H "X-API-KEY: createuser-password" \
//      -H "Content-Type: application/json" \
//      -d '{"name":"test" }' \
//      http://localhost:3000/api/services
// curl -X  POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"title":"test" }' http://localhost:3000/api/services

export async function getServices(req, content) {
  if ('title' in content) {
    const service = Services.findOne({ title: content.title });
    if (service) {
      const obj = generateDataForService(service);
      return obj;
    }
    logServer(`API - SERVICES - POST - Services Not found`, levels.ERROR, scopes.SYSTEM);
    throw new Meteor.Error('api.createStructures.notFound', 'Services not found');
  }
  logServer(`API - SERVICES - POST - Services not found`, levels.ERROR, scopes.SYSTEM);
  throw new Meteor.Error('api.createStructures.notFound', 'Services not found');
}

export async function getAllServices() {
  const res = [];
  const asams = Services.find().fetch();
  if (asams) {
    asams.forEach((asam) => {
      const obj = generateDataForService(asam);
      if (obj) {
        res.push(obj);
      }
    });
    return res;
  }
  logServer(`API - SERVICES - GET - No Service found`, levels.ERROR, scopes.SYSTEM);
  throw new Meteor.Error('api.createStructures.notFound', 'Services not found');
}
