import { Meteor } from 'meteor/meteor';
import logServer, { levels, scopes } from '../../logging';
import { _createUserBookmarkUrl } from '../methods';
import UserBookmarks from '../userBookmarks';

function CreateUserBookmarkFromAPI(bookmark) {
  const user = Meteor.users.findOne({ username: bookmark.user });
  if (user) {
    try {
      _createUserBookmarkUrl({ url: bookmark.url, name: bookmark.name, tag: bookmark.tag, userId: user._id });
      return 200;
    } catch (error) {
      error.statusCode = 409;
      throw error;
    }
  }
  logServer(`API - BOOKMARKS - POST - User not found`, levels.ERROR, scopes.SYSTEM, {
    user: bookmark.user,
  });
  const error = new Meteor.Error('api.CreateUserBookmarkFromAPI.userNotFound', `User not found (${bookmark.user})`);
  error.statusCode = 404;
  throw error;
}

export async function createOneUserBookmark(req, content) {
  // sample use:
  // curl -X POST -H "X-API-KEY: createuser-password" \
  //      -H "Content-Type: application/json" \
  //      -d '{"url":"http:\/\/url.com", "name":"bookmark1", "tag":"tag", "user": "username1"}' \
  //      http://localhost:3000/api/userbookmarks/create

  // eslint-disable-next-line spaced-comment
  // curl -X POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"url":"http:\/\/url.com", "name":"bookmark2", "tag":"tag", "user": "username2", "icon":"url"}' http://localhost:3000/api/userbookmarks/create

  if (content) {
    return CreateUserBookmarkFromAPI(content);
  }
  logServer(`API - USERBOOKMARKS - POST - Empty Content`, levels.ERROR, scopes.SYSTEM, {});
  const error = new Meteor.Error('api.createOneUserBookmark.emptyContent', 'Empty content');
  error.statusCode = 400;
  throw error;
}

function generateDataForUserBookmark(bookmark) {
  const user = bookmark.userId ? Meteor.users.findOne({ _id: bookmark.userId }) : null;

  const obj = {
    name: bookmark.name,
    tag: bookmark.tag,
    user: user ? user.username : null,
    url: bookmark.url,
    icon: bookmark.icon,
  };
  return obj;
}

// sample use:
// curl -X  POST -H "X-API-KEY: createuser-password" \
//      -H "Content-Type: application/json" \
//      -d '{"username": "username1", "url":"http:\/\/test.com" }' \
//      http://localhost:3000/api/userbookmarks
// curl -X  POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"username": "username1", "url":"http:\/\/test.com" }' http://localhost:3000/api/userbookmarks
export async function getUserBookmarks(req, content) {
  if ('username' in content && 'url' in content) {
    const user = Meteor.users.findOne({ username: content.username });
    if (user) {
      const bookmark = UserBookmarks.findOne({ $and: [{ userId: user._id }, { url: content.url }] });
      if (bookmark) return bookmark;
      const error = new Meteor.Error('api.getUserBookmarks.notFound', 'Bookmark not found');
      error.statusCode = 404;
      throw error;
    }
    logServer(`API - USERBOOKMARKS - POST - USER NOT FOUND`, levels.ERROR, scopes.SYSTEM, {});
    const error = new Meteor.Error('api.getUserBookmarks.userNotFound', 'User not found');
    error.statusCode = 404;
    throw error;
  }
  logServer(`API - USERBOOKMARKS - POST - Empty Content`, levels.ERROR, scopes.SYSTEM, {});
  const error = new Meteor.Error('api.getUserBookmarks.emptyContent', 'Empty or invalid content');
  error.statusCode = 400;
  throw error;
}

// sample use:
// curl -H "X-API-KEY: createuser-password" http://localhost:3000/api/userbookmarks | jq

export async function getAllUserBookmarks() {
  const bookmarks = UserBookmarks.find().fetch();
  const res = [];
  if (bookmarks) {
    bookmarks.forEach((bookmark) => {
      const obj = generateDataForUserBookmark(bookmark);
      res.push(obj);
    });
  }
  return res;
}

export async function deleteUserBookmark(req, content) {
  // sample use:
  // curl -X DELETE -H "X-API-KEY: createuser-password" \
  //      -H "Content-Type: application/json" \
  //      -d '{"url":"http:\/\/url.com", user: "username"}',
  //      http://localhost:3000/api/userbookmarks/delete

  // eslint-disable-next-line spaced-comment
  // curl -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"url":"http:\/\/url.com", user: "username"}' http://localhost:3000/api/userbookmarks/delete

  if (content) {
    const user = Meteor.users.findOne({ username: content.username });
    if (user) {
      const bookmark = UserBookmarks.findOne({ url: content.url });
      if (bookmark) {
        UserBookmarks.remove({ url: bookmark.url });
        return 200;
      }
      logServer(`API - USERBOOKMARKS - DELETE - UserBookmark not found`, levels.ERROR, scopes.SYSTEM, {});
      const error = new Meteor.Error('api.deleteUserBookmark.noFound', 'Bookmark not found');
      error.statusCode = 404;
      throw error;
    }
    logServer(`API - USERBOOKMARKS - DELETE - User not found`, levels.ERROR, scopes.SYSTEM, {});
    const error = new Meteor.Error('api.deleteUserBookmark.noFound', 'User not found');
    error.statusCode = 404;
    throw error;
  }
  logServer(`API - USERBOOKMARKS - DELETE - Empty content`, levels.ERROR, scopes.SYSTEM, {});
  const error = new Meteor.Error('api.deleteUserBookmark.emptyContent', 'Empty content');
  error.statusCode = 400;
  throw error;
}
