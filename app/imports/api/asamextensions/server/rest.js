import { Meteor } from 'meteor/meteor';
import logServer, { levels, scopes } from '../../logging';
import Structures from '../../structures/structures';
import { _createAsam } from '../methods';
import AsamExtensions from '../asamextensions';
import { GetStructure } from '../../structures/utils';

function CreateRuleFromAPI(asam) {
  const strucs = asam.path.split('/');
  if (strucs) {
    const struc = GetStructure(strucs, null, 0);
    if (struc) {
      try {
        _createAsam({
          extension: asam.extension,
          entiteNomCourt: '',
          entiteNomLong: '',
          familleNomCourt: '',
          familleNomLong: '',
          structureId: struc._id,
        });
        return 200;
      } catch (error) {
        error.statusCode = 409;
        throw error;
      }
    }
    const error = new Meteor.Error('api.createASAMFromAPI.structureNotFound', 'Structure not found');
    error.statusCode = 404;
    throw error;
  }
  const error = new Meteor.Error('api.createASAMFromAPI.structuresNotFound', 'Structures not found');
  error.statusCode = 404;
  throw error;
}

// sample use:
// curl -X POST -H "X-API-KEY: createuser-password" \
//      -H "Content-Type: application/json" \
//      -d '{"extension":"structure1.fr", "path": "api1/api2/api3"}' \
//      http://localhost:3000/api/structureruledispatchs/create
// eslint-disable-next-line spaced-comment
// curl -X POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"extension":"api3.fr", "path": "api1/api2/api3"}' http://localhost:3000/api/structureruledispatchs/create
export async function createOneStructureRuleDispatch(req, content) {
  if (content) {
    return CreateRuleFromAPI(content);
  }
  logServer(`API - ASAM - GET - Invalid or empty content`, levels.ERROR, scopes.SYSTEM);
  const error = new Meteor.Error('api.createOneDomainMail.emptyContent', 'Empty content');
  error.statusCode = 409;
  throw error;
}

function GeneratePath(structure, tree) {
  if (structure.parentId) {
    const parent = Structures.findOne({ _id: structure.parentId });
    if (parent) {
      tree.push(parent.name);
      GeneratePath(parent, tree);
    }
  }
}

function generateStructureRuleDispatchData(asam) {
  const struc = Structures.findOne({ _id: asam.structureId });
  if (struc) {
    const tree = [struc.name];
    if (struc.parentId) {
      const parent = Structures.findOne({ _id: struc.parentId });
      if (parent) {
        tree.push(parent.name);
        GeneratePath(parent, tree);
      }
    }

    const reversed = tree.reverse();
    const obj = { extension: asam.extension, path: reversed.join('/') };
    return obj;
  }
  return null;
}

// sample use:
// curl -X  POST -H "X-API-KEY: createuser-password" \
//      -H "Content-Type: application/json" \
//      -d '{"name":"test" }' \
//      http://localhost:3000/api/structureruledispatchs
// curl -X  POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"extension":"test" }' http://localhost:3000/api/structureruledispatchs

export async function getStructureRuleDispatchs(req, content) {
  if ('extension' in content) {
    const asam = AsamExtensions.findOne({ extension: content.extension });
    if (asam) {
      const obj = generateStructureRuleDispatchData(asam);
      return obj;
    }
  }
  logServer(`API - ASAM - GET - Invalid or empty content`, levels.ERROR, scopes.SYSTEM);
  const error = new Meteor.Error('api.getOneDomainMail.emptyContent', 'Invalid or empty content');
  error.statusCode = 409;
  throw error;
}

// sample use:
// curl -H "X-API-KEY: createuser-password" http://localhost:3000/api/structureruledispatchs | jq

export async function getAllStructureRuleDispatchs() {
  const res = [];
  const asams = AsamExtensions.find().fetch();
  if (asams) {
    asams.forEach((asam) => {
      const obj = generateStructureRuleDispatchData(asam);
      if (obj) {
        res.push(obj);
      }
    });
    return res;
  }
  logServer(`API - ASAM - GET - No Structure rule dispatch found`, levels.ERROR, scopes.SYSTEM);
  const error = new Meteor.Error('api.getAllDomainMail.emptyContent', 'No Structure rule dispatch found');
  error.statusCode = 404;
  throw error;
}

// sample use:
// curl -X DELETE -H "X-API-KEY: createuser-password" \
//      -H "Content-Type: application/json" \
//      -d '{"extension":"structure1.fr"}' \
//      http://localhost:3000/api/structureruledispatchs/delete
// eslint-disable-next-line spaced-comment
// curl -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"extension":"api3.fr"}' http://localhost:3000/api/structureruledispatchs/delete
export async function deleteStructureRuleDispatch(req, content) {
  if (content) {
    const asam = AsamExtensions.findOne({ extension: content.extension });
    if (asam) {
      AsamExtensions.remove({ _id: asam._id });
      return 200;
    }
    logServer(`API - ASAM - DELETE - Structure rule dispatch not found`, levels.ERROR, scopes.SYSTEM);
    const error = new Meteor.Error('api.deleteDomainMail.emptyContent', 'Structure rule dispatch not found');
    error.statusCode = 404;
    throw error;
  }
  logServer(`API - ASAM - DELETE - Invalid or empty content`, levels.ERROR, scopes.SYSTEM);
  const error = new Meteor.Error('api.deleteDomainMail.emptyContent', 'Empty content');
  error.statusCode = 409;
  throw error;
}
