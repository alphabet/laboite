import { FindFromPublication } from 'meteor/percolate:find-from-publication';
import { Roles } from 'meteor/alanning:roles';
import Polls from '../polls';
import Groups from '../../groups/groups';

import { checkPaginationParams, isActive } from '../../utils';
import logServer, { levels, scopes } from '../../logging';

// build query for all users from group
const queryGroupPolls = ({ search, group, onlyPublic, type }) => {
  const regex = new RegExp(search, 'i');
  const fieldsToSearch = ['title', 'description', 'updatedAt'];
  const searchQuery = fieldsToSearch.map((field) => ({
    [field]: { $regex: regex },
    groups: group._id,
    active: true,
  }));
  if (onlyPublic)
    return {
      $or: searchQuery,
      public: true,
      type,
    };
  return {
    $or: searchQuery,
    type,
  };
};

Meteor.methods({
  'get_groups.polls_count': function getPollsAllCount({ search, slug }) {
    try {
      const group = Groups.findOne({ slug });

      let query = {};
      if (group.type !== 0 && !Roles.userIsInRole(this.userId, ['admin', 'animator', 'member'], group._id)) {
        query = queryGroupPolls({ search, group, onlyPublic: true, type: 'POLL' });
      } else query = queryGroupPolls({ search, group, onlyPublic: false, type: 'POLL' });

      return Polls.find(query).count();
    } catch (error) {
      logServer(`POLLS - PUBLICATION - ERROR - get_groups.polls_count - ${error}`, levels.ERROR, scopes.SYSTEM, {
        search,
        slug,
      });
      return 0;
    }
  },
});

function getGroupPolls({ page, search, slug, itemPerPage, type, userId, ...rest }) {
  const group = Groups.findOne({ slug });
  // for protected/private groups, publish only public polls for non member users
  let query = {};
  if (group.type !== 0 && !Roles.userIsInRole(userId, ['admin', 'animator', 'member'], group._id)) {
    query = queryGroupPolls({ search, group, onlyPublic: true, type });
  } else query = queryGroupPolls({ search, group, onlyPublic: false, type });

  const res = Polls.find(query, {
    fields: Polls.publicFields,
    skip: itemPerPage * (page - 1),
    limit: itemPerPage,
    sort: { updatedAt: -1 },
    ...rest,
  });
  return res;
}

// publish all existing polls of type poll for one group
FindFromPublication.publish('groups.polls', function groupsPolls({ page, search, slug, itemPerPage, ...rest }) {
  try {
    checkPaginationParams.validate({ page, itemPerPage, search });
  } catch (err) {
    logServer(
      `POLLS - PUBLICATION - ERROR - groups.polls - publish groups.polls : ${err}`,
      levels.ERROR,
      scopes.SYSTEM,
      {
        page,
        search,
        slug,
        itemPerPage,
      },
    );
    this.error(err);
  }
  if (!isActive(this.userId)) {
    return this.ready();
  }
  try {
    return getGroupPolls({ page, search, slug, itemPerPage, type: 'POLL', userId: this.userId, ...rest });
  } catch (error) {
    return this.ready();
  }
});

// publish all existing polls of type meeting for one group
FindFromPublication.publish('groups.meeting', function groupsMeetings({ page, search, slug, itemPerPage, ...rest }) {
  try {
    checkPaginationParams.validate({ page, itemPerPage, search });
  } catch (err) {
    logServer(
      `POLLS - PUBLICATION - ERROR - groups.mettings - publish groups.meeting : ${err}`,
      levels.ERROR,
      scopes.SYSTEM,
      {
        page,
        search,
        slug,
        itemPerPage,
      },
    );
    this.error(err);
  }
  try {
    return getGroupPolls({ page, search, slug, itemPerPage, type: 'MEETING', userId: this.userId, ...rest });
  } catch (error) {
    return this.ready();
  }
});
