/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { PublicationCollector } from 'meteor/johanbrook:publication-collector';
import { assert } from 'chai';
import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import { _ } from 'meteor/underscore';
import '../../../startup/i18n/en.i18n.json';
import { faker } from '@faker-js/faker';
import { Factory } from 'meteor/dburles:factory';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';

import Invitations from '../invitations';
import { createInvitation, deleteInvitation, sendEmail } from './methods';
import './publications';
import './factories';
import Structures from '../../structures/structures';
import '../../structures/server/factories';

describe('invitations', function () {
  describe('mutators', function () {
    it('builds correctly from factory', function () {
      const categorie = Factory.create('invitation');
      assert.typeOf(categorie, 'object');
    });
  });
  describe('publications', function () {
    let userId;
    let adminUserId;
    let adminStructureUserId;
    before(function () {
      Meteor.users.remove({});
      Structures.remove({});
      Invitations.remove({});
      Roles.createRole('admin', { unlessExists: true });
      Roles.createRole('adminStructure', { unlessExists: true });
      const structureId = Factory.create('structure')._id;
      const email = faker.internet.email();
      userId = Accounts.createUser({
        email,
        username: email,
        password: 'toto',
        structure: Random.id(),
        firstName: faker.person.firstName(),
        lastName: faker.person.lastName(),
      });
      const emailAdmin = faker.internet.email();
      adminUserId = Accounts.createUser({
        email: emailAdmin,
        username: emailAdmin,
        password: 'toto',
        structure: Random.id(),
        firstName: faker.person.firstName(),
        lastName: faker.person.lastName(),
      });
      const emailAdminStructure = faker.internet.email();
      adminStructureUserId = Accounts.createUser({
        email: emailAdminStructure,
        username: emailAdminStructure,
        password: 'toto',
        structure: structureId,
        firstName: faker.person.firstName(),
        lastName: faker.person.lastName(),
      });
      Meteor.users.update({}, { $set: { isActive: true } }, { multi: true });
      Roles.addUsersToRoles(adminUserId, 'admin');
      Roles.addUsersToRoles(adminStructureUserId, 'adminStructure', structureId);
      _.times(3, () => {
        Factory.create('invitation');
      });
      Factory.create('invitation', { structureId });
    });
    describe('invitations.admin', function () {
      it('sends all invitations to global admin user', function (done) {
        const collector = new PublicationCollector({ userId: adminUserId });
        collector.collect('invitations.admin', (collections) => {
          assert.equal(collections.invitations.length, 4);
          done();
        });
      });
      it('sends only managed invitations to adminStructure user', function (done) {
        const collector = new PublicationCollector({ userId: adminStructureUserId });
        collector.collect('invitations.admin', (collections) => {
          assert.equal(collections.invitations.length, 1);
          done();
        });
      });
      it('sends nothing to normal user', function (done) {
        const collector = new PublicationCollector({ userId });
        collector.collect('invitations.admin', (collections) => {
          assert.equal(collections.invitations, undefined);
          done();
        });
      });
    });
  });
  describe('methods', function () {
    let userId;
    let adminId;
    let adminStructureId;
    let structureId;
    let otherStructureId;
    let invitationId;
    let invitationStructureId;
    beforeEach(function () {
      // Clear
      Meteor.users.remove({});
      Structures.remove({});
      Invitations.remove({});
      // init roles and adminStructure's structure
      Roles.createRole('admin', { unlessExists: true });
      Roles.createRole('adminStructure', { unlessExists: true });
      structureId = Factory.create('structure')._id;
      otherStructureId = Factory.create('structure')._id;
      // Generate 'users'
      const email = 'user@test.fr';
      userId = Accounts.createUser({
        email,
        username: email,
        password: 'toto',
        structure: Random.id(),
        firstName: faker.person.firstName(),
        lastName: faker.person.lastName(),
      });
      const emailAdmin = 'admin@test.fr';
      adminId = Accounts.createUser({
        email: emailAdmin,
        username: emailAdmin,
        password: 'toto',
        structure: Random.id(),
        firstName: faker.person.firstName(),
        lastName: faker.person.lastName(),
      });
      const emailAdminStructure = 'adminStructure@test.fr';
      adminStructureId = Accounts.createUser({
        email: emailAdminStructure,
        username: emailAdminStructure,
        password: 'toto',
        structure: structureId,
        firstName: faker.person.firstName(),
        lastName: faker.person.lastName(),
      });
      // set this user as global admin
      Roles.addUsersToRoles(adminId, 'admin');
      Roles.addUsersToRoles(adminStructureId, 'adminStructure', structureId);
      // set users as active
      Meteor.users.update({}, { $set: { isActive: true } }, { multi: true });
      invitationId = Factory.create('invitation', { structureId: otherStructureId })._id;
      invitationStructureId = Factory.create('invitation', { structureId })._id;
    });
    describe('createInvitation', function () {
      it('does create an invitation on any structure with admin user', function () {
        const emails = 'pouet@test.fr, pouet2@test.fr';
        createInvitation._execute(
          { userId: adminId },
          { email: emails, structureId: otherStructureId, language: 'fr' },
        );
        const invitation = Invitations.findOne({ email: 'pouet@test.fr' });
        assert.typeOf(invitation, 'object');
        const invitation2 = Invitations.findOne({ email: 'pouet2@test.fr' });
        assert.typeOf(invitation2, 'object');
      });
      it('does create an invitation on managed structure only with adminStructure user', function () {
        // create an invitation on managed structure succeeds
        const emailInvitation = faker.internet.email();
        createInvitation._execute(
          { userId: adminStructureId },
          { email: emailInvitation, structureId, language: 'fr' },
        );
        const invitation = Invitations.findOne({ email: emailInvitation });
        assert.typeOf(invitation, 'object');
        // create an invitation on non managed structure should fail
        assert.throws(
          () => {
            createInvitation._execute(
              { userId: adminStructureId },
              { email: faker.internet.email(), structureId: otherStructureId, language: 'fr' },
            );
          },
          Meteor.Error,
          /api.invitations.notPermitted/,
        );
      });
      it("does not create an invitation if you're not admin", function () {
        // Throws if non admin user, or logged out user, tries to create a categorie
        assert.throws(
          () => {
            createInvitation._execute(
              { userId },
              { email: faker.internet.email(), structureId: otherStructureId, language: 'fr' },
            );
          },
          Meteor.Error,
          /api.invitations.notPermitted/,
        );
        assert.throws(
          () => {
            createInvitation._execute(
              {},
              { email: faker.internet.email(), structureId: otherStructureId, language: 'fr' },
            );
          },
          Meteor.Error,
          /api.invitations.notPermitted/,
        );
      });
      it('does not create an invitation if account already exists with same email', function () {
        assert.throws(
          () => {
            createInvitation._execute(
              { userId: adminId },
              { email: 'user@test.fr', structureId: otherStructureId, language: 'fr' },
            );
          },
          Meteor.Error,
          /api.invitations.alreadyExists/,
        );
      });
      it('does not create an invitation if invitation already exists with same email', function () {
        createInvitation._execute(
          { userId: adminId },
          { email: 'toto@test.fr', structureId: otherStructureId, language: 'fr' },
        );
        assert.throws(
          () => {
            createInvitation._execute(
              { userId: adminId },
              { email: 'toto@test.fr', structureId: otherStructureId, language: 'fr' },
            );
          },
          Meteor.Error,
          /api.invitations.alreadyExists/,
        );
      });
    });
    describe('deleteInvitation', function () {
      it('does delete an invitation on any structure with admin user', function () {
        deleteInvitation._execute({ userId: adminId }, { invitationId });
        assert.equal(Invitations.findOne(invitationId), null);
      });
      it('does delete an invitation on managed structure only with adminStructure user', function () {
        // deleting an invitation on managed structure succeeds
        deleteInvitation._execute({ userId: adminStructureId }, { invitationId: invitationStructureId });
        assert.equal(Invitations.findOne(invitationStructureId), null);
        // deleting an invitation on non managed structure should fail
        assert.throws(
          () => {
            deleteInvitation._execute({ userId: adminStructureId }, { invitationId });
          },
          Meteor.Error,
          /api.invitations.deleteInvitation.notPermitted/,
        );
      });
      it("does not delete an invitation if you're not admin", function () {
        assert.throws(
          () => {
            deleteInvitation._execute({ userId }, { invitationId });
          },
          Meteor.Error,
          /api.invitations.deleteInvitation.notPermitted/,
        );
        assert.throws(
          () => {
            deleteInvitation._execute({}, { invitationId });
          },
          Meteor.Error,
          /api.invitations.deleteInvitation.notPermitted/,
        );
      });
    });
    describe('sendEmail', function () {
      it('does send an invitation email on any structure with admin user', function () {
        sendEmail._execute({ userId: adminId }, { invitationId });
      });
      it('does send an invitation email on managed structure only with adminStructure user', function () {
        // sending an invitation email on managed structure succeeds
        sendEmail._execute({ userId: adminStructureId }, { invitationId: invitationStructureId });
        // sendinig an invitation email on non managed structure should fail
        assert.throws(
          () => {
            sendEmail._execute({ userId: adminStructureId }, { invitationId });
          },
          Meteor.Error,
          /api.invitations.sendEmail.notPermitted/,
        );
      });
      it("does not send an invitation email if you're not admin", function () {
        assert.throws(
          () => {
            sendEmail._execute({ userId }, { invitationId });
          },
          Meteor.Error,
          /api.invitations.sendEmail.notPermitted/,
        );
        assert.throws(
          () => {
            sendEmail._execute({}, { invitationId });
          },
          Meteor.Error,
          /api.invitations.sendEmail.notPermitted/,
        );
      });
    });
  });
});
