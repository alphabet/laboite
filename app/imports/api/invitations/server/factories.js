import { Factory } from 'meteor/dburles:factory';
import { Random } from 'meteor/random';
import { faker } from '@faker-js/faker';

import Invitations from '../invitations';

Factory.define('invitation', Invitations, {
  email: faker.internet.email(),
  structureId: () => Random.id(),
  language: 'fr',
  invitedAt: new Date(),
});
