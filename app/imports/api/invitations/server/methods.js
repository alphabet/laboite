import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { Email } from 'meteor/email';
import SimpleSchema from 'simpl-schema';
import i18n from 'meteor/universe:i18n';
import { _ } from 'meteor/underscore';
import Invitations from '../invitations';
import { validateString } from '../../utils';

import logServer, { levels, scopes } from '../../logging';
import { hasRightToAcceptAwaitingStructure } from '../../structures/utils';
import Structures from '../../structures/structures';
import RegEx from '../../regExp';

const validateInvitation = (email, language) => {
  if (email) validateString(email, true);
  if (language) validateString(language, true);
};

function sendInvitationEmail(email, structureId, language) {
  const { appName = 'LaBoite' } = Meteor.settings.public;
  const inviteLink = Meteor.absoluteUrl('/signin');
  const structure = Structures.findOne(structureId);
  const subjectTemplate = {
    fr: `Invitation à rejoindre ${appName}`,
    en: `Invitation to join ${appName}`,
  };
  const textTemplate = {
    fr: `Vous avez été invité à rejoindre l'application ${appName} en tant que membre de la structure ${structure.name}.
Vous pouvez créer votre compte en suivant le lien ci dessous ("se connecter", puis "enregistrement"):

    ${inviteLink}

Merci de renseigner comme adresse email ${email} afin que votre compte soit validé automatiquement`,
    en: `You have been invited to application ${appName} as member of the structure ${structure.name}.
You can register your account by following the link below ("Login", then "register"):

    ${inviteLink}

Please fill in ${email} as your email address to allow your account to be validated automatically`,
  };
  if (!Meteor.isTest) {
    const from = Meteor.settings.smtp.fromEmail;
    try {
      Email.send({ to: email, from, subject: subjectTemplate[language], text: textTemplate[language] });
    } catch (err) {
      logServer(
        `INVITATIONS - METHOD - METEOR ERROR - sendInvitationEmail - ${i18n.__(
          'pages.AdminStructureMailModal.errorSendingMail',
        )}`,
        levels.ERROR,
        scopes.ADMIN,
        { error: err },
      );
      return false;
    }
  }
  return true;
}

export const createInvitation = new ValidatedMethod({
  name: 'invitations.createInvitation',
  validate: Invitations.schema.validator({ clean: true }),
  run({ email, structureId, language, invitedAt }) {
    if (!hasRightToAcceptAwaitingStructure({ userId: this.userId, awaitingStructureId: structureId })) {
      logServer(
        `INVITATIONS - METHOD - METEOR ERROR - createInvitation - ${i18n.__('api.users.adminNeeded')}}`,
        levels.ERROR,
        scopes.ADMIN,
        { email, structureId, language },
      );
      throw new Meteor.Error('api.invitations.notPermitted', i18n.__('api.users.adminNeeded'));
    }
    validateInvitation(email, language);
    // split email string in case several emails are given
    const emails = email.split(',').map((address) => address.trim());
    // perform checks on all email addresses
    emails.forEach((address) => {
      // check that email is not already invited
      const existingInvitation = Invitations.findOne({ email: address });
      if (existingInvitation) {
        throw new Meteor.Error(
          'api.invitations.alreadyExists',
          i18n.__('api.invitations.invitationExists', { email: address }),
        );
      }
      // check if an account already exists with this email
      const existingAccount = Accounts.findUserByEmail(address);
      if (existingAccount) {
        throw new Meteor.Error(
          'api.invitations.alreadyExists',
          i18n.__('api.invitations.accountExists', { email: address }),
        );
      }
    });
    const emailErrors = [];
    emails.forEach((address) => {
      logServer(`INVITATIONS - METHOD - CREATE - createInvitation`, levels.INFO, scopes.ADMIN, {
        address,
        structureId,
        language,
        invitedBy: this.userId,
      });
      Invitations.insert({ email: address, structureId, language, invitedAt });
      if (!sendInvitationEmail(address, structureId, language)) {
        emailErrors.push(address);
      }
    });
    if (emailErrors.length > 0) {
      throw new Meteor.Error(
        'api.invitations.sendInvitationEmail.errorMail',
        i18n.__('api.invitations.errorSendingMail', { emails: emailErrors.join(', ') }),
      );
    }
  },
});

export const deleteInvitation = new ValidatedMethod({
  name: 'invitations.deleteInvitation',
  validate: new SimpleSchema({ invitationId: { type: String, regEx: RegEx.Id } }).validator(),
  run({ invitationId }) {
    const invitation = Invitations.findOne(invitationId);
    if (!invitation) {
      logServer(
        `INVITATIONS - METHOD - METEOR ERROR - deleteInvitation - ${i18n.__('api.invitations.unknownInvitation')}`,
        levels.ERROR,
        scopes.ADMIN,
        { invitationId },
      );
      throw new Meteor.Error('api.invitations.deleteInvitation.notFound', i18n.__('api.invitations.unknownInvitation'));
    }
    if (!hasRightToAcceptAwaitingStructure({ userId: this.userId, awaitingStructureId: invitation.structureId })) {
      logServer(
        `INVITATIONS - METHOD - METEOR ERROR - deleteInvitation - ${i18n.__('api.users.adminNeeded')}`,
        levels.ERROR,
        scopes.ADMIN,
        { invitationId },
      );
      throw new Meteor.Error('api.invitations.deleteInvitation.notPermitted', i18n.__('api.users.adminNeeded'));
    }
    logServer(
      `INVITATIONS - METHOD - REMOVE - deleteInvitation - invitationID: ${invitationId}`,
      levels.VERBOSE,
      scopes.ADMIN,
    );
    return Invitations.remove({ _id: invitationId });
  },
});

export const sendEmail = new ValidatedMethod({
  name: 'invitations.sendEmail',
  validate: new SimpleSchema({ invitationId: { type: String, regEx: RegEx.Id } }).validator(),
  run({ invitationId }) {
    const invitation = Invitations.findOne(invitationId);
    if (!invitation) {
      logServer(
        `INVITATIONS - METHOD - METEOR ERROR - sendEmail - ${i18n.__('api.invitations.unknownInvitation')}`,
        levels.ERROR,
        scopes.ADMIN,
        { invitationId },
      );
      throw new Meteor.Error('api.invitations.sendEmail.notFound', i18n.__('api.invitations.unknownInvitation'));
    }
    if (!hasRightToAcceptAwaitingStructure({ userId: this.userId, awaitingStructureId: invitation.structureId })) {
      logServer(
        `INVITATIONS - METHOD - METEOR ERROR - sendEmail - ${i18n.__('api.users.adminNeeded')}`,
        levels.ERROR,
        scopes.ADMIN,
        { invitationId },
      );
      throw new Meteor.Error('api.invitations.sendEmail.notPermitted', i18n.__('api.users.adminNeeded'));
    }
    if (!sendInvitationEmail(invitation.email, invitation.structureId, invitation.language)) {
      throw new Meteor.Error(
        'api.invitations.sendInvitationEmail.errorMail',
        i18n.__('pages.AdminStructureMailModal.errorSendingMail'),
      );
    }
    logServer(`INVITATIONS - METHOD - EMAIL - sendEmail - invitationID: ${invitationId}`, levels.VERBOSE, scopes.ADMIN);
    Invitations.update({ _id: invitationId }, { $set: { invitedAt: new Date() } });
  },
});

const LISTS_METHODS = _.pluck([createInvitation, deleteInvitation, sendEmail], 'name');

if (Meteor.isServer) {
  // Only allow 5 list operations per connection per second
  DDPRateLimiter.addRule(
    {
      name(name) {
        return _.contains(LISTS_METHODS, name);
      },

      // Rate limit per connection ID
      connectionId() {
        return true;
      },
    },
    5,
    1000,
  );
}
