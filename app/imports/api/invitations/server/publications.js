import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import Invitations from '../invitations';
import Structures from '../../structures/structures';
import { isActive } from '../../utils';

function adminInvitationsQuery(user) {
  // determine structures accessible to current user (admin or adminStructure)
  const structureId = user.structure;
  // for global admin, send all invitations
  if (Roles.userIsInRole(user._id, 'admin')) return {};
  // otherwise, as adminStructure, get invitations for current
  // user structure and corresponding sub-structures
  const subStructsIds = Structures.find({ ancestorsIds: structureId })
    .fetch()
    .map((subStruct) => subStruct._id);
  const structures = [structureId, ...subStructsIds];
  return { structureId: { $in: structures } };
}

// publish all users that are awaiting for a given structure
Meteor.publish('invitations.admin', function adminInvitations() {
  const user = Meteor.users.findOne(this.userId);
  const isAuthorized =
    Roles.userIsInRole(this.userId, 'admin') || Roles.userIsInRole(this.userId, 'adminStructure', { anyScope: true });
  if (!isActive(this.userId) || !isAuthorized) {
    return this.ready();
  }

  const invitationsCursor = Invitations.find(adminInvitationsQuery(user));
  const allStructs = invitationsCursor.fetch().map((invitation) => invitation.structureId);
  const structsCursor = Structures.find({ _id: { $in: allStructs } }, { name: 1 });
  return [invitationsCursor, structsCursor];
});
