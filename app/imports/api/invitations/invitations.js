import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import { getLabel } from '../utils';
import RegEx from '../regExp';

const Invitations = new Mongo.Collection('invitations');

Invitations.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

/**
 * About:
 *
 * These are used for inviting users to a structure
 *
 * Invited users will be automatically validated and their structure will be set
 * based on email at account creation
 *
 */
Invitations.schema = new SimpleSchema({
  email: {
    type: String,
    label: getLabel('api.invitations.labels.email'),
    min: 1,
  },
  structureId: {
    type: String,
    regEx: RegEx.Id,
    label: getLabel('api.invitations.labels.structureId'),
  },
  language: {
    type: String,
    defaultValue: 'fr',
    label: getLabel('api.users.labels.language'),
  },
  invitedAt: {
    type: Date,
    label: getLabel('api.invitations.labels.invitedAt'),
    defaultValue: new Date(),
  },
});

Invitations.publicFields = {
  email: 1,
  structureId: 1,
  language: 1,
  invitedAt: 1,
};
export default Invitations;
