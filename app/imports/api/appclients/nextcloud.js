import fetch from 'node-fetch';
import { Meteor } from 'meteor/meteor';
import i18n from 'meteor/universe:i18n';
import { Roles } from 'meteor/alanning:roles';
import { isActive, testUrl } from '../utils';
import logServer, { levels, scopes } from '../logging';
import Groups from '../groups/groups';

const nextcloudPlugin = Meteor.settings.public.groupPlugins.nextcloud;
const { nextcloud } = Meteor.settings;

function ocsUrl(ncURL) {
  const origin = testUrl(ncURL);
  return new URL('/ocs/v1.php/cloud', origin).href;
}

function getInstance(user) {
  return user.nclocator ? new URL(testUrl(user.nclocator)).host : '';
}

function checkResponse(response) {
  if (!response.ok) {
    throw new Error(`status: ${response.status}, message: ${response.statusText}, url: ${response.url}`);
  }
}

function logError(error, action, callerId) {
  logServer(`APPCLIENT - NEXTCLOUD - ApiError - ${action}`, levels.ERROR, scopes.SYSTEM, {
    error: error.reason || error.message,
    callerId,
  });
}

function getShareName(name) {
  return `groupe-${name}`;
}

export default class NextcloudClient {
  constructor() {
    this.ncURL = (nextcloudPlugin && nextcloudPlugin.URL) || '';
    const ncUser = (nextcloud && nextcloud.nextcloudUser) || '';
    const ncPassword = (nextcloud && nextcloud.nextcloudPassword) || '';
    this.circlesUser = (nextcloud && nextcloud.circlesUser) || '';
    const circlesPassword = (nextcloud && nextcloud.circlesPassword) || '';
    this.appsURL = testUrl(`${this.ncURL}/ocs/v2.php/apps`);
    this.baseHeaders = {
      Accept: 'application/json',
      'OCS-APIRequest': true,
    };
    // headers for user management
    this.basicAuth = Buffer.from(`${ncUser}:${ncPassword}`, 'binary').toString('base64');
    this.ocsHeaders = {
      ...this.baseHeaders,
      Authorization: `Basic ${this.basicAuth}`,
    };
    this.ocsPostHeaders = {
      ...this.ocsHeaders,
      'Content-Type': 'application/json',
    };
    // headers for circles and shares management
    this.circlesAuth = Buffer.from(`${this.circlesUser}:${circlesPassword}`, 'binary').toString('base64');
    this.circlesHeaders = {
      ...this.baseHeaders,
      Authorization: `Basic ${this.circlesAuth}`,
    };
    this.circlesPostHeaders = {
      ...this.circlesHeaders,
      'Content-Type': 'application/json',
    };
  }

  async userExists(username, ncURL) {
    const params = new URLSearchParams({ search: username });
    const response = await fetch(`${ocsUrl(ncURL)}/users?${params.toString()}`, {
      method: 'GET',
      headers: this.ocsHeaders,
    });
    if (response.ok) {
      const respJson = await response.json();
      return respJson.ocs.data.users.includes(username);
    }
    logServer(
      `APPCLIENT - NEXTCLOUD - userExists - ${i18n.__('api.nextcloud.apiError', { groupName: username })}`,
      levels.ERROR,
      scopes.SYSTEM,
      {
        groupName: username,
        ncURL,
        error: `status: ${response.status}, message: ${response.statusText}, url: ${response.url}`,
      },
    );
    return false;
  }

  async _checkConfig() {
    // checks that 'Circles' API is responding
    const response = await fetch(`${this.appsURL}/circles/circles`, {
      method: 'GET',
      headers: this.circlesHeaders,
    });
    checkResponse(response);
    const data = await response.json();
    if (data === undefined || data.ocs === undefined) {
      logServer(
        `APPCLIENT - NEXTCLOUD - checkFolderActive - ${i18n.__('api.nextcloud.circlesInactive')}`,
        levels.ERROR,
        scopes.SYSTEM,
        {},
      );
      return false;
    }
    logServer(
      `APPCLIENT - NEXTCLOUD - checkConfig - ${i18n.__('api.nextcloud.configOk')}`,
      levels.INFO,
      scopes.SYSTEM,
      {},
    );
    return true;
  }

  async checkConfig() {
    logServer(
      `APPCLIENT - NEXTCLOUD - checkConfig - ${i18n.__('api.nextcloud.checkConfig', { URL: this.appsURL })}`,
      levels.INFO,
      scopes.SYSTEM,
      {
        URL: this.appsURL,
      },
    );
    return this._checkConfig().catch((error) => {
      logServer(
        `APPCLIENT - NEXTCLOUD - checkConfig - ${i18n.__('api.nextcloud.badConfig')}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          error: error.reason || error.message,
        },
      );
      return false;
    });
  }

  async _configureCircle(circleId, callerId) {
    const params = { value: 40960 };
    const response = await fetch(`${this.appsURL}/circles/circles/${circleId}/config`, {
      method: 'PUT',
      headers: this.circlesPostHeaders,
      body: JSON.stringify(params),
    });
    checkResponse(response);
    const respJson = await response.json();
    if (respJson.ocs.meta.status !== 'ok') {
      logServer(
        `APPCLIENT - NEXTCLOUD - _configureCircle - ${i18n.__('api.nextcloud.configureCircleError')}`,
        levels.ERROR,
        scopes.SYSTEM,
        { circleId },
        callerId,
      );
    }
    return circleId;
  }

  async _ensureCircle(group, callerId) {
    const groupId = group._id;
    const shareName = getShareName(group.shareName);
    if (!group.circleId) {
      const params = { name: shareName, personal: false, local: false };
      const response = await fetch(`${this.appsURL}/circles/circles`, {
        method: 'POST',
        headers: this.circlesPostHeaders,
        body: JSON.stringify(params),
      });
      checkResponse(response);
      const respJson = await response.json();
      const infos = respJson.ocs.meta;
      if (infos.status === 'ok') {
        const circleId = respJson.ocs.data.id;
        logServer(
          `APPCLIENT - NEXTCLOUD - ensureCircle - ${i18n.__('api.nextcloud.circleAdded')}`,
          levels.INFO,
          scopes.SYSTEM,
          {
            name: shareName,
            groupId,
            circleId,
          },
        );
        // store circleId in group
        Groups.update({ _id: groupId }, { $set: { circleId } });
        // configure circle as a federated circle
        return this._configureCircle(circleId, callerId);
      }
      logServer(
        `APPCLIENT - NEXTCLOUD - ensureCircle - ${i18n.__('api.nextcloud.circleAddError', { groupId })}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          name: shareName,
          error: infos.message,
          callerId,
        },
      );
      return null;
    }
    return group.circleId;
  }

  async ensureCircle(group, callerId) {
    return this._ensureCircle(group, callerId).catch((error) => {
      logServer(
        `APPCLIENT - NEXTCLOUD - ensureCircle - ${i18n.__('api.nextcloud.circleAddError', { groupId: group._id })}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          name: getShareName(group.shareName),
          error: error.reason || error.message,
          callerId,
        },
      );
      return null;
    });
  }

  async _ensureShare(group, circleId, callerId) {
    // ensures that a group share exists and is associated to group circle
    const groupId = group._id;
    if (!group.shareId) {
      const shareName = getShareName(group.shareName);
      const resp = await fetch(`${this.ncURL}/remote.php/dav/files/${this.circlesUser}/${shareName}`, {
        method: 'MKCOL',
        headers: this.circlesHeaders,
      });
      if (resp.statusText !== 'Created') {
        logServer(
          `APPCLIENT - NEXTCLOUD - ensureShare - ${i18n.__('api.nextcloud.shareCreateError', { groupId })}`,
          levels.ERROR,
          scopes.SYSTEM,
          {
            name: shareName,
            error: `status: ${resp.status}, message: ${resp.statusText}, url: ${resp.url}`,
            callerId,
          },
        );
      }
      const params = { password: null, path: shareName, permissions: 31, shareType: 7, shareWith: circleId };
      const response = await fetch(`${this.appsURL}/files_sharing/api/v1/shares`, {
        method: 'POST',
        headers: this.circlesPostHeaders,
        body: JSON.stringify(params),
      });
      checkResponse(response);
      const respJson = await response.json();
      if (respJson.ocs.meta.status === 'ok') {
        const shareId = respJson.ocs.data.id;
        logServer(
          `APPCLIENT - NEXTCLOUD - ensureShare - ${i18n.__('api.nextcloud.shareAdded', { groupId })}`,
          levels.INFO,
          scopes.SYSTEM,
          {
            path: shareName,
            groupId,
            circleId,
          },
        );
        // store shareId in group
        Groups.update({ _id: groupId }, { $set: { shareId } });
        return shareId;
      }
      logServer(
        `APPCLIENT - NEXTCLOUD - ensureShare - ${i18n.__('api.nextcloud.shareAddError', { groupId })}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          error: respJson.ocs.meta.message,
          callerId,
        },
      );
      return null;
    }
    return group.shareId;
  }

  async ensureShare(group, circleId, callerId) {
    return this._ensureShare(group, circleId, callerId).catch((error) => {
      logServer(
        `APPCLIENT - NEXTCLOUD - ensureShare - ${i18n.__('api.nextcloud.shareAddError', { groupId: group._id })}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          error: error.reason || error.message,
          callerId,
        },
      );
      return null;
    });
  }

  async _deleteCircle(group, callerId) {
    Groups.update({ _id: group._id }, { $unset: { circleId: 1 } });
    const response = await fetch(`${this.appsURL}/circles/circles/${group.circleId}`, {
      method: 'DELETE',
      headers: this.circlesHeaders,
    });
    checkResponse(response);
    const respJson = await response.json();
    if (respJson.ocs.meta.status === 'ok') {
      logServer(
        `APPCLIENT - NEXTCLOUD - deleteCircle - ${i18n.__('api.nextcloud.circleDeleted')}`,
        levels.INFO,
        scopes.SYSTEM,
        {
          groupId: group._id,
          circleId: group.circleId,
        },
      );
    } else {
      logServer(
        `APPCLIENT - NEXTCLOUD - deleteCircle - ${i18n.__('api.nextcloud.circleDeleteError')}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          circleId: group.circleId,
          error: respJson.ocs.meta.message,
          callerId,
        },
      );
    }
  }

  async deleteCircle(group, callerId) {
    if (group.circleId) {
      await this._deleteCircle(group, callerId).catch((error) => {
        logServer(
          `APPCLIENT - NEXTCLOUD - deleteCircle - ${i18n.__('api.nextcloud.circleDeleteError')}`,
          levels.ERROR,
          scopes.SYSTEM,
          {
            circleId: group.circleId,
            error: error.reason || error.message,
            callerId,
          },
        );
      });
    }
  }

  async _deleteFolder(group, shareName) {
    Groups.update({ _id: group._id }, { $unset: { shareId: 1 } });
    const response = await fetch(`${this.ncURL}/remote.php/dav/files/${this.circlesUser}/${shareName}`, {
      method: 'DELETE',
      headers: this.circlesHeaders,
    });
    checkResponse(response);
    logServer(
      `APPCLIENT - NEXTCLOUD - deleteFolder - ${i18n.__('api.nextcloud.folderDeleted')}`,
      levels.INFO,
      scopes.SYSTEM,
      {
        path: shareName,
      },
    );
  }

  async deleteFolder(group, callerId) {
    const shareName = getShareName(group.shareName);
    await this._deleteFolder(group, shareName).catch((error) => {
      logServer(
        `APPCLIENT - NEXTCLOUD - deleteFolder - ${i18n.__('api.nextcloud.folderDeleteError')}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          path: shareName,
          error: error.reason || error.message,
          callerId,
        },
      );
    });
  }

  async _inviteUser(user, group, callerId) {
    const { circleId } = group;
    if (user.nclocator) {
      const params = { type: 1, userId: `${user.username}@${getInstance(user)}` };
      const response = await fetch(`${this.appsURL}/circles/circles/${circleId}/members`, {
        method: 'POST',
        body: JSON.stringify(params),
        headers: this.circlesPostHeaders,
      });
      checkResponse(response);
      const respJson = await response.json();
      if (respJson.ocs.meta.status === 'ok') {
        logServer(
          `APPCLIENT - NEXTCLOUD - inviteUser - ${i18n.__('api.nextcloud.userInvited')}`,
          levels.INFO,
          scopes.SYSTEM,
          {
            username: user.username,
            circleId,
          },
        );
      } else {
        logServer(
          `APPCLIENT - NEXTCLOUD - inviteUser - ${i18n.__('api.nextcloud.userInviteError')}`,
          levels.ERROR,
          scopes.SYSTEM,
          {
            username: user.username,
            circleId: group.circleId,
            error: respJson.ocs.meta.message,
            callerId,
          },
        );
      }
    }
  }

  async inviteUser(userId, group, callerId) {
    const user = Meteor.users.findOne(userId);
    await this._inviteUser(user, group, callerId).catch((error) => {
      logServer(
        `APPCLIENT - NEXTCLOUD - inviteUser - ${i18n.__('api.nextcloud.userInviteError')}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          user: `${user.username}@${getInstance(user)}`,
          error: error.reason || error.message,
          callerId,
        },
      );
    });
  }

  async _inviteMembers(group, circleId, callerId) {
    const userIds = [...group.members, ...group.animators];
    const allUsers = Meteor.users.find({ _id: { $in: userIds } }, { fields: { username: 1, nclocator: 1 } }).fetch();
    const params = {
      // invite all group members except those who don't have an nclocator
      members: allUsers
        .map((user) => {
          return user.nclocator ? { type: 1, id: `${user.username}@${getInstance(user)}` } : null;
        })
        .filter((entry) => entry !== null),
    };
    const response = await fetch(`${this.appsURL}/circles/circles/${circleId}/members/multi`, {
      method: 'POST',
      body: JSON.stringify(params),
      headers: this.circlesPostHeaders,
    });
    checkResponse(response);
    const respJson = await response.json();
    if (respJson.ocs.meta.status !== 'ok') {
      logServer(
        `APPCLIENT - NEXTCLOUD - inviteMembers - ${i18n.__('api.nextcloud.userInviteError')}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          groupId: group._id,
          error: respJson.ocs.meta.message,
          callerId,
        },
      );
    }
  }

  async inviteMembers(group, circleId, callerId) {
    await this._inviteMembers(group, circleId, callerId).catch((error) => {
      logServer(
        `APPCLIENT - NEXTCLOUD - inviteMembers - ${i18n.__('api.nextcloud.userInviteError')}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          groupId: group._id,
          error: error.reason || error.message,
          callerId,
        },
      );
    });
  }

  async _kickUser(user, group, callerId) {
    const { circleId } = group;
    const response = await fetch(`${this.appsURL}/circles/circles/${circleId}/members`, {
      method: 'GET',
      headers: this.circlesHeaders,
    });
    checkResponse(response);
    const respJson = await response.json();
    if (respJson.ocs.meta.status !== 'ok') {
      logServer(
        `APPCLIENT - NEXTCLOUD - kickUser/getMembers - ${i18n.__('api.nextcloud.userKickError')}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          user: user.username,
          error: respJson.ocs.meta.message,
          callerId,
        },
      );
    } else {
      // find user memberId in circle
      const member = respJson.ocs.data.find((item) => item.userId === user.username);
      if (member !== undefined) {
        // remove member from circle
        const respDelete = await fetch(`${this.appsURL}/circles/circles/${circleId}/members/${member.id}`, {
          method: 'DELETE',
          headers: this.circlesHeaders,
        });
        checkResponse(respDelete);
        const respDelJson = await respDelete.json();
        if (respDelJson.ocs.meta.status !== 'ok') {
          logServer(
            `APPCLIENT - NEXTCLOUD - kickUser - ${i18n.__('api.nextcloud.userKickError')}`,
            levels.ERROR,
            scopes.SYSTEM,
            {
              user: user.username,
              error: respDelJson.ocs.meta.message,
              callerId,
            },
          );
        } else {
          logServer(
            `APPCLIENT - NEXTCLOUD - kickUser - ${i18n.__('api.nextcloud.userKicked')}`,
            levels.INFO,
            scopes.SYSTEM,
            {
              username: user.username,
              circleId,
            },
          );
        }
      } else {
        logServer(
          `APPCLIENT - NEXTCLOUD - kickUser - ${i18n.__('api.nextcloud.notMember')}`,
          levels.ERROR,
          scopes.SYSTEM,
          {
            username: user.username,
            circleId,
            callerId,
          },
        );
      }
    }
  }

  async kickUser(userId, group, callerId) {
    const user = Meteor.users.findOne(userId);
    await this._kickUser(user, group, callerId).catch((error) => {
      logServer(
        `APPCLIENT - NEXTCLOUD - kickUser - ${i18n.__('api.nextcloud.userKickError')}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          user: user.username,
          error: error.reason || error.message,
          callerId,
        },
      );
    });
  }

  async addUser(userId, callerId) {
    const user = Meteor.users.findOne(userId);
    if (!user.nclocator) {
      logServer(
        `APPCLIENT - NEXTCLOUD - ERROR - addUser - ${i18n.__('api.nextcloud.misingNCLocator')}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          userId,
          callerId,
        },
      );
      return false;
    }
    const resExists = await this.userExists(user.username, user.nclocator);
    if (resExists === false) {
      const nextcloudDisplayNameLength = Meteor.settings.private?.nextcloudDisplayNameLength || 64;
      // don't concatenate firstName and lastName if they are the same (probably for functional accounts).
      const displayName =
        user.displayName || (user.firstName === user.lastName ? user.firstName : `${user.firstName} ${user.lastName}`);
      const ncData = {
        userid: user.username,
        password: '',
        email: user.emails ? user.emails[0].address : '',
        // displayname is limited to 64 characters in Nextcloud.
        displayName: displayName.slice(0, nextcloudDisplayNameLength),
        language: user.language,
      };
      return this._addUser(ncData, user.nclocator, callerId).catch((error) => {
        logServer(
          `APPCLIENT - NEXTCLOUD - ERROR - addUser - ${i18n.__('api.nextcloud.userAddError', { userId })}`,
          levels.ERROR,
          scopes.SYSTEM,
          {
            userData: ncData,
            userId,
            ncURL: testUrl(user.nclocator),
            error: error.reason || error.message,
            callerId,
          },
        );
        return false;
      });
    }
    logServer(
      `APPCLIENT - NEXTCLOUD - addUser - ${i18n.__('api.nextcloud.userAlreadyExists', { username: user.username })}`,
      levels.VERBOSE,
      scopes.SYSTEM,
      {
        username: user.username,
        ncURL: testUrl(user.nclocator),
      },
    );
    return true;
  }

  async _addUser(userData, ncURL, callerId) {
    const userId = userData.userid;
    const response = await fetch(`${ocsUrl(ncURL)}/users`, {
      method: 'POST',
      body: JSON.stringify(userData),
      headers: this.ocsPostHeaders,
    });
    checkResponse(response);
    const respJson = await response.json();
    const infos = respJson.ocs.meta;
    if (infos.status === 'ok') {
      logServer(
        `APPCLIENT - NEXTCLOUD - _addUser - ${i18n.__('api.nextcloud.userAdded', { userId, ncURL })}`,
        levels.INFO,
        scopes.SYSTEM,
        {
          userData,
          userId,
          ncURL,
        },
      );
      return true;
    }
    logServer(
      `APPCLIENT - NEXTCLOUD - _addUser - ${i18n.__('api.nextcloud.userAddError', { userId })}`,
      levels.ERROR,
      scopes.SYSTEM,
      {
        userData,
        userId,
        ncURL,
        callerId,
      },
    );
    return false;
  }

  // updates user account info in Nextcloud through OCS API
  // key is the name of the field to update, value is its new value
  async _updateUser(user, key, value, callerId) {
    const userId = user._id;
    const ncURL = user.nclocator;
    const response = await fetch(`${ocsUrl(ncURL)}/users/${user.username}`, {
      method: 'PUT',
      body: JSON.stringify({ key, value }),
      headers: this.ocsPostHeaders,
    });
    checkResponse(response);
    const respJson = await response.json();
    const infos = respJson.ocs.meta;
    if (infos.status === 'ok') {
      logServer(
        `APPCLIENT - NEXTCLOUD - _updateUser - ${i18n.__('api.nextcloud.userUpdated')}`,
        levels.INFO,
        scopes.SYSTEM,
        {
          userId,
          key,
          value,
        },
      );
      return true;
    }
    logServer(
      `APPCLIENT - NEXTCLOUD - _updateUser - ${i18n.__('api.nextcloud.updateUserError')}`,
      levels.ERROR,
      scopes.SYSTEM,
      {
        userId,
        key,
        value,
        callerId,
      },
    );
    return false;
  }

  async updateUser(userId, key, value, callerId) {
    const user = Meteor.users.findOne(userId);
    if (!user) {
      logServer(
        `APPCLIENT - NEXTCLOUD - ERROR - updateUser - ${i18n.__('api.users.unknownUser')}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          userId,
          key,
          value,
          callerId,
        },
      );
      return false;
    }
    if (!user.isActive) {
      // inactive users are not created in nextcloud, ignore data update
      logServer(
        `APPCLIENT - NEXTCLOUD - updateUser - ${i18n.__('api.nextcloud.userNotActive')}`,
        levels.VERBOSE,
        scopes.SYSTEM,
        {
          username: user.username,
          key,
          value,
        },
      );
      return true;
    }
    const resExists = await this.userExists(user.username, user.nclocator);
    if (resExists === false) {
      // user has not yet been created in nextcloud, his displayName will be set at creation
      return false;
    }
    return this._updateUser(user, key, value, callerId).catch((error) => {
      logServer(
        `APPCLIENT - NEXTCLOUD - ERROR - updateUser - ${i18n.__('api.nextcloud.updateUserError', {
          username: user.username,
        })}`,
        levels.ERROR,
        scopes.SYSTEM,
        {
          userId,
          key,
          value,
          error: error.reason || error.message,
          callerId,
        },
      );
      return false;
    });
  }
}

if (Meteor.isServer && nextcloudPlugin && nextcloudPlugin.enable) {
  const nextClient = new NextcloudClient();
  // check that api is accessible and circles application is active
  nextClient.checkConfig();

  Meteor.afterMethod('groups.groupCreated', async function nextCreateGroup({ plugins }) {
    if (!this.error) {
      if (plugins.nextcloud === true) {
        const groupId = this.result;
        const group = Groups.findOne({ _id: groupId });
        // create nextcloud circle and share, invite group members/animators to circle
        let circleId;
        try {
          circleId = await nextClient.ensureCircle(group, this.userId);
        } catch (error) {
          logError(error, 'createGroup.ensureCircle', this.userId);
        }
        if (circleId) {
          try {
            await nextClient.inviteMembers(group, circleId, this.userId);
          } catch (error) {
            logError(error, 'createGroup.inviteMembers', this.userId);
          }
          try {
            await nextClient.ensureShare(group, circleId, this.userId);
          } catch (error) {
            logError(error, 'createGroup.ensureShare', this.userId);
          }
        }
      }
    }
  });

  Meteor.afterMethod('groups.groupRemoved', async function nextRemoveGroup({ groupId }) {
    if (!this.error) {
      const groupData = this.result;
      const isAdmin = isActive(this.userId) && Roles.userIsInRole(this.userId, 'admin', groupId);
      if (isAdmin || this.userId === groupData.owner) {
        if (groupData.plugins.nextcloud === true) {
          // remove circle and share folder group from nextcloud
          try {
            await nextClient.deleteCircle(groupData, this.userId);
          } catch (error) {
            logError(error, 'removeGroup.deleteCircle', this.userId);
          }
          try {
            await nextClient.deleteFolder(groupData, this.userId);
          } catch (error) {
            logError(error, 'removeGroup.deleteFolder', this.userId);
          }
        }
      }
    }
  });

  Meteor.afterMethod('groups.updateGroup', async function nextUpdateGroup({ groupId }) {
    if (!this.error) {
      const group = Groups.findOne({ _id: groupId });
      if (group.plugins.nextcloud === true && !group.circleId) {
        // create nextcloud circle and share if needed
        let circleId;
        try {
          circleId = await nextClient.ensureCircle(group, this.userId);
        } catch (error) {
          logError(error, 'updateGroup.ensureCircle', this.userId);
        }
        if (circleId) {
          // add all group members to circle
          try {
            await nextClient.inviteMembers(group, circleId, this.userId);
          } catch (error) {
            logError(error, 'updateGroup.inviteMembers', this.userId);
          }
          try {
            await nextClient.ensureShare(group, circleId, this.userId);
          } catch (error) {
            logError(error, 'updateGroup.ensureShare', this.userId);
          }
        }
      } else if (group.plugins.nextcloud === false && group.circleId) {
        // remove circle and share from nextcloud
        try {
          await nextClient.deleteCircle(group, this.userId);
        } catch (error) {
          logError(error, 'updateGroup.deleteCircle', this.userId);
        }
        try {
          await nextClient.deleteFolder(group, this.userId);
        } catch (error) {
          logError(error, 'updateGroup.deleteFolder', this.userId);
        }
      }
    }
  });

  Meteor.afterMethod('users.memberOfSet', async function nextSetMember({ userId, groupId }) {
    if (!this.error) {
      // invite user to group circle if needed
      const group = Groups.findOne({ _id: groupId });
      if (group.plugins.nextcloud === true) {
        try {
          await nextClient.inviteUser(userId, group, this.userId);
        } catch (error) {
          logError(error, 'setMemberOf.inviteUser', this.userId);
        }
      }
    }
  });

  Meteor.afterMethod('users.unsetMemberOf', async function nextUnsetMember({ userId, groupId }) {
    if (!this.error) {
      // remove user from circle if needed
      const group = Groups.findOne({ _id: groupId });
      if (group.plugins.nextcloud === true) {
        if (!group.animators.includes(userId)) {
          try {
            await nextClient.kickUser(userId, group, this.userId);
          } catch (error) {
            logError(error, 'unsetMemberOf.kickUser', this.userId);
          }
        }
      }
    }
  });

  Meteor.afterMethod('users.animatorOfSet', async function nextSetAnimator({ userId, groupId }) {
    if (!this.error) {
      // invite user to group circle if needed
      const group = Groups.findOne({ _id: groupId });
      if (group.plugins.nextcloud === true) {
        try {
          await nextClient.inviteUser(userId, group, this.userId);
        } catch (error) {
          logError(error, 'setAnimatorOf.inviteUser', this.userId);
        }
      }
    }
  });

  Meteor.afterMethod('users.unsetAnimatorOf', async function nextUnsetAnimator({ userId, groupId }) {
    if (!this.error) {
      // invite user from circle if needed
      const group = Groups.findOne({ _id: groupId });
      if (group.plugins.nextcloud === true) {
        if (!group.members.includes(userId)) {
          try {
            await nextClient.kickUser(userId, group, this.userId);
          } catch (error) {
            logError(error, 'unsetAnimatorOf.kickUser', this.userId);
          }
        }
      }
    }
  });

  Meteor.afterMethod('users.setActive', async function nextAddUser({ userId }) {
    if (!this.error) {
      // create nextcloud user if needed
      // get nclocator for this user
      try {
        await nextClient.addUser(userId, this.userId);
      } catch (error) {
        logError(error, 'setActive.addUser', this.userId);
      }
    }
  });

  Meteor.afterMethod('users.userUpdated', async function nextUserUpdated(params) {
    if (!this.error) {
      const { userId, data } = params;
      if (data.isActive && data.isActive === true) {
        try {
          await nextClient.addUser(userId, this.userId);
        } catch (error) {
          logError(error, 'userUpdated.addUser', this.userId);
        }
      }
      if (data.email) {
        // email has changed, update user data in Nextcloud
        try {
          nextClient.updateUser(userId, 'email', data.email, this.userId);
        } catch (error) {
          logError(error, 'userUpdated.updateEmail', this.userId);
        }
      }
    }
  });

  Meteor.afterMethod('users.displayNameSet', async function nextDisplayNameSet(params) {
    if (!this.error) {
      const { userId, displayName } = params;
      try {
        nextClient.updateUser(userId, 'displayname', displayName, this.userId);
      } catch (error) {
        logError(error, 'displayNameSet.updateDisplayName', this.userId);
      }
    }
  });
}
