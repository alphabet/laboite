import { Mongo } from 'meteor/mongo';

const PollsAnswers = new Mongo.Collection('polls_answers');

// Deny all client-side updates since we will be using methods to manage this collection
PollsAnswers.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

PollsAnswers.publicFields = {
  email: 1,
  pollId: 1,
  choices: 1,
  confirmed: 1,
  meetingSlot: 1,
  name: 1,
  userId: 1,
  createdAt: 1,
  updatedAt: 1,
};

// PollsAnswers.attachSchema(PollsAnswers.schema);

export default PollsAnswers;
