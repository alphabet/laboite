import { Migrations } from 'meteor/percolate:migrations';
import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { zonedTimeToUtc } from 'date-fns-tz';
import Articles from './articles/articles';
import Services from './services/services';
import Groups from './groups/groups';
import Structures, { defaultIntroduction } from './structures/structures';
import Tags from './tags/tags';
import logServer, { levels, scopes } from './logging';
import AppSettings from './appsettings/appsettings';
import { addItem } from './personalspaces/methods';
import PersonalSpaces from './personalspaces/personalspaces';
import EventsAgenda from './eventsAgenda/eventsAgenda';
import GlobalInfos from './globalinfos/globalInfo';
import Polls from './polls/polls';
import PollsAnswers from './polls_answers/polls_answers';
import { generatePathOfStructure } from './structures/server/methods';

Migrations.add({
  version: 1,
  name: 'Add state field to services',
  up: () => {
    Services.update({ state: null }, { $set: { state: 0 } }, { multi: true });
  },
  down: () => {
    Services.rawCollection().updateMany({}, { $unset: { state: true } });
  },
});

Migrations.add({
  version: 2,
  name: 'Add articles count and last publication date to users',
  up: () => {
    let updateInfos = {};
    Meteor.users
      .find()
      .fetch()
      .forEach((user) => {
        updateInfos = {
          articlesCount: Articles.find({ userId: user._id }).count(),
        };
        if (updateInfos.articlesCount > 0) {
          updateInfos.lastArticle = Articles.findOne({ userId: user._id }, { $sort: { updateAt: -1 } }).updatedAt;
        }
        Meteor.users.update({ _id: user._id }, { $set: updateInfos });
      });
  },
  down: () => {
    Meteor.users.rawCollection().updateMany({}, { $unset: { articlesCount: true, lastArticle: true } });
  },
});

Migrations.add({
  version: 3,
  name: 'Add candidates count to groups',
  up: () => {
    Groups.find()
      .fetch()
      .forEach((group) => {
        const numCandidates = group.candidates.length;
        Groups.update({ _id: group._id }, { $set: { numCandidates } });
      });
  },
  down: () => {
    Groups.rawCollection().updateMany({}, { $unset: { numCandidates: true } });
  },
});

Migrations.add({
  version: 4,
  name: 'Add visit count to articles',
  up: () => {
    Articles.update({}, { $set: { visits: 0 } }, { multi: true });
  },
  down: () => {
    Articles.rawCollection().updateMany({}, { $unset: { visits: true } });
  },
});

Migrations.add({
  version: 5,
  name: 'Add nextcloud setting to groups',
  up: () => {
    if (Groups.schema._schemaKeys.includes('nextcloud')) {
      Groups.update({}, { $set: { nextcloud: false } }, { multi: true });
    }
  },
  down: () => {
    Groups.rawCollection().updateMany({}, { $unset: { nextcloud: true } });
  },
});

Migrations.add({
  version: 6,
  name: 'Add plugins setting to groups and remove nextcloud',
  up: () => {
    Groups.update({}, { $set: { plugins: {} } }, { multi: true });
    Groups.update(
      { nextcloud: true },
      { $set: { plugins: { nextcloud: { enable: true } } }, $unset: { nextcloud: true } },
      { multi: true },
    );
  },
  down: () => {
    Groups.rawCollection().updateMany({ plugins: { nextcloud: { enable: true } } }, { $set: { nextcloud: true } });
    Groups.rawCollection().updateMany({}, { $unset: { plugins: true } });
  },
});

Migrations.add({
  version: 7,
  name: 'Add tags list to articles',
  up: () => {
    Articles.update({}, { $set: { tags: [] } }, { multi: true });
  },
  down: () => {
    Articles.rawCollection().updateMany({}, { $unset: { tags: true } });
  },
});

Migrations.add({
  version: 8,
  name: 'No update here (kept for compatibility)',
  up: () => {
    // nothing to do here, wrong code previsouly added by mistake
    // Articles.update({}, { $set: { tags: [] } }, { multi: true });
  },
  down: () => {
    // nothing to do here, wrong code previsouly added by mistake
    // Articles.rawCollection().updateMany({}, { $unset: { tags: true } });
  },
});

Migrations.add({
  version: 9,
  name: 'Add author structure to articles',
  up: () => {
    Articles.find({})
      .fetch()
      .forEach((article) => {
        const updateData = {};
        // set article structure when possible
        if (article.structure === undefined || article.structure === '') {
          const author = Meteor.users.findOne({ _id: article.userId }, { fields: { structure: 1 } });
          if (author) {
            updateData.structure = author.structure || '';
          } else {
            logServer(
              `MIGRATIONS - API - ERROR - Add author structure to articles -
              Migration: could not find author ${article.userId} for article ${article._id}`,
              levels.WARN,
              scopes.SYSTEM,
            );
          }
        }
        // store tag name in articles instead of _id
        const newTags = [];
        if (article.tags) {
          article.tags.forEach((tagId) => {
            const tag = Tags.findOne(tagId);
            if (tag && !newTags.includes(tag.name.toLowerCase())) {
              // add and force tag to lower case
              newTags.push(tag.name.toLowerCase());
            }
          });
          updateData.tags = newTags;
        }
        if (Object.keys(updateData).length > 0) {
          Articles.update({ _id: article._id }, { $set: updateData });
        }
      });
    // update Tags collection to be lowercase only
    Tags.find({})
      .fetch()
      .forEach((tag) => {
        const tagName = tag.name.toLowerCase();
        if (tag.name !== tagName) {
          if (Tags.findOne({ name: tagName })) {
            // tag names are unique, remove if lowercase version exists
            Tags.remove({ _id: tag._id });
          } else {
            // otherwise, update tag
            Tags.update({ _id: tag._id }, { $set: { name: tagName } });
          }
        }
      });
  },
  down: () => {
    Articles.rawCollection().updateMany({}, { $unset: { structure: true } });
    Articles.find({})
      .fetch()
      .forEach((article) => {
        // store back tag _id (unknown tags are removed to prevent schema check errors)
        const newTags = [];
        if (article.tags) {
          article.tags.forEach((tagName) => {
            const tag = Tags.findOne({ name: tagName });
            if (tag) newTags.push(tag._id);
          });
        }
        Articles.update({ _id: article._id }, { $set: { tags: newTags } });
      });
  },
});

Migrations.add({
  version: 10,
  name: 'Add articles boolean to groups with articles',
  up: () => {
    const articles = Articles.find({ groups: { $exists: true } }).fetch();
    articles.forEach(({ groups }) => {
      groups.forEach(({ _id }) => {
        Groups.update({ _id }, { $set: { articles: true } });
      });
    });
  },
  down: () => {
    Groups.rawCollection().updateMany({}, { $unset: { articles: true } });
  },
});

Migrations.add({
  version: 11,
  name: 'Add structure to services',
  up: () => {
    Services.update({}, { $set: { structure: '' } }, { multi: true });
  },
  down: () => {
    Services.rawCollection().updateMany({}, { $unset: { structure: true } });
  },
});

Migrations.add({
  version: 12,
  name: 'Update group count and quota on users',
  up: () => {
    let updateInfos = {};
    Meteor.users
      .find()
      .fetch()
      .forEach((user) => {
        updateInfos = {
          groupCount: Groups.find({ owner: user._id }).count(),
        };
        if (user.groupQuota === undefined) {
          updateInfos.groupQuota = Meteor.users.schema._schema.groupQuota.defaultValue;
        }
        Meteor.users.update({ _id: user._id }, { $set: updateInfos });
      });
  },
  down: () => {
    Meteor.users.rawCollection().updateMany({}, { $unset: { groupQuota: true, groupCount: true } });
  },
});

Migrations.add({
  version: 13,
  name: 'Update articles boolean on groups if not set in step 10',
  up: () => {
    Groups.update({ articles: null }, { $set: { articles: false } }, { multi: true });
  },
  down: () => {
    // no rollback on this step
  },
});

Migrations.add({
  version: 14,
  name: 'Add nclocator url to users',
  up: () => {
    Meteor.users
      .find()
      .fetch()
      .forEach((user) => {
        Meteor.users.rawCollection().updateOne({ _id: user._id }, { $set: { ncloud: '' } });
      });
  },
  down: () => {
    Meteor.users.rawCollection().updateMany({}, { $unset: { ncloud: true } });
  },
});

Migrations.add({
  version: 15,
  name: 'Add advancedPersonalPage to users',
  up: () => {
    Meteor.users
      .find()
      .fetch()
      .forEach((user) => {
        Meteor.users.update({ _id: user._id }, { $set: { advancedPersonalPage: false } });
      });
  },
  down: () => {
    Meteor.users.rawCollection().updateMany({}, { $unset: { advancedPersonalPage: true } });
  },
});

Migrations.add({
  version: 16,
  name: 'Add favUserBookmarks to users',
  up: () => {
    Meteor.users.update({}, { $set: { favUserBookmarks: [] } }, { multi: true });
  },
  down: () => {
    Meteor.users.rawCollection().updateMany({}, { $unset: { favUserBookmarks: true } });
  },
});

Migrations.add({
  version: 17,
  name: 'Rename nclocator field for users',
  up: () => {
    Meteor.users.rawCollection().updateMany({}, { $rename: { ncloud: 'nclocator' } });
  },
  down: () => {
    Meteor.users.rawCollection().updateMany({}, { $rename: { nclocator: 'ncloud' } });
  },
});

Migrations.add({
  version: 18,
  name: 'Add articlesEnable field for users (obsolete migration step)',
  up: () => {
    // This step has been removed as articlesEnable field does no longer exist in schema
    //
    // Meteor.users
    //   .find()
    //   .fetch()
    //   .forEach((user) => {
    //     Meteor.users.update({ _id: user._id }, { $set: { articlesEnable: false } });
    //   });
  },
  down: () => {
    Meteor.users.rawCollection().updateMany({}, { $unset: { articlesEnable: true } });
  },
});

Migrations.add({
  version: 19,
  name: 'Add maintenance and textMaintenance field for appsettings',
  up: () => {
    AppSettings.find()
      .fetch()
      .forEach((setting) => {
        AppSettings.update({ _id: setting._id }, { $set: { maintenance: false, textMaintenance: '' } });
      });
  },
  down: () => {
    AppSettings.rawCollection().updateMany({}, { $unset: { maintenance: true, textMaintenance: true } });
  },
});

const structureOptions = [
  'Ministère Education',
  'Éducation',
  'Auvergne-Rhône-Alpes',
  'Bourgogne-Franche-Comté',
  'Bretagne',
  'Centre-Val de Loire',
  'Corse',
  'Grand Est',
  'Guadeloupe',
  'Guyane',
  'Hauts-de-France',
  'Île-de-France',
  'Martinique',
  'Normandie',
  'Nouvelle-Aquitaine',
  'Occitanie',
  'Pays de la Loire',
  "Provence-Alpes-Côte d'Azur",
  'La Réunion',
  'Collectivité',
  'Autre',
];

Migrations.add({
  version: 20,
  name: 'Attach users to their structure',
  up: () => {
    const isStructuresSet = Meteor.users.findOne({ structure: { $exists: true } });
    if (isStructuresSet) {
      structureOptions.forEach((label) => {
        const structureDB = Structures.findOne({ name: label });
        let structureId = structureDB && structureDB._id;
        if (!structureId) {
          structureId = Structures.insert({ name: label });
        }
        Meteor.users.rawCollection().updateMany({ structure: label }, { $set: { structure: structureId } });
      });
    }
  },
  down: () => {
    Structures.find()
      .fetch()
      .forEach(({ name, _id }) => {
        Meteor.users.rawCollection().updateMany({ structure: _id }, { $set: { structure: name } });
      });
  },
});

Migrations.add({
  version: 21,
  name: 'Attach services to their structure if needed',
  up: () => {
    const allStructures = Structures.find({}).fetch();
    const structIds = allStructures.map((struct) => struct._id);
    const structuresInfo = {};
    allStructures.forEach((struct) => {
      structuresInfo[struct.name] = struct._id;
    });
    // check if we find services with structure set and not matching any structure id
    Services.find({ structure: { $nin: ['', ...structIds] } })
      .fetch()
      .forEach((serv) => {
        // if service structure matches any known structure name, replace by structure id
        if (structuresInfo[serv.structure])
          Services.update(serv._id, { $set: { structure: structuresInfo[serv.structure] } });
      });
  },
  down: () => {
    Structures.find()
      .fetch()
      .forEach(({ name, _id }) => {
        Services.rawCollection().updateMany({ structure: _id }, { $set: { structure: name } });
      });
  },
});

Migrations.add({
  version: 22,
  name: 'Attach structure admins to their structure',
  up: () => {
    const allStructures = Structures.find({}).fetch();
    const structuresInfo = {};
    allStructures.forEach((struct) => {
      structuresInfo[struct.name] = struct._id;
    });
    // check if we find services with structure set and not matching any structure id
    Meteor.roleAssignment
      .find({ 'role._id': 'adminStructure', scope: { $ne: null } })
      .fetch()
      .forEach((assignment) => {
        // if user structure matches any known structure name, replace by structure id
        if (structuresInfo[assignment.scope])
          Meteor.roleAssignment.update(assignment._id, { $set: { scope: structuresInfo[assignment.scope] } });
      });
  },
  down: () => {
    Structures.find()
      .fetch()
      .forEach(({ name, _id }) => {
        Meteor.roleAssignment.rawCollection().updateMany({ scope: _id }, { $set: { scope: name } });
      });
  },
});

Migrations.add({
  version: 23,
  name: 'add authToken to users',
  up: () => {
    Meteor.users
      .find()
      .fetch()
      .forEach(({ _id }) => {
        Meteor.users.update({ _id }, { $set: { authToken: Random.secret(150) } });
      });
  },
  down: () => {
    Meteor.users.rawCollection().updateMany({}, { $unset: { authToken: true } });
  },
});

Migrations.add({
  version: 24,
  name: 'Add parentId, childrenIds and ancestorsIds to structures',
  up: () => {
    Structures.find({})
      .fetch()
      .forEach((structure) => {
        Structures.update(
          { _id: structure._id },
          {
            $set: {
              childrenIds: structure.childrenIds || [],
              parentId: structure.parentId || null,
              ancestorsIds: structure.ancestorsIds || [],
            },
          },
        );
      });
  },
  down: () => {
    Structures.rawCollection().updateMany(
      {},
      { $unset: { childrenIds: 1, parentId: 1, ancestorsIds: 1 } },
      { multi: true },
    );
  },
});

Migrations.add({
  version: 25,
  name: 'Add introduction to structures',
  up: () => {
    Structures.find({})
      .fetch()
      .forEach((structure) => {
        const introduction = structure.introduction || defaultIntroduction;
        Structures.update({ _id: structure._id }, { $set: { introduction } });
      });
  },
  down: () => {
    Structures.rawCollection().updateMany({}, { $unset: { introduction: 1 } }, { multi: true });
  },
});

Migrations.add({
  version: 26,
  name: 'Add licence to articles',
  up: () => {
    Articles.find({})
      .fetch()
      .forEach((article) => {
        Articles.update({ _id: article._id }, { $set: { licence: '' } });
      });
  },
  down: () => {
    Articles.rawCollection().updateMany({}, { $unset: { licence: 1 } }, { multi: true });
  },
});

Migrations.add({
  version: 27,
  name: 'Add awaitingStructure to users',
  up: () => {
    // Add awaitingStructure prop to manage structure application validation
    Meteor.users.rawCollection().updateMany({}, { $set: { awaitingStructure: null } });
    // set initial value in appsettings (userStructureValidationMandatory)
    AppSettings.update({}, { $set: { userStructureValidationMandatory: false } });
  },
  down: () => {
    Meteor.users.rawCollection().updateMany({}, { $unset: { awaitingStructure: 1 } });
    AppSettings.rawCollection().updateMany({}, { $unset: { userStructureValidationMandatory: 1 } });
  },
});

Migrations.add({
  version: 28,
  name: 'Add general informations to appsettings',
  up: () => {
    AppSettings.update({}, { $set: { globalInfo: [] } });
  },
  down: () => {
    AppSettings.rawCollection().updateMany({}, { $unset: { globalInfo: 1 } });
  },
});

Migrations.add({
  version: 29,
  name: 'Add automatic group for structures',
  up: () => {
    let adminId = '';
    Structures.find({})
      .fetch()
      .forEach((structure) => {
        const strucName = `${structure._id}_${structure.name}`;
        Meteor.users
          .find({})
          .fetch()
          .every((user) => {
            if (Roles.userIsInRole(user._id, 'admin')) {
              adminId = user._id;
              return false;
            }
            return true;
          });
        let groupId = '';
        const existingGroup = Groups.findOne({ name: strucName });
        if (existingGroup) {
          groupId = existingGroup._id;
        } else {
          groupId = Groups.insert({
            name: strucName,
            type: 15,
            content: '',
            description: '',
            avatar: '',
            owner: adminId,
            animators: [],
            admins: [],
            active: true,
            plugins: {},
          });
        }
        Structures.update({ _id: structure._id }, { $set: { groupId } });
      });

    Meteor.users
      .find({})
      .fetch()
      .forEach((user) => {
        const structure = Structures.findOne({ _id: user.structure });
        if (structure) {
          if (structure.groupId) {
            const strucGroup = Groups.findOne(structure.groupId);
            if (strucGroup.members.indexOf(user._id) === -1) {
              Groups.update(
                { _id: structure.groupId },
                {
                  $push: { members: user._id },
                },
              );
            }

            if (user.favGroups === undefined) {
              Meteor.users.update(user._id, {
                $set: { favGroups: [structure.groupId] },
              });
            } else if (user.favGroups.indexOf(structure.groupId) === -1) {
              Meteor.users.update(user._id, {
                $push: { favGroups: structure.groupId },
              });
            }

            Roles.addUsersToRoles(user._id, 'member', structure.groupId);

            addItem(user._id, { type: 'group', element_id: structure.groupId });

            if (Roles.userIsInRole(user._id, 'adminStructure', user.structure)) {
              Roles.addUsersToRoles(user._id, 'admin', structure.groupId);
              if (strucGroup.admins.indexOf(user._id) === -1) {
                Groups.update(
                  { _id: structure.groupId },
                  {
                    $push: { admins: user._id },
                  },
                );
              }
            }
          }

          Structures.find({ _id: { $in: structure.ancestorsIds } })
            .fetch()
            .forEach((struc) => {
              if (struc.groupId) {
                const ancestorGroup = Groups.findOne({ _id: struc.groupId });
                if (ancestorGroup.members.indexOf(user._id) === -1) {
                  Groups.update(
                    { _id: struc.groupId },
                    {
                      $push: { members: user._id },
                    },
                  );
                }

                if (user.favGroups === undefined) {
                  Meteor.users.update(user._id, {
                    $set: { favGroups: [struc.groupId] },
                  });
                } else if (user.favGroups.indexOf(struc.groupId) === -1) {
                  Meteor.users.update(user._id, {
                    $push: { favGroups: struc.groupId },
                  });
                }
                Roles.addUsersToRoles(user._id, 'member', struc.groupId);
                addItem(user._id, { type: 'group', element_id: struc.groupId });
              }
            });
        }
      });
  },
  down: () => {
    Meteor.users
      .find({})
      .fetch()
      .forEach((user) => {
        const structure = Structures.findOne({ _id: user.structure });
        if (structure) {
          if (structure.groupId) {
            if (user.favGroups.indexOf(structure.groupId) === -1) {
              Meteor.users.update(
                { _id: user._id },
                {
                  $pull: { favGroups: structure.groupId },
                },
              );

              PersonalSpaces.update(
                { userId: user._id },
                {
                  $pull: {
                    unsorted: { type: 'group', element_id: structure.groupId },
                    'sorted.$[].elements': { type: 'group', element_id: structure.groupId },
                  },
                },
              );

              if (Roles.userIsInRole(user._id, 'member', structure.groupId))
                Roles.removeUsersFromRoles(user._id, 'member', structure.groupId);
              if (Roles.userIsInRole(user._id, 'admin', structure.groupId)) {
                Roles.removeUsersFromRoles(user._id, 'admin', structure.groupId);
              }
            }
          }

          Structures.find({ _id: { $in: structure.ancestorsIds } })
            .fetch()
            .forEach((struc) => {
              if (struc.groupId) {
                if (user.favGroups.indexOf(struc.groupId) === -1) {
                  Meteor.users.update(
                    { _id: user._id },
                    {
                      $pull: { favGroups: struc.groupId },
                    },
                  );

                  PersonalSpaces.update(
                    { userId: user._id },
                    {
                      $pull: {
                        unsorted: { type: 'group', element_id: struc.groupId },
                        'sorted.$[].elements': { type: 'group', element_id: struc.groupId },
                      },
                    },
                  );

                  if (Roles.userIsInRole(user._id, 'member', struc.groupId))
                    Roles.removeUsersFromRoles(user._id, 'member', struc.groupId);
                  if (Roles.userIsInRole(user._id, 'admin', struc.groupId)) {
                    Roles.removeUsersFromRoles(user._id, 'admin', struc.groupId);
                  }
                }
              }
            });
        }
      });

    Structures.find({})
      .fetch()
      .forEach((structure) => {
        Groups.remove({ _id: structure.groupId });
        Structures.update({ _id: structure._id }, { $unset: { groupId: 1 } });
      });
  },
});

Migrations.add({
  version: 30,
  name: 'Add group type to articles with groups',
  up: () => {
    const articles = Articles.find({ groups: { $exists: true } }).fetch();
    articles.forEach(({ _id: articleId, groups }) => {
      if (groups.length > 0) {
        const updatedGroups = [];
        groups.forEach(({ _id }) => {
          const group = Groups.findOne(_id);
          if (group) {
            updatedGroups.push({ _id: group._id, name: group.name, type: group.type });
          }
        });
        logServer(
          `MIGRATIONS - ARTICLES - UPDATE - 30 Add group type to articles with groups
          - articleId: ${articleId} / groups: ${JSON.stringify(updatedGroups)}`,
          levels.INFO,
          scopes.SYSTEM,
        );
        Articles.update({ _id: articleId }, { $set: { groups: updatedGroups } });
      }
    });
  },
  down: () => {
    // no rollback on this step
  },
});

Migrations.add({
  version: 31,
  name: 'Update structure.groupId default value',
  up: () => {
    logServer(
      `MIGRATIONS - STRUCTURE - UPDATE - 31 Update structure.groupId default value`,
      levels.INFO,
      scopes.SYSTEM,
    );
    Structures.update({ groupId: '' }, { $set: { groupId: null } });
  },
  down: () => {
    // no rollback on this step
  },
});

Migrations.add({
  version: 32,
  name: 'Fix incomplete events',
  up: () => {
    logServer(`MIGRATIONS - 32 Fix incomplete events - EVENTSAGENDA - UPDATE`, levels.INFO, scopes.SYSTEM);
    const now = new Date();
    EventsAgenda.update({ updatedAt: null }, { $set: { updatedAt: now, createdAt: now } });
  },
  down: () => {
    // no rollback on this step
  },
});

Migrations.add({
  version: 33,
  name: 'Fix all incomplete events',
  up: () => {
    logServer(`MIGRATIONS - 33 Fix all incomplete events - EVENTSAGENDA - UPDATE`, levels.INFO, scopes.SYSTEM);
    const now = new Date();
    EventsAgenda.rawCollection().updateMany({ updatedAt: null }, { $set: { updatedAt: now, createdAt: now } });
  },
  down: () => {
    // no rollback on this step
  },
});

Migrations.add({
  version: 34,
  name: 'Update minio objects URLs',
  up: () => {
    const { minioEndPoint, minioBucket, minioPort } = Meteor.settings.public;
    const sourceURL = `https://${minioEndPoint}${minioPort ? `:${minioPort}` : ''}/${minioBucket}/`;
    const destinationURL = new URL('/objects/', Meteor.absoluteUrl()).href;
    const findRegEx = new RegExp(sourceURL, 'g');
    const replaceURLs = (data) => {
      if (data === undefined || data === null) return null;
      if (Array.isArray(data)) {
        return data.map((elem) => elem.replace(findRegEx, destinationURL));
      }
      return data.replace(findRegEx, destinationURL);
    };
    logServer(`MIGRATIONS - MINIO - USERS - 34 Update minio objects URLs`, levels.INFO, scopes.SYSTEM);
    const users = Meteor.users.find({ avatar: { $regex: findRegEx } }).fetch();
    users.forEach((u) => {
      Meteor.users.rawCollection().updateOne({ _id: u._id }, { $set: { avatar: replaceURLs(u.avatar) } });
    });
    logServer(`MIGRATIONS - MINIO - ARTICLES - 34 Update minio objects URLs`, levels.INFO, scopes.SYSTEM);
    const articles = Articles.find({ content: { $regex: findRegEx } }).fetch();
    articles.forEach((a) => {
      Articles.rawCollection().updateOne({ _id: a._id }, { $set: { content: replaceURLs(a.content) } });
    });
    logServer(`MIGRATIONS - MINIO - GROUPS - 34 Update minio objects URLs`, levels.INFO, scopes.SYSTEM);
    const groups = Groups.find({
      $or: [{ avatar: { $regex: findRegEx } }, { content: { $regex: findRegEx } }],
    }).fetch();
    groups.forEach((g) => {
      Groups.rawCollection().updateOne(
        { _id: g._id },
        { $set: { avatar: replaceURLs(g.avatar), content: replaceURLs(g.content) } },
      );
    });
    logServer(`MIGRATIONS - MINIO - SERVICES - 34 Update minio objects URLs`, levels.INFO, scopes.SYSTEM);
    const services = Services.find({
      $or: [
        { screenshots: { $regex: findRegEx } },
        { logo: { $regex: findRegEx } },
        { content: { $regex: findRegEx } },
      ],
    }).fetch();
    services.forEach((s) => {
      Services.rawCollection().updateOne(
        { _id: s._id },
        {
          $set: { logo: replaceURLs(s.logo), content: replaceURLs(s.content), screenshots: replaceURLs(s.screenshots) },
        },
      );
    });
    logServer(`MIGRATIONS - MINIO - STRUCTURES - 34 Update minio objects URLs`, levels.INFO, scopes.SYSTEM);
    const structures = Structures.find({
      $or: [{ iconUrlImage: { $regex: findRegEx } }, { coverUrlImage: { $regex: findRegEx } }],
    }).fetch();
    structures.forEach((s) => {
      Structures.rawCollection().updateOne(
        { _id: s._id },
        { $set: { iconUrlImage: replaceURLs(s.iconUrlImage), coverUrlImage: replaceURLs(s.coverUrlImage) } },
      );
    });
    logServer(`MIGRATIONS - MINIO - GLOBALINFOS - 34 Update minio objects URLs`, levels.INFO, scopes.SYSTEM);
    const infos = GlobalInfos.find({ content: { $regex: findRegEx } }).fetch();
    infos.forEach((i) => {
      GlobalInfos.rawCollection().updateOne({ _id: i._id }, { $set: { content: replaceURLs(i.content) } });
    });
  },
  down: () => {
    // no rollback on this step
  },
});

Migrations.add({
  version: 35,
  name: 'Upgrade dates field for timezone in polls and polls_answers collections',
  up: () => {
    logServer(`MIGRATIONS - 35 Fix dates field for timezone - POLLS - UPDATE`, levels.INFO, scopes.SYSTEM);
    Polls.find()
      .fetch()
      .forEach((poll) => {
        const newDates = [];
        poll.dates.forEach((el) => {
          const hours = el.date.getHours();
          const modifiedDate = new Date(el.date.setHours(hours + 10));
          const date = modifiedDate.toISOString().slice(0, -1);
          if (poll.allDay) {
            const re = /T[\d:]{5}/;
            const dt = date.replace(re, 'T06:00');
            const newDate = zonedTimeToUtc(dt, 'Europe/Paris');
            newDates.push(newDate);
          } else {
            el.slots.forEach((slot) => {
              const re = /T[\d:]{5}/;
              const dt = date.replace(re, `T${slot}`);
              const newDate = zonedTimeToUtc(dt, 'Europe/Paris');
              newDates.push(newDate);
            });
          }
        });
        if (poll.allDay) {
          Polls.rawCollection().updateOne({ _id: poll._id }, { $set: { dates: newDates, duration: '16:00' } });
        } else {
          Polls.rawCollection().updateOne({ _id: poll._id }, { $set: { dates: newDates } });
        }
      });
    logServer(`MIGRATIONS - 35 Fix choices field for timezone - POLLSANSWERS - UPDATE`, levels.INFO, scopes.SYSTEM);
    const allPolls = {}; // {pollId : [{date: ..., present: ...}]}
    PollsAnswers.find()
      .fetch()
      .forEach((pa) => {
        const poll = Polls.findOne({ _id: pa.pollId });
        if (!poll) return; // no corresponding poll so continue the forEach on PollsAnswers

        if (!(pa.pollId in allPolls)) {
          allPolls[pa.pollId] = [];
          poll.dates.forEach((dt) => {
            allPolls[pa.pollId].push({ date: dt, present: false });
          });
        }
        let newChoices = allPolls[pa.pollId];
        pa.choices.forEach((choice) => {
          if (choice.present) {
            // allDay poll
            const hours = choice.date.getHours();
            const modifiedDate = new Date(choice.date.setHours(hours + 10));
            const date = modifiedDate.toISOString().slice(0, -1);
            const re = /T[\d:]{5}/;
            const dt = date.replace(re, 'T06:00');
            const newDate = zonedTimeToUtc(dt, 'Europe/Paris');
            newChoices = newChoices.filter((obj) => obj.date.toISOString() !== newDate.toISOString());
            newChoices.push({ date: newDate, present: true });
          } else if (choice.slots.length > 0) {
            const hours = choice.date.getHours();
            const modifiedDate = new Date(choice.date.setHours(hours + 10));
            const date = modifiedDate.toISOString().slice(0, -1);
            choice.slots.forEach((slot) => {
              const re = /T[\d:]{5}/;
              const dt = date.replace(re, `T${slot}`);
              const newDate = zonedTimeToUtc(dt, 'Europe/Paris');
              newChoices = newChoices.filter((obj) => obj.date.toISOString() !== newDate.toISOString());
              newChoices.push({ date: newDate, present: true });
            });
          }
        });
        PollsAnswers.rawCollection().updateOne(
          { _id: pa._id },
          { $set: { choices: newChoices.sort((date1, date2) => date1.date - date2.date) } },
        );
      });
  },
  down: () => {
    // never be the same again
    logServer(`MIGRATIONS - 35 No rollback for POLLS and POLLSANSWERS`, levels.INFO, scopes.SYSTEM);
  },
});

Migrations.add({
  version: 36,
  name: 'Add structure path',
  up: () => {
    logServer(`MIGRATIONS - 36 Add structure path`, levels.INFO, scopes.SYSTEM);

    const structures = Structures.find().fetch();
    if (structures && structures.length > 0) {
      structures.forEach((structure) => {
        const tree = [];
        generatePathOfStructure(structure, tree);
        const path = tree.reverse();
        Structures.rawCollection().updateOne({ _id: structure._id }, { $set: { structurePath: path } });
      });
    }
  },
  down: () => {
    Structures.rawCollection().updateMany({}, { $unset: { structurePath: 1 } });
  },
});

Migrations.add({
  version: 37,
  name: 'Add updatedAt to groups',
  up: () => {
    logServer(`MIGRATIONS - 37 Add updatedAt to groups`, levels.INFO, scopes.SYSTEM);
    const currentDate = new Date();
    Groups.rawCollection().updateMany({}, { $set: { updatedAt: currentDate } });
  },
  down: () => {
    Groups.rawCollection().updateMany({}, { $unset: { updatedAt: 1 } });
  },
});

Migrations.add({
  version: 38,
  name: 'Remove usage to services',
  up: () => {
    logServer(`MIGRATIONS - 38 Remove usage to services`, levels.INFO, scopes.SYSTEM);
    Services.rawCollection().updateMany({}, { $unset: { usage: 1 } });
  },
  down: () => {
    Services.rawCollection().updateMany({}, { $set: { usage: '' } });
  },
});
