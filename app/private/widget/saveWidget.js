/* eslint-disable */
(function (d, u) {
  typeof exports === 'object' && typeof module < 'u'
    ? u(exports)
    : typeof define === 'function' && define.amd
    ? define(['exports'], u)
    : ((d = typeof globalThis < 'u' ? globalThis : d || self), u((d['la-pastille'] = {})));
})(this, function (d) {
  const u = `._container_2k46u_3 ._loaderFrame_2k46u_3{display:none}._container_2k46u_3 ._widget_2k46u_6{position:fixed;bottom:10px;right:10px;width:75px;height:75px;z-index:9999;display:flex;justify-content:center;align-items:center;border-radius:50%;color:#fff;border:none;cursor:pointer;padding:0;margin:0;font-size:25px;transition:transform .1s ease-in-out;box-shadow:0 3px 5px -1px #0003,0 6px 10px #00000024,0 1px 18px #0000001f;touch-action:none}._container_2k46u_3 ._widget_2k46u_6._dragging_2k46u_27{cursor:grabbing}._container_2k46u_3 ._widget_2k46u_6._dropping_2k46u_30,._container_2k46u_3 ._widget_2k46u_6:hover{transform:scale(1.1)}._container_2k46u_3 ._widget_2k46u_6 img{user-select:none;width:100%}._container_2k46u_3 ._widget_2k46u_6 ._notifications_2k46u_40{position:absolute;background-color:#ce0500;font-size:15px;width:20px;height:20px;border-radius:50%;top:0;right:0;box-shadow:0 3px 5px -1px #0003,0 6px 10px #00000024,0 1px 18px #0000001f}._container_2k46u_3 ._widget_2k46u_6 ._notifications_2k46u_40:empty{display:none}._container_2k46u_3[data-open=true] ._widget_2k46u_6{display:none}._container_2k46u_3[data-open=true][data-fullscreen=true] ._frame_2k46u_57{height:100%;width:100%}._container_2k46u_3:not([data-open=true]) ._frame_2k46u_57{opacity:0;pointer-events:none;position:absolute;width:0;height:0}._container_2k46u_3 ._frame_2k46u_57{transition:all .2s ease-in-out;z-index:9999;position:fixed;display:flex;flex-direction:column;bottom:5px;right:5px;height:600px;width:500px;max-height:calc(100% - 10px);max-width:calc(100% - 10px);background:white;border-radius:8px;animation:_fadeIn_2k46u_1 .7s;overflow:hidden;box-shadow:0 3px 5px -1px #0003,0 6px 10px #00000024,0 1px 18px #0000001f}._container_2k46u_3 ._frame_2k46u_57 ._header_2k46u_86{height:40px;background-color:#000091;display:flex;padding:5px;justify-content:space-between}._container_2k46u_3 ._frame_2k46u_57 ._header_2k46u_86 ._nav_2k46u_93{display:flex}._container_2k46u_3 ._frame_2k46u_57 ._header_2k46u_86 ._nav_2k46u_93 ._navBtn_2k46u_96{cursor:pointer;background:0;margin:0;border:0;outline:0;padding:0;display:flex;justify-content:center;align-items:center;width:40px;height:100%;color:#fff;background-color:#000091;transition:all ease-in-out .2s;border-radius:8px 8px 0 0}._container_2k46u_3 ._frame_2k46u_57 ._header_2k46u_86 ._nav_2k46u_93 ._navBtn_2k46u_96 img{width:100%}._container_2k46u_3 ._frame_2k46u_57 ._header_2k46u_86 ._nav_2k46u_93 ._navBtn_2k46u_96 i{width:100%;height:100%}._container_2k46u_3 ._frame_2k46u_57 ._header_2k46u_86 ._nav_2k46u_93 ._navBtn_2k46u_96 i svg{width:100%;height:100%}._container_2k46u_3 ._frame_2k46u_57 ._header_2k46u_86 ._nav_2k46u_93 ._navBtn_2k46u_96 ._navTitle_2k46u_125{display:flex;justify-content:center;align-items:center;width:40px;height:100%;color:#fff;transition:all ease-in-out .2s;border-radius:8px 8px 0 0}._container_2k46u_3 ._frame_2k46u_57 ._header_2k46u_86 ._actions_2k46u_135{display:flex}._container_2k46u_3 ._frame_2k46u_57 ._header_2k46u_86 ._actions_2k46u_135 ._actionBtn_2k46u_138{cursor:pointer;background:0;margin:0;border:0;outline:0;display:flex;justify-content:center;align-items:center;width:40px;padding:5px;height:100%;color:#fff;transition:all ease-in-out .2s;border-radius:8px 8px 0 0}._container_2k46u_3 ._frame_2k46u_57 ._header_2k46u_86 ._actions_2k46u_135 ._actionBtn_2k46u_138 svg{width:100%;height:100%}._container_2k46u_3 ._frame_2k46u_57 ._content_2k46u_160,._container_2k46u_3 ._frame_2k46u_57 ._content_2k46u_160 ._iframe_2k46u_164{width:100%;height:100%}._container_2k46u_3 ._frame_2k46u_57 ._content_2k46u_160 ._iframe_2k46u_164[data-tab-active=true]{display:block}._container_2k46u_3 ._frame_2k46u_57 ._content_2k46u_160 ._iframe_2k46u_164:not([data-tab-active=true]){display:none}@keyframes _fadeIn_2k46u_1{0%{opacity:0}to{opacity:1}}
`;
  const a = {
    container: '_container_2k46u_3',
    loaderFrame: '_loaderFrame_2k46u_3',
    widget: '_widget_2k46u_6',
    dragging: '_dragging_2k46u_27',
    dropping: '_dropping_2k46u_30',
    notifications: '_notifications_2k46u_40',
    frame: '_frame_2k46u_57',
    fadeIn: '_fadeIn_2k46u_1',
    header: '_header_2k46u_86',
    nav: '_nav_2k46u_93',
    navBtn: '_navBtn_2k46u_96',
    navTitle: '_navTitle_2k46u_125',
    actions: '_actions_2k46u_135',
    actionBtn: '_actionBtn_2k46u_138',
    content: '_content_2k46u_160',
    iframe: '_iframe_2k46u_164',
  };
  var o = ((i) => (
    (i.NOTIFICATION = 'snap-notification'),
    (i.AUTH = 'snap-auth'),
    (i.OPEN = 'snap-open'),
    (i.CLOSE = 'snap-close'),
    (i.FULLSCREEN = 'snap-fullscreen'),
    (i.FULLSCREEN_EXIT = 'snap-fullscreen-exit'),
    (i.READY = 'main-frame-ready'),
    i
  ))(o || {});
  class h extends Event {
    constructor(t, e, n) {
      super(e, n), (this.widget = t);
    }
  }
  class E extends h {
    constructor(t, e) {
      super(t, 'main-frame-ready', e);
    }
  }
  class y extends h {
    constructor(t, e) {
      super(t, 'snap-open', e);
    }
  }
  class F extends h {
    constructor(t, e) {
      super(t, 'snap-close', e);
    }
  }
  class L extends h {
    constructor(t, e) {
      super(t, 'snap-fullscreen', e);
    }
  }
  class A extends h {
    constructor(t, e) {
      super(t, 'snap-fullscreen-exit', e);
    }
  }
  class S extends h {
    constructor(t, e, n) {
      super(t, 'snap-auth', n), (this.authState = e);
    }
  }
  class C extends h {
    constructor(t, e, n) {
      super(t, 'snap-notification', n), (this.notificationsCount = e);
    }
  }
  class H {
    constructor(t) {
      (this.posX = 0),
        (this.posY = 0),
        (this.mouseX = 0),
        (this.mouseY = 0),
        (this._isDragging = !1),
        (this._isDropping = !1),
        (this._widget = t),
        (this.btn = document.createElement('button')),
        this.btn.classList.add(a.widget),
        (this.btn.title = 'AccÃ©der aux services rÃ©servÃ©s aux agents de lâ€™Etat'),
        (this.notifications = document.createElement('span')),
        this.notifications.classList.add(a.notifications),
        (this.image = document.createElement('img')),
        this.image.setAttribute('src', `${t.appUrl}/widget/assets/${t.isAuthenticated ? 'connected' : 'disconnected'}`),
        this.btn.appendChild(this.image),
        this.btn.appendChild(this.notifications),
        (this.onPointerDown = this.onPointerDown.bind(this)),
        (this.onPointerUp = this.onPointerUp.bind(this)),
        (this.onPointerMove = this.onPointerMove.bind(this)),
        (this.onDrop = this.onDrop.bind(this)),
        (this.setNotification = this.setNotification.bind(this)),
        (this.setAuth = this.setAuth.bind(this)),
        this.btn.addEventListener('pointerdown', this.onPointerDown),
        this.btn.addEventListener('click', (e) => {
          e == null || e.preventDefault(), this._isDragging || t.open();
        }),
        this.btn.addEventListener('contextmenu', (e) => {
          e == null || e.preventDefault(), t.open();
        }),
        this.btn.addEventListener('dragover', (e) => e.preventDefault()),
        this.btn.addEventListener('dragenter', (e) => this.setDropping(!0)),
        this.btn.addEventListener('dragleave', (e) => this.setDropping(!1)),
        this.btn.addEventListener('drop', this.onDrop),
        t.addEventListener(o.AUTH, (e) => {
          this.setAuth(e.authState);
        }),
        t.addEventListener(o.NOTIFICATION, (e) => {
          this.setNotification(e.notificationsCount);
        });
    }

    get isDragging() {
      return this._isDragging;
    }

    get isDropping() {
      return this._isDropping;
    }

    get widget() {
      return this._widget;
    }

    setDragging(t) {
      (this._isDragging = t), t ? this.element.classList.add(a.dragging) : this.element.classList.remove(a.dragging);
    }

    setDropping(t) {
      (this._isDropping = t), t ? this.element.classList.add(a.dropping) : this.element.classList.remove(a.dropping);
    }

    onPointerDown(t) {
      t.preventDefault(),
        t.button === 0 &&
          ((this.posX = t.clientX - this.btn.offsetLeft),
          (this.posY = t.clientY - this.btn.offsetTop),
          this.btn.addEventListener('pointermove', this.onPointerMove, !1),
          document.addEventListener('pointerup', this.onPointerUp, {
            once: !0,
          }),
          this.btn.setPointerCapture(t.pointerId)),
        t.button === 1 && this.resetPosition();
    }

    onPointerUp(t) {
      this.btn.removeEventListener('pointermove', this.onPointerMove, !1),
        this.btn.releasePointerCapture(t.pointerId),
        window.requestAnimationFrame(() => {
          this.setDragging(!1);
        });
    }

    onPointerMove(t) {
      t.preventDefault(),
        (this.mouseX = t.clientX - this.posX),
        (this.mouseY = t.clientY - this.posY),
        this.updateButtonPosition(this.mouseX, this.mouseY),
        this.setDragging(!0);
    }

    async onDrop(t) {
      let s;
      let r;
      t.preventDefault(), await this.widget.loadMainFrame();
      const e = [];
      const n = t.dataTransfer;
      n &&
        (n.items
          ? [...n.items].forEach((l) => {
              l.kind === 'file' && e.push(l.getAsFile());
            })
          : [...n.files].forEach((l) => {
              e.push(l);
            })),
        this.widget.open(),
        this.widget.enterFullscreen(),
        (r = (s = this.widget.mainFrame) == null ? void 0 : s.contentWindow) == null ||
          r.postMessage({ type: 'widget', event: 'upload', files: e }, '*'),
        this.setDropping(!1);
    }

    updateButtonPosition(t, e) {
      t < 0 || t > window.innerWidth - this.btn.clientWidth
        ? t < 0
          ? (this.btn.style.left = '0px')
          : (this.btn.style.left = `${window.innerWidth - this.btn.clientWidth}px`)
        : (this.btn.style.left = `${t}px`),
        e < 0 || e > window.innerHeight - this.btn.clientHeight
          ? e < 0
            ? (this.btn.style.top = '0px')
            : (this.btn.style.top = `${window.innerHeight - this.btn.clientHeight}px`)
          : (this.btn.style.top = `${e}px`);
    }

    setAuth(t) {
      this.image.setAttribute('src', `${this.widget.appUrl}/widget/assets/${t ? 'connected' : 'disconnected'}`);
    }

    setNotification(t) {
      t === 0 ? (this.notifications.innerText = '') : (this.notifications.innerText = t.toString());
    }

    resetPosition() {
      (this.posX = 0), (this.posY = 0), (this.btn.style.top = ''), (this.btn.style.left = '');
    }

    get element() {
      return this.btn;
    }
  }
  var _ = ((i) => (
    (i.NOTIFICATION = 'notifications'), (i.AUTH = 'userLogged'), (i.LOAD = 'load'), (i.READY = 'ready'), i
  ))(_ || {});
  function c(i, t, e) {
    const n = document.createElement(i);
    if (t && typeof t === 'object') {
      if ('style' in t && t.style) for (const [s, r] of Object.entries(t.style)) n.style[s] = r;
      for (const [s, r] of Object.entries(t))
        (typeof r === 'string' || typeof r === 'number' || typeof r === 'boolean') && n.setAttribute(s, r.toString());
    }
    return e && n.append(...p(e)), n;
  }
  function p(i) {
    return i ? (Array.isArray(i) ? i.flatMap(p) : typeof i === 'function' ? p(i()) : [i]) : [];
  }
  const M = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor">
  <path d="M0 0h24v24H0V0z" fill="none" />
  <path d="M20 2H4c-1.1 0-2 .9-2 2v18l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm0 14H6l-2 2V4h16v12z" />
</svg>`;
  const T = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <path fill="currentColor"
    d="M12.0007 10.5865L16.9504 5.63672L18.3646 7.05093L13.4149 12.0007L18.3646 16.9504L16.9504 18.3646L12.0007 13.4149L7.05093 18.3646L5.63672 16.9504L10.5865 12.0007L5.63672 7.05093L7.05093 5.63672L12.0007 10.5865Z"></path>
</svg>`;
  const f = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <path fill="currentColor"
    d="M8 3V5H4V9H2V3H8ZM2 21V15H4V19H8V21H2ZM22 21H16V19H20V15H22V21ZM22 9H20V5H16V3H22V9Z"></path>
</svg>`;
  const g = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <path fill="currentColor"
    d="M18 7H22V9H16V3H18V7ZM8 9H2V7H6V3H8V9ZM18 17V21H16V15H22V17H18ZM8 15V21H6V17H2V15H8Z"></path>
</svg>`;
  var m = ((i) => ((i.RIZOMO = 'rizomo'), (i.CHATBOT = 'chatbot'), i))(m || {});
  const k = [
    {
      key: 'rizomo',
      iframe: (i) =>
        c('iframe', {
          id: 'lb_iframe-widget',
          name: 'lb_iframe-widget',
          src: i.appUrl,
          allow: 'fullscreen',
          style: { border: '0' },
        }),
      icon: (i) =>
        c('img', {
          src: `${i.appUrl}/widget/assets/logo`,
          style: { borderRadius: '4px' },
        }),
    },
    {
      key: 'chatbot',
      iframe: (i) =>
        c('iframe', {
          id: 'chatbot-frame',
          name: 'chatbot-frame',
          src: `${i.appUrl}/chat`,
          allow: 'fullscreen autoplay',
          style: { border: '0' },
        }),
      icon: (i) => {
        const t = document.createElement('i');
        return (t.innerHTML = M), t;
      },
    },
  ];
  const B = 'rizomo';
  class D {
    constructor(t) {
      (this._tab = B),
        (this._tabFrames = new Map()),
        (this._tabBtns = new Map()),
        (this._widget = t),
        (this.onCloseClick = this.onCloseClick.bind(this)),
        (this.onFullScreenToggleClick = this.onFullScreenToggleClick.bind(this)),
        (this.onNavClick = this.onNavClick.bind(this)),
        (this.frame = c('div', { class: a.frame }, [
          c('header', { class: a.header }, [
            c(
              'nav',
              { class: a.nav },
              k.map((e) => {
                const n = c('button', { class: a.navBtn }, e.icon(t));
                if (e.title && e.showTitle) {
                  const s = document.createElement('span');
                  s.classList.add(a.navTitle), (s.innerText = e.title), n.append(s);
                }
                return n.addEventListener('click', () => this.changeTab(e.key)), this._tabBtns.set(e.key, n), n;
              }),
            ),
            c('div', { class: a.actions }, [
              () => {
                const e = document.createElement('button');
                return (
                  (e.title = 'Activer / Desactiver le mode plein Ã©cran'),
                  e.classList.add(a.actionBtn),
                  (e.innerHTML = t.isFullscreen ? g : f),
                  e.addEventListener('click', this.onFullScreenToggleClick, !1),
                  t.addEventListener(o.FULLSCREEN, () => {
                    e.innerHTML = g;
                  }),
                  t.addEventListener(o.FULLSCREEN_EXIT, () => {
                    e.innerHTML = f;
                  }),
                  e
                );
              },
              () => {
                const e = document.createElement('button');
                return (
                  (e.title = 'Fermer la pastille'),
                  e.classList.add(a.actionBtn),
                  (e.innerHTML = T),
                  e.addEventListener('click', this.onCloseClick, !1),
                  e
                );
              },
            ]),
          ]),
          c(
            'div',
            { class: a.content },
            k.map((e) => {
              const n = e.iframe(t);
              return (
                (n.loading = 'lazy'),
                n.classList.add(a.iframe),
                n.setAttribute('data-tab', e.key),
                e.key === this._tab && n.setAttribute('data-tab-active', 'true'),
                this._tabFrames.set(e.key, n),
                n
              );
            }),
          ),
        ]));
    }

    onCloseClick(t) {
      this._widget.close();
    }

    onFullScreenToggleClick(t) {
      this._widget.toggleFullscreen();
    }

    onNavClick(t) {}

    changeTab(t) {
      const e = this._tabBtns.get(this._tab);
      const n = this._tabFrames.get(this._tab);
      this._tab = t;
      const s = this._tabBtns.get(t);
      const r = this._tabFrames.get(t);
      e == null || e.setAttribute('data-tab-active', 'false'),
        n == null || n.setAttribute('data-tab-active', 'false'),
        s == null || s.setAttribute('data-tab-active', 'true'),
        r == null || r.setAttribute('data-tab-active', 'true');
    }

    get tab() {
      return this._tab;
    }

    getTabFrame(t) {
      return this._tabFrames.get(t);
    }

    getTabButton(t) {
      return this._tabBtns.get(t);
    }

    get widget() {
      return this._widget;
    }

    get element() {
      return this.frame;
    }
  }
  class b {
    constructor(t) {
      (this._isMainFrameReady = !1),
        (this._isAuthenticated = !1),
        (this._notificationsCount = 0),
        (this._appUrl = (t == null ? void 0 : t.appUrl) ?? 'https://rizomo.numerique.gouv.fr'),
        (this._isOpen = (t == null ? void 0 : t.defaultOpen) ?? !1),
        (this._isFullscreen = (t == null ? void 0 : t.defaultFullscreen) ?? !1),
        (this.handleMessage = this.handleMessage.bind(this)),
        (this._container = document.createElement('div')),
        this._container.classList.add(a.container),
        (this._loadFrame = document.createElement('iframe')),
        this._loadFrame.classList.add(a.loaderFrame),
        (this._loadFrame.loading = 'eager'),
        (this._loadFrame.name = 'loaderFrame'),
        (this._loadFrame.src = `${this._appUrl}/widget/load`),
        (this._widgetButton = new H(this)),
        (this._widgetFrame = new D(this)),
        this._container.append(this._loadFrame),
        this._container.append(this._widgetButton.element),
        this._container.append(this._widgetFrame.element),
        (this._styleSheet = new CSSStyleSheet()),
        this._styleSheet.replaceSync(u),
        window.addEventListener('message', this.handleMessage, !1),
        this._loadFrame.addEventListener('load', () => {
          this.load();
        });
    }

    get styleSheet() {
      return this._styleSheet;
    }

    get container() {
      return this._container;
    }

    get appUrl() {
      return this._appUrl;
    }

    get isOpen() {
      return this._isOpen;
    }

    get isFullscreen() {
      return this._isFullscreen;
    }

    get isAuthenticated() {
      return this._isAuthenticated;
    }

    get notificationsCount() {
      return this._notificationsCount;
    }

    get isMainFrameReady() {
      return this._isMainFrameReady;
    }

    get mainFrame() {
      return this._widgetFrame.getTabFrame(m.RIZOMO);
    }

    addEventListener(t, e, n) {
      this.container.addEventListener(t, e, n);
    }

    removeEventListener(t, e, n) {
      this.container.removeEventListener(t, e, n);
    }

    open() {
      (this._isOpen = !0), this.container.setAttribute('data-open', 'true'), this.container.dispatchEvent(new y(this));
    }

    close() {
      (this._isOpen = !1), this.container.setAttribute('data-open', 'false'), this.container.dispatchEvent(new F(this));
    }

    enterFullscreen() {
      (this._isFullscreen = !0),
        this.container.setAttribute('data-fullscreen', 'true'),
        this.container.dispatchEvent(new L(this));
    }

    exitFullscreen() {
      (this._isFullscreen = !1),
        this.container.setAttribute('data-fullscreen', 'false'),
        this.container.dispatchEvent(new A(this));
    }

    toggleFullscreen() {
      this._isFullscreen ? this.exitFullscreen() : this.enterFullscreen();
    }

    loadMainFrame() {
      let t;
      let e;
      let n;
      if (!this.isMainFrameReady)
        return (
          ((t = this.mainFrame) != null && t.contentDocument) ||
            (n = (e = this.mainFrame) == null ? void 0 : e.contentWindow) == null ||
            n.document,
          this.mainFrame.setAttribute('loading', 'eager'),
          new Promise((s, r) => {
            const l = () => {
              s();
            };
            this.addEventListener(o.READY, l),
              setTimeout(() => {
                r(new Error('Loading timed out')), this.removeEventListener(o.READY, l);
              }, 3e4);
          })
        );
    }

    authenticate(t) {
      (this._isAuthenticated = t), this.container.dispatchEvent(new S(this, t));
    }

    notify(t) {
      (this._notificationsCount = t), this.container.dispatchEvent(new C(this, t));
    }

    load() {
      let t;
      (t = this._loadFrame.contentWindow) == null || t.postMessage('load', '*');
    }

    ready() {
      (this._isMainFrameReady = !0), this.container.dispatchEvent(new E(this));
    }

    handleMessage(t) {
      let s;
      let r;
      const e = t.data;
      const n = (l) => {
        let v;
        let x;
        'callback' in e &&
          e.callback &&
          ((x = (v = this.mainFrame) == null ? void 0 : v.contentWindow) == null ||
            x.postMessage({ type: 'callback', callback: e.callback, data: l }, '*'));
      };
      switch (e.type) {
        case _.NOTIFICATION:
          this.notify(Number(e.content)), n();
          break;
        case _.AUTH:
          this.authenticate(!!e.content), n();
          break;
        case _.READY:
          this.ready(), n();
          break;
        case _.LOAD:
          this.notify(Number((s = e.content) == null ? void 0 : s.notifications)),
            this.authenticate(!!((r = e.content) != null && r.authenticated));
          break;
        case 'isWiget':
          n(!0);
          break;
        case 'isFullScreen':
          n(this.isFullscreen);
          break;
        case 'isOpened':
          n(this.isOpen);
          break;
        case 'openWidget':
          this.open(), n();
          break;
        case 'closeWidget':
          this.close(), n();
          break;
        case 'setFullScreen':
          e.content === !0 ? this.enterFullscreen() : e.content === !1 && this.exitFullscreen(), n();
          break;
      }
    }

    attach(t) {
      let e;
      if (t)
        if (t instanceof HTMLElement) e = t;
        else {
          const n = document.getElementById(t);
          if (!n) throw new Error('Element not found');
          e = n;
        }
      else e = document.body;
      e.appendChild(this.container), (document.adoptedStyleSheets = [...document.adoptedStyleSheets, this.styleSheet]);
    }
  }
  class w extends HTMLElement {
    constructor() {
      if (
        (super(),
        (this.widget = new b({
          appUrl: this.getAttribute('url') || void 0,
          defaultFullscreen: this.getAttribute('fullscreen') === 'true',
          defaultOpen: this.getAttribute('open') === 'true',
        })),
        this.widget.addEventListener(o.OPEN, () => {
          this.setAttribute('open', 'true');
        }),
        this.widget.addEventListener(o.CLOSE, () => {
          this.setAttribute('open', 'false');
        }),
        this.widget.addEventListener(o.FULLSCREEN, () => {
          this.setAttribute('fullscreen', 'true');
        }),
        this.widget.addEventListener(o.FULLSCREEN_EXIT, () => {
          this.setAttribute('fullscreen', 'false');
        }),
        'attachShadow' in this)
      ) {
        const t = this.attachShadow({ mode: 'open' });
        t.appendChild(this.widget.container), (t.adoptedStyleSheets = [this.widget.styleSheet]);
      } else console.warn('Browser does not support ShadowDOM. Some bugs may occur.'), this.widget.attach(this);
    }

    static get observedAttributes() {
      return ['open', 'fullscreen'];
    }

    attributeChangedCallback(t, e, n) {
      switch (t) {
        case 'open':
          n === 'true' ? this.widget.isOpen || this.widget.open() : this.widget.isOpen && this.widget.close();
          break;
        case 'fullscreen':
          n === 'true'
            ? this.widget.isFullscreen || this.widget.enterFullscreen()
            : this.widget.isFullscreen && this.widget.exitFullscreen();
          break;
      }
    }
  }
  typeof window < 'u' && 'customElements' in window && window.customElements.define('snap-widget', w),
    (d.SnapWidget = b),
    (d.SnapWidgetElement = w),
    Object.defineProperty(d, Symbol.toStringTag, { value: 'Module' });
});
