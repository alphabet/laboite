import { SnapWidget } from '../lib/widget';
export declare enum WidgetEventType {
    NOTIFICATION = "snap-notification",
    AUTH = "snap-auth",
    OPEN = "snap-open",
    CLOSE = "snap-close",
    FULLSCREEN = "snap-fullscreen",
    FULLSCREEN_EXIT = "snap-fullscreen-exit",
    READY = "main-frame-ready"
}
export interface WidgetEventMap extends HTMLElementEventMap {
    [WidgetEventType.AUTH]: WidgetAuthEvent;
    [WidgetEventType.CLOSE]: WidgetCloseEvent;
    [WidgetEventType.OPEN]: WidgetOpenEvent;
    [WidgetEventType.FULLSCREEN]: WidgetFullScreenEvent;
    [WidgetEventType.FULLSCREEN_EXIT]: WidgetFullScreenExitEvent;
    [WidgetEventType.NOTIFICATION]: WidgetNotificationEvent;
    [WidgetEventType.READY]: WidgetReadyEvent;
}
export declare class WidgetEvent extends Event {
    widget: SnapWidget;
    constructor(widget: SnapWidget, type: WidgetEventType, eventInitDict?: EventInit | undefined);
}
export declare class WidgetReadyEvent extends WidgetEvent {
    constructor(widget: SnapWidget, eventInitDict?: EventInit | undefined);
}
export declare class WidgetOpenEvent extends WidgetEvent {
    constructor(widget: SnapWidget, eventInitDict?: EventInit | undefined);
}
export declare class WidgetCloseEvent extends WidgetEvent {
    constructor(widget: SnapWidget, eventInitDict?: EventInit | undefined);
}
export declare class WidgetFullScreenEvent extends WidgetEvent {
    constructor(widget: SnapWidget, eventInitDict?: EventInit | undefined);
}
export declare class WidgetFullScreenExitEvent extends WidgetEvent {
    constructor(widget: SnapWidget, eventInitDict?: EventInit | undefined);
}
export declare class WidgetAuthEvent extends WidgetEvent {
    /**
     * @param state `true`: The user is connected - `false`: The user is disconnected
     */
    authState: boolean;
    constructor(widget: SnapWidget, authState: boolean, eventInitDict?: EventInit | undefined);
}
export declare class WidgetNotificationEvent extends WidgetEvent {
    notificationsCount: number;
    constructor(widget: SnapWidget, notificationsCount: number, eventInitDict?: EventInit | undefined);
}
