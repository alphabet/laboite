type HTMLAttributes<T extends HTMLElement> = Partial<Omit<Pick<T, keyof T>, 'style'> & Record<string, any> & {
    style: Partial<CSSStyleDeclaration>;
}>;
type RenderNode = HTMLElement | (() => RenderNode) | RenderNode[];
export declare function createElement<K extends keyof HTMLElementTagNameMap>(tagName: K, attributes?: HTMLAttributes<HTMLElementTagNameMap[K]>, childrens?: RenderNode): HTMLElementTagNameMap[K];
export declare function render(childrens: RenderNode): HTMLElement[];
export {};
