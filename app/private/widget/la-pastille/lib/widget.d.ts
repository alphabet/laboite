import { WidgetEventMap } from '../events/WidgetEvent';
import { SnapWidgetOptions } from './types';
export declare class SnapWidget {
    private _styleSheet;
    private _container;
    private _loadFrame;
    private _widgetButton;
    private _widgetFrame;
    private _appUrl;
    private _isOpen;
    private _isFullscreen;
    private _isMainFrameReady;
    private _isAuthenticated;
    private _notificationsCount;
    constructor(options?: Partial<SnapWidgetOptions>);
    get styleSheet(): CSSStyleSheet;
    get container(): HTMLDivElement;
    get appUrl(): string;
    get isOpen(): boolean;
    get isFullscreen(): boolean;
    get isAuthenticated(): boolean;
    get notificationsCount(): number;
    get isMainFrameReady(): boolean;
    get mainFrame(): HTMLIFrameElement;
    addEventListener<K extends keyof WidgetEventMap>(type: K, listener: (this: HTMLDivElement, ev: WidgetEventMap[K]) => any, options?: boolean | AddEventListenerOptions): void;
    removeEventListener<K extends keyof WidgetEventMap>(type: K, listener: (this: HTMLDivElement, ev: WidgetEventMap[K]) => any, options?: boolean | EventListenerOptions): void;
    open(): void;
    close(): void;
    enterFullscreen(): void;
    exitFullscreen(): void;
    toggleFullscreen(): void;
    loadMainFrame(): Promise<void> | undefined;
    /**
     * @param state `true`: The user is connected - `false`: The user is disconnected
     */
    authenticate(state: true | false): void;
    notify(notificationsCount: number): void;
    load(): void;
    private ready;
    private handleMessage;
    /**
     *
     * @param elementId
     */
    attach(element?: string | HTMLElement): void;
}
export declare class SnapWidgetElement extends HTMLElement {
    widget: SnapWidget;
    constructor();
    static get observedAttributes(): readonly ["open", "fullscreen"];
    attributeChangedCallback(name: typeof SnapWidgetElement.observedAttributes[number], oldValue: string, newValue: string): void;
}
