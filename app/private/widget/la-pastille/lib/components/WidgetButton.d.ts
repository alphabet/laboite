import { SnapWidget } from '../widget';
export declare class WidgetButton {
    private _widget;
    private btn;
    private image;
    private notifications;
    private posX;
    private posY;
    private mouseX;
    private mouseY;
    private _isDragging;
    private _isDropping;
    constructor(widget: SnapWidget);
    get isDragging(): boolean;
    get isDropping(): boolean;
    get widget(): SnapWidget;
    private setDragging;
    private setDropping;
    private onPointerDown;
    private onPointerUp;
    private onPointerMove;
    private onDrop;
    updateButtonPosition(x: number, y: number): void;
    setAuth(isAuthenticated: boolean): void;
    setNotification(notificationsCount: number): void;
    resetPosition(): void;
    get element(): HTMLButtonElement;
}
