import { SnapWidget } from '../widget';
export declare enum Tab {
    RIZOMO = "rizomo",
    CHATBOT = "chatbot"
}
export declare class WidgetFrame {
    private _widget;
    private frame;
    private _tab;
    private _tabFrames;
    private _tabBtns;
    constructor(widget: SnapWidget);
    private onCloseClick;
    private onFullScreenToggleClick;
    private onNavClick;
    changeTab(tab: Tab): void;
    get tab(): Tab;
    getTabFrame(tab: Tab): HTMLIFrameElement | undefined;
    getTabButton(tab: Tab): HTMLButtonElement | undefined;
    get widget(): SnapWidget;
    get element(): HTMLDivElement;
}
