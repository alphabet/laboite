import { Tab } from './components/WidgetFrame';
import { SnapWidget } from './widget';
export interface SnapWidgetOptions {
    appUrl: string;
    defaultOpen: boolean;
    defaultFullscreen: boolean;
}
export declare enum MessageType {
    NOTIFICATION = "notifications",
    AUTH = "userLogged",
    LOAD = "load",
    READY = "ready"
}
export declare enum QuestionType {
    IS_WIDGET = "isWiget",
    IS_FULLSCREEN = "isFullScreen",
    IS_OPENED = "isOpened",
    OPEN_WIDGET = "openWidget",
    CLOSE_WIDGET = "closeWidget",
    SET_FULLSCREEN = "setFullScreen"
}
export type Message = {
    type: MessageType.NOTIFICATION;
    /**
     * Number of notifications the user has
     */
    content: number;
} | {
    type: MessageType.AUTH;
    /**
     * Number of notifications the user has
     */
    content: boolean;
} | {
    type: MessageType;
    content: any;
};
export type Question = {
    type: QuestionType;
    content: never;
    callback: string;
} | {
    type: QuestionType.SET_FULLSCREEN;
    content: boolean;
};
export interface NavItem {
    key: Tab;
    iframe: (widget: SnapWidget) => HTMLIFrameElement;
    icon: (widget: SnapWidget) => HTMLElement;
    title?: string;
    showTitle?: boolean;
}
