import os
from pymongo import MongoClient
from dotenv import load_dotenv
from datetime import datetime, date, timedelta
import time
import sys
from smtplib import SMTP_SSL as SMTP    
from email.mime.text import MIMEText


load_dotenv()


MONGO_URI = os.getenv("MONGO_URI")
MONGO_DATABASE = os.getenv("MONGO_DATABASE")


def get_database():
    client = MongoClient(MONGO_URI)
    return client[MONGO_DATABASE]




def send_mail(content, subject, destination):
    # typical values for text_subtype are plain, html, xml
    text_subtype = 'plain'

    sender = db["contextsettings"].find_one({"key": "smtp.fromEmail"})
    smtpServer = db["contextsettings"].find_one({"key": "smtp.url"})

    if sender != None and smtpServer != None:
        serverInfo = smtpServer["value"][0].split("://")
        username = serverInfo[1].split(":")[0]
        password = serverInfo[1].split(':')[1].split('@')[0]
        host = serverInfo[1].split(':')[1].split('@')[1].split(":")[0]

        if username != None and password != None and host != None:
            try:
                msg = MIMEText(content, text_subtype)
                msg['Subject'] = subject
                msg['From'] = sender["value"][0]
                
                conn = SMTP(host)
                conn.set_debuglevel(False)
                conn.login(username, password)
                try:
                    conn.sendmail(sender["value"][0], destination, msg.as_string())
                finally:
                    conn.quit()
                    
            except Exception as err:
                print( "mail failed; %s" % str(err) ) # give an error message
        else:
            print( "mail failed: SMTP server not found" ) # give an error message



def checkUsersForFirstMail():
    print("Check users inactivity to sending first mail")
    date = datetime.now()
    date = date.replace(year = date.year - 1)
    users = db['users'].find({"$and": [{"profileStatus": {"$nin": [2,3]}}, {"lifeCycleDates.departure": None}, {"lastLogin": {"$lt": date }}, {"$or": [{"lifeCycleDates": None }, {"lifeCycleDates.firstMail": None}]}]})

    mailContent = db['contextsettings'].find_one({"key": "public.mails.firstInactiveAccountWarning"})
    
    for user in users:
        newDate = datetime.now()
        newDate = newDate + timedelta(days = newDate.day + 90)
        if not hasattr(user, "lifeCycleDates"):
            user["lifeCycleDates"] = {"firstMail": newDate, "inactivation": None, "departure": None, "deletion": None}
        else:
            user["lifeCycleDates"]["firstMail"] = newDate

        db['users'].update_one({"_id": user["_id"]}, {"$set": {"lifeCycleDates": user["lifeCycleDates"]}})
        if(mailContent != None):
            mailContent["value"][0] = mailContent["value"][0].format(firstname=user['firstName'], lastname=user["lastName"], magiclink="todo", mailto="mailto:apps@education.gouv.fr")
            send_mail(mailContent["value"][0], "[APPS-EDUCATION] Inactivité du compte", user["emails"][0]["address"])
        print("User modified: {} ({} {})".format(user["username"], user["firstName"], user["lastName"]))
    print("=============================================")


def checkUserForInactivity():
    print("Check users inactivity to sending second mail")
    date = datetime.now()
    users = db['users'].find({"$and": [{"profileStatus": {"$nin": [2,3]}}, {"lifeCycleDates.departure": None}, {"lifeCycleDates.firstMail": {"$lt": date }}, {"lifeCycleDates.inactivation": None}]})
    
    mailContent = db['contextsettings'].find_one({"key": "public.mails.secondInactiveAccountWarning"})
    
    for user in users:
        newDate = datetime.now()
        newDate = newDate + timedelta(days = newDate.day + 90)
        user["lifeCycleDates"]["inactivation"] = newDate
        db['users'].update_one({"_id": user["_id"]}, {"$set": {"lifeCycleDates.inactivation": user["lifeCycleDates"]["inactivation"]}})
        if(mailContent != None):
            mailContent["value"][0] = mailContent["value"][0].format(firstname=user['firstName'], lastname=user["lastName"], magiclink="todo", mailto="mailto:apps@education.gouv.fr")
            send_mail(mailContent["value"][0], "[APPS-EDUCATION] Inactivité du compte", user["emails"][0]["address"])
        print("User modified: {} ({} {})".format(user["username"], user["firstName"], user["lastName"]))
    print("=============================================")

def checkUserForDeparture():
    print("Check users inactivity to set inactive status")
    date = datetime.now()
    users = db['users'].find({"$and": [{"profileStatus": {"$nin": [2,3]}}, {"lifeCycleDates.departure": None}, {"lifeCycleDates.inactivation": {"$lt": date }}]})
 
    mailContent = db['contextsettings'].find_one({"key": "public.mails.accountDisabled"})
    
    for user in users:
        newDate = datetime.now()
        newDate = newDate + timedelta(days = newDate.day + 30)
        user["profileStatus"] = 2
        user["lifeCyclesDates"]["deletion"] = newDate
        db['users'].update_one({"_id": user["_id"]}, {"$set": {"lifeCycleDates.deletion": user["lifeCycleDates"]["deletion"], "profileStatus": user["profileStatus"]}})
        if(mailContent != None):
            mailContent["value"][0] = mailContent["value"][0].format(firstname=user['firstName'], lastname=user["lastName"], mailto="mailto:apps@education.gouv.fr")
            send_mail(mailContent["value"][0], "[APPS-EDUCATION] Désactivation du compte", user["emails"][0]["address"])
        #KC account deactivation
        print("User modified: {} ({} {})".format(user["username"], user["firstName"], user["lastName"]))
    print("=============================================")


def checkUserForPreventDeletion():
    print("Check users on departure to set inactive status")
    date = datetime.now()
    users = db['users'].find({"$and": [{"profileStatus": {"$nin": [2,3]}}, {"lifeCycleDates.deletion": None}, {"lifeCycleDates.departure": {"$lt": date }}]})  
    
    for user in users:
        newDate = datetime.now()
        newDate = newDate + timedelta(days=newDate.day+30)
        user["profileStatus"] = 2
        user["lifeCycleDates"]["deletion"] = newDate
        db['users'].update_one({"_id": user["_id"]}, {"$set": {"profileStatus": user["profileStatus"], "lifeCycleDates.deletion": user["lifeCycleDates"]["deletion"]}})
        
        if(user.delegate != None):
            delegate = db['users'].find_one({"_id": user.delegate})
            if(delegate):
                mailContent = db['contextsettings'].find_one({"key": "public.mails.departureConfirmed"})
                if(mailContent != None):
                    mailContent["value"][0] = mailContent["value"][0].format(firstname=user['firstName'], lastname=user["lastName"], firstnameDelegatee=delegate["firstName"], lastnameDelegatee=delegate["lastName"], mailto="mailto:apps@education.gouv.fr")
                    send_mail(mailContent["value"][0], "[APPS-EDUCATION] Confirmation de départ", user["emails"][0]["address"])                   
        else:
            mailContent = db['contextsettings'].find_one({"key": "public.mails.departureConfirmedWithoutDelegate"})  
            if(mailContent != None):
                mailContent["value"][0] = mailContent["value"][0].format(firstname=user['firstName'], lastname=user["lastName"], mailto="mailto:apps@education.gouv.fr")
                send_mail(mailContent["value"][0], "[APPS-EDUCATION] Confirmation de départ", user["emails"][0]["address"])
        
        print("User modified: {} ({} {})".format(user["username"], user["firstName"], user["lastName"]))
        #KC account deactivation
    print("=============================================")


def checkUserForDeletion():
    print("Check users inactive status to set deletion status")
    date = datetime.now()
    users = db['users'].find({"$and": [{"profileStatus": 2}, {"lifeCycleDates.deletion": {"$lt": date }}]})

    for user in users:
        user["profileStatus"] = 3
        db['users'].update_one({"_id": user["_id"]}, {"$set": {"profileStatus": user["profileStatus"]}})
        print("User modified: {} ({} {})".format(user["username"], user["firstName"], user["lastName"]))
    print("=============================================")     


def checkUserDeletion():
    users = db['users'].find({"profileStatus": 3}) 
    for user in users:
        print(user["username"])
        #Créer le processus de suppression


db = get_database()
checkUsersForFirstMail()
checkUserForInactivity()
checkUserForDeparture()
checkUserForPreventDeletion()
checkUserForDeletion()
