#!/bin/bash
# shellcheck disable=SC2086

function testApis1()
{
    laBoiteApiKey2 POST "createuser" '{"username":"utilisateur1", "firstname":"", "lastname":"", "email":"" }'
    laBoiteApiKey2 POST "notifications" '{"userId":"eZRRKbaTDty4Xk4kX", "title":"test","content":"test notif par API","type":"info"}'
}

function testApis()
{
    laBoiteApiKey1 POST "nctoken" '{"username":"ggrandgerard"}'
    laBoiteApiKey1 POST "nctoken" '{"email":"gilles.grandgerard@ac-dijon.fr"}'

    laBoiteApiKey2 GET "stats" 
    
    #laBoiteApiKey2 POST "notifications" '{"userId":"eZRRKbaTDty4Xk4kX", "title":"test", "content":"test notif par API", "type":"info"}'
    #laBoiteApiKey2 POST "notifications" '{"email":"eZRRKbaTDty4Xk4kX", "title":"test", "content":"test notif par API", "type":"info"}'
    #laBoiteApiKey2 POST "notifications" '{"username":"ggrandgerard", "title":"test", "content":"test notif par API", "type":"info"}'
    #laBoiteApiKey2 POST "notifications" '{"groupId":"smbjvtgABqvRtFg4S", "title":"test", "content":"test single group notif par API"}'
    #laBoiteApiKey2 POST "notifications" '{"groupsId":["smbjvtgABqvRtFg4S","eZRRKbaTDty4Xk4kX"], "title":"test", "content":"test multi groups notif par API"}'
    
    laBoiteApiKey2 POST "createuser" '{"username":"utilisateur1", "firstname":"", "lastname":"", "email":"" }'

    laBoiteApiKey GET "createusertoken" '{"email":"email@domaine.fr"}'
    
    # Structures
    laBoiteApi GET "structures"
    laBoiteApi GET "structures" '{"path": "api1/api2/api3"}'
    laBoiteApi POST "structures/create" '{"path": "api1/api2/api3"}' 
    laBoiteApi GET "structures"
    
    # Extension d'email (asams)
    laBoiteApi GET "asams"
    laBoiteApi GET "asams" '{"extension":"api3.fr"}'
    laBoiteApi POST "asams/create" '{"extension":"api3.fr", "path": "api1/api2/api3"}'
    laBoiteApi POST "asams/createmany" '[{"extension":"api3.fr", "path": "api1/api2/api3"}]'
    laBoiteApi GET "asams"

    
    # Groupes
    laBoiteApi GET "groups"
    #laBoiteApi GET "groups" '{"name":"groupTest" }'
    laBoiteApi POST "groups/create" '{"name":"groupTest", "description":"test", "content":"test", "type":"5", "owner":"yoyo2000" }'
    laBoiteApi POST "groups/create" '{"name":"groupTest2", "description":"test", "content":"test", "type":"5", "owner":"yoyo2000", "admins": [], "animators": [], "members": [], "candidates": [] }'
    laBoiteApi POST "groups/createmany" '[{"name":"groupTest1", "description":"test", "content":"test", "type":"5", "owner":"admin", "admins": [], "animators": [], "members": [], "candidates": [] },\
                                          {"name":"groupTest2", "description":"test", "content":"test", "type":"5", "owner":"admin", "admins": [], "animators": [], "members": [], "candidates": [] }]'
    laBoiteApi GET "groups"
    laBoiteApi POST "groups/addmembers" '[{"name":"groupTest", "members": ["test3","yo_test"]}]' # attention Tableau ! 
    laBoiteApi POST "groups/removemembers" '[{"name":"groupTest", "members": ["test3","yo_test"]}]' # attention Tableau ! 
    laBoiteApi POST "groups/addadmins" '[{"name":"groupTest", "admins": ["admin3"]}]' # attention Tableau !
    laBoiteApi POST "groups/removeadmins" '[{"name":"groupTest", "admins": ["admin3","yo_test"]}]' # attention Tableau !
    laBoiteApi POST "groups/addanimators" '{"name":"groupTest", "animators": ["admin3"]}' 
    laBoiteApi POST "groups/removeanimators" '{"name":"groupTest", "animators": ["admin3","yo_test"]}' 
    laBoiteApi GET "groups"
    
    # Group Bookmarks
    laBoiteApi GET "bookmarks"
    laBoiteApi GET "bookmarks" '{"name":"bookmark2"}'
    laBoiteApi POST "bookmarks/create" '{"url":"https://portail.eole3.dev", "name":"bookmark1", "tag":"test", "group": "groupTest", "author":"yoyo2000"}'
    laBoiteApi GET "bookmarks"
    
    # User Bookmarks
    laBoiteApi GET "userbookmarks"
    laBoiteApi GET "userbookmarks" '{"name":"bookmark2"}'
    laBoiteApi POST "userbookmarks/create" '{"url":"http://url.com", "name":"bookmark2", "tag":"test", "user": "yoyo2000", "icon":"url"}'
    laBoiteApi POST "userbookmarks/createmany" '[{"url":"http://url.com", "name":"bookmark2", "tag":"test", "user": "yoyo2000", "icon":"url"}]'
    laBoiteApi GET "userbookmarks"
}

function displayCurl()
{
    if [ -f "/tmp/result.out" ]
    then
       cat "/tmp/result.out"
       rm -f "/tmp/result.out"
    fi
}

function testCurl()
{
    if [ ! -f /tmp/verbose ]
    then
        return 1
    fi
    HTTPCODE=$(cat /tmp/verbose)
    while (( "$#" )); do
        if [[ "$1" == "$HTTPCODE" ]];
        then
            echo "  > HTTPCODE=$HTTPCODE"
            return 0
        fi
        shift
    done
    return 1
}

function curlToJson()
{
    # creation de json, ou rien si erreur
    grep "{" /tmp/result.out >/tmp/result.json || exit 1

    # affichage du json
    jq . </tmp/result.json
    return 0
}


function doCurl()
{
    local KEY_NAME="${1}"
    local KEY="${2}"
    local HTTP_METHOD="${3}"
    local ROUTE="${4}"
    local DATA="${5}"
    if [ -z "$DATA" ]
    then
        curl --show-error --silent -i -w "%{http_code}\n" -X "$HTTP_METHOD" -H "${KEY_NAME}: ${KEY}" -H "Content-Type: application/json" "${LABOITE_URL}/api/${ROUTE}" -o /tmp/result.out >/tmp/verbose  
        return "$?"
    else
        curl --show-error --silent -i -w "%{http_code}\n" -X "$HTTP_METHOD" -H "${KEY_NAME}: ${KEY}" -H "Content-Type: application/json"  -d "$DATA" "${LABOITE_URL}/api/${ROUTE}" -o /tmp/result.out >/tmp/verbose 
        return "$?"
    fi
}

function laBoiteApiKey()
{
    local KEY_NAME="${1}"
    local KEY="${2}"
    local HTTP_METHOD="${3}"
    local ROUTE="${4}"
    local DATA="${5}"
    local NO_DISPLAY="${6}"

    echo "---------------------------------------------------"
    echo "Test : $HTTP_METHOD $ROUTE "
    if [ -f /tmp/result.out ]
    then
        rm /tmp/result.out
    fi
    CDU=0
    case "$HTTP_METHOD" in
        GET)
            doCurl "${KEY_NAME}" "${KEY}" "$HTTP_METHOD"  "${ROUTE}" "$DATA" 
            CDU="$?"
            ;;
        POST)
            doCurl "${KEY_NAME}" "${KEY}" "$HTTP_METHOD"  "${ROUTE}" "$DATA"
            CDU="$?"
            ;;
        PUT)
            doCurl "${KEY_NAME}" "${KEY}" "$HTTP_METHOD"  "${ROUTE}" "$DATA"
            CDU="$?"
            ;;
        DELETE)
            doCurl "${KEY_NAME}" "${KEY}" "$HTTP_METHOD"  "${ROUTE}" "$DATA"
            CDU="$?"
            ;;
        PATCH)
            doCurl "${KEY_NAME}" "${KEY}" "$HTTP_METHOD"  "${ROUTE}" "$DATA"
            CDU="$?"
            ;;
        HEAD)
            doCurl "${KEY_NAME}" "${KEY}" "$HTTP_METHOD"  "${ROUTE}" "$DATA"
            CDU="$?"
            ;;
        OPTIONS)
            doCurl "${KEY_NAME}" "${KEY}" "$HTTP_METHOD"  "${ROUTE}" "$DATA"
            CDU="$?"
            ;;
        TRACE)
            doCurl "${KEY_NAME}" "${KEY}" "$HTTP_METHOD"  "${ROUTE}" "$DATA"
            CDU="$?"
            ;;
        *)
    esac
    HTTPCODE=$(cat /tmp/verbose)
    case "$CDU" in
        0)
         if testCurl "200"
         then
             if [ "${NO_DISPLAY}" == "DISPLAY" ]
             then
                 if ! curlToJson
                 then
                     echo "   PAS UN JSON"
                 else
                     echo "   OK JSON"
                 fi
             else
                 if ! curlToJson 
                 then
                     echo "   PAS UN JSON "
                 else
                     echo "   OK JSON"
                 fi
             fi
             return 0
         fi
         if testCurl "400" "401" "500"
         then
             echo "   Erreur API, Stop"
             return 0
         fi
         echo "   Erreur Autre, Stop ($HTTPCODE)"
         return 0
         ;;
    
       23)
         echo "# write error !"
         return 0
         ;;
         
       22)
         echo "22 : HTTP page not retrieved. The requested url was not found or returned another error with the HTTP error code being 400 or above." 
         echo "     This return code only appears if -f, --fail is used."
         return 0
         ;;
       7)
         echo "7 :  Failed to connect to host. curl managed to get an IP address to the machine and it tried to setup a TCP connection to the host but failed."
         echo "      This can be because you have specified the wrong port number, entered the wrong host name, the wrong protocol or perhaps because there is" 
         echo "      a firewall or another network equipment in between that blocks the traffic from getting through."
         return 0
         ;;
       *)
        echo "---------------------------------------------------"
        echo "Erreur $CDU"
        if [ -f /tmp/result.json ]
        then
            if ! jq . /tmp/result.json
            then
                cat /tmp/result.json
            fi
        fi
        echo "---------------------------------------------------"
    esac
}

function laBoiteApi()
{
    laBoiteApiKey "X-API-KEY" "${API_KEY}" "$@"
}

function laBoiteApiKey1()
{
    laBoiteApiKey "X-API-KEY" "${NC_API_KEY}" "$@"
}

function laBoiteApiKey2()
{
    laBoiteApiKey "X-API-KEY" "${X_API_KEY}" "$@"
}

function main()
{
    if [ -f "$HOME/.laboite" ]
    then
        source "$HOME/.laboite"
    else
        echo "$HOME/.laboite manque, utilise les valeurs par défaut"
        LABOITE_URL=https://portail.eole3.dev
        NC_API_KEY="une-cle-api-de-ton-choix"
        X_API_KEY="eolo123456"
        API_KEY="eolo123456"
    fi
    export X_API_KEY
    export API_KEY
    export LABOITE_URL
    export NC_API_KEY 
    echo "Use : LABOITE_URL=$LABOITE_URL"
    echo "Use : NC_API_KEY=$NC_API_KEY"
    echo "Use : X_API_KEY=$X_API_KEY"
    echo "Use : API_KEY=$API_KEY"
    testApis
}

SCRIPT_PATH="${BASH_SOURCE[0]}"
if [ -h "${SCRIPT_PATH}" ]
then
  while [ -h "${SCRIPT_PATH}" ]
  do
      SCRIPT_PATH=$(readlink "${SCRIPT_PATH}")
  done
fi
pushd . > /dev/null
DIR_SCRIPT=$(dirname "${SCRIPT_PATH}" )
cd "${DIR_SCRIPT}" > /dev/null || exit 1
SCRIPT_PATH=$(pwd);
main
popd  > /dev/null || exit 1



