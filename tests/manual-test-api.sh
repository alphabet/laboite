set -o xtrace

owner="yoyo2000"	    # username de référence
member1="test3"		    # username pour l'attribution de droit à des groupes
member2="yo_test"		# username pour l'attribution de droit à des groupes
# L'ajout ou la suppression d'un utilisateur parmi les rôles (admins, animators, members, candidats...) n'est possible que si l'utilisateur existe dans la collection Users.

# Structures
echo "===> Structures"
curl -sH "X-API-KEY: createuser-password" http://localhost:3000/api/structures | jq

echo "===> Create api1/api2/api3"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path": "api1/api2/api3"}' http://localhost:3000/api/structures/create
echo "===> Résultat"
curl -sH "X-API-KEY: createuser-password" http://localhost:3000/api/structures | jq

echo "===> Create Many struc1 struc2 struc3 struc4"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '[{"path": "struc1/struc2/struc3"}, {"path": "struc1/struc2/struc3/struc4"}]' http://localhost:3000/api/structures/createmany
echo "===> Résultat"
curl -sH "X-API-KEY: createuser-password" http://localhost:3000/api/structures | jq

echo "===> Get one structure (elles doivent toutes exister)"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path":"api1"}' http://localhost:3000/api/structures | jq
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path":"api1/api2"}' http://localhost:3000/api/structures | jq
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path":"api1/api2/api3"}' http://localhost:3000/api/structures | jq
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path":"struc1"}' http://localhost:3000/api/structures | jq
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path":"struc1/struc2"}' http://localhost:3000/api/structures | jq
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path":"struc1/struc2/struc3"}' http://localhost:3000/api/structures | jq
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path":"struc1/struc2/struc3/struc4"}' http://localhost:3000/api/structures | jq

# Extension d'email (asams)
echo "===> Extension d'email (asams)"
curl -sH "X-API-KEY: createuser-password" http://localhost:3000/api/structureruledispatchs | jq

echo "===> Create extension api3.fr pour api1/api2/api3"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"extension":"api3.fr", "path": "api1/api2/api3"}' http://localhost:3000/api/structureruledispatchs/create
echo "===> Résultat"
curl -sH "X-API-KEY: createuser-password" http://localhost:3000/api/structureruledispatchs | jq

echo "===> Create many extensions pour api1/api2/api3"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '[{"extension": "momo.fr", "path": "api1/api2/api3"}, {"extension": "happy3.fr", "path": "api1/api2/api3"}]' http://localhost:3000/api/structureruledispatchs/createmany
echo "===> Résultat"
curl -sH "X-API-KEY: createuser-password" http://localhost:3000/api/structureruledispatchs | jq

echo "===> Get one extension (elles doivent toutes exister)"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"extension": "api3.fr"}' http://localhost:3000/api/structureruledispatchs | jq
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"extension": "momo.fr"}' http://localhost:3000/api/structureruledispatchs | jq
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"extension": "happy3.fr"}' http://localhost:3000/api/structureruledispatchs | jq

# Groupes
echo "===> Groupe"
curl -sH "X-API-KEY: createuser-password" http://localhost:3000/api/groups | jq

echo "===> Create groupTest"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"groupTest", "description":"test", "content":"test", "type":"5", "owner":"'${owner}'", "admins": [], "animators": [], "members": [], "candidates": [] }' http://localhost:3000/api/groups/create
echo "===> Résultat"
curl -sH "X-API-KEY: createuser-password" http://localhost:3000/api/groups | jq

echo "===> Get one group"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"groupTest"}' http://localhost:3000/api/groups | jq

echo "===> Add Members to groupTest"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '[{"name":"groupTest", "members": ["'${member1}'", "'${member2}'"]}]' http://localhost:3000/api/groups/addmembers
echo "===> Résultat"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"groupTest"}' http://localhost:3000/api/groups | jq

echo "===> Remove members to groupTest"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '[{"name":"groupTest", "members": ["'${member1}'", "'${member2}'"]}]' http://localhost:3000/api/groups/removemembers
echo "===> Résultat"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"groupTest"}' http://localhost:3000/api/groups | jq

echo "===> Create Many group1 group2"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '[{"name":"group1", "customSlug": "", "description":"", "content":"", "type":"5", "owner":"'${owner}'", "admins": ["'${member2}'"], "animators": ["'${member1}'"], "members": [], "candidates": [] }, {"name":"group2", "customSlug": "", "description":"", "content":"", "type":"5", "owner":"'${owner}'", "admins": [], "animators": [], "members": ["'${member1}'"], "candidates": ["'${member2}'"] }]' http://localhost:3000/api/groups/createmany
echo "===> Résultat"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"group1"}' http://localhost:3000/api/groups | jq
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"group2"}' http://localhost:3000/api/groups | jq

echo "===> Add admins"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '[{"name":"group1", "admins": ["'${member1}'"]},{"name":"group2", "admins": ["'${member2}'"]}]' http://localhost:3000/api/groups/addadmins
echo "===> Résultat"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"group1"}' http://localhost:3000/api/groups | jq
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"group2"}' http://localhost:3000/api/groups | jq

echo "===> Remove admins"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '[{"name":"group1", "admins": ["'${member1}'"]},{"name":"group2", "admins": ["'${member2}'"]}]' http://localhost:3000/api/groups/removeadmins
echo "===> Résultat"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"group1"}' http://localhost:3000/api/groups | jq
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"group2"}' http://localhost:3000/api/groups | jq

echo "===> Add animators"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '[{"name":"group1", "animators": ["'${member2}'"]}, {"name":"group2", "animators": ["'${member2}'"]}]' http://localhost:3000/api/groups/addanimators
echo "===> Résultat"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"group1"}' http://localhost:3000/api/groups | jq
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"group2"}' http://localhost:3000/api/groups | jq

echo "===> Remove animators"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '[{"name":"group1", "animators": ["'${member2}'"]}, {"name":"group2", "animators": ["'${member2}'"]}]' http://localhost:3000/api/groups/removeanimators
echo "===> Résultat"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"group1"}' http://localhost:3000/api/groups | jq
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"group2"}' http://localhost:3000/api/groups | jq

# Group Bookmarks
echo "===> Group Bookmarks"
curl -sH "X-API-KEY: createuser-password" http://localhost:3000/api/bookmarks | jq

echo "===> Create one group bookmarks"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"url":"https://portail.eole3.dev", "name":"bookmark1", "tag":"test", "group": "groupTest", "author":"'${owner}'"}' http://localhost:3000/api/bookmarks/create
echo "===> Résultat"
curl -sH "X-API-KEY: createuser-password" http://localhost:3000/api/bookmarks | jq

echo "===> Create many groups bookmarks"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '[{"url":"https://sondage.eole3.dev", "name":"bookmark2", "tag":"tag", "group": "group1", "author":"'${owner}'"}, {"url":"https://gitlab.com", "name":"bookmark3", "tag":"tag", "group": "group1", "author":"'${owner}'"}]' http://localhost:3000/api/bookmarks/createmany
echo "===> Résultat"
curl -sH "X-API-KEY: createuser-password" http://localhost:3000/api/bookmarks | jq

echo "===> Get one groups bookmarks"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"bookmark1"}' http://localhost:3000/api/bookmarks | jq

# User Bookmarks
echo "===> User Bookmarks"
curl -sH "X-API-KEY: createuser-password" http://localhost:3000/api/userbookmarks | jq

echo "===> Create user bookmarks"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"url":"http://url.com", "name":"bookmark4", "tag":"test", "user": "'${owner}'", "icon":"url"}' http://localhost:3000/api/userbookmarks/create
echo "===> Résultat"
curl -sH "X-API-KEY: createuser-password" http://localhost:3000/api/userbookmarks | jq

echo "===> Create many user bookmarks"
curl -sX POST -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '[{"url":"http://youpi.com", "name":"bookmark5", "tag":"test1", "user": "'${owner}'", "icon":"url"}, {"url":"http://noel.fr", "name":"bookmark6", "tag":"test2", "user": "'${owner}'", "icon":"url"}]' http://localhost:3000/api/userbookmarks/createmany
echo "===> Résultat"
curl -sH "X-API-KEY: createuser-password" http://localhost:3000/api/userbookmarks | jq

echo "===> Get one user bookmarks"
curl -sX POST -H "X-API-KEY: createuser-password"  -H "Content-Type: application/json" -d '{"name":"bookmark4"}' http://localhost:3000/api/userbookmarks | jq

# Clean dataBase
echo "===> Delete one bookmark"
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"url":"https://portail.eole3.dev"}' http://localhost:3000/api/bookmarks/delete
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"url":"https://sondage.eole3.dev"}' http://localhost:3000/api/bookmarks/delete
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"url":"https://gitlab.com"}' http://localhost:3000/api/bookmarks/delete

echo "===> Delete one userbookmark"
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"url":"http://url.com", "username": "'${owner}'"}' http://localhost:3000/api/userbookmarks/delete
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"url":"http://youpi.com", "username": "'${owner}'"}' http://localhost:3000/api/userbookmarks/delete
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"url":"http://noel.fr", "username": "'${owner}'"}' http://localhost:3000/api/userbookmarks/delete

echo "===> Delete one group"
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"group1"}' http://localhost:3000/api/groups/delete
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"group2"}' http://localhost:3000/api/groups/delete
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"name":"groupTest"}' http://localhost:3000/api/groups/delete

echo "===> Delete one extension"
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"extension":"momo.fr"}' http://localhost:3000/api/structureruledispatchs/delete
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"extension":"api3.fr"}' http://localhost:3000/api/structureruledispatchs/delete
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"extension":"happy3.fr"}' http://localhost:3000/api/structureruledispatchs/delete

echo "===> Delete one structure"
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path": "api1/api2/api3"}' http://localhost:3000/api/structures/delete
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path": "api1/api2"}' http://localhost:3000/api/structures/delete
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path": "api1"}' http://localhost:3000/api/structures/delete
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path": "struc1/struc2/struc3/struc4"}' http://localhost:3000/api/structures/delete
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path": "struc1/struc2/struc3"}' http://localhost:3000/api/structures/delete
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path": "struc1/struc2"}' http://localhost:3000/api/structures/delete
curl -w "\n" -X DELETE -H "X-API-KEY: createuser-password" -H "Content-Type: application/json" -d '{"path": "struc1"}' http://localhost:3000/api/structures/delete
