import os
from datetime import datetime
from pymongo import MongoClient
from dotenv import load_dotenv

load_dotenv()

MONGO_URI = os.getenv("MONGO_URI")
MONGO_DATABASE = os.getenv("MONGO_DATABASE")

client = MongoClient(MONGO_URI)
db = client[MONGO_DATABASE]

def progress(prefixe, current_value, max_value):
    percent = round(100*current_value/max_value)
    message = f"{prefixe} {percent}% - ["+"#"*int(percent/5)+"."*(20-int(percent/5))+"]"
    print(message, end='\r')

def generatePathOfStructure(structure, tree):
  if structure["parentId"] != None:
    parent = db["structures"].find_one({ "_id": structure["parentId"] })
    if parent != None:
      obj = { "structureName": parent["name"], "structureId": parent["_id"] }
      tree.append(obj)
      generatePathOfStructure(parent, tree)

structureNumber = db["structures"].count_documents({})
print("[{}] Start generating path for {} structures".format(datetime.now(), structureNumber))
structures = db["structures"].find()
if structures != None:
    for i, structure in enumerate(structures):
        tree = []
        generatePathOfStructure(structure, tree)
        path = list(reversed(tree))
        # print("Path of {}: {}".format(structure["name"], path))
        progress("Generate path for Structures : ", i, structureNumber)
        db["structures"].update_one({"_id": structure["_id"]}, {"$set": {"structurePath": path}})
print("[{}] End generating path for {} structures".format(datetime.now(), structureNumber))


