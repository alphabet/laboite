# Règles générales

codes de retour :

- si réussite: code 200 avec données si applicable (format json)
- si paramètres manquants dans le corps de la requête: code 400 avec message d'erreur (string)
- si entrées non retrouvées en base: code 404 avec message d'erreur (string)
- autres cas d'erreur : code 409 avec message d'erreur (string)

Pour récupérer les données existantes (groupes, bookmarks, ...), 2 routes sont disponibles :

- une route GET qui renvoie l'ensemble des entrées
- une route POST avec paramêtres (ex: nom du groupe) qui renvoie une entrée précise

# Utilisateurs

- POST '/createuser' (créer un utilisateur)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"username":"utilisateur1", "firstname":"", "lastname":"", "email":"" }' \
 http://localhost:3000/api/createuser
```

Tous les champs sont obligatoires, username et email doivent être uniques

- POST '/users/setstructure' (associer une structure à un utilisateur)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"username":"utilisateur1", "path": "api1/api2/api3" }' \
 http://localhost:3000/api/users/setstructure
```

# Groupes

- POST '/groups/create' (créer un groupe)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"name":"group1", "customSlug":"", "description":"desciption du groupe", "content":"", "type":0, "owner":"owner_username", "admins": [], "animators": [], "members": [], "candidates": [] }' \
 http://localhost:3000/api/groups/create
```

champs obligatoires: name (unique), type, description, owner
valeurs possibles pour type: { 0: 'public', 5: 'modéré', 10: 'privé', 20: 'restreint' }

- POST '/groups' (récupérer un groupe)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"name":"test" }' \
 http://localhost:3000/api/groups
```

- GET '/groups' (récupérer tous les groupes)

```
curl -H "X-API-KEY: createuser-password" http://localhost:3000/api/groups
```

- DELETE '/groups/delete' (supprimer un groupe)

```
curl -X DELETE -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"name":"group1"}' \
 http://localhost:3000/api/groups/delete
```

- POST '/groups/addmember' (ajouter un membre à un groupe)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"name":"groupname2, "member": "username1"}' \
 http://localhost:3000/api/groups/addmember
```

- POST '/groups/addadmin' (ajouter un administrateur à un groupe)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"name":"groupname, "admin": "username1"}' \
 http://localhost:3000/api/groups/addadmin
```

- POST '/groups/addanimator' (ajouter un animateur à un groupe)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"name":"groupname, "animator": "username1"}' \
 http://localhost:3000/api/groups/addanimator
```

- POST '/groups/removemember' (supprimer un membre d'un groupe)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"name":"groupname, "member": "username1"}' \
 http://localhost:3000/api/groups/removemember
```

- POST '/groups/removeadmin' (supprimer un administrateur d'un groupe)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"name":"groupname, "admin": "username1"}' \
 http://localhost:3000/api/groups/removeadmin
```

- POST '/groups/removeanimator' (supprimer un animateur d'un groupe)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"name":"groupname, "animator": "username1"}' \
 http://localhost:3000/api/groups/removeanimator
```

# Structure rule dispatch - ASAM (associations domaine mail / structure)

- POST '/structureruledispatchs/create' (créer une association)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"extension":"structure1.fr", "path": "api1/api2/api3"}' \
 http://localhost:3000/api/structureruledispatchs/create
```

- POST '/structureruledispatchs' (récupérer une association)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"name":"test" }' \
 http://localhost:3000/api/structureruledispatchs
```

- GET '/structureruledispatchs' (récupérer toutes les associations)

```
curl -H "X-API-KEY: createuser-password" http://localhost:3000/api/structureruledispatchs
```

- DELETE '/structureruledispatchs/delete' (supprimer une association)

```
curl -X DELETE -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"extension":"structure1.fr"}' \
 http://localhost:3000/api/structureruledispatchs/delete
```

# Bookmarks de groupe

- POST '/bookmarks/create' (créer un bookmark de groupe)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"url":"http:\/\/url.com", "name":"bookmark1", "tag":"tag", "group": "groupe1", "author":"username1"}'
 http://localhost:3000/api/bookmarks/create
```

- POST '/bookmarks' (récupérer un bookmark de groupe)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"group": "groupName", "url":"http:\/\/test.com" }' \
 http://localhost:3000/api/bookmarks
```

- GET '/bookmarks' (récupérer tous les bookmarks de groupe)

```
curl -H "X-API-KEY: createuser-password" http://localhost:3000/api/bookmarks
```

- DELETE '/bookmarks/delete' (supprimer un bookmark de groupe)

```
curl -X DELETE -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"group": "groupName", "url":"http:\/\/url.com"}',
 http://localhost:3000/api/bookmarks/delete
```

# Bookmarks utilisateurs

- POST '/userbookmarks/create' (créer un bookmark utilisateur)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"url":"http:\/\/url.com", "name":"bookmark1", "tag":"tag", "user": "username1"}' \
 http://localhost:3000/api/userbookmarks/create
```

- POST '/userbookmarks' (récupérer un bookmark utilisateur)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"username": "username1", "url":"http:\/\/test.com" }' \
 http://localhost:3000/api/userbookmarks
```

- GET '/userbookmarks' (récupérer tous les bookmarks utilisateurs)

```
curl -H "X-API-KEY: createuser-password" http://localhost:3000/api/userbookmarks
```

- DELETE '/userbookmarks/delete' (supprimer un bookmark d'un utilisateur)

```
curl -X DELETE -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"url":"http:\/\/url.com", "user": "username"}' \
 http://localhost:3000/api/userbookmarks/delete
```

# Structures

- POST '/structures/create' (créer une structure)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"path": "struc1/struc2/struc3"}' \
 http://localhost:3000/api/structures/create
```

- POST '/structures' (récupérer une structure)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"path":"api1/api2" }' \
 http://localhost:3000/api/structures
```

- GET '/structures' (récupérer toutes les structures)

```
curl -H "X-API-KEY: createuser-password" http://localhost:3000/api/structures
```

- DELETE '/structures/delete' (supprimer une structure)

```
curl -X DELETE -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"path": "struc1/struc2/struc3"}' \
 http://localhost:3000/api/structures/delete
```

# Services

- POST '/services/create' (créer un service)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"title": "test",
"description": "test description",
"url": "http:\/\/test.com",
"logo": "logo_url",
"categories": "category_name",
"businessReGrouping": 1,
"usage": "usage",
"team": "team",
"state": 0,
"offline": false,
"isBeta": true,
"structure": "structureName"'} \
 http://localhost:3000/api/services/create
```

valeurs possibles pour state : { 0: affiché, 5: inactif, 10: caché, 15: maintenance }

- POST '/services' (récupérer un service)

```
curl -X POST -H "X-API-KEY: createuser-password" \
 -H "Content-Type: application/json" \
 -d '{"name":"test" }' \
 http://localhost:3000/api/services
```

- GET '/services' (récupérer tous les services)

```
curl -H "X-API-KEY: createuser-password" http://localhost:3000/api/services
```
