## Paramètres Meteor

## Architecture

Cette documentation permet de comprendre la structure des fichiers en cause dans le préchargement et la mise à jour des paramètres Meteor.

```bash
Laboite
└─ app
   └─ imports
      ├─ api
      │  └─ contextsettings
      │     ├─ contextsettings.js # Schema d'un objet dans la base de donnée
      │     ├─ defaultValue.contextSettings.js # Les données Meteor par default
      │     └─ server
      │        └─ methods.js # Les méthodes appelées dans le startup ou dans la page AdminSettingsPage
      ├─ startup
      │  ├─ i18n
      │  │  ├─ en.i18n.json # Les labels de l'application (en anglais)
      │  │  └─ fr.i18n.json # Les labels de l'application (en français)
      │  └─ server
      │     ├─ index.js # startup global de l'application dans lequel sont initialisés les paramètres Meteor via l’import de config/settings.js
      │     ├─ db-initialize
      │     │  └─ ContextSettings.js # Préchargements de paramètres Meteor par défaut si la base est vide
      │     └─ config
      │        └─ settings.js # Affectation de l’objet newSettings à Meteor.settings
      └─ ui
         └─ pages
            └─ admin
               └─ AdminSettingsPage.jsx # La page des réglages globaux
```

## Sources

L'application contient trois sources pour les paramètres:

1. __developpement.settings.json__
   qui est le fichier de configuration passé en argument au lancement de l'application
   
2. __defaultValue.contextSettings.js__
   qui sont les valeurs par défaut qui précharge l'application dans le cas où la base de donnée est vide

3. __La base Mongo__
   Une fois les valeurs defauts préchargées au tout premier lancement de l'application, ce sont les valeurs en base qui prévalent.
   sauf si un nouveau paramètres est créé et que sa clé n’apparait pas dans la base.

À noter:
   __methods.js__ contient plusieurs méthodes mais celle qui nous intéresse est : `contextSetting.createSettings`
   Elle reconstruit un objet des settings à partir des données contenu dans __defaultValue.contextSettings.js__ et des données contenu en __base__.

## Données

Les données présentes dans __defaultValue.contextSettings.js__ sont un tableau d'objet, chacun contenant trois pairs de clé, valeur:
- key
- type
- value

Ces trois pairs sont divisés en deux groupes.
Les clés __key__ et __value__ sont stockés en base.
Alors que la clé __type__ n'est utilisé qu'à l'affichage dans la page __AdminSettingsPage.jsx__.
C’est elle qui détermine le type d’input à afficher pour modifier la valeur du paramètre.

## Procédures

Au Lancement de l'application si la base est vide un objet __Meteor.settings__ est reconstruit à partir des valeurs par défaut et chargé en base.
La config (__settings.development.json__) prend au minimum les paramètres system. Si d’autres paramètres déjà présent dans les valeurs par default (__defaultValue.contextSettings.js__) sont présent dans la config, ils prévaleront sur les valeurs defauts

Côtés interface (__AdminSettingsPage.jsx__), après changement des paramètres dans __administration__ --> __réglages globaux__ --> __Paramètres contextuels de l'application__, il y a deux boutons en bas:
1. __Enregistrer__
2. __Relancer l'application__

À l'enregistrement, les données sont sauvegardées dans la base, mais l'application doit être relancée pour que les changements soient effectifs.
Dans la page __AdminSettingsPage__, le bouton d'enregistrement des nouveaux paramètres appelle la méthodes `contextSetting.updateSettings` dans __methods.js__ et met à jour les données en base.
Ce n'est qu'au lancement de l'application (__index.js__) que les paramètres en base écrasent les paramètres de Meteor.

Le bouton __Relancer l'application__ stop l'application qui est relancée par Kubernetes.
Il appelle la méthodes `contextSetting.stopMeteor` dans __methods.js__.

L'application doit être relancer via la commande: (mode dev uniquement)

`Meteor npm start`

## Rédaction des description dans i18n:

Pour l'affichage du __markdown__ on utilise la bibliothèque [ToastUI](https://ui.toast.com/tui-editor)

__exemple dans i18n:__

```json
    "AdminSettingsPage": {
      "modalDescription": {
        "appName": "C'est le titre indiqué dans l'onglet du navigateur.\n#### H4 :\n__texte gras__\n_texte souligné_\n~~texte barré~~\n`Du code`\nCi dessous un tableau :\n|col 1|col 2|col 3|\n|---|---|---|\n|one|two|three|\nListe à puce :\n- item 1\n- item 2\n- item 3\n\nListe à puce numéroté :\n1. un\n2. deux\n3. trois\n\n[Lien vers la doc](https://ui.toast.com/tui-editor)",
```

Malheureusement il faut indiquer des retours chariots.
Remarquez aussi le __double retour chariot__ juste avant "Liste à puce numéroté" pour s'assurer que cette chaîne de caractère ne soit pas indenté comme les éléments qui la précède mais qu'elle retourne bien à la ligne.

## Création d’un nouveau paramètre:

Pour créer un nouveau paramètre il faut créer un nouvel objet dans dans le fichier __defaultValue.contextSettings.js__
Les clés __key__ et __type__ sont des __chaînes de caractère__ alors que la clé __valeur__ est un __tableau__.
Ce tableau peut contenir un entier, un booléen, ou une chaîne de caractère.
Il peut contenir plusieurs __chaînes de caractère__ si le paramètre est de type __textArea__.

```
{
   key: "",
   type: ("boolean", "string", "number" ou "textArea")
   value: []
}
```

Relancer l'application sera nécessaire pour enregistrer les nouveaux paramètres dans la base.

