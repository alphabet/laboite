# Thème CUSTOM pour laBoite

## Présentation

Le thème CUSTOM permet de modifier le logo principal de l'application et son favicon.
Cela inclut la modification du logo dans la page de connexion et celui dans le bandeau en haut à gauche dans le portail (logo).
Mais aussi la vignette présente dans l'onglet (favicon).

Le logo sera aussi changé en version mobile.

## Ajout des logos

Pour ajouter les logos, il faut aller dans le dossier **public/images/logos/custom**.
Une fois dans le dossier, vous pouvez ajouter vos deux logos:

- un qui sera le logo principal de l'application
- un qui sera le favicon

Les fichiers doivent `impérativement respecter le nommge` suivant :

- logo.png
- favicon.png

## Modification des paramètres

Une fois les fichiers inclus dans le dossier avec le nom adapté, il suffit de changer les settings:
_theme: 'custom'_

## Modification de la palette de couleur

Il est possible de personnaliser la palette de couleur du thème en allant dans le fichier `import/ui/themes/custom/palette.js`.

Il est possible de mettre les couleurs de votre choix au format hexadécimal.

Attention cependant à ne pas modifier le nom des attributs et à ne pas en ajouter. Ce genre de modifications ne serait pas pris en compte ou pourrait nuire au bon fonctionnement de l'application.
